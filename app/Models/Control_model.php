<?php namespace App\Models;
use CodeIgniter\Model;
class Control_model extends BaseModel
{
     public function getAll($estatus=null)
     {
	  $builder =$this->dbconn('public.control as c');
	  $builder->select
	  (
	       "c.id
	       ,c.descripcion
		   ,to_char(c.fecha_creacion,'dd/mm/yyyy') as fecha_creacion
	       ,CASE WHEN c.borrado='t' THEN 'Eliminado' ELSE 'Activo' END AS Estatus"
	  );
	  $query = $builder->get();
	  return $query;	
     }
	 public function getAllActivos()
     {
	  $builder = $this->dbconn('public.control as c');
	  $builder->select
	  (
		"c.id
		,c.descripcion
		,to_char(c.fecha_creacion,'dd/mm/yyyy') as fecha_creacion
		,CASE WHEN c.borrado='t' THEN 'Eliminado' ELSE 'Activo' END AS Estatus"
	  );
	  $builder->where(['c.borrado'=>false]);
	  $builder->OrderBy('c.descripcion');
	  $query = $builder->get();
	  return $query;	
     }
	 public function Agregar($data)
	 {
		$builder = $this->dbconn('public.control');
		$query = $builder->insert($data);  
		return $query;
     }
    public function actualizar($data)
	{
		$builder = $this->dbconn('public.control');
		$builder->where('id', $data['id']);
		$query = $builder->update($data);
		return $query;
	}
     public function getAllParaSistemas($estatus=null)
     {
	  $builder = $this->dbconn('seguridad.rol as r');
	  $builder->select
	  (
	       "r.id
	       ,r.rol
	       ,CASE WHEN r.activo='t' THEN 'Activo' ELSE 'Bloqueado' END AS Estatus"
	  );
	  $query = $builder->get();
	  return $query;
     }
     
     public function getDatosRol($id=null){
	  $builder = $this->dbconn('seguridad.rol r');
	  $builder->select
	       (
		    'r.id
		    ,r.rol
		    ,r.activo'
	       );
	  $builder->where('r.id', $id);
	  $query = $builder->get();
	  return $query;
     }
}
