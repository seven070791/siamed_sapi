<?php

namespace App\Models;

use CodeIgniter\Model;

class Habitos_Model extends BaseModel
{
	public function getAll($estatus = null)
	{
		$db      = \Config\Database::connect();
		$strQuery = "SELECT h.id,h.descripcion
		FROM historial_clinico.habitos_psicosociales as h ";

		//return $strQuery;	
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}

	public function listar_habitos_personales($historial_medico)
	{
		$db      = \Config\Database::connect();
		$strQuery = "SELECT h.id,hc.descripcion,h.borrado";
		$strQuery .= ",to_char(h.fecha_creacion,'dd/mm/yyyy')as fecha_creacion";
		$strQuery .= " FROM historial_clinico.habito_historial as h ";
		$strQuery .= "JOIN historial_clinico.habitos_psicosociales as hc on h.habito_id=hc.id ";
		$strQuery .= "where h.historial_id=$historial_medico and  h.borrado='false'";
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		//return $strQuery;	
		return $resultado;
	}

	public function listar_Habitos_Model_Individual($id_historial_medico, $id_consulta)
	{
		$db      = \Config\Database::connect();
		$strQuery = "SELECT h.id,hc.descripcion,h.borrado";
		$strQuery .= ",to_char(h.fecha_creacion,'dd/mm/yyyy')as fecha_creacion";
		$strQuery .= " FROM historial_clinico.habito_historial as h ";
		$strQuery .= "JOIN historial_clinico.habitos_psicosociales as hc on h.habito_id=hc.id ";
		$strQuery .= "where h.historial_id=$id_historial_medico and  h.borrado='false'";
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		//return $strQuery;	
		return $resultado;
	}


	public function agregar_habito_historial($data)
	{
		$builder = $this->dbconn('historial_clinico.habito_historial');
		$query = $builder->insert($data);
		return $query;
	}
	public function borrar_habitos($data)
	{
		$builder = $this->dbconn('historial_clinico.habito_historial as h');
		$builder->where('h.id', $data['id']);
		$query = $builder->update($data);
		return $query;
	}
}
