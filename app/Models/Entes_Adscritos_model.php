<?php

namespace App\Models;

use CodeIgniter\Model;

class Entes_Adscritos_model extends BaseModel
{

	public function getAll($estatus = null)
	{
		$builder = $this->dbconn('public.ente_adscrito as ea');
		$builder->select(
			"ea.id
		,ea.descripcion
	    ,ea.rif
		,to_char(ea.fecha_creacion,'dd/mm/yyyy') as fecha_creacion
	    ,CASE WHEN ea.borrado='t' THEN 'Eliminado' ELSE 'Activo' END AS Estatus"
		);
		$query = $builder->get();
		return $query;
	}

	public function Agregar($data)
	{
		// var_dump($data);
		// die();
		$builder = $this->dbconn('public.ente_adscrito');
		$query = $builder->insert($data);
		return $query;
	}

	public function  insetar_cortesia_adscrita($data)
	{
		$builder = $this->dbconn('public.cortesia_adscrito');
		$query = $builder->insert($data);
		return $query;
	}

	public function  actualizar_cortesia_adscrita($datos_adscritos)
	{


		$builder = $this->dbconn('public.cortesia_adscrito');
		$builder->where('id_cortesia', $datos_adscritos['id_cortesia']);
		$query = $builder->update($datos_adscritos);
		return $query;
	}
	public function buscar_cortesia_adscrita($datos_adscritos)
	{


		$db = \Config\Database::connect();
		$strQuery = "select ";
		$strQuery .= " c_ascrita.id";
		$strQuery .= ",c_ascrita.id_cortesia";
		$strQuery .= ",c_ascrita.id_adscrito ";
		$strQuery .= "FROM  public.cortesia_adscrito as c_ascrita ";
		$strQuery .= " WHERE c_ascrita.id_cortesia=$datos_adscritos[id_cortesia]";
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}




	public function actualizar($data)
	{
		$builder = $this->dbconn('public.ente_adscrito');
		$builder->where('id', $data['id']);
		$builder->where('borrado', 'false');
		$query = $builder->update($data);
		return $query;
	}

	public function getAllActivos()
	{
		$builder = $this->dbconn('public.ente_adscrito as ea');
		$builder->select(
			"ea.id
		   ,ea.rif
	       ,ea.descripcion
		   ,to_char(ea.fecha_creacion,'dd/mm/yyyy') as fecha_creacion
	       ,CASE WHEN ea.borrado='t' THEN 'Eliminado' ELSE 'Activo' END AS Estatus"
		);
		$builder->where(['ea.borrado' => false]);
		$builder->OrderBy('ea.descripcion');
		$query = $builder->get();
		return $query;
	}







	public function getAllParaSistemas($estatus = null)
	{
		$builder = $this->dbconn('seguridad.rol as r');
		$builder->select(
			"r.id
	       ,r.rol
	       ,CASE WHEN r.activo='t' THEN 'Activo' ELSE 'Bloqueado' END AS Estatus"
		);
		$query = $builder->get();
		return $query;
	}

	public function getDatosRol($id = null)
	{
		$builder = $this->dbconn('seguridad.rol r');
		$builder->select(
			'r.id
		    ,r.rol
		    ,r.activo'
		);
		$builder->where('r.id', $id);
		$query = $builder->get();
		return $query;
	}
}
