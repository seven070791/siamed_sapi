<?php namespace App\Models;
use CodeIgniter\Model;
class Alergias_Medicamentos_Model extends BaseModel
{

	
    public function agregar_alergias_medicamentos($data)
	{
		 $builder = $this->dbconn('historial_clinico.alergias_medicamentos');
		 $query = $builder->insert($data);  
		return $query;
    }


	public function buscar_consulta($id_consulta)
	{
	   //$builder = $this->dbconn('historial_clinico.consultas as hc');
	   $db      = \Config\Database::connect();
	   $strQuery ="";
	   $strQuery .="SELECT";
	   $strQuery .=" am.id";  
	   $strQuery .=",am.descripcion "; 
	   $strQuery .="FROM ";
	   $strQuery .=" historial_clinico.alergias_medicamentos as am  ";	
	   $strQuery  =$strQuery . " where am.id_consulta='$id_consulta'";
	   $query = $db->query($strQuery);
	   $resultado=$query->getResult(); 
	   return $resultado;
	   //return  $strQuery;
	}
	 

	public function listar_alergias_medicamentos($n_historial)
	{
 
	   //$builder = $this->dbconn('historial_clinico.consultas as hc');
	   $db      = \Config\Database::connect();
	   $strQuery ="";
	   $strQuery .="SELECT";
	   $strQuery .=" distinct hc.id_medico";  
	   $strQuery .=",am.id";  
	   $strQuery .=",am.descripcion "; 
	   $strQuery .=",to_char(am.fecha_creacion,'dd/mm/yyyy') as fecha_creacion "; 
	   $strQuery .=",to_char(am.fecha_actualizacion,'dd/mm/yyyy') as fecha_actualizacion "; 
	   $strQuery .=",CONCAT(m.nombre,' ', m.apellido) AS nombre ";
	   $strQuery .="FROM ";
	   $strQuery .="  historial_clinico.alergias_medicamentos as am ";
	   $strQuery .="  join historial_clinico.consultas  as hc on am.id_consulta=hc.id";
	   $strQuery .=" join historial_clinico.medicos as m on hc.id_medico=m.id";
	   $strQuery  =$strQuery . " where am.n_historial='$n_historial'";
	   $query = $db->query($strQuery);
	   $resultado=$query->getResult(); 
	   return $resultado;
	   //return  $strQuery;
	}
	public function listar_Alergias_Medicamentos_Individual($n_historial,$id_consulta)
	{
 
	   //$builder = $this->dbconn('historial_clinico.consultas as hc');
	   $db      = \Config\Database::connect();
	   $strQuery ="";
	   $strQuery .="SELECT";
	   $strQuery .=" distinct hc.id_medico";  
	   $strQuery .=",am.id";  
	   $strQuery .=",am.descripcion "; 
	   $strQuery .=",to_char(am.fecha_creacion,'dd/mm/yyyy') as fecha_creacion "; 
	   $strQuery .=",to_char(am.fecha_actualizacion,'dd/mm/yyyy') as fecha_actualizacion "; 
	   $strQuery .=",CONCAT(m.nombre,' ', m.apellido) AS nombre ";
	   $strQuery .="FROM ";
	   $strQuery .="  historial_clinico.alergias_medicamentos as am ";
	   $strQuery .="  join historial_clinico.consultas  as hc on am.id_consulta=hc.id";
	   $strQuery .=" join historial_clinico.medicos as m on hc.id_medico=m.id";
	   $strQuery  =$strQuery . " where am.n_historial='$n_historial'";
	   $query = $db->query($strQuery);
	   $resultado=$query->getResult(); 
	   return $resultado;
	   //return  $strQuery;


	   



	}

	public function actualizar_alergias_medicamentos($data)
	{
		$builder = $this->dbconn('historial_clinico.alergias_medicamentos');
		$builder->where('id', $data['id']);
		$query = $builder->update($data);
		return $query;
	   //return  $strQuery;
	}

}

