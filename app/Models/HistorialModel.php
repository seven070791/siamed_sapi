<?php namespace App\Models;
use CodeIgniter\Model;
class HistorialModel extends BaseModel
{
public function getAll($estatus=null)
     {
		$db      = \Config\Database::connect();
		$strQuery ="SELECT c.id,c.descripcion,c.fecha_creacion,CASE WHEN C.borrado='t' THEN 'Bloqueado' ELSE 'Activo' END AS borrado
		FROM public.categoria_inventario as c ";

	    //return $strQuery;	
		   $query = $db->query($strQuery);
		    $resultado=$query->getResult(); 
		   return $resultado;	
     }



	 public function getAllHistorialFamiliar($n_historial,$cedula)
     {
	
	
	  $builder = $this->dbconn('historial_clinico.historial_medico as h ');
	  $builder->select
	  (
		"h.n_historial
		,h.id
		,f.nombre
		,case when h.borrado='t' then 'Inactivo' else 'Activo' end as borrado
		,f.apellido
		,h.cedula
		,to_char(f.fecha_nac_familiares,'dd/mm/yyyy') as fecha_nac
		,to_char(h.fecha_regist,'dd/mm/yyyy') as fecha_regist");
	   $builder->where('h.cedula', $cedula);
	  // $builder->where('h.borrado', false);
	  $builder->join('familiares as f ', 'f.cedula=h.cedula');
	  $query = $builder->get();
	  return $query;	
     } 
	
	 public function getAllHistorialCortesia($n_historial,$cedula)
     {
	  $builder = $this->dbconn('historial_clinico.historial_medico as h ');
	  $builder->select
	  (
		"h.n_historial
		,h.id
		,case when h.borrado='t' then 'Inactivo' else 'Activo' end as borrado
		,c.nombre
		,c.apellido
		,h.cedula
		,to_char(c.fecha_nac_cortesia,'dd/mm/yyyy') as fecha_nac
		,to_char(h.fecha_regist,'dd/mm/yyyy') as fecha_regist");
		$builder->where('h.cedula', $cedula);
	   	//$builder->where('h.borrado', false);
	  	$builder->join('cortesia as c ', 'c.cedula=h.cedula');
	  $query = $builder->get();
	  return $query;	
     } 



	 
	 public function getAllHistorial($cedulabeneficiario)
     {
		/*
		La variable $cedulabeneficiarioNumerica se utiliza para
		poder hacer join con titulares por el tipo de dato
		en querybuilder ya que da error el casteo de datos de forma directa
		es decir, asi: ($builder->join('titulares as t ', 't.cedula_trabajador=h.cedula')
		*/
	  $cedulabeneficiarioNumerica=intval($cedulabeneficiario);
	  $builder = $this->dbconn('historial_clinico.historial_medico as h ');
	  $builder->select
	  (
		"h.id
		,h.n_historial
		,case when h.borrado='t' then 'Inactivo' else 'Activo' end as borrado
		,t.nombre
		,t.apellido
		,h.cedula
		,to_char(t.fecha_nacimiento,'dd/mm/yyyy') as fecha_nac
		,to_char(h.fecha_regist,'dd/mm/yyyy') as fecha_regist");
	  $builder->where('h.cedula', $cedulabeneficiario);
	  //$builder->where('h.borrado', false);
	  //$builder->join('titulares as t ', 't.cedula_trabajador=h.cedula');
	  $builder->join('titulares as t ', "t.cedula_trabajador=$cedulabeneficiarioNumerica");
	  $query = $builder->get();
	  return $query;	
     } 


	  
	//  ***Metodo que verifica si la cedula exixte***
	public function getHistorial_inactivo($cedulabeneficiario, $n_historial)
	
	{
		  $db      = \Config\Database::connect();
	   $strQuery ="SELECT h.cedula,n_historial,h.cedula,h.borrado
	   FROM historial_clinico.historial_medico as h where h.n_historial='$n_historial' and h.cedula='$cedulabeneficiario' ";
		 $query = $db->query($strQuery);
		 $resultado=$query->getResult(); 
		return $resultado;	
	} 


	 
	//  ***Metodo que verifica si la cedula exixte***
	 public function getHistorial($cedulabeneficiario)
	
     {
	  	 $db      = \Config\Database::connect();
		$strQuery ="SELECT h.cedula,h.borrado
		FROM historial_clinico.historial_medico as h where h.cedula='$cedulabeneficiario' and  h.borrado='false'";
	  	$query = $db->query($strQuery);
	  	$resultado=$query->getResult(); 
	 	return $resultado;	
     } 

     public function agregar_historial_medico($data)
	 {
		 $builder = $this->dbconn('historial_clinico.historial_medico');
		 $query = $builder->insert($data);  
		return $query;
     }

     public function borrar_historial($data)
     { 
         $builder = $this->dbconn('historial_clinico.historial_medico as h');
         $builder->where('h.id', $data['id'] , 'h.borrado', 'false');
         $query = $builder->update($data);
         return $query;
     }

    }