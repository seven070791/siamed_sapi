<?php namespace App\Models;
use CodeIgniter\Model;
class Compuesto_Model extends BaseModel
{
	 public function agregar_compuesto($data)
	 {
		 $builder = $this->dbconn('public.compuestos');
		 $query = $builder->insert($data);  
		return $query;
     }
	 public function getAll($id_medicamento=null)
     {
		$db      = \Config\Database::connect();
		$strQuery ="SELECT c.id_tipo_medicamento,c.id_compuesto,c.descripcioncompuesta,c.borrado ,tipo_medicamento.descripcion as tipo_medicamento,c.cantidad,CASE WHEN C.borrado='t' THEN 'Bloqueado' ELSE 'Activo' END AS borrado,c.id_unidad_medida ,unidad_medida.descripcion as unidad_medida
		FROM public.compuestos as c 
		JOIN unidad_medida on c.id_unidad_medida= unidad_medida.id
		JOIN tipo_medicamento on c.id_tipo_medicamento=tipo_medicamento.id
		WHERE c.id_medicamento=$id_medicamento";
	    //return $strQuery;	
		   $query = $db->query($strQuery);
		    $resultado=$query->getResult(); 
		   return $resultado;
	
     }
	 public function getAllActivos()
     {
	  $builder = $this->dbconn('public.compuestos as c');
	  $builder->select
	  (
		"c.id_compuesto
		,c.descripcioncompuesta"
	  );
	  $builder->where(['c.borrado'=>false]);
	  $query = $builder->get();
	  return $query;	
     }
	 public function actualizar_compuestos($data)
	{
		
		$builder = $this->dbconn('public.compuestos');
		$builder->where('id_compuesto', $data['id_compuesto']);
		$query = $builder->update($data);
		return $query;
	}

	}

	