<?php namespace App\Models;
use CodeIgniter\Model;
class Medicamentos_patologias_Model extends BaseModel
{



	public function agregar($data)
	{
		 $builder = $this->dbconn(' historial_clinico.medicamentos_patologias ');
		 $query = $builder->insert($data);  
		return $query;
    }

	public function listar_medicamentos($n_historial,$id_especialidad,$id_consulta)
	{
 
	   //$builder = $this->dbconn('historial_clinico.consultas as hc');
	   $db      = \Config\Database::connect();
	   $strQuery ="";
	   $strQuery .="SELECT";
	   $strQuery .=" distinct hc.id_medico"; 
	   $strQuery .=",mp.id";  
	   $strQuery .=",mp.descripcion "; 
	   $strQuery .=",to_char(mp.fecha_creacion,'dd/mm/yyyy') as fecha_creacion "; 
	   $strQuery .=",to_char(mp.fecha_actualizacion,'dd/mm/yyyy') as fecha_actualizacion "; 
	   $strQuery .=",CONCAT(m.nombre,' ', m.apellido) AS nombre ";
	   $strQuery .=",e.descripcion as especialidad ";
	   $strQuery .="FROM ";
	   $strQuery .="   historial_clinico.medicamentos_patologias as mp ";	
	   $strQuery .="  join historial_clinico.consultas  as hc on mp.id_consulta=hc.id";
	   $strQuery .="  join  historial_clinico.medicos as m on hc.id_medico=m.id";
	   $strQuery .="  join historial_clinico.especialidades as e on m.especialidad=e.id_especialidad ";
	   $strQuery .= " where mp.n_historial='$n_historial'";
	   $strQuery .="  AND mp.id_consulta=$id_consulta";
	   $strQuery .="  AND mp.id_especialidad=$id_especialidad";
	   $strQuery .="  AND mp.borrado=false";
	   $query = $db->query($strQuery);
	   $resultado=$query->getResult(); 
	   return $resultado;
	   //return  $strQuery;
	}
	
	public function listar_Medicamentos_patologias_Individual($n_historial,$id_especialidad,$id_consulta)
	{
 
	   //$builder = $this->dbconn('historial_clinico.consultas as hc');
	   $db      = \Config\Database::connect();
	   $strQuery ="";
	   $strQuery .="SELECT";
	   $strQuery .=" distinct hc.id_medico"; 
	   $strQuery .=",mp.id";  
	   $strQuery .=",mp.descripcion "; 
	   $strQuery .=",to_char(mp.fecha_creacion,'dd/mm/yyyy') as fecha_creacion "; 
	   $strQuery .=",to_char(mp.fecha_actualizacion,'dd/mm/yyyy') as fecha_actualizacion "; 
	   $strQuery .=",CONCAT(m.nombre,' ', m.apellido) AS nombre ";
	   $strQuery .=",e.descripcion as especialidad ";
	   $strQuery .="FROM ";
	   $strQuery .="   historial_clinico.medicamentos_patologias as mp ";	
	   $strQuery .="  join historial_clinico.consultas  as hc on mp.id_consulta=hc.id";
	   $strQuery .="  join  historial_clinico.medicos as m on hc.id_medico=m.id";
	   $strQuery .="  join historial_clinico.especialidades as e on m.especialidad=e.id_especialidad ";
	   $strQuery .= " where mp.n_historial='$n_historial'";
	   $strQuery .="  AND mp.id_consulta=$id_consulta";
	   $strQuery .="  AND mp.id_especialidad=$id_especialidad";
	   $strQuery .="  AND mp.borrado=false";
	   $query = $db->query($strQuery);
	   $resultado=$query->getResult(); 
	   return $resultado;
	   //return  $strQuery;
	}

	
	public function actualizar_medicamentos($data)
	{
		$builder = $this->dbconn(' historial_clinico.medicamentos_patologias');
		$builder->where('id', $data['id']);
		$query = $builder->update($data);
		return $query;
	   //return  $strQuery;
	}


}

