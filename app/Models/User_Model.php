<?php

namespace App\Models;

use CodeIgniter\Model;

class User_Model extends BaseModel
{
    protected $table = 'usuarios';

    protected $allowedFields = ['id', 'cedula', 'nombre', 'apellido', 'usuario', 'password', 'tipousuario', 'borrado'];
    // Aqui retorno los datos del usuario dependiendo de la cedula
    public function getUsuarios()
    {
        $builder =$this->dbconn('public.usuarios as u');
        $builder->select
        (
             "u.id,u.password,u.nombre,u.apellido,u.cedula,u.usuario,u.tipousuario as nivel_usu ,g.nivel_usuario as tipousuario
             ,u.email,CASE WHEN u.borrado='t' THEN 'Eliminado' ELSE 'Activo' END AS estatus"
        );
        $builder->join('grupo_usuario as g', 'g.id=u.tipousuario');
        $query = $builder->get();
        return $query;	
    }

    
    public function listar_nivel_usuario()
    {
        $builder =$this->dbconn('public.grupo_usuario as g');
        $builder->select
        (
             "g.id,g.nivel_usuario"
        );
        $query = $builder->get();
        return $query;	
    }

    public function actualizar($data)
	{
      
		$builder = $this->dbconn('public.usuarios');
		$builder->where('id', $data['id']);
		$query = $builder->update($data);
		return $query;
	}
    public function actualizar_password($data)
	{
      
		$builder = $this->dbconn('public.usuarios');
		$builder->where('email', $data['email']);
		$query = $builder->update($data);
		return $query;
	}

    public function buscarusuario($busca_cedula_usuario = false)
    {
        if ($busca_cedula_usuario === false) {
            $this->select(['usuarios.id', 'cedula', 'nombre', 'apellido', 'usuario', 'password', 'nivel_usuario', 'usuarios.borrado'])
                ->join('grupo_usuario', 'grupo_usuario.id=usuarios.tipousuario');
            return $this->findAll();
        }
        return $this->where(['cedula' => $busca_cedula_usuario])->first();
    }

    public function agregar($data)
    {

        $db      = \Config\Database::connect();
        $builder = $db->table('usuarios');
        $query = $builder->insert($data);
        return $query;
    }




    // public function ActualizarUsuario($datos)
    // {
    //     $db      = \Config\Database::connect();
    //     $builder = $db->table('usuarios');

    //     $data =
    //         [

    //             'cedula' => $datos['cedula'],
    //             'nombre' => $datos['nombre'],
    //             'apellido' => $datos['apellido'],
    //             'password' => $datos['password'],
    //             'usuario' => $datos['usuario'],
    //             // 'correo' => $datos['correo'],
    //             'tipousuario' => $datos['tipousuario'],
    //             'borrado' => $datos['borrado'],
    //         ];
    //     $builder->where('id', $datos['id']);
    //     $builder->update($data);
    // }

    public function delete($id = null, $estatus = null)
    {
        $db      = \Config\Database::connect();
        $builder = $db->table('usuarios');
        $data =
            [
                'id' => $id,
                'borrado' => $estatus,
            ];
        $builder->where('id', $id);
        $query = $builder->update($data);
        return $query;
    }
}
