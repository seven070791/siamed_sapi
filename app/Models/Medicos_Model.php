<?php

namespace App\Models;

use CodeIgniter\Model;

class Medicos_Model extends BaseModel
{
	public function agregar_especialidad($data)
	{

		$builder = $this->dbconn('historial_clinico.especialidades');
		$query = $builder->insert($data);
		return $query;
	}

	public function agregar_medico($data)
	{
		$builder = $this->dbconn('historial_clinico.medicos');
		$query = $builder->insert($data);
		return $query;
	}

	public function listar_especialidad($estatus = null)
	{
		$db      = \Config\Database::connect();
		$strQuery = "SELECT e.id_especialidad,e.descripcion,CASE WHEN e.borrado='true' THEN 'Bloqueado' ELSE 'Activo' END AS borrado
		FROM historial_clinico.especialidades as e ";

		//return $strQuery;	
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}


	public function listar_medicos($estatus = null)
	{
		$db = \Config\Database::connect();
		$strQuery = "SELECT m.id,m.cedula,m.nombre,m.apellido,m.telefono,m.especialidad as id_especialidad";
		$strQuery .= ",e.descripcion as especialidad";
		$strQuery .= ",CASE WHEN m.borrado='true' THEN 'Bloqueado' ELSE 'Activo' END AS borrado";
		$strQuery .= " FROM historial_clinico.medicos as m";
		$strQuery .= " JOIN historial_clinico.especialidades as e on m.especialidad=e.id_especialidad";
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;

		//return $strQuery;
	}
	public function Buscarmedicos($cedula = null)
	{
		$db = \Config\Database::connect();
		$strQuery = "SELECT m.id,m.nombre,m.apellido,m.especialidad as id_especialidad";
		$strQuery .= ",e.descripcion as especialidad, e.acceso_citas";
		$strQuery .= ",CASE WHEN m.borrado='true' THEN 'Bloqueado' ELSE 'Activo' END AS borrado";
		$strQuery .= " FROM historial_clinico.medicos as m";
		$strQuery .= " JOIN historial_clinico.especialidades as e on m.especialidad=e.id_especialidad";
		$strQuery .= " WHERE m.cedula=$cedula";
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
		//return $strQuery;
	}

	public function listar_medicos_activos($estatus = null)
	{
		$db = \Config\Database::connect();
		$strQuery = "SELECT m.id,m.cedula,m.nombre,m.apellido,m.telefono,m.especialidad as id_especialidad";
		$strQuery .= ",e.descripcion as especialidad";
		$strQuery .= ",CASE WHEN m.borrado='true' THEN 'Bloqueado' ELSE 'Activo' END AS borrado";
		$strQuery .= " FROM historial_clinico.medicos as m";
		$strQuery .= " JOIN historial_clinico.especialidades as e on m.especialidad=e.id_especialidad";
		$strQuery .= " WHERE m.borrado='false'";
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;

		//return $strQuery;
	}


	public function medicos_activos_por_especialidad($id_especialidad = null)
	{
		$db = \Config\Database::connect();
		$strQuery = "SELECT m.id,m.cedula,m.nombre,m.apellido,m.telefono,m.especialidad as id_especialidad";
		$strQuery .= ",e.descripcion as especialidad";
		$strQuery .= ",CASE WHEN m.borrado='true' THEN 'Bloqueado' ELSE 'Activo' END AS borrado";
		$strQuery .= " FROM historial_clinico.medicos as m";
		$strQuery .= " JOIN historial_clinico.especialidades as e on m.especialidad=e.id_especialidad";
		$strQuery .= " WHERE m.borrado='false'";
		$strQuery .= " AND m.especialidad=$id_especialidad";
		//return $strQuery;
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}




	public function actualizar_especialidad($data)
	{

		$builder = $this->dbconn('historial_clinico.especialidades');
		$builder->where('id_especialidad', $data['id_especialidad']);
		$query = $builder->update($data);
		return $query;
	}

	public function actualizar_Medicos($data)
	{

		$builder = $this->dbconn('historial_clinico.medicos');
		$builder->where('id', $data['id']);
		$query = $builder->update($data);
		return $query;
	}
}
