<?php namespace App\Models;
use CodeIgniter\Model;
class Antece_Gineco_Obstetrico_Model extends BaseModel
{
	
	public function agregar($data)
	{
		
		$builder = $this->dbconn('historial_clinico.antecedentes_gineco_osbtetrico');
		$query = $builder->insert($data);  
	   return $query;
	}

	public function listar_Antece_Gineco_obtetrico($n_historial,$id_consulta)
	{

	   //$builder = $this->dbconn('historial_clinico.consultas as hc');
	   $db      = \Config\Database::connect();
	   $strQuery ="";
	   $strQuery .="SELECT ";
	   $strQuery .="atc_giobs.id";  
	   $strQuery .=",atc_giobs.n_historial ";
	   $strQuery .=",atc_giobs.id_consulta "; 
	   $strQuery .=",atc_giobs.menarquia "; 
	   $strQuery .=",atc_giobs.sexarquia "; 
	   $strQuery .=",atc_giobs.nps "; 
	   $strQuery .=",atc_giobs.diu "; 
	   $strQuery .=",atc_giobs.t_cobre ";
	   $strQuery .=",atc_giobs.mirena ";  
	   $strQuery .=",atc_giobs.aspiral "; 
	   $strQuery .=",atc_giobs.i_subdermico "; 
	   $strQuery .=",atc_giobs.otro "; 
	   $strQuery .=",atc_giobs.ciclo_mestrual "; 
	   $strQuery .=",to_char(atc_giobs.f_u_r,'dd/mm/yyyy') as f_u_r "; 
	   $strQuery .=",atc_giobs.edad_gestacional "; 
	   $strQuery .=",atc_giobs.dismenorrea "; 
	   $strQuery .=",CASE WHEN atc_giobs.dismenorrea='f' THEN 'NO'  WHEN atc_giobs.dismenorrea='t' THEN 'SI' ELSE 'NO' end  as descr_dismenorrea "; 
	   $strQuery .=",atc_giobs.eumenorrea "; 
	   $strQuery .=",CASE WHEN atc_giobs.eumenorrea='f' THEN 'NO'  WHEN atc_giobs.eumenorrea='t' THEN 'SI' ELSE 'NO' end  as descr_eumenorrea ";
	   $strQuery .=",atc_giobs.anticonceptivo "; 
	   $strQuery .=",CASE WHEN atc_giobs.anticonceptivo='f' THEN 'NO'  WHEN atc_giobs.anticonceptivo='t' THEN 'SI' ELSE 'NO' end  as descr_anticon "; 
	   $strQuery .=",atc_giobs.detalles_anticon "; 
	   $strQuery .=",to_char(atc_giobs.t_colocacion,'dd/mm/yyyy') as t_colocacion ";  
	   $strQuery .=",atc_giobs.ets "; 
	   $strQuery .=",CASE WHEN atc_giobs.ets='f' THEN 'NO'  WHEN atc_giobs.ets='t' THEN 'SI' ELSE 'NO' end  as descr_ets "; 
	   $strQuery .=",atc_giobs.tipo_ets "; 
	   $strQuery .=",ets.descripcion as descrip_tipo_ets ";
	   $strQuery .=",atc_giobs.detalle_ets ";
	   $strQuery .=",atc_giobs.citologia ";
	   $strQuery .=",atc_giobs.eco_mamario "; 
	   $strQuery .=",atc_giobs.mamografia "; 
	   $strQuery .=",atc_giobs.desintometria ";   
	   $strQuery .=",atc_giobs.detalle_historia_obst ";
	   $strQuery .="FROM ";
	   $strQuery .=" historial_clinico.antecedentes_gineco_osbtetrico as atc_giobs ";	
	   $strQuery .=" JOIN historial_clinico.enfermedades_sexuales as ets on atc_giobs.tipo_ets=ets.id ";	

	   
	   $strQuery  .=" where atc_giobs.n_historial='$n_historial'";
	   $strQuery  .=" or atc_giobs.id_consulta='$id_consulta'";
	   $query = $db->query($strQuery);
	   $resultado=$query->getResult(); 
	   return $resultado;
	   
	}

	public function actualizar_Antecedentes_gineco_obstetricos($data)
	{
		$builder = $this->dbconn('historial_clinico.antecedentes_gineco_osbtetrico');
		$builder->where('id_consulta', $data['id_consulta']);
		$query = $builder->update($data);
		return $query;
	   //return  $strQuery;
	}
}

	