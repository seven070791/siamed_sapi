<?php namespace App\Models;
use CodeIgniter\Model;
use PhpOffice\PhpSpreadsheet\Worksheet\Row;

class Patologias_Model extends BaseModel
{
	
/*
      * METODO QUE INSETAR UNA NUEVA PATOLOGIA EN LA TABLA PRINCIPAL PATOLOGIAS
 */
	public function agregar_patologia_master($data)
	{

	   $builder = $this->dbconn('historial_clinico.patologias');
	   $query = $builder->insert($data);  
	   return $query;
	}
/*
      * METODO QUE ACTUALIZA PATOLOGIA EN LA TABLA PRINCIPAL PATOLOGIAS
 */
	public function actualizar_patologias_master($data)
	{
		$builder = $this->dbconn('historial_clinico.patologias as p');
		$builder->where('p.id', $data['id']);
		$query = $builder->update($data);
		return $query;
	}




/*
      * METODO QUE AGREGA LAS PATOLOGIAS EN FUNCION DE LA ESPECIALIDAD 
 */
	public function agregar($data)

	{

		$builder = $this->dbconn('historial_clinico.patologia_historial');
		$query = $builder->insert($data);
		return $query ? true : false;
	
	}
	
	public function listar_patologias()
	{
 
	   $db      = \Config\Database::connect();
	   $strQuery ="";
	   $strQuery .="SELECT";
	   $strQuery .=" patologias.id";  
	   $strQuery .=",patologias.descripcion "; 
	   $strQuery .=", case when patologias.borrado='f' then 'Activo ' ELSE 'Inactivo' end as borrado  "; 
	   $strQuery .="FROM ";
	   $strQuery .="  historial_clinico.patologias ";	
	   $strQuery .="  where patologias.borrado='f' ";
	  // return  $strQuery;
	  $query = $db->query($strQuery);
	  $resultado=$query->getResult(); 
	  return $resultado;
	}

	public function Listar_patologias_activas()
	{
 
	   $db      = \Config\Database::connect();
	   $strQuery ="";
	   $strQuery .="SELECT";
	   $strQuery .=" patologias.id";  
	   $strQuery .=",patologias.descripcion ";  
	   $strQuery .="FROM ";
	   $strQuery .="  historial_clinico.patologias ";	
	   $strQuery .=" where patologias.borrado='false' ";  
	  // return  $strQuery;
	  $query = $db->query($strQuery);
	  $resultado=$query->getResult(); 
	  return $resultado;
	}


	




	public function listar_patologias_Historial($numeroHistorial=null)
	{
 
	   $db      = \Config\Database::connect();
	   $strQuery ="";
	   $strQuery .="SELECT";
	   $strQuery .=" patologias.id_patologia,patologias.borrado ";  
	   $strQuery .="FROM ";
	   $strQuery .="  historial_clinico.patologia_historial as patologias ";	
	   $strQuery .="  where patologias.n_historial='$numeroHistorial' ";	
	   $strQuery .="  and patologias.borrado='false'";
	   //return  $strQuery;
	  $query = $db->query($strQuery);
	  $resultado=$query->getResult(); 
	 return $resultado;
	}

	//METODO QUE PINTA LOS CHECKBOX ACTIVOS EN FUNSION DE LAS PATOLOGIAS 
	public function buscar_patologias_existentes($datos=null)
	{
 
		$n_historial = $datos['n_historial'];
		$id_patologia = $datos['id_patologia'];
	   $db      = \Config\Database::connect();
	   $strQuery ="";
	   $strQuery .="SELECT";
	   $strQuery .=" patologias.id_patologia ";  
	   $strQuery .="FROM ";
	   $strQuery .="  historial_clinico.patologia_historial as patologias ";	
	   $strQuery .="  where patologias.n_historial='$n_historial' ";	
	   $strQuery .="  and patologias.id_patologia=$id_patologia ";	
	   $strQuery .="  and patologias.borrado='false'";
	   $query = $db->query($strQuery);
	   $resultado=$query->getResult(); 
	 return $resultado;
	}
	public function patologias_existentes($datos=null)
	{
 
		$n_historial = $datos['n_historial'];
		$id_patologia = $datos['id_patologia'];
	   $db      = \Config\Database::connect();
	   $strQuery ="";
	   $strQuery .="SELECT";
	   $strQuery .=" patologias.id_patologia,patologias.borrado,patologias.n_historial ";  
	   $strQuery .="FROM ";
	   $strQuery .="  historial_clinico.patologia_historial as patologias ";	
	   $strQuery .="  where patologias.n_historial='$n_historial' ";	
	   $strQuery .="  and patologias.id_patologia=$id_patologia ";	
	   $query = $db->query($strQuery);
	   $resultado=$query->getResult(); 
	 return $resultado;
	}


	// METODO QUE  ACTIVA LAS PATOLOGIAS EXISTENTES 
	public function Activar_patologias_existentes($patologias)
	{
		
		$builder = $this->dbconn(' historial_clinico.patologia_historial as patologias');
		
			$builder->where('n_historial',$patologias['n_historial'] );
			$builder->where('id_patologia',$patologias['id_patologia']);
			$query = $builder->update([
				'borrado' => false,
			]);
	
		return $query;
	}

	// METODO QUE ELIMINA LAS PATOLOGIAS EXISTENTES 
	public function Eliminar_patologias_existentes($check_borrados,$n_historia)
	{
		$builder = $this->dbconn(' historial_clinico.patologia_historial as patologias');
		foreach ($check_borrados as $Row)
		{	
			$builder->where('n_historial',$n_historia);
			$builder->where('id_patologia',$Row);
			$query = $builder->update([
				'borrado' => true,
			]);
		}
		return $query;
	}
	
	
}

	