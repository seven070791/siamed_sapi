<?php

namespace App\Models;

use CodeIgniter\Model;
use phpDocumentor\Reflection\DocBlock\Tags\Var_;

class Entrada_Model extends BaseModel
{
	// *********ESTE METODO LO USO PARA CARGAR EL DATATABLE DE ENTRADAS********
	public function getAll($id_medicamento = null)
	{
		$db      = \Config\Database::connect();
		$strQuery = "";
		$strQuery .= "SELECT";
		$strQuery .= " a.id";
		$strQuery .= ",a.fecha_entrada";
		$strQuery .= ",a.fecha_vencimiento";
		$strQuery .= ",a.borrado";
		$strQuery .= ",a.cantidad as entradas";
		$strQuery .= ",CASE WHEN b.salidas IS NULL THEN 0 ELSE b.salidas END AS salidas";
		$strQuery .= ",CASE WHEN a.cantidad-b.salidas IS NULL THEN a.cantidad ELSE a.cantidad-b.salidas END AS Stock";
		$strQuery .= ",a.vigencia";
		$strQuery .= " FROM";
		$strQuery .= "(";
		$strQuery .= "SELECT ";
		$strQuery .= "e.id";
		$strQuery .= ",e.user_id";
		$strQuery .= ",e.id_medicamento";
		$strQuery .= ",e.cantidad";
		//$strQuery .=",e.stock";
		$strQuery .= ",e.borrado";
		$strQuery .= ",to_char(e.fecha_entrada,'dd/mm/yyyy') as fecha_entrada";
		$strQuery .= ",to_char(e.fecha_vencimiento,'dd/mm/yyyy') as fecha_vencimiento";
		$strQuery .= ",CASE WHEN";
		$strQuery .= " CURRENT_DATE";
		$strQuery .= '>=';
		$strQuery .= " fecha_vencimiento";
		$strQuery .= " THEN 'Vencido'";
		$strQuery .= " ELSE 'Vigente'  ";
		$strQuery .= " END AS vigencia";
		$strQuery .= ",CASE WHEN e.borrado='t' THEN 'Eliminado' ELSE 'Activo' END AS Estatus";
		$strQuery .= " FROM ";
		$strQuery .= "public.entradas as e";
		$strQuery .= " JOIN ";
		$strQuery .= "medicamentos as m ON e.id_medicamento=m.id ";
		$strQuery .= "WHERE e.borrado='false'";
		$strQuery .= ")";
		$strQuery .= " as a";
		$strQuery .= " LEFT JOIN ";
		$strQuery .= "(";
		$strQuery .= "select id_entrada, sum(cantidad) as salidas from salidas ";
		$strQuery .= "WHERE salidas.borrado='false' ";
		$strQuery .= "group by salidas.id_entrada";
		$strQuery .= ")";
		$strQuery .= " as b ON a.id=b.id_entrada";
		if ($id_medicamento != null) {
			$strQuery .= " WHERE a.id_medicamento=$id_medicamento";
		}
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
		//return $strQuery;	
	}



	public function VerEntradasMedicamentosPdf($id_categoria = 0, $cronicos = null, $desde = null, $hasta = null, $nombre_categoria = null, $id_estatus = 0, $nombre_estatus = null)
	{



		$db      = \Config\Database::connect();
		$strQuery = "SELECT ";
		$strQuery .= "user_id,  to_char(fecha_entrada,'dd/mm/yyyy') as fecha_entrada,to_char(fecha_vencimiento,'dd/mm/yyyy') as fecha_vencimiento_convertida, fecha_vencimiento, id_medicamento,cantidad,medicamentos.descripcion";
		$strQuery .= ",usuarios.nombre,categoria_inventario.descripcion as categoria_inventario";
		$strQuery .= ",case when  fecha_vencimiento >current_date then'Activo' else 'Vencido' end as estatus";
		$strQuery .= ",case when medicamentos.med_cronico='t' then 'SI'  else 'NO' end as med_cronico";
		$strQuery .= " FROM public.entradas ";
		$strQuery .= "join medicamentos on entradas.id_medicamento=medicamentos.id ";
		$strQuery .= "join usuarios on entradas.user_id= usuarios.id ";
		$strQuery .= "join tipo_medicamento on medicamentos.id_tipo_medicamento=tipo_medicamento.id ";
		$strQuery .= "join categoria_inventario on tipo_medicamento.categoria_id=categoria_inventario.id ";
		$strQuery .= " WHERE entradas.borrado=false";
		$strWhere = "";
		if ($id_categoria != 0) {
			if (trim($strWhere) == "") {
				$strWhere .= " AND categoria_inventario.id=$id_categoria";
			} else {
				$strWhere .= " AND categoria_inventario.id=$id_categoria";
			}
		}



		//$strWhere ="";




		if ($cronicos != 'Seleccione') {
			if ($cronicos == 'true') {
				$strQuery .= " AND med_cronico=true";
			} else if ($cronicos == 'false') {
				$strQuery .= " AND med_cronico=false";
			}
		}




		if ($id_estatus != 'null') {
			if ($id_estatus == '1') {
				$strWhere .= "  AND fecha_vencimiento >current_date";
			} else if ($id_estatus == '2') {
				$strWhere .= "  AND fecha_vencimiento <current_date";
			}
		}



		if ($desde != 'null' and $hasta != 'null') {
			if (trim($strWhere) == "") {
				$strWhere .= " AND fecha_entrada BETWEEN '$desde'AND '$hasta'";
			} else {
				$strWhere .= " AND fecha_entrada BETWEEN '$desde'AND '$hasta'";
			}
		}



		$strQuery = $strQuery . $strWhere;

		//return $strQuery;
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}


	// *********ESTE METODO LO USO PARA GENERAR EL REPORTE GENERAL DE LAS ENTRADAS********
	public function getAllEntradas($id_medicamento = null)
	{
		$db      = \Config\Database::connect();
		$strQuery = "SELECT user_id,  to_char(fecha_entrada,'dd/mm/yyyy') as fecha_entrada, fecha_vencimiento, id_medicamento,cantidad,medicamentos.descripcion";
		$strQuery .= ",usuarios.nombre FROM public.entradas join medicamentos on entradas.id_medicamento=medicamentos.id";
		$strQuery .= " join usuarios on entradas.user_id= usuarios.id";
		if ($id_medicamento != 'null') {
			$strQuery = $strQuery . " where id_medicamento=$id_medicamento";
		}
		$strQuery .= " ORDER BY fecha_entrada ";
		//return $strQuery; 
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}
	// *********ESTE METODO LO USO PARA GENERAR EL REPORTE INDIVIDUAL DE LAS ENTRADAS********
	public function GenerarReportesEntradaPorFecha(string $desde = null, string $hasta = null, $id_medicamento = null)
	{
		$db      = \Config\Database::connect();
		$strQuery = "SELECT user_id,  to_char(fecha_entrada,'dd/mm/yyyy') as fecha_entrada,to_char(fecha_vencimiento,'dd/mm/yyyy') as fecha_vencimiento, id_medicamento,cantidad,medicamentos.descripcion";
		$strQuery .= ",usuarios.nombre FROM public.entradas join medicamentos on entradas.id_medicamento=medicamentos.id";
		$strQuery .= " join usuarios on entradas.user_id= usuarios.id";
		$strQuery .= " WHERE fecha_entrada BETWEEN '$desde'AND '$hasta'";
		if ($id_medicamento != 'null') {
			$strQuery = $strQuery . " AND id_medicamento=$id_medicamento";
		}
		$strQuery .= " ORDER BY fecha_entrada ";

		// return $strQuery; 
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}
	public function agregar($data)
	{

		$builder = $this->dbconn('public.entradas');

		$query = $builder->insert($data);
		return $query;
	}

	public function Reverso_entrada($data)
	{
		$builder = $this->dbconn('public.entradas');
		$builder->where('id', $data['id']);
		$query = $builder->update($data);
		return $query;
	}

	public function actualizar_fecha_v($data)
	{
		$builder = $this->dbconn('public.entradas');
		$builder->where('id', $data['id']);
		$query = $builder->update($data);
		return $query;
	}
}
