<?php namespace App\Models;
use CodeIgniter\Model;
class Reverso_Model extends BaseModel
{
    public function ReversoSalidaTabla($data,$datosReverso)
	{

        $db      = \Config\Database::connect();
        $builder = $db->table('public.reversos');
        $dato['tipo_de_reverso']='Salida';
        $dato['id_usuario']= $data['user_id'];
        $dato['id_medicamento']= $data['id_medicamento'];
        $dato['cantidad']= $data['cantidad'];
        $dato['id_entrada']= $data['id_entrada'];
        $dato['observacion']= $datosReverso['observacion'];
        $query_reverso_tabla= $builder->insert($dato);	
		return  $query_reverso_tabla;
	}
}