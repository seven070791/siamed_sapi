<?php namespace App\Models;
use CodeIgniter\Model;
class Especialidad_Model extends BaseModel
{
	 public function agregar_especialidad($data)
	 {

		$builder = $this->dbconn('historial_clinico.especialidades');
		$query = $builder->insert($data);  
		return $query;
     }

	 public function listar_especialidad($estatus=null)
     {
		$db      = \Config\Database::connect();
		$strQuery ="SELECT e.id_especialidad,e.descripcion,acceso_citas,CASE WHEN e.borrado='true' THEN 'Bloqueado' ELSE 'Activo' END AS borrado
		,CASE WHEN e.acceso_citas='true' THEN 'S' ELSE 'N' END AS acceso_citas
		FROM historial_clinico.especialidades as e ";

	    //return $strQuery;	
		   $query = $db->query($strQuery);
		    $resultado=$query->getResult(); 
		   return $resultado;	
     }



	 public function listar_especialidades_activas()
     {
		
		$db      = \Config\Database::connect();
		$strQuery ="SELECT e.id_especialidad,e.descripcion,acceso_citas,CASE WHEN e.borrado='true' THEN 'Bloqueado' ELSE 'Activo' END AS borrado
		FROM historial_clinico.especialidades as e WHERE e.borrado='f'  ";
	    //return $strQuery;	
		$query = $db->query($strQuery);
		$resultado=$query->getResult(); 
		return $resultado;	
     }

	 public function buscar_especialidad($id_especialidad)
	 {
		
		$db      = \Config\Database::connect();
		$strQuery ="SELECT e.id_especialidad,e.descripcion,acceso_citas,CASE WHEN e.borrado='true' THEN 'Bloqueado' ELSE 'Activo' END AS borrado
		FROM historial_clinico.especialidades as e WHERE e.borrado='f' and acceso_citas='f' and e.id_especialidad='$id_especialidad' ";
 
		//return $strQuery;	
		   $query = $db->query($strQuery);
			$resultado=$query->getResult(); 
		   return $resultado;	
	 }
 


	 public function listar_acceso_citas_especialidades()
     {
		
		$db      = \Config\Database::connect();
		$strQuery ="SELECT e.id_especialidad,e.descripcion,acceso_citas,CASE WHEN e.borrado='true' THEN 'Bloqueado' ELSE 'Activo' END AS borrado
		FROM historial_clinico.especialidades as e WHERE e.borrado='f' and acceso_citas='true' ";

	    //return $strQuery;	
		   $query = $db->query($strQuery);
		    $resultado=$query->getResult(); 
		   return $resultado;	
     }

	 public function listar_especialidades_activas_sin_filtro()
     {
		
		$db      = \Config\Database::connect();
		$strQuery ="SELECT e.id_especialidad,e.descripcion,acceso_citas,CASE WHEN e.borrado='true' THEN 'Bloqueado' ELSE 'Activo' END AS borrado
		FROM historial_clinico.especialidades as e WHERE e.borrado='f'  ";

	    //return $strQuery;	
		   $query = $db->query($strQuery);
		    $resultado=$query->getResult(); 
		   return $resultado;	
     }


	 public function listar_especialidades_activas_sin_filtro_caja_chica()
     {
		
		$db      = \Config\Database::connect();
		$strQuery ="SELECT e.id_especialidad,e.descripcion,acceso_citas,CASE WHEN e.borrado='true' THEN 'Bloqueado' ELSE 'Activo' END AS borrado
		FROM historial_clinico.especialidades as e WHERE e.borrado='f' AND  caja_chica='f'  ";

	    //return $strQuery;	
		   $query = $db->query($strQuery);
		    $resultado=$query->getResult(); 
		   return $resultado;	
     }


	 public function actualizar_especialidad($data)
	{
		
		$builder = $this->dbconn('historial_clinico.especialidades');
		$builder->where('id_especialidad', $data['id_especialidad']);
		$query = $builder->update($data);
		return $query;
	}

	}

	