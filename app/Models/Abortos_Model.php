<?php namespace App\Models;
use CodeIgniter\Model;
class Abortos_Model extends BaseModel
{
	
	public function agregar($data)
	{
		
		$builder = $this->dbconn('historial_clinico.abortos');
		$query = $builder->insert($data);  
	   return $query;
	}

	
	public function actualizar($data)
	{
		$builder = $this->dbconn('historial_clinico.abortos');
		$builder->where('n_historial', $data['n_historial']);
		$query = $builder->update($data);
		return $query;
	   //return  $strQuery;
	}
	public function listar_abortos($n_historial,$id_consulta)
	{
 
	   $db      = \Config\Database::connect();
	   $strQuery ="";
	   $strQuery .="SELECT";
	   $strQuery .=" abortos.id";  
	   $strQuery .=",abortos.id_abortos_numeros_romanos "; 
	   $strQuery .=",nr.descripcion as descri_abortos "; 
	   $strQuery .="FROM ";
	   $strQuery .="  historial_clinico.abortos ";	
	   $strQuery .="  JOIN historial_clinico.numeros_romanos as nr on abortos.id_abortos_numeros_romanos=nr.id";	
	   $strQuery .=" where abortos.n_historial='$n_historial'";
	   //return  $strQuery;
	   $query = $db->query($strQuery);
	   $resultado=$query->getResult(); 
	   return $resultado;
	}
}

	