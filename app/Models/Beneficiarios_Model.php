<?php

namespace App\Models;

use CodeIgniter\Model;

class Beneficiarios_model extends BaseModel
{
	public function getAll($cedula_trabajador = null)
	{
		$db      = \Config\Database::connect();

		$strQuery = "";
		$strQuery .= "SELECT ";
		$strQuery .= "t.id";
		$strQuery .= ",t.cedula_trabajador";
		$strQuery .= ",t.nacionalidad";
		$strQuery .= ",t.nombre";
		$strQuery .= ",t.apellido";
		$strQuery .= ",t.telefono";
		$strQuery .= ",t.borrado as estatus";
		$strQuery .= ",t.sexo  as sexon";
		$strQuery .= ",CASE WHEN t.borrado='f' THEN  'ACTIVO' ELSE 'INACTIVO' END AS borrado";
		$strQuery .= ",CASE WHEN t.sexo='1' THEN  'M' ELSE 'F' END AS sexo";
		$strQuery .= ",to_char(t.fecha_nacimiento,'dd/mm/yyyy') as fecha_nacimiento";
		$strQuery .= ",t.estado_civil";
		$strQuery .= ",est.descripcion as descrip_estado_civil";
		$strQuery .= ",t.tipo_de_sangre";
		$strQuery .= ",t.tipo_de_personal";
		$strQuery .= ",t.grado_intruccion_id";
		$strQuery .= ",grado.descripcion as descrip_grado_intruccion";
		$strQuery .= ",t.ocupacion_id";
		$strQuery .= ",ocupacion.descripcion as descrip_ocupacion";
		$strQuery .= ",t.ubicacion_administrativa";
		$strQuery .= ",extract(year from (select age(CURRENT_DATE,t.fecha_nacimiento))) as edad_actual";
		$strQuery .= " FROM ";
		$strQuery .= " titulares as t";
		$strQuery .= " LEFT JOIN estado_civil as est on t.estado_civil=est.id ";
		$strQuery .= " LEFT JOIN grado_instruccion as grado on t.grado_intruccion_id=grado.id ";
		$strQuery .= " LEFT JOIN ocupacion   on t.ocupacion_id=ocupacion.id";

		//return $strQuery ;
		if ($cedula_trabajador != null) {
			$strQuery = $strQuery . " where cedula_trabajador=$cedula_trabajador";
		}
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}


	public function getAll_tipodesangre($cedula_trabajador = null)
	{
		$db      = \Config\Database::connect();

		$strQuery = "";
		$strQuery .= "SELECT ";
		$strQuery .= "t.id";
		$strQuery .= ",t.cedula_trabajador";
		$strQuery .= ",t.nacionalidad";
		$strQuery .= ",t.nombre";
		$strQuery .= ",t.apellido";
		$strQuery .= ",t.telefono";
		$strQuery .= ",t.sexo  as sexon";
		$strQuery .= ",CASE WHEN t.sexo='1' THEN  'M' ELSE 'F' END AS sexo";
		$strQuery .= ",to_char(t.fecha_nacimiento,'dd/mm/yyyy') as fecha_nacimiento";
		$strQuery .= ",t.estado_civil";
		$strQuery .= ",t.tipo_de_sangre";
		$strQuery .= ",tipo_de_sangre.descripcion as tipodesangre";
		$strQuery .= ",t.tipo_de_personal";
		$strQuery .= ",t.grado_intruccion_id";
		$strQuery .= ",t.ocupacion_id";
		$strQuery .= ",t.ubicacion_administrativa";
		$strQuery .= ",extract(year from (select age(CURRENT_DATE,t.fecha_nacimiento))) as edad_actual";
		$strQuery .= " FROM ";
		$strQuery .= " titulares as t ";
		$strQuery .= "join tipo_de_sangre on t.tipo_de_sangre=tipo_de_sangre.id";
		if ($cedula_trabajador != null) {
			$strQuery = $strQuery . " where cedula_trabajador=$cedula_trabajador";
		}
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
		//return $strQuery ;
	}


	public function getAllTitular($datos)
	{
		//var_dump($datos['cedula_trabajador']);

		$db      = \Config\Database::connect();

		$strQuery = "";
		$strQuery .= "SELECT ";

		$strQuery .= "t.id";
		$strQuery .= ",t.cedula_trabajador";
		$strQuery .= ",t.nacionalidad";
		$strQuery .= ",t.nombre";
		$strQuery .= ",t.apellido";
		$strQuery .= ",t.telefono";
		$strQuery .= ",t.sexo  as sexon";
		$strQuery .= ",CASE WHEN t.sexo='1' THEN  'M' ELSE 'F' END AS sexo";
		$strQuery .= ",to_char(t.fecha_nacimiento,'dd/mm/yyyy') as fecha_nacimiento";
		$strQuery .= ",t.estado_civil";
		$strQuery .= ",t.tipo_de_sangre";
		$strQuery .= ",t.tipo_de_personal";
		$strQuery .= ",t.grado_intruccion_id";
		$strQuery .= ",t.ocupacion_id";
		$strQuery .= ",t.ubicacion_administrativa";
		$strQuery .= ",extract(year from (select age(CURRENT_DATE,t.fecha_nacimiento))) as edad_actual";
		$strQuery .= " FROM ";
		$strQuery .= " titulares as t";
		$strQuery = $strQuery . " where cedula_trabajador=$datos[cedula_trabajador]";

		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}


	public function buscartitular_NotasEntregas($cedula_trabajador = null)
	{
		$db      = \Config\Database::connect();
		$strQuery = "";
		$strQuery .= "SELECT ";
		$strQuery .= "t.id";
		$strQuery .= ",t.cedula_trabajador";
		$strQuery .= ",t.telefono";
		$strQuery .= ",t.nacionalidad";
		$strQuery .= ",t.nombre";
		$strQuery .= ",t.apellido";
		$strQuery .= ",t.borrado";
		$strQuery .= ",t.sexo  as sexon";
		$strQuery .= ",CASE WHEN t.sexo='1' THEN  'M' ELSE 'F' END AS sexo";
		$strQuery .= ",to_char(t.fecha_nacimiento,'dd/mm/yyyy') as fecha_nacimiento";
		$strQuery .= ",t.estado_civil";
		$strQuery .= ",t.tipo_de_sangre";
		$strQuery .= ",t.tipo_de_personal";
		$strQuery .= ",t.grado_intruccion_id";
		$strQuery .= ",t.ocupacion_id";
		$strQuery .= ",t.ubicacion_administrativa";
		$strQuery .= ",extract(year from (select age(CURRENT_DATE,t.fecha_nacimiento))) as edad_actual";
		$strQuery .= ", hc.n_historial";
		$strQuery .= " FROM ";
		$strQuery .= " titulares as t ";
		$strQuery .= "join historial_clinico.historial_medico as hc on t.cedula_trabajador::text=hc.cedula";
		if ($cedula_trabajador != null) {
			$strQuery = $strQuery . " where cedula_trabajador=$cedula_trabajador";
		}
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
		//return $strQuery ;
	}


	// *********MUESTRA TODDOS LOS FAMILIARES*******************
	public function getAllFamiliares($cedula_trabajador = null)
	{
		//echo($cedula_trabajador);
		//die();
		$db      = \Config\Database::connect();
		$strQuery = "";
		$strQuery .= "SELECT";
		$strQuery .= " familiares.id ";
		$strQuery .= ",familiares.cedula_trabajador";
		$strQuery .= ",familiares.cedula";
		$strQuery .= ",familiares.nombre";
		$strQuery .= ",familiares.apellido";
		$strQuery .= ",familiares.telefono";
		$strQuery .= ",familiares.tipo_de_sangre";
		$strQuery .= ",familiares.estado_civil";
		$strQuery .= ",familiares.grado_intruccion_id";
		$strQuery .= ", CASE WHEN familiares.sexo= 1 THEN 'M' ELSE 'F' END as sexo";
		$strQuery .= ",tipo_de_sangre.descripcion as tipodesangre";
		$strQuery .= ",estado_civil.descripcion as estadocivil";
		$strQuery .= ",to_char(fecha_nac_familiares,'dd/mm/yyyy') as fecha_nac_familiares";
		$strQuery .= ",parentesco.descripcion as parentesco";
		$strQuery .= " from familiares";
		$strQuery .= " JOIN parentesco on familiares.parentesco_id=parentesco.id";
		$strQuery .= " LEFT JOIN tipo_de_sangre on familiares.tipo_de_sangre=tipo_de_sangre.id";
		$strQuery .= " LEFT JOIN estado_civil on familiares.estado_civil=estado_civil.id";
		if ($cedula_trabajador != null) {
			$strQuery = $strQuery . " where cedula_trabajador=$cedula_trabajador";
		}
		//return $strQuery;
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}

	// *********infomedicamento cortesia*******************
	public function getAllInfoMedicamentosFamiliares($cedula = null)
	{


		$db      = \Config\Database::connect();
		$strQuery = "";
		$strQuery .= "SELECT";
		$strQuery .= " familiares.id ";
		$strQuery .= ",familiares.cedula_trabajador";
		$strQuery .= ",familiares.cedula";
		$strQuery .= ",familiares.nombre";
		$strQuery .= ",familiares.apellido";
		$strQuery .= ",familiares.telefono";
		$strQuery .= ",familiares.sexo";
		$strQuery .= ",familiares.ocupacion_id";
		$strQuery .= ",ocupacion.descripcion as descrip_ocupacion";
		$strQuery .= ",grado.descripcion as descrip_grado_intruccion";
		$strQuery .= ",est.descripcion as descrip_estado_civil";
		$strQuery .= ",to_char(fecha_nac_familiares,'dd/mm/yyyy') as fecha_nac_familiares";
		$strQuery .= ",parentesco.descripcion";
		$strQuery .= " from familiares";
		$strQuery .= " LEFT JOIN parentesco on familiares.parentesco_id=parentesco.id";
		$strQuery .= " LEFT JOIN estado_civil as est on familiares.estado_civil=est.id ";
		$strQuery .= " LEFT JOIN grado_instruccion as grado on familiares.grado_intruccion_id=grado.id ";
		$strQuery .= " LEFT JOIN ocupacion   on familiares.ocupacion_id=ocupacion.id";
		if ($cedula != null) {
			$strQuery = $strQuery . " where cedula='$cedula'";
		}
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}


	// ******** VERIFICAR SI EXIXTE UN FAMILIAR EN LA TABLA PARA AGREGARLO *******************
	public function Buscar_Familiares_exixtente($cedula = null)
	{


		$db      = \Config\Database::connect();
		$strQuery = "";
		$strQuery .= "SELECT";
		$strQuery .= " familiares.id ";
		$strQuery .= ",familiares.cedula_trabajador";
		$strQuery .= ",familiares.cedula";
		$strQuery .= ",familiares.nombre";
		$strQuery .= ",familiares.apellido";
		$strQuery .= ",familiares.telefono";
		$strQuery .= ",familiares.sexo";
		$strQuery .= ",to_char(fecha_nac_familiares,'dd/mm/yyyy') as fecha_nac_familiares";
		$strQuery .= ",parentesco.descripcion";
		$strQuery .= " from familiares";
		$strQuery .= " JOIN parentesco on familiares.parentesco_id=parentesco.id";
		$strQuery = $strQuery . " where cedula='$cedula'";
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}


	// *********infomedicamnto cortesia*******************
	public function getAllInfoMedicamentosCortesia($cedula = null)
	{


		$db      = \Config\Database::connect();
		$strQuery = "";
		$strQuery .= "SELECT";
		$strQuery .= " cortesia.id ";
		$strQuery .= ",cortesia.cedula_trabajador";
		$strQuery .= ",cortesia.cedula";
		$strQuery .= ",cortesia.nombre";
		$strQuery .= ",cortesia.apellido";
		$strQuery .= ",cortesia.telefono";
		$strQuery .= ",cortesia.sexo";
		$strQuery .= ",ocupacion.descripcion as descrip_ocupacion";
		$strQuery .= ",grado.descripcion as descrip_grado_intruccion";
		$strQuery .= ",est.descripcion as descrip_estado_civil";
		$strQuery .= ",to_char(fecha_nac_cortesia,'dd/mm/yyyy') as fecha_nac_cortesia";
		$strQuery .= " from cortesia";
		$strQuery .= " LEFT JOIN estado_civil as est on cortesia.estado_civil=est.id ";
		$strQuery .= " LEFT JOIN grado_instruccion as grado on cortesia.grado_intruccion_id=grado.id ";
		$strQuery .= " LEFT  JOIN ocupacion   on cortesia.ocupacion_id=ocupacion.id";
		if ($cedula != null) {
			$strQuery = $strQuery . " where cedula='$cedula'";
		}
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}



	// *********MUESTRA LOS FAMILIARES EN FUNSION DE LA CEDULA *******************
	public function getFamiliar($cedula = null)
	{

		//echo($cedula);
		//die();
		$db      = \Config\Database::connect();
		$strQuery = "";
		$strQuery .= "SELECT";
		$strQuery .= " familiares.id ";
		$strQuery .= ",familiares.cedula_trabajador";
		$strQuery .= ",familiares.cedula";
		$strQuery .= ",familiares.nombre";
		$strQuery .= ",familiares.apellido";
		$strQuery .= ",familiares.telefono";
		$strQuery .= ",familiares.borrado";
		$strQuery .= ", CASE WHEN familiares.sexo= 1 THEN 'M' ELSE 'F' END as sexo";
		$strQuery .= ", familiares.sexo as id_sexo";
		$strQuery .= ",familiares.borrado";
		$strQuery .= ",to_char(fecha_nac_familiares,'dd/mm/yyyy') as fecha_nac_familiares";
		$strQuery .= ",familiares.parentesco_id";
		$strQuery .= ",familiares.tipo_de_sangre";
		$strQuery .= ",tipo_de_sangre.descripcion as tipodesangre";
		$strQuery .= ",familiares.grado_intruccion_id";
		$strQuery .= ",familiares.ocupacion_id";
		$strQuery .= ",familiares.estado_civil";
		$strQuery .= ",titulares.nombre as nombre_titular";
		$strQuery .= ",titulares.apellido as apellido_titular";
		$strQuery .= ",titulares.ubicacion_administrativa as departamento";
		$strQuery .= ",titulares.sexo  as sexon";
		$strQuery .= ",titulares.telefono  as telefonot";
		$strQuery .= ",CASE WHEN titulares.sexo='1' THEN  'M' ELSE 'F' END AS sexot";
		$strQuery .= ",titulares.tipo_de_personal ";
		$strQuery .= ",to_char(titulares.fecha_nacimiento,'dd/mm/yyyy') as fecha_nacimientot";
		$strQuery .= ",parentesco.descripcion as descripcion_parentesco ";
		$strQuery .= ",ocupacion.descripcion as ocupacion ";
		$strQuery .= ",extract(year from (select age(CURRENT_DATE,titulares.fecha_nacimiento))) as edad_actualt";
		$strQuery .= ",extract(year from (select age(CURRENT_DATE,familiares.fecha_nac_familiares))) as edad_actual";
		$strQuery .= " from familiares";
		$strQuery .= " JOIN titulares on familiares.cedula_trabajador=titulares.cedula_trabajador";
		$strQuery .= " JOIN parentesco on familiares.parentesco_id=parentesco.id";
		$strQuery .= " LEFT JOIN tipo_de_sangre on familiares.tipo_de_sangre=tipo_de_sangre.id";
		$strQuery .= " LEFT JOIN ocupacion on familiares.ocupacion_id=ocupacion.id";

		if ($cedula != null) {
			$strQuery = $strQuery . " where cedula='$cedula'";
		}
		//return $strQuery;
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}



	// *********MUESTRA LOS FAMILIARES EN FUNSION DE LA CEDULA *******************
	public function  buscarfamiliar_NotasEntregas($cedula = null)
	{

		//echo($cedula);
		//die();
		$db      = \Config\Database::connect();
		$strQuery = "";
		$strQuery .= "SELECT";
		$strQuery .= " familiares.id ";
		$strQuery .= ",familiares.cedula_trabajador";
		$strQuery .= ",familiares.cedula";
		$strQuery .= ",familiares.nombre";
		$strQuery .= ",familiares.apellido";
		$strQuery .= ",familiares.telefono";
		$strQuery .= ",familiares.sexo";
		$strQuery .= ", CASE WHEN familiares.sexo= 1 THEN 'M' ELSE 'F' END as sexof";
		$strQuery .= ",familiares.borrado";
		$strQuery .= ",to_char(fecha_nac_familiares,'dd/mm/yyyy') as fecha_nac_familiares";
		$strQuery .= ",familiares.parentesco_id";
		$strQuery .= ",parentesco.descripcion as descripcionparentesco";
		$strQuery .= ",familiares.tipo_de_sangre";
		$strQuery .= ",familiares.grado_intruccion_id";
		$strQuery .= ",familiares.ocupacion_id";
		$strQuery .= ",familiares.estado_civil";
		$strQuery .= ",titulares.cedula_trabajador";
		$strQuery .= ",titulares.telefono as telefonot";
		$strQuery .= ",titulares.nombre as nombre_titular";
		$strQuery .= ",titulares.apellido as apellido_titular";
		$strQuery .= ",CASE WHEN titulares.sexo='1' THEN  'M' ELSE 'F' END AS sexot";
		$strQuery .= ",titulares.ubicacion_administrativa as departamento";
		$strQuery .= ",to_char(titulares.fecha_nacimiento,'dd/mm/yyyy') as fecha_nacimientot";
		$strQuery .= ",extract(year from (select age(CURRENT_DATE,titulares.fecha_nacimiento))) as edad_actualt";
		$strQuery .= ",extract(year from (select age(CURRENT_DATE,familiares.fecha_nac_familiares))) as edad_actual";
		$strQuery .= " ,hc.n_historial";
		$strQuery .= " from familiares";
		$strQuery .= " JOIN titulares on familiares.cedula_trabajador=titulares.cedula_trabajador";
		$strQuery .= " JOIN parentesco on familiares.parentesco_id=parentesco.id";
		$strQuery .= " join historial_clinico.historial_medico as hc on familiares.cedula=hc.cedula";

		if ($cedula != null) {
			$strQuery = $strQuery . " where familiares.cedula='$cedula'";
		}
		//return $strQuery;
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}





	// ****BUSCA LOS BENEFICIARIOS DE CORTESIA EN FUNCION DE LA CEDULA DEL TRABAJADOR**
	public function getAllCortesia($cedula_trabajador = null)
	{

		$db      = \Config\Database::connect();
		$strQuery = "";
		$strQuery .= "SELECT";
		$strQuery .= " cortesia.id";
		$strQuery .= ",cortesia.cedula_trabajador";
		$strQuery .= ",cortesia.cedula";
		$strQuery .= ",cortesia.nacionalidad";
		$strQuery .= ",cortesia.adscrito";
		$strQuery .= ",cortesia.nombre";
		$strQuery .= ",cortesia.apellido";
		$strQuery .= ",cortesia.telefono";
		$strQuery .= ",cortesia_adscrito.id_adscrito ";
		$strQuery .= ",cortesia.tipo_de_sangre";
		$strQuery .= ",cortesia.estado_civil";
		$strQuery .= ",cortesia.grado_intruccion_id";
		$strQuery .= ",cortesia.ocupacion_id";
		$strQuery .= ",CASE WHEN sexo='1' THEN  'M' WHEN sexo='2' THEN 'F' END AS sexo";
		$strQuery .= ",to_char(cortesia.fecha_nac_cortesia,'dd/mm/yyyy') as fecha_nac_cortesia";
		$strQuery .= ",cortesia.observacion";
		$strQuery .= " from cortesia";
		$strQuery .= " left join cortesia_adscrito on cortesia.id=cortesia_adscrito.id_cortesia ";

		if ($cedula_trabajador != null) {
			$strQuery = $strQuery . " where cedula_trabajador=$cedula_trabajador";
		}
		//return($strQuery);
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}

	// *********MUESTRA LA PERSONA DE CORTESIA  EN FUNSION DE LA CEDULA PARA EDITAR  *******************
	public function getCortersia($cedula = null)
	{

		$db      = \Config\Database::connect();
		$strQuery = "";
		$strQuery .= "SELECT";
		$strQuery .= " cortesia.id ";
		$strQuery .= ",cortesia.cedula_trabajador";
		$strQuery .= ",cortesia.nacionalidad";
		$strQuery .= ",cortesia.cedula";
		$strQuery .= ",cortesia.nombre";
		$strQuery .= ",cortesia.adscrito";
		$strQuery .= ",cortesia.apellido";
		$strQuery .= ",cortesia_adscrito.id_adscrito ";
		$strQuery .= ",cortesia.borrado";
		$strQuery .= ",cortesia.telefono";
		$strQuery .= ",cortesia.sexo ";
		$strQuery .= ",CASE WHEN cortesia.sexo='1' THEN  'M' ELSE 'F' END AS sexo";
		$strQuery .= ",to_char(cortesia.fecha_nac_cortesia,'dd/mm/yyyy') as fecha_nac_cortesia";
		$strQuery .= ",cortesia.observacion";
		$strQuery .= ",cortesia.tipo_de_sangre";
		$strQuery .= ",tipo_de_sangre.descripcion as tipodesangre";
		$strQuery .= ",cortesia.grado_intruccion_id";
		$strQuery .= ",cortesia.ocupacion_id";
		$strQuery .= ",cortesia.estado_civil";
		$strQuery .= ",extract(year from (select age(CURRENT_DATE,cortesia.fecha_nac_cortesia))) as edad_actual";
		$strQuery .= ",titulares.nombre as nombre_titular";
		$strQuery .= ",titulares.apellido as apellido_titular";
		$strQuery .= ",titulares.ubicacion_administrativa as departamento";
		$strQuery .= ",titulares.ubicacion_administrativa ";
		$strQuery .= ",titulares.sexo  as sexon";
		$strQuery .= ",titulares.telefono  as telefonot";
		$strQuery .= ",ocupacion.descripcion  as descripcion_ocupacion";
		$strQuery .= ",CASE WHEN titulares.sexo='1' THEN  'M' ELSE 'F' END AS sexot";
		$strQuery .= ",titulares.tipo_de_personal ";
		$strQuery .= ",to_char(titulares.fecha_nacimiento,'dd/mm/yyyy') as fecha_nacimientot";
		$strQuery .= ",extract(year from (select age(CURRENT_DATE,titulares.fecha_nacimiento))) as edad_actualt";
		$strQuery .= " from cortesia";
		$strQuery .= " LEFT JOIN tipo_de_sangre on cortesia.tipo_de_sangre=tipo_de_sangre.id";
		$strQuery .= " LEFT JOIN titulares on cortesia.cedula_trabajador=titulares.cedula_trabajador";
		$strQuery .= " LEFT JOIN ocupacion on cortesia.ocupacion_id=ocupacion.id";
		$strQuery .= " LEFT join cortesia_adscrito on cortesia.id=cortesia_adscrito.id_cortesia ";


		if ($cedula != null) {
			$strQuery = $strQuery . " where cedula='$cedula'";
		}
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}


	// *********MUESTRA LA PERSONA DE CORTESIA  EN FUNSION DE LA CEDULA   *******************
	public function 	 buscarcortesia_NotasEntregas($cedula = null)
	{

		$db      = \Config\Database::connect();
		$strQuery = "";
		$strQuery .= "SELECT";
		$strQuery .= " cortesia.id ";
		$strQuery .= ",cortesia.cedula_trabajador";
		$strQuery .= ",cortesia.nacionalidad";
		$strQuery .= ",cortesia.cedula";
		$strQuery .= ",cortesia.nombre";
		$strQuery .= ",cortesia.apellido";
		$strQuery .= ",cortesia.telefono";
		$strQuery .= ",cortesia.borrado";
		$strQuery .= ",CASE WHEN cortesia.sexo='1' THEN  'M' ELSE 'F' END AS sexo";
		$strQuery .= ",to_char(cortesia.fecha_nac_cortesia,'dd/mm/yyyy') as fecha_nac_cortesia";
		$strQuery .= ",cortesia.observacion";
		$strQuery .= ",cortesia.tipo_de_sangre";
		$strQuery .= ",cortesia.grado_intruccion_id";
		$strQuery .= ",cortesia.ocupacion_id";
		$strQuery .= ",cortesia.estado_civil";
		$strQuery .= ",extract(year from (select age(CURRENT_DATE,cortesia.fecha_nac_cortesia))) as edad_actual";
		$strQuery .= ",titulares.cedula_trabajador";
		$strQuery .= ",titulares.telefono as telefonot";
		$strQuery .= ",titulares.nombre as nombre_titular";
		$strQuery .= ",titulares.apellido as apellido_titular";
		$strQuery .= ",titulares.ubicacion_administrativa as departamento";
		$strQuery .= ",CASE WHEN titulares.sexo='1' THEN  'M' ELSE 'F' END AS sexot";
		$strQuery .= ",to_char(titulares.fecha_nacimiento,'dd/mm/yyyy') as fecha_nacimientot";
		$strQuery .= ",extract(year from (select age(CURRENT_DATE,titulares.fecha_nacimiento))) as edad_actualt";
		$strQuery .= ",hc.n_historial";
		$strQuery .= ",ente.descripcion";
		$strQuery .= " from cortesia";
		$strQuery .= " JOIN titulares on cortesia.cedula_trabajador=titulares.cedula_trabajador";
		$strQuery .= " join historial_clinico.historial_medico as hc on cortesia.cedula=hc.cedula";
		$strQuery .= " left join cortesia_adscrito as ca on cortesia.id= ca.id_cortesia";
		$strQuery .= " left join ente_adscrito as ente on ca.id_adscrito=ente.id  ";
		

		if ($cedula != null) {
			$strQuery = $strQuery . " where cortesia.cedula='$cedula'";
		}

		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}

	public function agregar($data)
	{
		$builder = $this->dbconn('public.titulares');
		$query = $builder->insert($data);
		return $query;
	}
	public function agregar_Familiares($data)
	{

		$builder = $this->dbconn('public.familiares');
		$query = $builder->insert($data);
		return $query;
	}

	public function agregar_Cortesia($data)
	{


		$builder = $this->dbconn('public.cortesia');
		$query = $builder->insert($data);


		return $query;
	}
	public function actualizar_titular($data)
	{


		$builder = $this->dbconn('public.titulares');
		$builder->where('cedula_trabajador', $data['cedula_trabajador']);
		$query = $builder->update($data);
		return $query;
	}

	public function cambio_de_titular_familiar($datos_update)
	{
		
		$builder = $this->dbconn('public.familiares');
		$builder->where('cedula', $datos_update['cedula']  );
		$query = $builder->update($datos_update);
		return $query;
	}








	public function actualizar_Cortesia($data, $cedula_anterior)
	{
		$builder = $this->dbconn('public.cortesia');
		$builder->where('cedula', $cedula_anterior);
		$query = $builder->update($data);
		return $query;
	}
	public function actualizar_familiar($datos_beneficiario, $cedula_anterior)
	{


		$builder = $this->dbconn('public.familiares');
		$builder->where('cedula', $cedula_anterior);
		$query = $builder->update($datos_beneficiario);
		return $query;
	}



	public function actualizar_EstatusCortesia($data)
	{

		$builder = $this->dbconn('public.cortesia');
		$builder->where('cedula_trabajador', $data['cedula_trabajador']);
		$query = $builder->update($data);
		return $query;
	}

	public function actualizar_Estatusfamiliar($data)
	{
		$builder = $this->dbconn('public.familiares');
		$builder->where('cedula_trabajador', $data['cedula_trabajador']);
		$query = $builder->update($data);
		return $query;
	}
	public function getAllParentesco()
	{
		$builder = $this->dbconn('public.parentesco as p');
		$builder->select(
			"p.id
	       ,p.descripcion"
		);
		$query = $builder->get();
		return $query;
	}

	public function  getAllTipodeSangre()
	{
		$builder = $this->dbconn('public.tipo_de_sangre as t');
		$builder->select(
			"t.id
	       ,t.descripcion"
		);
		$query = $builder->get();
		return $query;
	}


	public function  getAllEstadoCivil()
	{
		$builder = $this->dbconn('public.estado_civil as e');
		$builder->select(
			"e.id
	       ,e.descripcion"
		);
		$query = $builder->get();
		return $query;
	}

	public function  getAllGrado_Instruccion()
	{
		$builder = $this->dbconn('public.grado_instruccion as g');
		$builder->select(
			"g.id
	       ,g.descripcion"
		);
		$query = $builder->get();
		return $query;
	}

	public function  getAllOcupacion()
	{
		$builder = $this->dbconn('public.ocupacion as o');
		$builder->select(
			"o.id
	       ,o.descripcion"
		);
		$query = $builder->get();
		return $query;
	}

	// *********MUESTRA LA PERSONA DE CORTESIA  EN FUNSION DE LA CEDULA PARA EDITAR  *******************
	public function buscar_id_cortesia()
	{

		$builder = $this->dbconn('public.cortesia');
		$builder->selectMax('id');
		$query = $builder->get();
		return $query;

	}

	//METODO QUE BUSCA EL NOMBRE Y EL APELLIDO DEL BENEFICIARIO EN FUNCION DEL NUMERO DE HISTORIAL

	public function buscar_datos_beneficiarios(string $cedula_string)
	{
		
		$db      = \Config\Database::connect();
		$strQuery = "";
		$strQuery .= "SELECT ";
		$strQuery .= " CONCAT(b.nombre,' ', b.apellido) AS nombre";
		$strQuery .= " FROM vista_beneficiarios as b ";
		$strQuery .= " where b.cedula='$cedula_string' ";
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}







}
