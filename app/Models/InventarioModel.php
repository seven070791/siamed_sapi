<?php

namespace App\Models;

use CodeIgniter\Model;

class InventarioModel extends Model
{
    protected $table = 'medicamentos';
    
    protected $allowedFields = [ 'id','nombre','laboratorio','stock','descripcion','echa_entrada','fecha_vencimiento'];
    public function insertar_registro($dato)
    {
      return  $this->save($dato);
    }
      public function mostrar_productos($slug = false)
      {
          if ($slug === false) 
          {
              // return $this->where(['borrado'=>false])->findAll(); 
  
              return $this->findAll(); 
          }
          return $this->where(['pro_id'=>$slug])->first();
          
      }

      public function actualizar_entrada2($datos)
      {
          $db      = \Config\Database::connect();
          $builder = $db->table('productos');
         
           $builder->where('pro_id',$datos['pro_id']);
           $builder->update($datos);
               
      }
      public function actualizar_salida2($datos)
      {
          $db      = \Config\Database::connect();
          $builder = $db->table('productos');
         
         
           $data = 
           [ 
           'pro_id'=>$datos['pro_id'],
            'pro_stock'=>$datos['pro_stock'],
           
         ];
           $builder->where('pro_id',$datos['pro_id']);
           $builder->update($data);
               
      }
 }

