<?php namespace App\Models;
use CodeIgniter\Model;
class Numeros_Romanos_Model extends BaseModel
{
	
	 public function Listar_Numeros_Romanos()
     {
	  $builder = $this->dbconn(' historial_clinico.numeros_romanos as nr');
	  $builder->select
	  (
		" nr.id
		,nr.descripcion"
	  );
	  $builder->where(['nr.borrado'=>false]);
	  $query = $builder->get();
	  return $query;	
     }
	

	}

	