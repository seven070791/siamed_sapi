<?php namespace App\Models;
use CodeIgniter\Model;
class Paraclinicos_Model extends BaseModel
{

	
    public function agregar_paraclinicos($data)
	{
		 $builder = $this->dbconn('historial_clinico.paraclinicos');
		 $query = $builder->insert($data);  
		return $query;
    }
 
	public function buscar_consulta($id_consulta)
	{
	   //$builder = $this->dbconn('historial_clinico.consultas as hc');
	   $db      = \Config\Database::connect();
	   $strQuery ="";
	   $strQuery .="SELECT";
	   $strQuery .=" p.id";  
	   $strQuery .=",p.descripcion "; 
	   $strQuery .="FROM ";
	   $strQuery .=" historial_clinico.paraclinicos as p  ";	
	   $strQuery  =$strQuery . " where p.id_consulta='$id_consulta'";
	   $query = $db->query($strQuery);
	   $resultado=$query->getResult(); 
	   return $resultado;
	   //return  $strQuery;
	}

	public function listar_paraclinicos($n_historial)
	{
 
	   //$builder = $this->dbconn('historial_clinico.consultas as hc');
	   $db      = \Config\Database::connect();
	   $strQuery ="";
	   $strQuery .="SELECT";
	   $strQuery .=" distinct hc.id_medico"; 
	   $strQuery .=",p.id";  
	   $strQuery .=",p.descripcion "; 
	   $strQuery .=",to_char(p.fecha_creacion,'dd/mm/yyyy') as fecha_creacion "; 
	   $strQuery .=",to_char(p.fecha_actualizacion,'dd/mm/yyyy') as fecha_actualizacion "; 
	   $strQuery .=",CONCAT(m.nombre,' ', m.apellido) AS nombre ";
	   $strQuery .=",e.descripcion as especialidad ";
	   $strQuery .="FROM ";
	   $strQuery .="  historial_clinico.paraclinicos as p ";	
	   $strQuery .="  join historial_clinico.consultas  as hc on p.id_consulta=hc.id";
	   $strQuery .="  join  historial_clinico.medicos as m on hc.id_medico=m.id";
	   $strQuery .="  join historial_clinico.especialidades as e on m.especialidad=e.id_especialidad ";
	   $strQuery  =$strQuery . " where p.n_historial='$n_historial'";
	   $query = $db->query($strQuery);
	   $resultado=$query->getResult(); 
	   return $resultado;
	   //return  $strQuery;
	}
	public function listar_Paraclinicos_Individual($n_historial,$id_consulta)
	{
 
	   //$builder = $this->dbconn('historial_clinico.consultas as hc');
	   $db      = \Config\Database::connect();
	   $strQuery ="";
	   $strQuery .="SELECT";
	   $strQuery .=" distinct hc.id_medico"; 
	   $strQuery .=",p.id";  
	   $strQuery .=",p.descripcion "; 
	   $strQuery .=",to_char(p.fecha_creacion,'dd/mm/yyyy') as fecha_creacion "; 
	   $strQuery .=",to_char(p.fecha_actualizacion,'dd/mm/yyyy') as fecha_actualizacion "; 
	   $strQuery .=",CONCAT(m.nombre,' ', m.apellido) AS nombre ";
	   $strQuery .=",e.descripcion as especialidad ";
	   $strQuery .="FROM ";
	   $strQuery .="  historial_clinico.paraclinicos as p ";	
	   $strQuery .="  join historial_clinico.consultas  as hc on p.id_consulta=hc.id";
	   $strQuery .="  join  historial_clinico.medicos as m on hc.id_medico=m.id";
	   $strQuery .="  join historial_clinico.especialidades as e on m.especialidad=e.id_especialidad ";
	   $strQuery  =$strQuery . " where p.n_historial='$n_historial'";
	   $strQuery .=" and hc.id=$id_consulta";
	   $strQuery .=" and p.id_consulta=$id_consulta";
	   $query = $db->query($strQuery);
	   $resultado=$query->getResult(); 
	   return $resultado;
	   //return  $strQuery;
	}


	


	public function actualizar_paraclinicos($data)
	{
		$builder = $this->dbconn('historial_clinico.paraclinicos');
		$builder->where('id', $data['id']);
		$query = $builder->update($data);
		return $query;
	   //return  $strQuery;
	}


}

