<?php

namespace App\Models;

use CodeIgniter\Model;

class Detalles_Nota_Entrega_Model extends BaseModel
{
	public function agregar($datos_detalle_notaentrega, $contDetalles)
	{
		$builder = $this->dbconn('historial_clinico.detalle_notas_entrega');

		$contDetallesPost = 0;
		foreach ($datos_detalle_notaentrega as $detalle) {
			$arrDetalle = [];
			$arrDetalle['nota_entrega_id'] = $datos_detalle_notaentrega[$contDetallesPost]['nota_entrega_id'];
			$arrDetalle['salida_id'] = $datos_detalle_notaentrega[$contDetallesPost]['salida_id'];
			$query = $builder->insert($arrDetalle);
			$contDetallesPost++;
		}
		/*
		$query = $builder->insert($datos_detalle_notaentrega);
		*/
		//$query = var_dump($datos_detalle_notaentrega);
		return $query;
	}

	public function buscar_salida_en_detalle_notas($id_salida = null)
	{

		$db = \Config\Database::connect();
		$strQuery = "select ";
		$strQuery .= "d_notas.salida_id ";
		$strQuery .= "FROM  historial_clinico.detalle_notas_entrega d_notas";
		$strQuery .= " WHERE d_notas.salida_id=$id_salida";
		//return $strQuery;
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}

	public function buscar_detalle_nota_entrega($datos)
	{

		$db = \Config\Database::connect();
		$strQuery = "select ";
		$strQuery .= "d_notas.salida_id ";
		$strQuery .= "FROM  historial_clinico.detalle_notas_entrega d_notas";
		$strQuery .= " WHERE d_notas.nota_entrega_id=$datos[id]";
		//return $strQuery;
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}



	// public function borrar_detalle_nota_entrega($datos)
	// {
		
	// 	$db      = \Config\Database::connect();
	// 	$builder = $db->table('historial_clinico.detalle_notas_entrega as notas');
	// 	$data =
	// 		[
	// 			'borrado' => $datos['borrado'],
	// 		];
	// 	$builder->where('notas.nota_entrega_id', $datos['id']);
	// 	$builder->update($data);
	// }

	public function borrar_detalle_nota_entrega($datos)

{

    $db      = \Config\Database::connect();
    $builder = $db->table('historial_clinico.detalle_notas_entrega as notas');
    $data =

        [
            'borrado' => $datos['borrado'],

        ];
    $builder->where('notas.nota_entrega_id', $datos['id']);
    $resultado = $builder->update($data);
    if ($resultado === false) {
        // Aquí puedes manejar el error, por ejemplo, mostrando un mensaje de error
        return false;
    }
    return true;

}
}
