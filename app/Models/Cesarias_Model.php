<?php namespace App\Models;
use CodeIgniter\Model;
class Cesarias_Model extends BaseModel
{
	
	public function agregar($data)
	{
		//var_dump($data);
		//die();
		$builder = $this->dbconn('historial_clinico.cesarias');
		$query = $builder->insert($data);  
	   return $query;
	}

	
	public function actualizar($data)
	{
		$builder = $this->dbconn('historial_clinico.cesarias');
		$builder->where('n_historial', $data['n_historial']);
		$query = $builder->update($data);
		return $query;
	   //return  $strQuery;
	}

	public function listar_cesarias($n_historial,$id_consulta)
	{
 
	   $db      = \Config\Database::connect();
	   $strQuery ="";
	   $strQuery .="SELECT";
	   $strQuery .=" cesarias.id";  
	   $strQuery .=",cesarias.id_cesarias_numeros_romanos "; 
	   $strQuery .=",nr.descripcion as descri_cesarias "; 
	   $strQuery .="FROM ";
	   $strQuery .="  historial_clinico.cesarias ";	
	   $strQuery .="  JOIN historial_clinico.numeros_romanos as nr on cesarias.id_cesarias_numeros_romanos=nr.id";	
	   $strQuery .=" where cesarias.n_historial='$n_historial'";
	   $query = $db->query($strQuery);
	   $resultado=$query->getResult(); 
	   return $resultado;
	}
}

	