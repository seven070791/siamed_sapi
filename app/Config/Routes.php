<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}



/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Login');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);
/*
 * --------------------------------------------------------------------
 * RUTAS DE MENUS
 * --------------------------------------------------------------------
 */



if (!session('nombreUsuario') === NULL) {
    return redirect()->to(base_url() . '/index.php');
} else {
    $routes->get('/', 'Login::index');
    $routes->get('/admin_menu', 'Home::admin_menu');
    $routes->get('/mantenimiento', 'Home::mantenimiento');
    $routes->get('/footer', 'Home::footer');
    $routes->get('/header', 'Home::header');
    $routes->post('/special_menu', 'Home::special_menu');
    $routes->get('/user_menu', 'Home::user_menu');
    $routes->get('/menucontenedor', 'Home::menucontenedor');
    $routes->get('/supermenu', 'Home::supermenu/');
    $routes->get('/imagenCentral', 'Home::imagenCentral/');
    $routes->get('/menu_administrador', 'Home::menu_administrador/');
    $routes->get('/menu_farmacia', 'Home::menu_farmacia/');
    $routes->get('/menu', 'Home::menu/');
    $routes->get('/admin_template', 'Home::admin_template/');
    $routes->get('/login', 'Home::login');
    $routes->get('/pantalla', 'Home::pantalla');
    $routes->get('/pantalla2', 'Home::pantalla2');
    $routes->post('/pantalla2', 'Home::pantalla2');
    // ******************************HASTA AQUI EL MENU************************
    $routes->get('/mensaje0', 'Home::mensaje0');
    $routes->get('/mensaje', 'Home::mensaje');
    $routes->get('/mensaje2', 'Home::mensaje2');
    $routes->get('/datosuser', 'Home::datosuser');
    $routes->get('/datosuser2', 'Home::datosuser2');
    $routes->get('/create', 'Home::create');
    $routes->post('/create', 'Home::create');
    $routes->get('/vistagrupos', 'Home::vistagrupos');
    $routes->get('/crear2', 'Home::crear2');
    $routes->post('/crear2', 'Home::crear2');
    $routes->post('/envioPost', 'Prueba::freddy2');
    $routes->get('/edituser', 'Home::edituser');
    $routes->get('/edituser2', 'Home::edituser2');
    /*
     * --------------------------------------------------------------------
     * RUTAS DEL CONTROLADOR USER_CONTROLLERS
     * --------------------------------------------------------------------
     */



     $routes->get('/listar_usuarios', 'User_controllers::listar_usuarios');
     $routes->get('/listar_nivel_usuario', 'User_controllers::listar_nivel_usuario');
     $routes->post('/ActualizarUsuario', 'User_controllers::ActualizarUsuario');
     $routes->get('/forget', "User_controllers::recuperarPass");
     $routes->post('/sendEmail',"Login::sendRecoverEmail");
     $routes->get('/confirmRecover/(:any)', "Login::settingNewPass/$1");

     $routes->post('/recoverPassword', 'User_controllers::recoverPassword');

    $routes->match(['get', 'post'], 'User_controllers/create', 'User_controllers::create');
    $routes->match(['get', 'post'], 'User_controllers/save1', 'User_controllers::save1');
    $routes->match(['get', 'post'], 'User_controllers/delete1', 'User_controllers::delete1');
    $routes->get('User_controllers/guardar/(:int)', 'User_controllers/save1/$1');
    $routes->get('User_controllers/eliminar/(:any)/(:any)', 'User_controllers::delete/$1/$2');
    $routes->get('User_controllers/(:segment)', 'User_controllers::view/$1');
    $routes->get('User_controllers', 'User_controllers::index');
    $routes->post('User_controllers', 'User_controllers::index');
    $routes->get('tipo', 'User_controllers::getTipo');

    // ********************************Grupo de usuarios***********************************
    // ***********AQUI LE PASO EL ID  AL CONTROLADOR Grupousuarios/editgrupo1**********
    $routes->get('Grupousuarios/editgrupo1/(:int)', 'Grupousuario/editgrupo1/$1');
    /*
     * --------------------------------------------------------------------
     * RUTAS DEL CONTROLADOR GRUPOUSUARIO
     * --------------------------------------------------------------------
     */
    $routes->post('Grupousuario/savegrupo1', 'Grupousuario::savegrupo1');
    $routes->get('Grupousuario', 'Grupousuario::index');
    $routes->match(['get', 'post'], 'Grupousuario/crear', 'Grupousuario::crear');
    $routes->get('/creargrupo', 'Home::creargrupo');
    $routes->get('/editargrupo', 'Home::editargrupo');
    /*
     * --------------------------------------------------------------------
     * RUTAS DE PRODUCTOS O INVENTARIO
     * --------------------------------------------------------------------
     */
    $routes->get('Inventario', 'Inventario::index');
    $routes->match(['get', 'post'], 'Inventario/crear_registro', 'Inventario::crear_registro');
    $routes->match(['get', 'post'], '/entrada_inventario', 'Inventario::entrada_inventario');
    $routes->match(['get', 'post'], '/salida_inventario', 'Inventario::salida_inventario');
    $routes->get('Inventario/entrada_producto/(:int)', 'Inventario/entrada_producto/$1');
    // **********************Metodo Nuevo*******************
    $routes->get('añadir_entrada/', 'Inventario::añadir_entrada');
    $routes->get('detalles_entrada_pro/(:any)', 'Inventario::detalles_entrada_prod/$1');
    $routes->get('Inventario/detalles_entrada_prod/(:int)', 'Inventario::detalles_entrada_prod/$1');
    // ***************************************************************************
    $routes->post('Inventario/actualizar_entrada/(:int)', 'Inventario::actualizar_entrada/$1');
    //  $routes->post('Inventario/detalles_entrada_prod/(:int)', 'Inventario::detalles_entrada_prod/$1');
    $routes->post('Inventario/detalles_salida_prod/(:int)', 'Inventario::detalles_salida_prod/$1');
    $routes->get('/registro', 'Inventario::registro');
    $routes->get('/vistaproductos', 'Inventario::vistaproductos');
    $routes->get('/actualizar_inventario', 'Inventario::actualizar_inventario');
    $routes->get('/prueba1', 'Inventario::prueba1');
    $routes->get('/prueba2', 'Home::prueba2');
    $routes->get('/prueba3', 'Home::prueba3');
    /*
     * --------------------------------------------------------------------
     * RUTAS DEL CONTROLADOR AUDITORIA CONTROLLERS
     * --------------------------------------------------------------------
     */
    $routes->get('/AuditoriaController', 'AuditoriaController::index');
    //------------------------------------------------------------------------
    /*
     * --------------------------------------------------------------------
     * RUTAS DEL CONTROLADOR BENEFICIARIOS MODULO (TITULARES)
     * --------------------------------------------------------------------
     */
    $routes->get('/titulares', 'Beneficiarios_Controler::titulares');
    $routes->post('/agregar_titulares', 'Beneficiarios_Controler::agregar');
    $routes->GET('/listar_titulares', 'Beneficiarios_Controler::getAll');
    $routes->get('/editar_titular/(:any)', 'Beneficiarios_Controler::editar_titular/$1');
    $routes->post('/actualizar_titular', 'Beneficiarios_Controler::actualizar_titular');
    $routes->GET('/info_medicamentos_titulares/(:any)', 'Beneficiarios_Controler::info_medicamentos_titulares/$1');
    $routes->GET('/info_medicamentos_familiares/(:any)/(:any)', 'Beneficiarios_Controler::info_medicamentos_familiares/$1/$2');
    $routes->GET('/info_medicamentos_cortesia/(:any)/(:any)', 'Beneficiarios_Controler::info_medicamentos_cortesia/$1/$2');
    // *********ESTE METODO SE USA PARA PASAR LOS DATOS DEL TITULAR EN MODAL DE RETIRAR**********
    $routes->get('/buscartitular', 'Beneficiarios_Controler::getAllTitulares');
    /*
     * --------------------------------------------------------------------
     * RUTAS DEL CONTROLADOR BENEFICIARIOS MODULO (FAMILIARES)
     * --------------------------------------------------------------------
     */
    $routes->get('/listar_Familiares/(:any)', 'Beneficiarios_Controler::listar_Familiares/$1');
    $routes->get('/buscar_Familiares/(:any)', 'Beneficiarios_Controler::getAllFamiliares/$1');
    $routes->post('/agregar_Familiares', 'Beneficiarios_Controler::agregar_Familiares');
    $routes->get('/editar_Familiar/(:any)', 'Beneficiarios_Controler::editar_Familiar/$1');
    $routes->post('/actualizar_familiar/(:any)', 'Beneficiarios_Controler::actualizar_familiar/$1');
    // *********ESTE METODO SE USA PARA PASAR LOS DATOS DEL TITULAR EN MODAL DE RETIRAR**********
    $routes->get('/buscarfamiliar', 'Beneficiarios_Controler::getAllfamiliar');
    $routes->get('/listar_Grado_Instruccion', 'Beneficiarios_Controler::listar_Grado_Instruccion');
    $routes->get('/listar_Parentesco', 'Beneficiarios_Controler::listar_Parentesco');
    $routes->get('/listar_tipo_de_sangre', 'Beneficiarios_Controler::listar_tipo_de_sangre');
    $routes->get('/listar_estado_civil', 'Beneficiarios_Controler::listar_estado_civil');
    $routes->get('/listar_Ocupacion', 'Beneficiarios_Controler::listar_Ocupacion');
    /*
     * --------------------------------------------------------------------
     * RUTAS DEL CONTROLADOR BENEFICIARIOS MODULO (CORTESIA)
     * --------------------------------------------------------------------
     */
    $routes->get('/listar_Cortesia/(:any)', 'Beneficiarios_Controler::listar_Cortesia/$1');
    $routes->post('/agregar_Cortesia', 'Beneficiarios_Controler::agregar_Cortesia');
    $routes->get('/buscar_Cortesia/(:any)', 'Beneficiarios_Controler::getAllCortesia/$1');
    $routes->get('/editar_Cortesia/(:any)', 'Beneficiarios_Controler::editar_Cortesia/$1');
    $routes->post('/actualizar_Cortesia/(:any)', 'Beneficiarios_Controler::actualizar_Cortesia/$1');
    // *********ESTE METODO SE USA PARA PASAR LOS DATOS DEL TITULAR EN MODAL DE RETIRAR**********
    $routes->get('/getAllBuscarCortesia', 'Beneficiarios_Controler::getAllBuscarCortesia');
    /*
     * --------------------------------------------------------------------
     * RUTAS DE MEDICAMENTOS
     * --------------------------------------------------------------------
     */
    $routes->get('/vistamedicamentos', 'Medicamento_Controllers::vistamedicamentos');
    $routes->get('/listar_medicamentos/(:any)/(:any)', 'Medicamento_Controllers::getAll/$1/$2');
    $routes->get('/listar_stock_minimo/(:any)/(:any)', 'Medicamento_Controllers::listar_stock_minimo/$1/$2');
    $routes->post('/agregar_medicamento', 'Medicamento_Controllers::agregar');
    $routes->get('/datosdelmedicamento', 'Medicamento_Controllers::getDatosDelMedicamento');
    $routes->post('/actualizar_medicamento', 'Medicamento_Controllers::actualizar');
    /*
     * --------------------------------------------------------------------
     * RUTAS DE  TIPO DE  MEDICAMENTOS
     * --------------------------------------------------------------------
     */
    $routes->get('/vistaTipoMedicamento', 'Tipomedicamento::vista');
    $routes->get('/listar_tipo_medicamentos', 'Tipomedicamento::getAll');
    $routes->get('/listar_medicamentos_activos/(:any)', 'Tipomedicamento::getAllActivos/$1');
    ///----- se listan lo tipos de medicamentos///
    //$routes->get('/listar_medicamentos_activos', 'Tipomedicamento::getAllActivos');
    $routes->post('/agregarTipoMedicamento', 'Tipomedicamento::agregar');
    $routes->post('/actualizarTipoMedicamento', 'Tipomedicamento::actualizar');

    /* --------------------------------------------------------------------
     * RUTAS DE UNIDAD DE MEDIDAS
     * --------------------------------------------------------------------
     */
    $routes->get('/vistaUnidadMedida', 'Unidad_Medida_Controllers::vistaUnidadMedida');
    $routes->get('/listar_unidadmedida', 'Unidad_Medida_Controllers::getAll');
    $routes->post('/agregarUnidadMedida', 'Unidad_Medida_Controllers::agregar');
    $routes->post('/actualizar_medida', 'Unidad_Medida_Controllers::actualizar');
    /* --------------------------------------------------------------------
     * RUTAS DE UNIDAD DEL PRESENTACION
     * --------------------------------------------------------------------
     */
    $routes->get('/vistapresentacion', 'Presentacion_producto::vistapresentacion');
    $routes->get('/listar_presentacion', 'Presentacion_producto::getAll');
    $routes->post('/agregar_presentacion', 'Presentacion_producto::agregar');
    $routes->post('/actualizar_presentacion', 'Presentacion_producto::actualizar');
    $routes->get('/listar_presentaciones_activas', 'Presentacion_producto::getAllActivos');
    /* --------------------------------------------------------------------
     * RUTAS DE UNIDAD DE CONTROL
     * --------------------------------------------------------------------
     */
    $routes->get('/vistacontrol', 'Control_Controllers::vistacontrol');
    $routes->get('/listar_control', 'Control_Controllers::getAll');
    $routes->post('/agregar_control', 'Control_Controllers::agregar');
    $routes->post('/actualizar_control', 'Control_Controllers::actualizar');
    $routes->get('/listar_controles_activos', 'Control_Controllers::getAllActivos');
    /* --------------------------------------------------------------------
     * RUTAS DE UNIDAD DE COMPUESTOS
     * --------------------------------------------------------------------
     */
    $routes->get('/vistacompuestos/(:any)', 'Compuesto_Controllers::vistacompuestos/$1');
    $routes->get('/listar_compuesto/(:any)', 'Compuesto_Controllers::getAll/$1');
    $routes->post('/agregar_compuesto', 'Compuesto_Controllers::agregar_compuesto');
    $routes->post('/actualizar_compuestos', 'Compuesto_Controllers::actualizar_compuestos');
    $routes->get('/listar_Compuestos_activos', 'Compuesto_Controllers::getAllActivos');
    /* --------------------------------------------------------------------
     * RUTAS DE UNIDAD DE MEDICOS
     * --------------------------------------------------------------------
     */
    $routes->get('/vistaMedicos', 'Medicos_Controllers::vistaMedicos');
    $routes->post('/agregar_Medicos', 'Medicos_Controllers::agregar_Medicos');
    $routes->get('/listar_medicos', 'Medicos_Controllers::listar_medicos');
    $routes->get('/Buscarmedicos', 'Medicos_Controllers::Buscarmedicos');
    $routes->get('/listar_medicos_activos', 'Medicos_Controllers::listar_medicos_activos');
    $routes->get('/medicos_activos_por_especialidad/(:any)', 'Medicos_Controllers::medicos_activos_por_especialidad/$1');

    $routes->post('/actualizar_Medicos', 'Medicos_Controllers::actualizar_Medicos');
    $routes->get('/medico_Citas', 'Medicos_Controllers::medico_Citas');
    $routes->get('/medico_Consultas', 'Medicos_Controllers::medico_Consultas');
    /* --------------------------------------------------------------------
     * RUTAS DE NOTAS DE ENTREGAS
     * --------------------------------------------------------------------
     */
    $routes->get('/vistaNotasEntregas', 'Notas_Entregas_Controllers::vistaNotasEntregas');
    $routes->get('/buscartitular_NotasEntregas', 'Beneficiarios_Controler::buscartitular_NotasEntregas');
    $routes->get('/buscarfamiliar_NotasEntregas', 'Beneficiarios_Controler::buscarfamiliar_NotasEntregas');
    $routes->get('/buscarcortesia_NotasEntregas', 'Beneficiarios_Controler::buscarcortesia_NotasEntregas');
    $routes->POST('/agregar_nota_entrega','Notas_Entregas_Controllers::agregar_nota_entrega');
    $routes->get('/listar_notas_entregas/(:any)/(:any)', 'Notas_Entregas_Controllers::listar_notas_entregas/$1/$2');
    $routes->post('/Reversar_nota_entrega', 'Notas_Entregas_Controllers::Reversar_nota_entrega');


 /* --------------------------------------------------------------------
     * RUTAS DEL AUTORIZADOR
     * --------------------------------------------------------------------
     */
    $routes->get('/vistaAutorizador', 'Autorizador::vistaAutorizador');
    $routes->get('/listar_Autorizador', 'Autorizador::getAll');
    $routes->post('/agregar_Autorizador', 'Autorizador::agregar');
    $routes->post('/actualizar_Autorizador', 'Autorizador::actualizar');
    $routes->get('/listar_Autorizadores_activas', 'Autorizador::getAllActivos');














    /* --------------------------------------------------------------------
     * RUTAS DE UNIDAD DE ESPECIALIDADES
    
    //actualizar_Psicologia_Primaria
    
    // $routes->get('/vista_historial', 'HistoriaControllers::vista_historial');
    
    // $routes->get('/getAllHistorial/(:any)','HistoriaControllers::getAllHistorial/$1');
    // $routes->get('/getHistorial/(:any)','HistoriaControllers::getHistorial/$1');
    // $routes->get('/datos_titular/(:any)','HistoriaControllers::datos_titular/$1');
    // $routes->get('/vista_citas/(:any)', 'HistoriaControllers::vista_citas/$1');
    // $routes->get('/vista_habitos/(:any)/(:any)/(:any)','HistoriaControllers::vista_habitos/$1/$2/$3');
    // $routes->post('/borrar_historial','HistoriaControllers::borrar_historial');
    
     * --------------------------------------------------------------------
     */
    $routes->get('/Vita_Especialidad', 'Medicos_Controllers::Vita_Especialidad');
    $routes->post('/agregar_Especialidad', 'Medicos_Controllers::agregar_Especialidad');
    $routes->get('/listar_especialidad', 'Medicos_Controllers::listar_especialidad');
    $routes->post('/actualizar_especialidad', 'Medicos_Controllers::actualizar_especialidad');
    $routes->get('/listar_especialidades_activas', 'Medicos_Controllers::listar_especialidades_activas');
    $routes->get('/listar_acceso_citas_especialidades', 'Medicos_Controllers::listar_acceso_citas_especialidades');
    $routes->get('/listar_especialidades_activas_sin_filtro', 'Medicos_Controllers::listar_especialidades_activas_sin_filtro');

    $routes->get('/listar_especialidades_activas_sin_filtro_caja_chica', 'Medicos_Controllers::listar_especialidades_activas_sin_filtro_caja_chica');



    /* --------------------------------------------------------------------
     * RUTAS DE UNIDAD DE CATEGORIA
     * --------------------------------------------------------------------
     */

    $routes->get('/vistacategoria', 'Categoria_Controller::vistacategoria');
    $routes->get('/listar_categoria', 'Categoria_Controller::getAll');
    $routes->post('/agregar_categoria', 'Categoria_Controller::agregar_categoria');
    $routes->post('/actualizar_categoria', 'Categoria_Controller::actualizar_categoria');
    $routes->get('/listar_Categoria_activas', 'Categoria_Controller::getAllActivos');
    $routes->get('/filtrarcategoriasactivas_id/(:any)', 'Categoria_Controller::filtrarcategoriasactivas_id/$1');
    /*
     * --------------------------------------------------------------------
     * RUTAS DE LAS ENTRADAS
     * --------------------------------------------------------------------
     */
    // $routes->get('/vista_entradas_regreso/(:any)' ,'Entrada_Controller::infomedicamento/$1');
    $routes->get('/vistaEntradas/(:any)', 'Entrada_Controller::vistaEntradas/$1');
    $routes->get('/infomedicamento', 'Entrada_Controller::infomedicamento');
    $routes->get('/listar_entradas/(:any)', 'Entrada_Controller::getAll/$1');
    $routes->post('/agregar_entrada', 'Entrada_Controller::agregar');
    $routes->post('/actualizar_entrada', 'Entrada_Controller::actualizar_entrada');
    $routes->post('/actualizar_fecha_v', 'Entrada_Controller::actualizar_fecha_v');
    $routes->get('/Relacion_entradas_medicamentos', 'Entrada_Controller::Relacion_entradas_medicamentos');
    /*
     * --------------------------------------------------------------------
     * RUTAS DE SALIDAS
     * --------------------------------------------------------------------
     */
    $routes->get('/vista_salidas/(:any)', 'Salida_Controller::VistaEntradaParaDarSalida/$1');
    $routes->get('/Relacion_salidas_medicamentos', 'Salida_Controller::Relacion_salidas_medicamentos');
    $routes->get('/listar_salidas/(:any)', 'Salida_Controller::getAll/$1');
    $routes->get('/VistaSalidasContraEntrada/(:any)/(:any)', 'Salida_Controller::VistaSalidasContraEntrada/$1/$2');
    $routes->get('/listar_salidas_contra_entrada/(:any)/(:any)', 'Salida_Controller::getAllSalidasContraEntradas/$1/$2');
    $routes->post('/agregar_salida', 'Salida_Controller::agregar');
    $routes->post('/actualizar_salida', 'Salida_Controller::actualizar_salida');
    $routes->get('/listar_salidas_medicos/(:any)', 'Salida_Controller::listar_salidas_medicos/$1');
    /*
     * --------------------------------------------------------------------
     * RUTAS DE REVERSOS
     * --------------------------------------------------------------------
     */
    $routes->post('/reversos', 'Reverso_Controller::reversos');
    // * --------------------------------------------------------------------
    $routes->get('/getAll_infomedicamento_titulares/(:any)/(:any)/(:any)/(:any)', 'Salida_Controller::getAll_infomedicamento_titulares/$1/$2/$3/$4');
    $routes->get('/getAll_infomedicamento_familiares/(:any)/(:any)/(:any)/(:any)', 'Salida_Controller::getAll_infomedicamento_familiares/$1/$2/$3/$4');
    $routes->get('/getAll_infomedicamento_cortesia/(:any)/(:any)/(:any)/(:any)', 'Salida_Controller::getAll_infomedicamento_cortesia/$1/$2/$3/$4');
    /*
     
    /*
     * --------------------------------------------------------------------
     * MODULO DE HISTORIAS CLINICAS 
     * --------------------------------------------------------------------
     */
    $routes->get('/vista_historial', 'HistoriaControllers::vista_historial');
    $routes->post('/agregar_historial_medico/(:any)', 'HistoriaControllers::agregar_historial_medico/$1');
    $routes->get('/getAllHistorial/(:any)', 'HistoriaControllers::getAllHistorial/$1');
    $routes->get('/getHistorial/(:any)', 'HistoriaControllers::getHistorial/$1');
    $routes->get('/getAllHistorialFamiliar/(:any)/(:any)', 'HistoriaControllers::getAllHistorialFamiliar/$1/$2');
    $routes->get('/getAllHistorialCortesia/(:any)/(:any)', 'HistoriaControllers::getAllHistorialCortesia/$1/$2');
    $routes->get('/datos_titular/(:any)', 'HistoriaControllers::datos_titular/$1');
    $routes->get('/vista_habitos/(:any)/(:any)/(:any)/(:any)/(:any)', 'HistoriaControllers::vista_habitos/$1/$2/$3/$4/$5');
    $routes->post('/borrar_historial', 'HistoriaControllers::borrar_historial');
    $routes->get('/datos_titular_familiares/(:any)/(:any)', 'HistoriaControllers::datos_titular_familiares/$1/$2');
    $routes->get('/datos_titular_cortesia/(:any)/(:any)', 'HistoriaControllers::datos_titular_cortesia/$1/$2');
    /*
     * --------------------------------------------------------------------
     * MODULO DE CONSULTA HISTORIAL
     * --------------------------------------------------------------------
     */
    $routes->post('/agregar_consulta_historial', 'Consulta_Historia_Controllers::agregar_consulta_historial');
    $routes->get('/vista_Consultas/(:any)/(:any)/(:any)', 'Consulta_Historia_Controllers::vista_Consultas/$1/$2/$3');
    $routes->get('/vista_ConsultasFamiliares/(:any)/(:any)/(:any)', 'Consulta_Historia_Controllers::vista_ConsultasFamiliares/$1/$2/$3');
    $routes->get('/vista_ConsultasCortesia/(:any)/(:any)/(:any)', 'Consulta_Historia_Controllers::vista_ConsultasCortesia/$1/$2/$3');
    $routes->get('/listar_Historial_consultas/(:any)', 'Consulta_Historia_Controllers::listar_Historial_consultas/$1');
    $routes->post('/borrar_consulta', 'Consulta_Historia_Controllers::borrar_consulta');
    $routes->post('/actualizar_asistencia', 'Consulta_Historia_Controllers::actualizar_asistencia');
    $routes->get('/listar_consultas_atendidas/(:any)', 'Consulta_Historia_Controllers::listar_consultas_atendidas/$1');
    $routes->get('/listar_consultas_not_atendidas/(:any)', 'Consulta_Historia_Controllers::listar_consultas_not_atendidas/$1');



    /*
     * --------------------------------------------------------------------
     * MODULO DE  CITAS HISTORIAL
     * --------------------------------------------------------------------
     */
    $routes->post('/agregar_cita_historial', 'Consulta_Historia_Controllers::agregar_cita_historial');
    $routes->post('/actualizar_signos_vitales_citas', 'Consulta_Historia_Controllers::actualizar_signos_vitales_citas');
    $routes->get('/vista_Citas/(:any)/(:any)/(:any)', 'Consulta_Historia_Controllers::vista_Citas/$1/$2/$3');
    $routes->get('/vista_Citas_Familiares/(:any)/(:any)/(:any)', 'Consulta_Historia_Controllers::vista_Citas_Familiares/$1/$2/$3');
    $routes->get('/vista_Citas_Cortesia/(:any)/(:any)/(:any)', 'Consulta_Historia_Controllers::vista_Citas_Cortesia/$1/$2/$3');
    $routes->get('/listar_citas_medicos/(:any)', 'Consulta_Historia_Controllers::listar_citas_medicos/$1');
    $routes->get('/listar_consultas_medicos/(:any)', 'Consulta_Historia_Controllers::listar_consultas_medicos/$1');
    $routes->get('/listar_Historial_citas/(:any)', 'Consulta_Historia_Controllers::listar_Historial_citas/$1');
    $routes->get('/listar_reporte_citas/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)', 'Consulta_Historia_Controllers::listar_reporte_citas/$1/$2/$3/$4/$5/$6/$7');
    $routes->get('/reporte_citas', 'Consulta_Historia_Controllers::reporte_citas');
    $routes->get('/Actualizar_Citas/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)', 'Consulta_Historia_Controllers::Actualizar_Citas/$1/$2/$3/$4/$5/$6/$7');
    $routes->get('/listar_citas_atendidas/(:any)', 'Consulta_Historia_Controllers::listar_citas_atendidas/$1');
    $routes->get('/listar_citas_not_atendidas/(:any)', 'Consulta_Historia_Controllers::listar_citas_not_atendidas/$1');


    
/*
     * --------------------------------------------------------------------
     * MODULO DE ODONTODIAGRAMA
     * --------------------------------------------------------------------
     */
    $routes->get('/Listar_odontodiagrama', 'OdontodiagramaControler::Listar_odontodiagrama');
   // $routes->post('/agregar_Riesgos', 'Riesgos_Controler::agregar_Riesgos');
   // $routes->get('/listar_riesgos_Historial/(:any)', 'Riesgos_Controler::listar_riesgos_Historial/$1');
    



    /*
     * --------------------------------------------------------------------
     * MODULO DE  CITAS : PSICOLOGIA PRIMARIA
     * --------------------------------------------------------------------
     */
    $routes->get('/listar_psicologia_primaria/(:any)', 'Psicologia_Primaria_Controler::listar_psicologia_primaria/$1');
    $routes->post('/atencion_psicologia_p', 'Psicologia_Primaria_Controler::Guardar_atencion_psicologia_p');
    $routes->post('/actualizar_Psicologia_Primaria', 'Psicologia_Primaria_Controler::actualizar_Psicologia_Primaria');


    /*
     * --------------------------------------------------------------------
     * RUTAS DE  TIPO DE LOS ENTES ADSCRITOS
     * --------------------------------------------------------------------
     */
    $routes->get('/entes_adscritos', 'Entes_Adscritos_Controllers::vista');
    $routes->get('/listar_entes_adscritos', 'Entes_Adscritos_Controllers::getAll');
    $routes->post('/agregarEnteAdscrito', 'Entes_Adscritos_Controllers::agregar');
    $routes->get('/listar_entes_activos', 'Entes_Adscritos_Controllers::getAllActivos');

    $routes->post('/actualizarEnte_Adscrito', 'Entes_Adscritos_Controllers::actualizar');

    /*
     * --------------------------------------------------------------------
     * MODULO DE ENFERMEDAD ACTUAL
     * --------------------------------------------------------------------
     */
    $routes->post('/agregar_enfermedad_actual', 'Enfermedad_Actual_Controllers::agregar');
    $routes->post('/actualizar_enfermedad_actual', 'Enfermedad_Actual_Controllers::actualizar_enfermedad_actual');
    $routes->get('/listar_enfermedad_actual/(:any)/(:any)', 'Enfermedad_Actual_Controllers::listar_enfermedad_actual/$1/$2');


    /*
     * --------------------------------------------------------------------
     * MODULO DE ALERGIAS A MEDICAMENTOS
     * --------------------------------------------------------------------
     */
    $routes->post('/agregar_alergias_medicamentos', 'Alergias_Medicamentos_Controllers::agregar_alergias_medicamentos');
    $routes->post('/actualizar_alergias_medicamentos', 'Alergias_Medicamentos_Controllers::actualizar_alergias_medicamentos');
    $routes->get('/listar_alergias_medicamentos/(:any)', 'Alergias_Medicamentos_Controllers::listar_alergias_medicamentos/$1');

    /*
     * --------------------------------------------------------------------
     * MODULO DE ANTECEDENTES PATOLOGICOS FAMILIARES
     * --------------------------------------------------------------------
     */
    $routes->post('/agregar_antecedentes_patologicosf', 'Antecedentes_Patologicosf_Controllers::agregar_antecedentes_patologicosf');
    $routes->get('/listar_antecedentes_patologicosf/(:any)', 'Antecedentes_Patologicosf_Controllers::listar_antecedentes_patologicosf/$1');
    $routes->post('/actualizar_antecedentes_patologicosf', 'Antecedentes_Patologicosf_Controllers::actualizar_antecedentes_patologicosf');

    /*
     * --------------------------------------------------------------------
     * MODULO DE ANTECEDENTES QUIRURGICOS
     * --------------------------------------------------------------------
     */
    $routes->post('/agregar_antecedentes_quirurgicos', 'Antecedentes_Quirurgicos_Controllers::agregar_antecedentes_quirurgicos');
    $routes->post('/actualizar_antecedentes_quirurgicos', 'Antecedentes_Quirurgicos_Controllers::actualizar_antecedentes_quirurgicos');
    $routes->get('/listar_antecedentesquirurgicos/(:any)', 'Antecedentes_Quirurgicos_Controllers::listar_antecedentesquirurgicos/$1');

    /*
     * --------------------------------------------------------------------
     * MODULO DE ANTECEDENTES PATOLOGICOS PERSONALES
     * --------------------------------------------------------------------
     */
    $routes->post('/agregar_antecedentes_patologicosp', 'Antecedentes_Patologicosp_Controllers::agregar_antecedentes_patologicosp');
    $routes->post('/actualizar_antecedentes_patologicosp', 'Antecedentes_Patologicosp_Controllers::actualizar_antecedentes_patologicosp');
    $routes->get('/listar_antecedentes_patologicosp/(:any)', 'Antecedentes_Patologicosp_Controllers::listar_antecedentes_patologicosp/$1');
    /*
     * --------------------------------------------------------------------
     * MODULO DE EXAMEN FISICO
     * --------------------------------------------------------------------
     */
    $routes->post('/agregar_examen_fisico', 'Examen_Fisico_Controllers::agregar_examen_fisico');
    $routes->post('/actualizar_examen_fisico', 'Examen_Fisico_Controllers::actualizar_examen_fisico');
    $routes->get('/listar_examen_fisico/(:any)', 'Examen_Fisico_Controllers::listar_examen_fisico/$1');

    /*
     * --------------------------------------------------------------------
     * MODULO DE PARACLINICOS
     * --------------------------------------------------------------------
     */
    $routes->post('/agregar_paraclinicos', 'Paraclinicos_Controllers::agregar_paraclinicos');
    $routes->post('/actualizar_paraclinicos', 'Paraclinicos_Controllers::actualizar_paraclinicos');
    $routes->get('/listar_paraclinicos/(:any)', 'Paraclinicos_Controllers::listar_paraclinicos/$1');

    /*
     * --------------------------------------------------------------------
     * MODULO DE IMPRESION DIAGNOSTICA
     * --------------------------------------------------------------------
     */
    $routes->post('/agregar_impresiondiagnostica', 'Impresion_Diag_Controllers::agregar_impresiondiagnostica');
    $routes->post('/actualizar_impresiondiagnostica', 'Impresion_Diag_Controllers::actualizar_impresiondiagnostica');
    $routes->get('/listar_impresion_diag/(:any)', 'Impresion_Diag_Controllers::listar_impresion_diag/$1');
    /*
     * --------------------------------------------------------------------
     * MODULO DE PLAN
     * --------------------------------------------------------------------
     */
    $routes->post('/agregar_plan', 'Plan_Controllers::agregar_plan');
    $routes->post('/actualizar_plan', 'Plan_Controllers::actualizar_plan');

    $routes->get('/listar_plan/(:any)', 'Plan_Controllers::listar_plan/$1');

    /*
     * --------------------------------------------------------------------
     * MODULO PARA MEDICAMENTOS MEDICINA INTERNA
     * --------------------------------------------------------------------
     */
    $routes->post('/agregar_medicamentos', 'Medicamentos_patologias_Controllers::agregar_medicamentos');
    $routes->post('/actualizar_medicamentos', 'Medicamentos_patologias_Controllers::actualizar_medicamentos');
    $routes->get('/listar_medicamentos_mi/(:any)/(:any)/(:any)', 'Medicamentos_patologias_Controllers::listar_medicamentos/$1/$2/$3');
    $routes->get('/listar_medicamentos_pp/(:any)/(:any)/(:any)', 'Medicamentos_patologias_Controllers::listar_medicamentos/$1/$2/$3');


    /*
     * --------------------------------------------------------------------
     * MODULO DE METODOS ANTICONCEPTIVOS
     * --------------------------------------------------------------------
     */
    $routes->get('/listar_metodos_Anticonceptivos', 'Metodo_Anticonceptivo_Controllers::ListarMetodosAnticonceptivos_Activos');
    /*
     * --------------------------------------------------------------------
     * MODULO DE METODOS ANTICONCEPTIVOS
     * --------------------------------------------------------------------
     */
    $routes->get('/ListarEnfermedadesSexuales', 'Enfermedades_Sexuales_Controller::ListarEnfermedadesSexuales');
    /*
     * --------------------------------------------------------------------
     * MODULO DE NUMEROS ROMANOS
     * --------------------------------------------------------------------
     */
    $routes->get('/Listar_Numeros_Romanos', 'Numero_Romanos_Controllers::Listar_Numeros_Romanos');
    /*
     * --------------------------------------------------------------------
     * MODULO PARA ANTECEDENTES GINECO OBSTETRICOS 
     * --------------------------------------------------------------------
     */
    $routes->post('/agregar_Antecedentes_gineco_obstetricos', 'Antec_Gineco_Obstetrico_Controller::agregar');
    $routes->post('/actualizar_Antecedentes_gineco_obstetricos', 'Antec_Gineco_Obstetrico_Controller::actualizar_Antecedentes_gineco_obstetricos');
    // $routes->get('/listar_medicamentos_mi/(:any)','Antec_Gineco_Obstetrico_Controller::listar_medicamentos/$1');
    // $routes->get('/listar_medicamentos_pp/(:any)','Antec_Gineco_Obstetrico_Controller::listar_medicamentos/$1');
    /*
    /*
     * --------------------------------------------------------------------
     * MODULO DE Habitos
     * --------------------------------------------------------------------
     */
    $routes->get('/listar_habitos', 'Habitos_Controller::listar_habitos');
    $routes->get('/listar_habitos_personales/(:any)', 'Habitos_Controller::listar_habitos_personales/$1');
    $routes->post('/agregar_habito_historial', 'Habitos_Controller::agregar_habito_historial');
    $routes->post('/borrar_habitos', 'Habitos_Controller::borrar_habitos');
    /*
     * --------------------------------------------------------------------
     * MODULO DE REPORTES 
     * --------------------------------------------------------------------
     */
    $routes->get('/VerMedicamentosPdf', 'Medicamento_Controllers::VerMedicamentosPdf');
    $routes->get('/VerMedicamentoExcel', 'Medicamento_Controllers::VerMedicamentoExcel');
    $routes->get('/GenerarReportesEntradaPorFecha/(:any)/(:any)/(:any)', 'Entrada_Controller::GenerarReportesEntradaPorFecha/$1/$2/$3');
    $routes->get('/GenerarReportesSalidasPorFecha/(:any)/(:any)/(:any)', 'Salida_Controller::GenerarReportesSalidasPorFecha/$1/$2/$3');
    $routes->get('/GenerarReportesEntradaPorFechaExcel/(:any)/(:any)/(:any)/(:any)', 'Entrada_Controller::GenerarReportesEntradaPorFechaExcel/$1/$2/$3/$4');
    $routes->get('/GenerarReportesSalidasPorFechaExcel/(:any)/(:any)/(:any)/(:any)', 'Salida_Controller::GenerarReportesSalidasPorFechaExcel/$1/$2/$3/$4');
    $routes->get('/Nota_Entrega_medicamento/(:any)/(:any)/(:any)/(:any)', 'Salida_Controller::Nota_Entrega_medicamento/$1/$2/$3/$4');
    $routes->get('/Relacion_salidas_medicamentosPDF/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)', 'Salida_Controller::Relacion_salidas_medicamentosPDF/$1/$2/$3/$4/$5/$6/$7');
    $routes->get('/Relacion_salidas_medicamentos_entes/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)', 'Salida_Controller::Relacion_salidas_medicamentos_entes/$1/$2/$3/$4/$5/$6/$7/$8');
    $routes->get('/Hoja_morbilidad', 'Morbilidad_Controller::Hoja_morbilidad');
    $routes->get('/listar_mobilidad/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)', 'Morbilidad_Controller::listar_mobilidad/$1/$2/$3/$4/$5/$6');
    $routes->get('/listar_mobilidad_entes/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)', 'Morbilidad_Controller::listar_mobilidad_entes/$1/$2/$3/$4/$5/$6/$7');
    $routes->get('/VerEntradasMedicamentosPdf/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)', 'Entrada_Controller::VerEntradasMedicamentosPdf/$1/$2/$3/$4/$5/$6/$7');
    $routes->get('/vista_stock_minimo', 'Medicamento_Controllers::vista_stock_minimo');
    /*
     * --------------------------------------------------------------------
     * MODULO DE FRECUENCIA DE ATENCION MEDICA 
     * --------------------------------------------------------------------
     */
    $routes->get('/frecuencia_atencion', 'Reportes_Controller::frecuencia_atencion');
    $routes->get('/listar_frecuencia_atencion/(:any)/(:any)/(:any)/(:any)', 'Reportes_Controller::listar_frecuencia_atencion/$1/$2/$3/$4');
    // $routes->get('/listar_frecuencia_atencion_entes/(:any)/(:any)/(:any)', 'Reportes_Controller::listar_frecuencia_atencion_entes/$1/$2/$3');
    // * --------------------------------------------------------------------
    //  *
    //  * --------------------------------------------------------------------
    //  */
    $routes->get('/vista_reportes/(:any)', 'Reportes_Controller::Vistareportes/$1');
    $routes->get('/PDF_Medicina_General/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)', 'Reportes_Controller::PDF_Medicina_General/$1/$2/$3/$4/$5/$6/$7');
    $routes->get('/PDF_Medicina_Interna/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)', 'Reportes_Controller::PDF_Medicina_Interna/$1/$2/$3/$4/$5/$6/$7/$8');
    $routes->get('/PDF_Psicologia_Primaria/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)', 'Reportes_Controller::PDF_Psicologia_Primaria/$1/$2/$3/$4/$5/$6/$7/$8');
    $routes->get('/PDF_Paciente_Pediatrico/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)', 'Reportes_Controller::PDF_Paciente_Pediatrico/$1/$2/$3/$4/$5/$6/$7/$8');
    $routes->get('/PDF_Ginecologia_Obtertricia/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)', 'Reportes_Controller::PDF_Ginecologia_Obtertricia/$1/$2/$3/$4/$5/$6/$7/$8');
    $routes->get('/PDF_Odontologia/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)', 'Reportes_Controller::PDF_Odontologia/$1/$2/$3/$4/$5/$6/$7/$8');
    
}




 /*
     * --------------------------------------------------------------------
     * MODULO DE  PATOLOGIAS
     * --------------------------------------------------------------------
     */
    $routes->get('/patologias_personales', 'Patologias_Controler::patologias_personales');
    $routes->get('/Listar_patologias', 'Patologias_Controler::Listar_patologias');
    $routes->get('/Listar_patologias_activas', 'Patologias_Controler::Listar_patologias_activas');

    $routes->post('/agregar_Patologias', 'Patologias_Controler::agregar_Patologias');
    $routes->get('/listar_patologias_Historial/(:any)', 'Patologias_Controler::listar_patologias_Historial/$1');
 
     $routes->post('/agregar_patologia_master', 'Patologias_Controler::agregar_patologia_master');
     $routes->post('/actualizar_patologias_master', 'Patologias_Controler::actualizar_patologias_master');
   
     /*
     * --------------------------------------------------------------------
     * MODULO DE  RIESGOS
     * --------------------------------------------------------------------
     */
    $routes->get('/habitos_psicobiologicos', 'Riesgos_Controler::habitos_psicobiologicos');
    $routes->get('/Listar_riesgos', 'Riesgos_Controler::Listar_riesgos');
    $routes->get('/Listar_riesgos_activos', 'Riesgos_Controler::Listar_riesgos_activos');
    $routes->post('/agregar_Riesgos', 'Riesgos_Controler::agregar_Riesgos');
    $routes->get('/listar_riesgos_Historial/(:any)', 'Riesgos_Controler::listar_riesgos_Historial/$1');
    $routes->post('/agregar_riesgos_master', 'Riesgos_Controler::agregar_riesgos_master');
    $routes->post('/actualizar_riesgos_master', 'Riesgos_Controler::actualizar_riesgos_master');
  


/*
     * --------------------------------------------------------------------
     * MODULO DE HISTORIAL INFORMATIVO
     * --------------------------------------------------------------------
     */
    $routes->get('/Listar_Historial_Informativo', 'Historial_info_Controllers::Listar_Historial_Informativo');
   
    $routes->get('/especialidad_hist_informativo/(:any)', 'Historial_info_Controllers::especialidad_hist_informativo/$1');



/*
     * --------------------------------------------------------------------
     * MODULO AUDITORIA SISTEMA
     * --------------------------------------------------------------------
     */
    $routes->get('/auditoria_sistema', 'Auditoria_sistema_Controllers::auditoria_sistema');
    $routes->get('/listar_auditoria_sistema/(:any)/(:any)', 'Auditoria_sistema_Controllers::listar_auditoria_sistema/$1/$2');
    $routes->get('/listar_auditoria/(:any)/(:any)', 'AuditoriaController::listar_auditoria/$1/$2');
/*
     * --------------------------------------------------------------------
     * MODULO REPOSOS (FARMACIA)
     * --------------------------------------------------------------------
     */
    $routes->get('/reposos', 'Reposos_Controllers::reposos');
    $routes->get('/listar_reposos/(:any)/(:any)/(:any)/(:any)', 'Reposos_Controllers::listar_reposos/$1/$2/$3/$4');
    $routes->get('/listar_reposos_con_filtro/(:any)/(:any)/(:any)', 'Reposos_Controllers::listar_reposos_con_filtro/$1/$2/$3');
    $routes->get('/PDF_Reposos/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)', 'Reposos_Controllers::PDF_Reposos/$1/$2/$3/$4/$5/$6/$7');
    $routes->post('/Reversar_reposo', 'Reposos_Controllers::Reversar_reposo');
    $routes->post('/Actualizar_Reposo', 'Reposos_Controllers::Actualizar_Reposo');
    $routes->post('/Agregar_Reposo', 'Reposos_Controllers::Agregar_Reposo');

/*
     * --------------------------------------------------------------------
     * MODULO REPOSOS (MEDICOS)
     * --------------------------------------------------------------------
     */
    $routes->get('/medico_reposo', 'Reposos_Medicos_Controllers::medico_reposo');
    $routes->get('/listar_reposos/(:any)/(:any)/(:any)/(:any)', 'Reposos_Medicos_Controllers::listar_reposos/$1/$2/$3/$4');
    $routes->get('/listar_reposos_con_filtro/(:any)/(:any)/(:any)', 'Reposos_Medicos_Controllers::listar_reposos_con_filtro/$1/$2/$3');
    $routes->get('/PDF_Reposos/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)', 'Reposos_Medicos_Controllers::PDF_Reposos/$1/$2/$3/$4/$5/$6/$7');
    $routes->post('/Reversar_reposo', 'Reposos_Medicos_Controllers::Reversar_reposo');
    $routes->post('/Actualizar_Reposo', 'Reposos_Medicos_Controllers::Actualizar_Reposo');
    $routes->post('/Agregar_Reposo', 'Reposos_Medicos_Controllers::Agregar_Reposo');










//$routes->get('/listar_auditoria_sistema/(:any)/(:any)', 'Reposos_Controllers::listar_auditoria_sistema/$1/$2');
// * --------------------------------------------------------------------
// * MODULO UBICACIONES ADMINISTRATIVAS
// * --------------------------------------------------------------------
// */
$routes->get('/listar_ubicaciones', 'Ubi_Admini_Controler::listar_Ubicacion_Administrativa');
/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
