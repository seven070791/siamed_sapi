<?php

namespace App\Controllers;
use App\Models\Autorizador_Model;
use CodeIgniter\API\ResponseTrait;
use CodeIgniter\RESTful\ResourceController;
use App\Models\Beneficiarios_Model;
use App\Models\Auditoria_sistema_Model;
use App\Models\Consulta_Histotia_Model;
use App\Models\Enfermedad_Actual_Model;
use App\Models\HistorialModel;
use App\Models\Alergias_Medicamentos_Model;
use App\Models\Antecendentes_Patologicosf_Model;
use App\Models\Antecedentes_Quirurgicos_Model;
use App\Models\Antecendentes_Patologicosp_Model;
use App\Models\Examen_Fisico_Model;
use App\Models\Paraclinicos_Model;
use App\Models\Impresion_Diag_Model;
use App\Models\Plan_Model;
use App\Models\Habitos_Model;
use App\Models\Antece_Gineco_Obstetrico_Model;
use App\Models\Partos_Model;
use App\Models\Cesarias_Model;
use App\Models\Abortos_Model;
use App\Models\Gestacion_Model;
use App\Models\Patologias_Model;
use App\Models\Riesgos_Model;
use App\Models\Especialidad_Model;

class Consulta_Historia_Controllers extends BaseController
{

    public function vista_Consultas($cedula, $tipo_beneficiario, $n_historial)
    {
        if (!session('nombreUsuario')) {
            return redirect()->to(base_url() . '/index.php');
        }
        $modelo = new Beneficiarios_model();
        $modelo_historia = new HistorialModel();
        $query = $modelo->getAll($cedula);
        if (empty($query)) {
            $datos_titular = [];
        } else {
            foreach ($query as $fila) {
                $datos_titular['cedula_trabajador']       = $fila->cedula_trabajador;
                $datos_titular['nombre']                  = $fila->nombre;
                $datos_titular['apellido']                  = $fila->apellido;
                $datos_titular['ubicacion_administrativa'] = $fila->ubicacion_administrativa;
                $datos_titular['fecha_nacimiento'] = $fila->fecha_nacimiento;
                $datos_titular['edad_actual'] = $fila->edad_actual;
                $datos_titular['sexo'] = $fila->sexo;
                $datos_titular['estatus'] = $fila->estatus;
                $datos_titular['telefono'] = $fila->telefono;
                $datos_titular['tipo_beneficiario'] = $tipo_beneficiario;
                $datos_titular['n_historial'] = $n_historial;
            }
            $query_historia = $modelo_historia->getHistorial_inactivo($cedula,$n_historial);
           
            foreach ($query_historia as $fila) {
                $datos_titular['borrado']       = $fila->borrado;
           }
          
            echo view('/historia_clinica/vista_consultas', $datos_titular);
            echo view('/historia_clinica/footer_vista_consultas');
        }
    }

   
   




    
    public function Actualizar_Citas($cedula, $tipo_beneficiario, $n_historial, $id_especialidad, $fecha_asistencia, $id_historial_medico, $id_consulta)
    {
        //echo($id_historial_medico);
        //die();
        if (!session('nombreUsuario')) {
            return redirect()->to(base_url() . '/index.php');
        }

        $modelo_especialidad = new Especialidad_Model();
        $query_especialidad = $modelo_especialidad->buscar_especialidad($id_especialidad);
       
        if (empty($query_especialidad)) {
            $datos_titular['nombre_especialidad']    = '';
        } else {
            foreach ($query_especialidad as $especialidad) {
                $datos_titular['nombre_especialidad']    = $especialidad->descripcion;
            }
        }

       
        if ($tipo_beneficiario == 'T') {
            $modelo = new Beneficiarios_model();
            $modelo_signosvitales = new Consulta_Histotia_Model();
            $modelo_enfermedad_actual = new Enfermedad_Actual_Model();
            $modelo_alergias_medicamentos = new Alergias_Medicamentos_Model();
            $modelo_antecedentes_patologicosf = new Antecendentes_Patologicosf_Model();
            $modelo_antecedentes_quirurgicos = new Antecedentes_Quirurgicos_Model();
            $modelo_antecedentes_patologicosp = new Antecendentes_Patologicosp_Model();
            $modelo_examen_fisico = new Examen_Fisico_Model();
            $modelo_paraclinicos = new  Paraclinicos_Model();
            $modelo_impresion_diag = new  Impresion_Diag_Model();
            $modelo_atc_gineco_obste = new  Antece_Gineco_Obstetrico_Model();
            $modelo_habitos = new  Habitos_Model();
            $modelo_plan = new  Plan_Model();
            $query_impresion_diagnostica = $modelo_impresion_diag->buscar_impresion_diag($n_historial);

            $modelo_abortos = new  Abortos_Model();
            $modelo_partos = new  Partos_Model();
            $modelo_cesarias = new  Cesarias_Model();
            $modelo_gestacion = new  Gestacion_Model();

         
          
          

         
            $query_abortos = $modelo_abortos->listar_abortos($n_historial, $id_consulta);
            $query_partos = $modelo_partos->listar_partos($n_historial, $id_consulta);
            $query_cesarias = $modelo_cesarias->listar_cesarias($n_historial, $id_consulta);
            $query_gestacion = $modelo_gestacion->listar_gestacion($n_historial, $id_consulta);
            $query = $modelo->getAll($cedula);
            $tipo_de_sangre = $modelo->getAll_tipodesangre($cedula);
            $signosvitales = $modelo_signosvitales->listar_signos_vitales($n_historial, $id_consulta);
            $atc_gineco_obste = $modelo_atc_gineco_obste->listar_Antece_Gineco_obtetrico($n_historial, $id_consulta);
            $modelo_psicologia_impre_diag = $modelo_enfermedad_actual->listar_psicologia_impre_diag($n_historial, $id_especialidad);
            if (empty($query)) {
                $datos_titular = [];
            } else {
                //***********ENFERMEDAD ACTUAL***********
                if (empty($enfermedad_actual)) {
                    $datos_titular['descripcion_enfermedada_actual']    = '';
                } else {
                    foreach ($enfermedad_actual as $enfermedadactual) {
                        $datos_titular['descripcion_enfermedada_actual']    = $enfermedadactual->descripcion;
                    }
                }
                //************ALERGIAS A MEDICAMENTOS*******
                if (empty($alergias_medicamentos)) {
                    $datos_titular['descripcion_alergias_medicamentos'] = '';
                } else {
                    foreach ($alergias_medicamentos as $alergiasmedicamentos) {
                        $datos_titular['descripcion_alergias_medicamentos']    = $alergiasmedicamentos->descripcion;
                    }
                }
                //************ANTECEDENTES PATOLOGICOS FAMILIARES*******
                if (empty($antecedentes_patologicosf)) {
                    $datos_titular['descripcion_antecedentes_patologicosf'] = '';
                } else {
                    foreach ($antecedentes_patologicosf as $antecedentespatologicosf) {
                        $datos_titular['descripcion_antecedentes_patologicosf']  = $antecedentespatologicosf->descripcion;
                    }
                }
                //************ANTECEDENTES QUIRURGICOS*******
                if (empty($antecedentes_quirurgicos)) {
                    $datos_titular['descripcion_antecedentes_quirurgicos'] = '';
                } else {
                    foreach ($antecedentes_quirurgicos as $antecedentesquirurgicos) {
                        $datos_titular['descripcion_antecedentes_quirurgicos']  = $antecedentesquirurgicos->descripcion;
                    }
                }


                //************ANTECEDENTES PATOLOGICOS PERSONALES*******
                if (empty($antecedentes_patologicosp)) {
                    $datos_titular['descripcion_antecedentes_patologicosp'] = '';
                } else {
                    foreach ($antecedentes_patologicosp as $antecedentespatologicosp) {
                        $datos_titular['descripcion_antecedentes_patologicosp']  = $antecedentespatologicosp->descripcion;
                    }
                }

                //************EXAMEN FISICO*******
                if (empty($examen_fisico)) {
                    $datos_titular['descripcion_examen_fisico'] = '';
                } else {
                    foreach ($examen_fisico as $examenfisico) {
                        $datos_titular['descripcion_examen_fisico']  = $examenfisico->descripcion;
                    }
                }
                //************PARACLINICOS*******
                if (empty($paraclinicos)) {
                    $datos_titular['descripcion_paraclinicos'] = '';
                } else {
                    foreach ($paraclinicos as $paraclinicos) {
                        $datos_titular['descripcion_paraclinicos']  = $paraclinicos->descripcion;
                    }
                }


                //************IMPRESION DIAGNOSTICA*******
                if (empty($impresiondiagnostica)) {
                    $datos_titular['descripcion_impresiondiagnostica'] = '';
                } else {
                    foreach ($impresiondiagnostica as $impresiondiag) {
                        $datos_titular['descripcion_impresiondiagnostica']  = $impresiondiag->descripcion;
                    }
                }
                //************PLAN*******
                if (empty($plan)) {
                    $datos_titular['descripcion_plan'] = '';
                } else {
                    foreach ($plan as $plan) {
                        $datos_titular['descripcion_plan']  = $plan->descripcion;
                    }
                }
                //************TIPO DE SANGRE*******
                if (empty($tipo_de_sangre)) {
                    $datos_titular['descripcion_tipodesangre'] = '';
                } else {
                    foreach ($tipo_de_sangre as $tiposangre) {
                        $datos_titular['descripcion_tipodesangre']  = $tiposangre->tipodesangre;
                    }
                }


                //************singnos vitales*******
                if (empty($signosvitales)) {
                    $datos_titular['descripcion_plan'] = '';
                    $datos_titular['peso']         = '';
                    $datos_titular['talla']        = '';
                    $datos_titular['spo2']         = '';
                    $datos_titular['imc']         = '';
                    $datos_titular['frecuencia_c'] = '';
                    $datos_titular['frecuencia_r'] = '';
                    $datos_titular['temperatura']  = '';
                    $datos_titular['tension_alta'] = '';
                    $datos_titular['tension_baja'] = '';
                    $datos_titular['fecha_creacion'] = '';
                    $datos_titular['motivo_consulta'] = '';
                } else {
                    foreach ($signosvitales as $signos) {
                        $datos_titular['peso']         = $signos->peso;
                        $datos_titular['talla']        = $signos->talla;
                        $datos_titular['spo2']         = $signos->spo2;
                        $datos_titular['frecuencia_c'] = $signos->frecuencia_c;
                        $datos_titular['frecuencia_r'] = $signos->frecuencia_r;
                        $datos_titular['temperatura']  = $signos->temperatura;
                        $datos_titular['tension_alta'] = $signos->tension_alta;
                        $datos_titular['tension_baja'] = $signos->tension_baja;
                        $datos_titular['imc'] = $signos->imc;
                        $datos_titular['fecha_creacion'] = $signos->fecha_creacion;
                        $datos_titular['motivo_consulta'] = $signos->motivo_consulta;
                        $datos_titular['user_id'] = $signos->user_id;
                        $datos_titular['user_nombre'] = $signos->user_nombre;
                    }
                }

                //************ANTECEDENTES GINECO OBSTETRICO*******
                if (empty($atc_gineco_obste)) {

                    $datos_titular['menarquia']        = '0';
                    $datos_titular['sexarquia']         = '0';
                    $datos_titular['nps']         = '0';
                    $datos_titular['ciclo_mestrual'] = '';
                    $datos_titular['gestacion'] = '';
                    $datos_titular['partos']  = '';
                    $datos_titular['cesarias'] = '';
                    $datos_titular['abortos'] = '';
                    $datos_titular['f_u_r'] = '';
                    $datos_titular['edad_gestacional'] = '';
                    $datos_titular['dismenorrea'] = '';
                    $datos_titular['eumenorrea'] = '';
                    $datos_titular['anticonceptivo'] = '';
                    $datos_titular['tipo_anticonceptivo'] = '';
                    $datos_titular['detalles_anticon'] = '';
                    $datos_titular['t_colocacion'] = '';
                    $datos_titular['ets'] = '';
                    $datos_titular['tipo_ets'] = '';
                    $datos_titular['detalle_ets'] = '';
                    $datos_titular['citologia'] = '';
                    $datos_titular['eco_mamario'] = '';
                    $datos_titular['mamografia'] = '';
                    $datos_titular['desintometria'] = '';
                    $datos_titular['detalle_historia_obst'] = '';
                    $datos_titular['bandera_de_llenado'] = 'false';

                    $datos_titular['diu'] = '';
                    $datos_titular['t_cobre'] = '';
                    $datos_titular['mirena'] = '';
                    $datos_titular['aspiral'] = '';
                    $datos_titular['i_subdermico'] = '';
                    $datos_titular['otro'] = '';
                } else {
                    foreach ($atc_gineco_obste as $atcgisobst) {
                        $datos_titular['menarquia']         = $atcgisobst->menarquia;
                        $datos_titular['sexarquia']        = $atcgisobst->sexarquia;
                        $datos_titular['nps']         = $atcgisobst->nps;
                        $datos_titular['ciclo_mestrual'] = $atcgisobst->ciclo_mestrual;
                        $datos_titular['f_u_r'] = $atcgisobst->f_u_r;
                        $datos_titular['edad_gestacional'] = $atcgisobst->edad_gestacional;
                        $datos_titular['dismenorrea'] = $atcgisobst->dismenorrea;
                        $datos_titular['eumenorrea'] = $atcgisobst->eumenorrea;
                        $datos_titular['anticonceptivo'] = $atcgisobst->anticonceptivo;
                        $datos_titular['detalles_anticon'] = $atcgisobst->detalles_anticon;
                        $datos_titular['t_colocacion'] = $atcgisobst->t_colocacion;
                        $datos_titular['ets'] = $atcgisobst->ets;
                        $datos_titular['tipo_ets'] = $atcgisobst->tipo_ets;
                        $datos_titular['detalle_ets'] = $atcgisobst->detalle_ets;
                        $datos_titular['citologia'] = $atcgisobst->citologia;
                        $datos_titular['eco_mamario'] = $atcgisobst->eco_mamario;
                        $datos_titular['mamografia'] = $atcgisobst->mamografia;
                        $datos_titular['desintometria'] = $atcgisobst->desintometria;
                        $datos_titular['detalle_historia_obst'] = $atcgisobst->detalle_historia_obst;
                        $datos_titular['bandera_de_llenado'] = 'true';

                        $datos_titular['diu'] = $atcgisobst->diu;
                        $datos_titular['t_cobre'] = $atcgisobst->t_cobre;
                        $datos_titular['mirena'] = $atcgisobst->mirena;
                        $datos_titular['aspiral'] = $atcgisobst->aspiral;
                        $datos_titular['i_subdermico'] = $atcgisobst->i_subdermico;
                        $datos_titular['otro'] = $atcgisobst->otro;
                    }
                }
                //************PARTOS /CESARIAS/ABORTOS/GESTACION*******
                foreach ($query_gestacion as $fila) {
                    $datos_titular['gestacion'] = $fila->id_gestacion_numeros_romanos;
                }

                foreach ($query_partos as $fila) {
                    $datos_titular['partos'] = $fila->id_partos_numeros_romanos;
                }
                foreach ($query_cesarias as $fila) {
                    $datos_titular['cesarias'] = $fila->id_cesarias_numeros_romanos;
                }
                foreach ($query_abortos as $fila) {
                    $datos_titular['abortos'] = $fila->id_abortos_numeros_romanos;
                }

                foreach ($query as $fila) {
                    $datos_titular['cedula_trabajador']       = $fila->cedula_trabajador;
                    $datos_titular['nombre']                  = $fila->nombre;
                    $datos_titular['apellido']                  = $fila->apellido;
                    $datos_titular['ubicacion_administrativa'] = $fila->ubicacion_administrativa;
                    $datos_titular['fecha_nacimiento'] = $fila->fecha_nacimiento;
                    $datos_titular['edad_actual'] = $fila->edad_actual;
                    $datos_titular['sexo'] = $fila->sexo;
                    $datos_titular['telefonot'] = $fila->telefono;
                    $datos_titular['tipo_beneficiario'] = $tipo_beneficiario;
                    $datos_titular['n_historial'] = $n_historial;
                    $datos_titular['especialidad'] = $id_especialidad;
                    $datos_titular['fecha_asistencia'] = $fecha_asistencia;
                    $datos_titular['tipo_de_personal'] = $fila->tipo_de_personal;
                    $datos_titular['id_consulta'] = $id_consulta;
                    $datos_titular['id_historial_medico'] = $id_historial_medico;
                    $datos_titular['descrip_estado_civil'] = $fila->descrip_estado_civil;
                    $datos_titular['descrip_grado_intruccion'] = $fila->descrip_grado_intruccion;
                    $datos_titular['descrip_ocupacion'] = $fila->descrip_ocupacion;
                }


                //************BUSCAR IMPRESION DIAGNOSTICA  PARA LA VISTA *******
                if (empty($query_impresion_diagnostica)) {
                    $datos_titular['impresion_diag'] = '';
                } else {
                    foreach ($query_impresion_diagnostica as $impre) {
                        $datos_titular['impresion_diag']         = $impre->descripcion;
                    }
                }

                //************IMPRESION DIAGNOSTICA PARA EL PDF EN REPORTES *******
                if (empty($modelo_psicologia_impre_diag)) {
                    $datos_titular['psicologia_impre_diag'] = '';
                } else {
                    foreach ($modelo_psicologia_impre_diag as $impre_diag) {
                        $datos_titular['psicologia_impre_diag']  = $impre_diag->descripcion;
                    }
                }
             
              

                echo view('medicos/actualizar_citas_titulares',$datos_titular);
                echo view('/medicos/footer_actualizar_citas_titulares');
            }
        } elseif ($tipo_beneficiario == 'F') {
            $modelo = new Beneficiarios_model();
            $modelo_signosvitales = new Consulta_Histotia_Model();
            $modelo_enfermedad_actual = new Enfermedad_Actual_Model();
            $modelo_alergias_medicamentos = new Alergias_Medicamentos_Model();
            $modelo_antecedentes_patologicosf = new Antecendentes_Patologicosf_Model();
            $modelo_antecedentes_quirurgicos = new Antecedentes_Quirurgicos_Model();
            $modelo_antecedentes_patologicosp = new Antecendentes_Patologicosp_Model();
            $modelo_examen_fisico = new Examen_Fisico_Model();
            $modelo_paraclinicos = new  Paraclinicos_Model();
            $modelo_impresion_diag = new  Impresion_Diag_Model();
            $modelo_plan = new  Plan_Model();
            $modelo_atc_gineco_obste = new  Antece_Gineco_Obstetrico_Model();
            $modelo_abortos = new  Abortos_Model();
            $modelo_partos = new  Partos_Model();
            $modelo_cesarias = new  Cesarias_Model();
            $modelo_gestacion = new  Gestacion_Model();
            $query_impresion_diagnostica = $modelo_impresion_diag->buscar_impresion_diag($n_historial);
            $query_abortos = $modelo_abortos->listar_abortos($n_historial, $id_consulta);
            $query_partos = $modelo_partos->listar_partos($n_historial, $id_consulta);
            $query_cesarias = $modelo_cesarias->listar_cesarias($n_historial, $id_consulta);
            $query_gestacion = $modelo_gestacion->listar_gestacion($n_historial, $id_consulta);
            $signosvitales = $modelo_signosvitales->listar_signos_vitales($n_historial, $id_consulta);
            $alergias_medicamentos = $modelo_alergias_medicamentos->listar_alergias_medicamentos($n_historial);
            $antecedentes_patologicosf = $modelo_antecedentes_patologicosf->antecedentes_patologicosf($n_historial);
            $antecedentes_quirurgicos = $modelo_antecedentes_quirurgicos->listar_antecedentes_quirurgicos($n_historial);
            $antecedentes_patologicosp = $modelo_antecedentes_patologicosp->antecedentes_patologicosp($n_historial);
            $examen_fisico = $modelo_examen_fisico->listar_examen_fisico($n_historial);
            $paraclinicos = $modelo_paraclinicos->listar_paraclinicos($n_historial);
            $impresiondiagnostica = $modelo_impresion_diag->listar_impresion_diag($n_historial);
            $plan = $modelo_plan->listar_plan($n_historial);
            $atc_gineco_obste = $modelo_atc_gineco_obste->listar_Antece_Gineco_obtetrico($n_historial, $id_consulta);
            $modelo_psicologia_impre_diag = $modelo_enfermedad_actual->listar_psicologia_impre_diag($n_historial, $id_especialidad);
            $query = $modelo->getFamiliar($cedula);

            if (empty($query)) {
                $datos_titular = [];
            } else {
                //***********ENFERMEDAD ACTUAL***********
                if (empty($enfermedad_actual)) {
                    $datos_titular['descripcion_enfermedada_actual']    = '';
                } else {
                    foreach ($enfermedad_actual as $enfermedadactual) {
                        $datos_titular['descripcion_enfermedada_actual']    = $enfermedadactual->descripcion;
                    }
                }
                //************ALERGIAS A MEDICAMENTOS*******
                if (empty($alergias_medicamentos)) {
                    $datos_titular['descripcion_alergias_medicamentos'] = '';
                } else {
                    foreach ($alergias_medicamentos as $alergiasmedicamentos) {
                        $datos_titular['descripcion_alergias_medicamentos']    = $alergiasmedicamentos->descripcion;
                    }
                }
                //************ANTECEDENTES PATOLOGICOS FAMILIARES*******
                if (empty($antecedentes_patologicosf)) {
                    $datos_titular['descripcion_antecedentes_patologicosf'] = '';
                } else {
                    foreach ($antecedentes_patologicosf as $antecedentespatologicosf) {
                        $datos_titular['descripcion_antecedentes_patologicosf']  = $antecedentespatologicosf->descripcion;
                    }
                }
                //************ANTECEDENTES QUIRURGICOS*******
                if (empty($antecedentes_quirurgicos)) {
                    $datos_titular['descripcion_antecedentes_quirurgicos'] = '';
                } else {
                    foreach ($antecedentes_quirurgicos as $antecedentesquirurgicos) {
                        $datos_titular['descripcion_antecedentes_quirurgicos']  = $antecedentesquirurgicos->descripcion;
                    }
                }


                //************ANTECEDENTES PATOLOGICOS PERSONALES*******
                if (empty($antecedentes_patologicosp)) {
                    $datos_titular['descripcion_antecedentes_patologicosp'] = '';
                } else {
                    foreach ($antecedentes_patologicosp as $antecedentespatologicosp) {
                        $datos_titular['descripcion_antecedentes_patologicosp']  = $antecedentespatologicosp->descripcion;
                    }
                }

                //************EXAMEN FISICO*******
                if (empty($examen_fisico)) {
                    $datos_titular['descripcion_examen_fisico'] = '';
                } else {
                    foreach ($examen_fisico as $examenfisico) {
                        $datos_titular['descripcion_examen_fisico']  = $examenfisico->descripcion;
                    }
                }
                //************PARACLINICOS*******
                if (empty($paraclinicos)) {
                    $datos_titular['descripcion_paraclinicos'] = '';
                } else {
                    foreach ($paraclinicos as $paraclinicos) {
                        $datos_titular['descripcion_paraclinicos']  = $paraclinicos->descripcion;
                    }
                }


                //************IMPRESION DIAGNOSTICA*******
                if (empty($impresiondiagnostica)) {
                    $datos_titular['descripcion_impresiondiagnostica'] = '';
                } else {
                    foreach ($impresiondiagnostica as $impresiondiag) {
                        $datos_titular['descripcion_impresiondiagnostica']  = $impresiondiag->descripcion;
                    }
                }
                //************PLAN*******
                if (empty($plan)) {
                    $datos_titular['descripcion_plan'] = '';
                } else {
                    foreach ($plan as $plan) {
                        $datos_titular['descripcion_plan']  = $plan->descripcion;
                    }
                }
                //************SIGNOS VITALES*******

                if (empty($signosvitales)) {
                    $datos_titular['descripcion_plan'] = '';
                    $datos_titular['peso']         = '';
                    $datos_titular['talla']        = '';
                    $datos_titular['spo2']         = '';
                    $datos_titular['frecuencia_c'] = '';
                    $datos_titular['frecuencia_r'] = '';
                    $datos_titular['temperatura']  = '';
                    $datos_titular['tension_alta'] = '';
                    $datos_titular['tension_baja'] = '';
                    $datos_titular['fecha_creacion'] = '';
                    $datos_titular['motivo_consulta'] = '';
                    $datos_titular['imc']         = '';
                } else {
                    foreach ($signosvitales as $signos) {
                        $datos_titular['peso']         = $signos->peso;
                        $datos_titular['talla']        = $signos->talla;
                        $datos_titular['spo2']         = $signos->spo2;
                        $datos_titular['frecuencia_c'] = $signos->frecuencia_c;
                        $datos_titular['frecuencia_r'] = $signos->frecuencia_r;
                        $datos_titular['temperatura']  = $signos->temperatura;
                        $datos_titular['tension_alta'] = $signos->tension_alta;
                        $datos_titular['tension_baja'] = $signos->tension_baja;
                        $datos_titular['fecha_creacion'] = $signos->fecha_creacion;
                        $datos_titular['motivo_consulta'] = $signos->motivo_consulta;
                        $datos_titular['imc'] = $signos->imc;
                        $datos_titular['user_nombre'] = $signos->user_nombre;
                    }
                }


                //************ANTECEDENTES GINECO OBSTETRICO*******
                if (empty($atc_gineco_obste)) {

                    $datos_titular['menarquia']        = '0';
                    $datos_titular['sexarquia']         = '0';
                    $datos_titular['nps']         = '0';
                    $datos_titular['ciclo_mestrual'] = '';
                    $datos_titular['gestacion'] = '';
                    $datos_titular['partos']  = '';
                    $datos_titular['cesarias'] = '';
                    $datos_titular['abortos'] = '';
                    $datos_titular['f_u_r'] = '';
                    $datos_titular['edad_gestacional'] = '';
                    $datos_titular['dismenorrea'] = '';
                    $datos_titular['eumenorrea'] = '';
                    $datos_titular['anticonceptivo'] = '';
                    $datos_titular['tipo_anticonceptivo'] = '';
                    $datos_titular['detalles_anticon'] = '';
                    $datos_titular['t_colocacion'] = '';
                    $datos_titular['ets'] = '';
                    $datos_titular['tipo_ets'] = '';
                    $datos_titular['detalle_ets'] = '';
                    $datos_titular['citologia'] = '';
                    $datos_titular['eco_mamario'] = '';
                    $datos_titular['mamografia'] = '';
                    $datos_titular['desintometria'] = '';
                    $datos_titular['detalle_historia_obst'] = '';
                    $datos_titular['bandera_de_llenado'] = 'false';
                    $datos_titular['diu'] = '';
                    $datos_titular['t_cobre'] = '';
                    $datos_titular['mirena'] = '';
                    $datos_titular['aspiral'] = '';
                    $datos_titular['i_subdermico'] = '';
                    $datos_titular['otro'] = '';
                } else {
                    foreach ($atc_gineco_obste as $atcgisobst) {
                        $datos_titular['menarquia']         = $atcgisobst->menarquia;
                        $datos_titular['sexarquia']        = $atcgisobst->sexarquia;
                        $datos_titular['nps']         = $atcgisobst->nps;
                        $datos_titular['ciclo_mestrual'] = $atcgisobst->ciclo_mestrual;
                        $datos_titular['f_u_r'] = $atcgisobst->f_u_r;
                        $datos_titular['edad_gestacional'] = $atcgisobst->edad_gestacional;
                        $datos_titular['dismenorrea'] = $atcgisobst->dismenorrea;
                        $datos_titular['eumenorrea'] = $atcgisobst->eumenorrea;
                        $datos_titular['anticonceptivo'] = $atcgisobst->anticonceptivo;

                        $datos_titular['detalles_anticon'] = $atcgisobst->detalles_anticon;
                        $datos_titular['t_colocacion'] = $atcgisobst->t_colocacion;
                        $datos_titular['ets'] = $atcgisobst->ets;
                        $datos_titular['tipo_ets'] = $atcgisobst->tipo_ets;
                        $datos_titular['detalle_ets'] = $atcgisobst->detalle_ets;
                        $datos_titular['citologia'] = $atcgisobst->citologia;
                        $datos_titular['eco_mamario'] = $atcgisobst->eco_mamario;
                        $datos_titular['mamografia'] = $atcgisobst->mamografia;
                        $datos_titular['desintometria'] = $atcgisobst->desintometria;
                        $datos_titular['detalle_historia_obst'] = $atcgisobst->detalle_historia_obst;
                        $datos_titular['bandera_de_llenado'] = 'true';
                        $datos_titular['diu'] = $atcgisobst->diu;
                        $datos_titular['t_cobre'] = $atcgisobst->t_cobre;
                        $datos_titular['mirena'] = $atcgisobst->mirena;
                        $datos_titular['aspiral'] = $atcgisobst->aspiral;
                        $datos_titular['i_subdermico'] = $atcgisobst->i_subdermico;
                        $datos_titular['otro'] = $atcgisobst->otro;
                    }
                }

                //************PARTOS /CESARIAS/ABORTOS/GESTACION*******
                foreach ($query_gestacion as $fila) {
                    $datos_titular['gestacion'] = $fila->id_gestacion_numeros_romanos;
                }

                foreach ($query_partos as $fila) {
                    $datos_titular['partos'] = $fila->id_partos_numeros_romanos;
                }
                foreach ($query_cesarias as $fila) {
                    $datos_titular['cesarias'] = $fila->id_cesarias_numeros_romanos;
                }
                foreach ($query_abortos as $fila) {
                    $datos_titular['abortos'] = $fila->id_abortos_numeros_romanos;
                }


                foreach ($query as $fila) {
                    $datos_titular['cedula_trabajador']       = $fila->cedula_trabajador;
                    $datos_titular['nombre_titular']                  = $fila->nombre_titular;
                    $datos_titular['apellido_titular']                  = $fila->apellido_titular;
                    $datos_titular['telefonot'] = $fila->telefonot;
                    $datos_titular['cedula']       = $fila->cedula;
                    $datos_titular['nombre']                  = $fila->nombre;
                    $datos_titular['apellido']                  = $fila->apellido;
                    $datos_titular['departamento'] = $fila->departamento;
                    $datos_titular['fecha_nac_familiares'] = $fila->fecha_nac_familiares;
                    $datos_titular['edad_actual'] = $fila->edad_actual;
                    $datos_titular['sexo'] = $fila->sexo;
                    $datos_titular['telefono'] = $fila->telefono;
                    $datos_titular['tipo_beneficiario'] = $tipo_beneficiario;
                    $datos_titular['n_historial'] = $n_historial;
                    $datos_titular['especialidad'] = $id_especialidad;
                    $datos_titular['fecha_asistencia'] = $fecha_asistencia;
                    $datos_titular['sexot'] = $fila->sexot;
                    $datos_titular['edad_actualt'] = $fila->edad_actualt;
                    $datos_titular['fecha_nacimientot'] = $fila->fecha_nacimientot;
                    $datos_titular['tipo_de_personal'] = $fila->tipo_de_personal;
                    $datos_titular['descripcion_parentesco'] = $fila->descripcion_parentesco;
                    $datos_titular['ocupacion'] = $fila->ocupacion;
                    $datos_titular['tipo_de_sangref'] = $fila->tipodesangre;
                    $datos_titular['id_consulta'] = $id_consulta;
                    $datos_titular['id_historial_medico'] = $id_historial_medico;
                }

                //************IMPRESION DIAGNOSTICA*******
                if (empty($modelo_psicologia_impre_diag)) {
                    $datos_titular['psicologia_impre_diag'] = '';
                } else {
                    foreach ($modelo_psicologia_impre_diag as $impre_diag) {
                        $datos_titular['psicologia_impre_diag']  = $impre_diag->descripcion;
                    }
                }
                //************BUSCAR IMPRESION DIAGNOSTICA  PARA LA VISTA *******
                if (empty($query_impresion_diagnostica)) {
                    $datos_titular['impresion_diag'] = '';
                } else {
                    foreach ($query_impresion_diagnostica as $impre) {
                        $datos_titular['impresion_diag']         = $impre->descripcion;
                    }
                }

                echo view('/medicos/actualizar_Citas_Familiares', $datos_titular);
                echo view('/medicos/footer_actualizar_CitasFamiliares');
            }
        } elseif ($tipo_beneficiario == 'C') {
            $modelo = new Beneficiarios_model();
            $modelo_signosvitales = new Consulta_Histotia_Model();
            $modelo_enfermedad_actual = new Enfermedad_Actual_Model();
            $modelo_alergias_medicamentos = new Alergias_Medicamentos_Model();
            $modelo_antecedentes_patologicosf = new Antecendentes_Patologicosf_Model();
            $modelo_antecedentes_quirurgicos = new Antecedentes_Quirurgicos_Model();
            $modelo_antecedentes_patologicosp = new Antecendentes_Patologicosp_Model();
            $modelo_examen_fisico = new Examen_Fisico_Model();
            $modelo_paraclinicos = new  Paraclinicos_Model();
            $modelo_impresion_diag = new  Impresion_Diag_Model();
            $modelo_plan = new  Plan_Model();
            $modelo_atc_gineco_obste = new  Antece_Gineco_Obstetrico_Model();
            $modelo_abortos = new  Abortos_Model();
            $modelo_partos = new  Partos_Model();
            $modelo_cesarias = new  Cesarias_Model();
            $modelo_gestacion = new  Gestacion_Model();
            $query_abortos = $modelo_abortos->listar_abortos($n_historial, $id_consulta);
            $query_partos = $modelo_partos->listar_partos($n_historial, $id_consulta);
            $query_cesarias = $modelo_cesarias->listar_cesarias($n_historial, $id_consulta);
            $query_gestacion = $modelo_gestacion->listar_gestacion($n_historial, $id_consulta);
            $atc_gineco_obste = $modelo_atc_gineco_obste->listar_Antece_Gineco_obtetrico($n_historial, $id_consulta);
            $signosvitales = $modelo_signosvitales->listar_signos_vitales($n_historial, $id_consulta);
            $alergias_medicamentos = $modelo_alergias_medicamentos->listar_alergias_medicamentos($n_historial);
            $antecedentes_patologicosf = $modelo_antecedentes_patologicosf->antecedentes_patologicosf($n_historial);
            $antecedentes_quirurgicos = $modelo_antecedentes_quirurgicos->listar_antecedentes_quirurgicos($n_historial);
            $antecedentes_patologicosp = $modelo_antecedentes_patologicosp->antecedentes_patologicosp($n_historial);
            $examen_fisico = $modelo_examen_fisico->listar_examen_fisico($n_historial);
            $paraclinicos = $modelo_paraclinicos->listar_paraclinicos($n_historial);
            $impresiondiagnostica = $modelo_impresion_diag->listar_impresion_diag($n_historial);
            $plan = $modelo_plan->listar_plan($n_historial);
            $query = $modelo->getCortersia($cedula);
            $query_impresion_diagnostica = $modelo_impresion_diag->buscar_impresion_diag($n_historial);

            $modelo_psicologia_impre_diag = $modelo_enfermedad_actual->listar_psicologia_impre_diag($n_historial, $id_especialidad);
            if (empty($query)) {
                $datos_titular = [];
            } else {


                //***********ENFERMEDAD ACTUAL***********
                if (empty($enfermedad_actual)) {
                    $datos_titular['descripcion_enfermedada_actual']    = '';
                } else {
                    foreach ($enfermedad_actual as $enfermedadactual) {
                        $datos_titular['descripcion_enfermedada_actual']    = $enfermedadactual->descripcion;
                    }
                }
                //************ALERGIAS A MEDICAMENTOS*******
                if (empty($alergias_medicamentos)) {
                    $datos_titular['descripcion_alergias_medicamentos'] = '';
                } else {
                    foreach ($alergias_medicamentos as $alergiasmedicamentos) {
                        $datos_titular['descripcion_alergias_medicamentos']    = $alergiasmedicamentos->descripcion;
                    }
                }
                //************ANTECEDENTES PATOLOGICOS FAMILIARES*******
                if (empty($antecedentes_patologicosf)) {
                    $datos_titular['descripcion_antecedentes_patologicosf'] = '';
                } else {
                    foreach ($antecedentes_patologicosf as $antecedentespatologicosf) {
                        $datos_titular['descripcion_antecedentes_patologicosf']  = $antecedentespatologicosf->descripcion;
                    }
                }
                //************ANTECEDENTES QUIRURGICOS*******
                if (empty($antecedentes_quirurgicos)) {
                    $datos_titular['descripcion_antecedentes_quirurgicos'] = '';
                } else {
                    foreach ($antecedentes_quirurgicos as $antecedentesquirurgicos) {
                        $datos_titular['descripcion_antecedentes_quirurgicos']  = $antecedentesquirurgicos->descripcion;
                    }
                }


                //************ANTECEDENTES PATOLOGICOS PERSONALES*******
                if (empty($antecedentes_patologicosp)) {
                    $datos_titular['descripcion_antecedentes_patologicosp'] = '';
                } else {
                    foreach ($antecedentes_patologicosp as $antecedentespatologicosp) {
                        $datos_titular['descripcion_antecedentes_patologicosp']  = $antecedentespatologicosp->descripcion;
                    }
                }

                //************EXAMEN FISICO*******
                if (empty($examen_fisico)) {
                    $datos_titular['descripcion_examen_fisico'] = '';
                } else {
                    foreach ($examen_fisico as $examenfisico) {
                        $datos_titular['descripcion_examen_fisico']  = $examenfisico->descripcion;
                    }
                }
                //************PARACLINICOS*******
                if (empty($paraclinicos)) {
                    $datos_titular['descripcion_paraclinicos'] = '';
                } else {
                    foreach ($paraclinicos as $paraclinicos) {
                        $datos_titular['descripcion_paraclinicos']  = $paraclinicos->descripcion;
                    }
                }


                //************IMPRESION DIAGNOSTICA*******
                if (empty($impresiondiagnostica)) {
                    $datos_titular['descripcion_impresiondiagnostica'] = '';
                } else {
                    foreach ($impresiondiagnostica as $impresiondiag) {
                        $datos_titular['descripcion_impresiondiagnostica']  = $impresiondiag->descripcion;
                    }
                }
                //************PLAN*******
                if (empty($plan)) {
                    $datos_titular['descripcion_plan'] = '';
                } else {
                    foreach ($plan as $plan) {
                        $datos_titular['descripcion_plan']  = $plan->descripcion;
                    }
                }

                // ******************SIGNOS VITALES***************
                if (empty($signosvitales)) {
                    $datos_titular['descripcion_plan'] = '';
                    $datos_titular['peso']         = '';
                    $datos_titular['talla']        = '';
                    $datos_titular['spo2']         = '';
                    $datos_titular['frecuencia_c'] = '';
                    $datos_titular['frecuencia_r'] = '';
                    $datos_titular['temperatura']  = '';
                    $datos_titular['tension_alta'] = '';
                    $datos_titular['tension_baja'] = '';
                    $datos_titular['fecha_creacion'] = '';
                    $datos_titular['motivo_consulta'] = '';
                    $datos_titular['imc'] = '';
                } else {
                    foreach ($signosvitales as $signos) {
                        $datos_titular['peso']         = $signos->peso;
                        $datos_titular['talla']        = $signos->talla;
                        $datos_titular['spo2']         = $signos->spo2;
                        $datos_titular['frecuencia_c'] = $signos->frecuencia_c;
                        $datos_titular['frecuencia_r'] = $signos->frecuencia_r;
                        $datos_titular['temperatura']  = $signos->temperatura;
                        $datos_titular['tension_alta'] = $signos->tension_alta;
                        $datos_titular['tension_baja'] = $signos->tension_baja;
                        $datos_titular['fecha_creacion'] = $signos->fecha_creacion;
                        $datos_titular['motivo_consulta'] = $signos->motivo_consulta;
                        $datos_titular['imc'] = $signos->imc;
                        $datos_titular['user_nombre'] = $signos->user_nombre;
                    }
                }


                //************ANTECEDENTES GINECO OBSTETRICO*******
                if (empty($atc_gineco_obste)) {

                    $datos_titular['menarquia']        = '0';
                    $datos_titular['sexarquia']         = '0';
                    $datos_titular['nps']         = '0';
                    $datos_titular['ciclo_mestrual'] = '';
                    $datos_titular['gestacion'] = '';
                    $datos_titular['partos']  = '';
                    $datos_titular['cesarias'] = '';
                    $datos_titular['abortos'] = '';
                    $datos_titular['f_u_r'] = '';
                    $datos_titular['edad_gestacional'] = '';
                    $datos_titular['dismenorrea'] = '';
                    $datos_titular['eumenorrea'] = '';
                    $datos_titular['anticonceptivo'] = '';
                    $datos_titular['tipo_anticonceptivo'] = '';
                    $datos_titular['detalles_anticon'] = '';
                    $datos_titular['t_colocacion'] = '';
                    $datos_titular['ets'] = '';
                    $datos_titular['tipo_ets'] = '';
                    $datos_titular['detalle_ets'] = '';
                    $datos_titular['citologia'] = '';
                    $datos_titular['eco_mamario'] = '';
                    $datos_titular['mamografia'] = '';
                    $datos_titular['desintometria'] = '';
                    $datos_titular['detalle_historia_obst'] = '';
                    $datos_titular['bandera_de_llenado'] = 'false';
                    $datos_titular['diu'] = '';
                    $datos_titular['t_cobre'] = '';
                    $datos_titular['mirena'] = '';
                    $datos_titular['aspiral'] = '';
                    $datos_titular['i_subdermico'] = '';
                    $datos_titular['otro'] = '';
                } else {
                    foreach ($atc_gineco_obste as $atcgisobst) {
                        $datos_titular['menarquia']         = $atcgisobst->menarquia;
                        $datos_titular['sexarquia']        = $atcgisobst->sexarquia;
                        $datos_titular['nps']         = $atcgisobst->nps;
                        $datos_titular['ciclo_mestrual'] = $atcgisobst->ciclo_mestrual;
                        $datos_titular['f_u_r'] = $atcgisobst->f_u_r;
                        $datos_titular['edad_gestacional'] = $atcgisobst->edad_gestacional;
                        $datos_titular['dismenorrea'] = $atcgisobst->dismenorrea;
                        $datos_titular['eumenorrea'] = $atcgisobst->eumenorrea;
                        $datos_titular['anticonceptivo'] = $atcgisobst->anticonceptivo;

                        $datos_titular['detalles_anticon'] = $atcgisobst->detalles_anticon;
                        $datos_titular['t_colocacion'] = $atcgisobst->t_colocacion;
                        $datos_titular['ets'] = $atcgisobst->ets;
                        $datos_titular['tipo_ets'] = $atcgisobst->tipo_ets;
                        $datos_titular['detalle_ets'] = $atcgisobst->detalle_ets;
                        $datos_titular['citologia'] = $atcgisobst->citologia;
                        $datos_titular['eco_mamario'] = $atcgisobst->eco_mamario;
                        $datos_titular['mamografia'] = $atcgisobst->mamografia;
                        $datos_titular['desintometria'] = $atcgisobst->desintometria;
                        $datos_titular['detalle_historia_obst'] = $atcgisobst->detalle_historia_obst;
                        $datos_titular['bandera_de_llenado'] = 'true';
                        $datos_titular['diu'] = $atcgisobst->diu;
                        $datos_titular['t_cobre'] = $atcgisobst->t_cobre;
                        $datos_titular['mirena'] = $atcgisobst->mirena;
                        $datos_titular['aspiral'] = $atcgisobst->aspiral;
                        $datos_titular['i_subdermico'] = $atcgisobst->i_subdermico;
                        $datos_titular['otro'] = $atcgisobst->otro;
                    }
                }
                //************PARTOS /CESARIAS/ABORTOS/GESTACION*******
                foreach ($query_gestacion as $fila) {
                    $datos_titular['gestacion'] = $fila->id_gestacion_numeros_romanos;
                }

                foreach ($query_partos as $fila) {
                    $datos_titular['partos'] = $fila->id_partos_numeros_romanos;
                }
                foreach ($query_cesarias as $fila) {
                    $datos_titular['cesarias'] = $fila->id_cesarias_numeros_romanos;
                }
                foreach ($query_abortos as $fila) {
                    $datos_titular['abortos'] = $fila->id_abortos_numeros_romanos;
                }

                foreach ($query as $fila) {
                    $datos_titular['cedula_trabajador']       = $fila->cedula_trabajador;
                    $datos_titular['nombre_titular']                  = $fila->nombre_titular;
                    $datos_titular['apellido_titular']                  = $fila->apellido_titular;
                    $datos_titular['telefonot'] = $fila->telefonot;
                    $datos_titular['cedula']       = $fila->cedula;
                    $datos_titular['nombre']                  = $fila->nombre;
                    $datos_titular['apellido']                  = $fila->apellido;
                    $datos_titular['departamento'] = $fila->departamento;
                    $datos_titular['fecha_nac_cortesia'] = $fila->fecha_nac_cortesia;
                    $datos_titular['edad_actual'] = $fila->edad_actual;
                    $datos_titular['sexo'] = $fila->sexo;
                    $datos_titular['telefono'] = $fila->telefono;
                    $datos_titular['tipo_beneficiario'] = $tipo_beneficiario;
                    $datos_titular['n_historial'] = $n_historial;
                    $datos_titular['especialidad'] = $id_especialidad;
                    $datos_titular['fecha_asistencia'] = $fecha_asistencia;
                    $datos_titular['tipo_de_personal'] = $fila->tipo_de_personal;
                    $datos_titular['sexot'] = $fila->sexot;
                    $datos_titular['edad_actualt'] = $fila->edad_actualt;
                    $datos_titular['fecha_nacimientot'] = $fila->fecha_nacimientot;
                    $datos_titular['ocupacion'] = $fila->descripcion_ocupacion;
                    $datos_titular['id_consulta'] = $id_consulta;
                    $datos_titular['id_historial_medico'] = $id_historial_medico;
                    $datos_titular['tipo_de_sangrec'] = $fila->tipodesangre;
                }
                //************IMPRESION DIAGNOSTICA*******
                if (empty($modelo_psicologia_impre_diag)) {
                    $datos_titular['psicologia_impre_diag'] = '';
                } else {
                    foreach ($modelo_psicologia_impre_diag as $impre_diag) {
                        $datos_titular['psicologia_impre_diag']  = $impre_diag->descripcion;
                    }
                }
                //************BUSCAR IMPRESION DIAGNOSTICA  PARA LA VISTA *******
                if (empty($query_impresion_diagnostica)) {
                    $datos_titular['impresion_diag'] = '';
                } else {
                    foreach ($query_impresion_diagnostica as $impre) {
                        $datos_titular['impresion_diag']         = $impre->descripcion;
                    }
                }
                echo view('/medicos/actualizar_CitasCortesia', $datos_titular);
                echo view('/medicos/footer_actualizar_CitasCortesia');
            }
        }
    }



    public function vista_Citas($cedula, $tipo_beneficiario, $n_historial)
    {
        if (!session('nombreUsuario')) {
            return redirect()->to(base_url() . '/index.php');
        }
        $modelo = new Beneficiarios_model();
        $modelo_historia = new HistorialModel();

        
        $query = $modelo->getAll($cedula);
        if (empty($query)) {
            $datos_titular = [];
        } else {
            foreach ($query as $fila) {
                $datos_titular['cedula_trabajador']       = $fila->cedula_trabajador;
                $datos_titular['nombre']                  = $fila->nombre;
                $datos_titular['apellido']                  = $fila->apellido;
                $datos_titular['ubicacion_administrativa'] = $fila->ubicacion_administrativa;
                $datos_titular['fecha_nacimiento'] = $fila->fecha_nacimiento;
                $datos_titular['edad_actual'] = $fila->edad_actual;
                $datos_titular['sexo'] = $fila->sexo;
                $datos_titular['estatus'] = $fila->estatus;
                $datos_titular['telefono'] = $fila->telefono;
                $datos_titular['tipo_beneficiario'] = $tipo_beneficiario;
                $datos_titular['n_historial'] = $n_historial;
            }
            $query_historia = $modelo_historia->getHistorial_inactivo($cedula,$n_historial);
            foreach ($query_historia as $fila) {
                $datos_titular['borrado']       = $fila->borrado;
           }


    
            echo view('/historia_clinica/citas/vista_citas', $datos_titular);
            echo view('/historia_clinica/citas/footer_vista_citas');
        }
    }



    public function vista_ConsultasFamiliares($cedula, $tipo_beneficiario, $n_historial)
    {
        if (!session('nombreUsuario')) {
            return redirect()->to(base_url() . '/index.php');
        }
        $modelo = new Beneficiarios_model();
        $query = $modelo->getFamiliar($cedula);
        $modelo_historia = new HistorialModel();
        if (empty($query)) {
            $datos_titular = [];
        } else {
            foreach ($query as $fila) {
                $datos_titular['cedula_trabajador']       = $fila->cedula_trabajador;
                $datos_titular['nombre_titular']                  = $fila->nombre_titular;
                $datos_titular['apellido_titular']                  = $fila->apellido_titular;
                $datos_titular['cedula']       = $fila->cedula;
                $datos_titular['nombre']                  = $fila->nombre;
                $datos_titular['apellido']                  = $fila->apellido;
                $datos_titular['departamento'] = $fila->departamento;
                $datos_titular['fecha_nac_familiares'] = $fila->fecha_nac_familiares;
                $datos_titular['edad_actual'] = $fila->edad_actual;
                $datos_titular['sexo'] = $fila->sexo;
                $datos_titular['telefono'] = $fila->telefono;
                $datos_titular['tipo_beneficiario'] = $tipo_beneficiario;
                $datos_titular['n_historial'] = $n_historial;
                $datos_titular['estatus'] = $fila->borrado;
            }


            $query_historia = $modelo_historia->getHistorial_inactivo($cedula,$n_historial);
            foreach ($query_historia as $fila) {
                $datos_titular['borrado']       = $fila->borrado;
           }


            echo view('/historia_clinica/vista_ConsultasFamiliares', $datos_titular);
            echo view('/historia_clinica/footer_vista_consultasFamiliares');
        }
    }

    public function vista_Citas_Familiares($cedula, $tipo_beneficiario, $n_historial)
    {
        if (!session('nombreUsuario')) {
            return redirect()->to(base_url() . '/index.php');
        }
        $modelo = new Beneficiarios_model();
        $query = $modelo->getFamiliar($cedula);
        $modelo_historia = new HistorialModel();
        if (empty($query)) {
            $datos_titular = [];
        } else {
            foreach ($query as $fila) {
                $datos_titular['cedula_trabajador']       = $fila->cedula_trabajador;
                $datos_titular['nombre_titular']                  = $fila->nombre_titular;
                $datos_titular['apellido_titular']                  = $fila->apellido_titular;
                $datos_titular['cedula']       = $fila->cedula;
                $datos_titular['nombre']                  = $fila->nombre;
                $datos_titular['apellido']                  = $fila->apellido;
                $datos_titular['departamento'] = $fila->departamento;
                $datos_titular['fecha_nac_familiares'] = $fila->fecha_nac_familiares;
                $datos_titular['edad_actual'] = $fila->edad_actual;
                $datos_titular['sexo'] = $fila->sexo;
                $datos_titular['estatus'] = $fila->borrado;
                $datos_titular['telefono'] = $fila->telefono;
                $datos_titular['tipo_beneficiario'] = $tipo_beneficiario;
                $datos_titular['n_historial'] = $n_historial;
            }

            $query_historia = $modelo_historia->getHistorial_inactivo($cedula,$n_historial);
            foreach ($query_historia as $fila) {
                $datos_titular['borrado']       = $fila->borrado;
           }

            echo view('/historia_clinica/citas/vista_Citas_Familiares', $datos_titular);
            echo view('/historia_clinica/citas/footer_vista_CitasFamiliares');
        }
    }








    public function vista_ConsultasCortesia($cedula, $tipo_beneficiario, $n_historial)
    {
        if (!session('nombreUsuario')) {
            return redirect()->to(base_url() . '/index.php');
        }
        $modelo = new Beneficiarios_model();
        $modelo_historia = new HistorialModel();
        $query = $modelo->getCortersia($cedula);
        if (empty($query)) {
            $datos_titular = [];
        } else {
            foreach ($query as $fila) {
                $datos_titular['cedula_trabajador']       = $fila->cedula_trabajador;
                $datos_titular['nombre_titular']                  = $fila->nombre_titular;
                $datos_titular['apellido_titular']                  = $fila->apellido_titular;
                $datos_titular['cedula']       = $fila->cedula;
                $datos_titular['nombre']                  = $fila->nombre;
                $datos_titular['apellido']                  = $fila->apellido;
                $datos_titular['departamento'] = $fila->departamento;
                $datos_titular['fecha_nac_cortesia'] = $fila->fecha_nac_cortesia;
                $datos_titular['edad_actual'] = $fila->edad_actual;
                $datos_titular['sexo'] = $fila->sexo;
                $datos_titular['telefono'] = $fila->telefono;
                $datos_titular['tipo_beneficiario'] = $tipo_beneficiario;
                $datos_titular['n_historial'] = $n_historial;
                $datos_titular['estatus'] = $fila->borrado;
            }
            $query_historia = $modelo_historia->getHistorial_inactivo($cedula,$n_historial);
            foreach ($query_historia as $fila) {
                $datos_titular['borrado']       = $fila->borrado;
           }
          
            echo view('/historia_clinica/vista_ConsultasCortesia', $datos_titular);
            echo view('/historia_clinica/footer_vista_consultasCortesia');
        }
    }
    public function   vista_Citas_Cortesia($cedula, $tipo_beneficiario, $n_historial)
    {
        if (!session('nombreUsuario')) {
            return redirect()->to(base_url() . '/index.php');
        }
        $modelo = new Beneficiarios_model();
        $modelo_historia = new HistorialModel();
        $query = $modelo->getCortersia($cedula);
        if (empty($query)) {
            $datos_titular = [];
        } else {
            foreach ($query as $fila) {
                $datos_titular['cedula_trabajador']       = $fila->cedula_trabajador;
                $datos_titular['nombre_titular']                  = $fila->nombre_titular;
                $datos_titular['apellido_titular']                  = $fila->apellido_titular;
                $datos_titular['cedula']       = $fila->cedula;
                $datos_titular['nombre']                  = $fila->nombre;
                $datos_titular['estatus'] = $fila->borrado;
                $datos_titular['apellido']                  = $fila->apellido;
                $datos_titular['departamento'] = $fila->departamento;
                $datos_titular['fecha_nac_cortesia'] = $fila->fecha_nac_cortesia;
                $datos_titular['edad_actual'] = $fila->edad_actual;
                $datos_titular['sexo'] = $fila->sexo;
                $datos_titular['telefono'] = $fila->telefono;
                $datos_titular['tipo_beneficiario'] = $tipo_beneficiario;
                $datos_titular['n_historial'] = $n_historial;
            }
            $query_historia = $modelo_historia->getHistorial_inactivo($cedula, $n_historial);
            

            foreach ($query_historia as $fila) {
                 $datos_titular['borrado']       = $fila->borrado;
            }
            echo view('/historia_clinica/citas/vista_CitasCortesia', $datos_titular);
            echo view('/historia_clinica/citas/footer_vista_CitasCortesia');
        }
    }




  





    public function actualizar_asistencia()
    {
        if (!session('nombreUsuario')) {
            return redirect()->to(base_url() . '/index.php');
        }
        $modelo = new  Consulta_Histotia_Model();
        $data = json_decode(base64_decode($this->request->getPost('data')));
        $datos['id']   = $data->consulta_id;
        $datos['asistencia'] = $data->asistencia;
        $query = $modelo->actualizar_asistencia($datos);
        if (isset($query)) {
            $mensaje = 1;
        } else {
            $mensaje = 0;
        }
        //$mensaje=$datos;
        return json_encode($mensaje);
    }






    public function listar_Historial_consultas($n_historial)
    {
        
        $model = new Consulta_Histotia_Model();
        $query = $model->listar_Historial_consultas($n_historial);
       

        if (empty($query)) {
            $consultas = [];
        } else {
            $consultas = $query->getResultArray();
        }
        echo json_encode($consultas);
    }






    public function listar_citas_medicos($medico_id)
    {
        // echo($medico_id);
        //die();
        $model = new Consulta_Histotia_Model();
        $query = $model->listar_citas_medicos($medico_id);
        //var_dump($query);
        //die();
        if (empty($query)) {
            $citas = [];
        } else {
            $citas = $query;
        }

        echo json_encode($citas);
    }

    public function listar_consultas_medicos($medico_id)
    {
        $model = new Consulta_Histotia_Model();
        $query = $model->listar_consultas_medicos($medico_id);
        if (empty($query)) {
            $consultas = [];
        } else {
            $consultas = $query;
        }

        echo json_encode($consultas);
    }



    public function listar_Historial_citas($n_historial)
    {
        $model = new Consulta_Histotia_Model();
        $query = $model->listar_Historial_citas($n_historial);

        if (empty($query)) {
            $citas = [];
        } else {
            $citas = $query->getResultArray();
        }
        echo json_encode($citas);
    }




    public function agregar_consulta_historial()
    {

        $model = new Consulta_Histotia_Model();
        $model_beneficiarios = new Beneficiarios_Model();
        $model_auditoria = new Auditoria_sistema_Model();
        $data = json_decode(base64_decode($this->request->getPost('data')));
        $datos['n_historial']           = $data->n_historial;
        $datos['id_medico']             = $data->id_medico;
        $datos['peso']                  = $data->peso;
        $datos['talla']                 = $data->talla;
        $datos['spo2']                  = $data->spo2;
        $datos['frecuencia_c']          = $data->frecuencia_c;
        $datos['frecuencia_r']          = $data->frecuencia_r;
        $datos['temperatura']           = $data->temperatura;
        $datos['tension_alta']          = $data->ta_alta;
        $datos['tension_baja']          = $data->ta_baja;
        $datos['fecha_asistencia']      = $data->fechaconvertida;
        $datos['asistencia']            = 'true';
        $datos['consulta']              = 'true';
        $datos['tipo_beneficiario']     = $data->tipo_beneficiario;
        $datos['motivo_consulta']       = $data->motivo_consulta;
        $datos['imc']   = $data->imc;
        $datos['user_id']       = $data->user_id;
        $cedula_beneficiario['cedula']       = $data->cedula;
        $cedula_string = (string) $cedula_beneficiario['cedula'];
        $query = $model->agregar_consulta_historial($datos);
        if (isset($query)) {
            $query_beneficiarios = $model_beneficiarios->buscar_datos_beneficiarios($cedula_string);
            foreach ($query_beneficiarios as $fila) 
            {
                $nombre['nombre'] = $fila->nombre;
            }	
            $mensaje = 1;
            $auditoria['accion'] = 'REGISTRO UNA CONSUTA PARA '.''.$nombre['nombre'];
			$Auditoria_sistema_Model = $model_auditoria->agregar($auditoria);

        } else {
            $mensaje = 0;
        }
       return json_encode($mensaje);
        
    }

    public function agregar_cita_historial()
    {
        $model_beneficiarios = new Beneficiarios_Model();
        $model_auditoria = new Auditoria_sistema_Model();
        $model = new Consulta_Histotia_Model();
        $data = json_decode(base64_decode($this->request->getPost('data')));
        $datos['n_historial']   = $data->n_historial;
        $datos['id_medico']   = $data->id_medico;
        $datos['peso']   = $data->peso;
        $datos['talla'] = $data->talla;
        $datos['spo2'] = $data->spo2;
        $datos['frecuencia_c']   = $data->frecuencia_c;
        $datos['frecuencia_r']   = $data->frecuencia_r;
        $datos['temperatura']   = $data->temperatura;
        $datos['tension_alta']   = $data->ta_alta;
        $datos['tension_baja']   = $data->ta_baja;
        $datos['fecha_asistencia']   = $data->fecha_asistencia;
        $datos['consulta']   = 'false';
        $datos['tipo_beneficiario']   = $data->tipo_beneficiario;
        $datos['imc']   = $data->imc;
        $datos['user_id']       = $data->user_id;
        $datos['motivo_consulta']       = $data->motivo_consulta;
        $cedula_beneficiario['cedula']       = $data->cedula;
        $cedula_string = (string) $cedula_beneficiario['cedula'];
        $query = $model->agregar_consulta_historial($datos);
        if (isset($query)) {
            $query_beneficiarios = $model_beneficiarios->buscar_datos_beneficiarios($cedula_string);
            foreach ($query_beneficiarios as $fila) 
            {
                $nombre['nombre'] = $fila->nombre;
            }	
            $mensaje = 1;
            $auditoria['accion'] = 'REGISTRO UNA CITA PARA '.''.$nombre['nombre'];
			$Auditoria_sistema_Model = $model_auditoria->agregar($auditoria);
        } else {
            $mensaje = 0;
        }
        return json_encode($mensaje);
    }


    public function borrar_consulta()
    {
        if (!session('nombreUsuario')) {
            return redirect()->to(base_url() . '/index.php');
        }
        $modelo = new  Consulta_Histotia_Model();
        $model_auditoria = new Auditoria_sistema_Model();
        $data = json_decode(base64_decode($this->request->getPost('data')));
        $datos['id']   = $data->id;
        $datos['borrado'] = $data->borrado;
        $datos_nombre['datos_nombre'] = $data->nombre;
        $datos_cedula['datos_cedula'] = $data->cedula;
        $datos_control['datos_control'] = $data->control;
        $query = $modelo->borrar_consulta($datos);
        if (isset($query)) {
            $mensaje = 1;
            if ($datos_control['datos_control']=='Consulta') 
            {
                $auditoria['accion'] = 'ELIMINO LA CONSULTA Nª '.''.$datos['id'].' '.' PARA '.''.$datos_nombre['datos_nombre'].' '.' '.'C.I :'.''.$datos_cedula['datos_cedula'];
			    $Auditoria_sistema_Model = $model_auditoria->agregar($auditoria);
            }else
            {
                $auditoria['accion'] = 'ELIMINO LA CITA Nª '.''.$datos['id'].' '.' PARA '.''.$datos_nombre['datos_nombre'].' '.' '.'C.I :'.''.$datos_cedula['datos_cedula'];
			    $Auditoria_sistema_Model = $model_auditoria->agregar($auditoria);
            }
           
        } else {
            $mensaje = 0;
        }
        $mensaje = $datos;
        return json_encode($mensaje);
    }







    public function actualizar_signos_vitales_citas()
    {

        $model = new Consulta_Histotia_Model();
        $data = json_decode(base64_decode($this->request->getPost('data')));
        $datos['id']   = $data->id_consulta;
        $datos['peso']   = $data->peso;
        $datos['talla'] = $data->talla;
        $datos['spo2'] = $data->spo2;
        $datos['frecuencia_c']   = $data->frecuencia_c;
        $datos['frecuencia_r']   = $data->frecuencia_r;
        $datos['temperatura']   = $data->temperatura;
        $datos['tension_alta']   = $data->ta_alta;
        $datos['tension_baja']   = $data->ta_baja;
        $datos['imc']   = $data->imc;
        $datos['user_id'] = $data->user_id;
        $datos['motivo_consulta'] = $data->motivo_consulta;
        $query = $model->actualizar_signos_vitales_citas($datos);

        if (isset($query)) {
            $mensaje = 1;
        } else {
            $mensaje = 0;
        }
        return json_encode($mensaje);
    }




    public function  datos_titular($cedula_trabajador)
    {

        $modelo = new Beneficiarios_model();
        $query = $modelo->getAll($cedula_trabajador);
        if (empty($query)) {
            $datos_titular = [];
        } else {
            foreach ($query as $fila) {
                $datos_titular['cedula_trabajador']       = $fila->cedula_trabajador;
                $datos_titular['nombre']                  = $fila->nombre;
                $datos_titular['apellido']                  = $fila->apellido;
                $datos_titular['ubicacion_administrativa'] = $fila->ubicacion_administrativa;
                $datos_titular['fecha_nacimiento'] = $this->formatearFecha($fila->fecha_nacimiento);
            }
        }
        echo view('/historia_clinica/vista_historial', $datos_titular);
        echo view('/historia_clinica/footer_vista_historial');
    }


    public function vista_habitos($n_historial, $cedula, $historial_id)
    {

        $modelo = new Beneficiarios_model();
        $query = $modelo->getAll($cedula);
        if (empty($query)) {
            $datos_titular = [];
        } else {
            foreach ($query as $fila) {
                $datos_titular['cedula_trabajador']       = $fila->cedula_trabajador;
                $datos_titular['nombre']                  = $fila->nombre;
                $datos_titular['apellido']                  = $fila->apellido;
                $datos_titular['ubicacion_administrativa'] = $fila->ubicacion_administrativa;
                $datos_titular['fecha_nacimiento'] = $this->formatearFecha($fila->fecha_nacimiento);
                $datos_titular['edad_actual'] = $fila->edad_actual;
                $datos_titular['historial_id'] = $historial_id;
                $datos_titular['n_historial'] = $n_historial;
            }

            echo view('/historia_clinica/vista_habitos', $datos_titular);
            echo view('/historia_clinica/footer_vista_habitos');
        }
    }



    public function agregar_historial_medico($cedulabeneficiario)
    {
        $modelo = new  HistorialModel();
        $query = $modelo->getHistorial($cedulabeneficiario);
        if (empty($query)) {
            $data = json_decode(base64_decode($this->request->getPost('data')));
            $datos['n_historial']   = $data->numero_historial;
            $datos['cedula']   = $data->cedula;
            $datos['fecha_regist'] = $this->formatearFecha($data->fecha_registro);
            $query = $modelo->agregar_historial_medico($datos);
            if (isset($query)) {
                $mensaje = 1;
            } else {
                $mensaje = 0;
            }
        } else {
            $mensaje = 2;
        }

        // $model = new HistorialModel();

        return json_encode($mensaje);
    }

    public function borrar_historial()
    {

        $modelo = new  HistorialModel();
        $data = json_decode(base64_decode($this->request->getPost('data')));
        $datos['id']   = $data->historial_id;
        $datos['borrado'] = $data->borrado;
        $query = $modelo->borrar_historial($datos);
        if (isset($query)) {
            $mensaje = 1;
        } else {
            $mensaje = 0;
        }
        //$mensaje=$datos;
        return json_encode($mensaje);
    }






    public function  morbilidad()
    {
        return view('historia_clinica/morbilidad');
    }


    public function listar_psicologia_primaria()
    {
        $model = new Consulta_Histotia_Model();
        $query = $model->listar_psicologia_primaria();
        if (empty($query->getResult())) {
            $psicologia = [];
        } else {
            $psicologia = $query->getResultArray();
        }
        echo json_encode($psicologia);
    }


    public function reporte_citas()
    {
        if (!session('nombreUsuario')) {
            return redirect()->to(base_url() . '/index.php');
        }
        echo view('/reportes/reporte_citas');
        echo view('/reportes/footer_reportes_citas');
    }


    public function  listar_reporte_citas($desde = null, $hasta = null, $medico = 0, $especialidad = 0, $control = 0, $asistio = 0, $today = null)
    {
        //$desde=formatearFecha($desde);
        $model = new Consulta_Histotia_Model();
        $query = $model->listar_reporte_citas($desde, $hasta, $medico, $especialidad, $control, $asistio, $today);
      
        if (empty($query)) {
            $consultas = [];
        } else {
            $consultas = $query;
        }

        echo json_encode($consultas);
    }
    public function listar_consultas_atendidas($medico_id)
    {
        $model = new Consulta_Histotia_Model();
        $query = $model->listar_consultas_atendidas($medico_id);

        if (empty($query)) {
            $consultas = [];
        } else {
            $consultas = $query;
        }

        echo json_encode($consultas);
    }
    public function listar_consultas_not_atendidas($medico_id)
    {
        $model = new Consulta_Histotia_Model();
        $query = $model->listar_consultas_not_atendidas($medico_id);

        if (empty($query)) {
            $consultas = [];
        } else {
            $consultas = $query;
        }

        echo json_encode($consultas);
    }
    public function listar_citas_atendidas($medico_id)
    {
        $model = new Consulta_Histotia_Model();
        $query = $model->listar_citas_atendidas($medico_id);

        if (empty($query)) {
            $citas = [];
        } else {
            $citas = $query;
        }

        echo json_encode($citas);
    }
    public function listar_citas_not_atendidas($medico_id)
    {
        $model = new Consulta_Histotia_Model();
        $query = $model->listar_citas_not_atendidas($medico_id);

        if (empty($query)) {
            $citas = [];
        } else {
            $citas = $query;
        }

        echo json_encode($citas);
    }
}
