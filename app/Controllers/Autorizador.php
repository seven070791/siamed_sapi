<?php

namespace App\Controllers;

use App\Models\Autorizador_Model;
use CodeIgniter\API\ResponseTrait;
use CodeIgniter\RESTful\ResourceController;
use App\Models\Auditoria_sistema_Model;


class Autorizador extends BaseController
{
	use ResponseTrait;
	public function index()
	{
		return ('Esta es la Página de tipo de Medicamentos ...');
	}
	public function vistaAutorizador()
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		echo view('/listarAutorizador/content_A');
		echo view('/listarAutorizador/footer_A');
	}
	/*
      * Función parar cargar los registros del Módulo en el Data Table o en las Persianas
      */
	public function getAll()
	{
		$model = new Autorizador_Model();
		$query = $model->getAll();
		
		if (empty($query->getResult())) {
			$autorizador = [];
		} else {
			$autorizador = $query->getResultArray();
		}
		echo json_encode($autorizador);
	}
	/*
      * Método que guarda el registro nuevo
      */
	//public function save()

	

	public function agregar()
	{

		$model = new Autorizador_Model();
		$model_Auditoria_sistema_Model = new Auditoria_sistema_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$query_buscar_ultimo_id = $model->buscar_ultimo_id();
		foreach ($query_buscar_ultimo_id as $fila) 
		{
			$verificador_id['id'] = $fila->max;
		}	
		//Insertamos el nuevo registro	
		if ($verificador_id['id']==null) {
			$datos['cedula']   = $data->cedula;
			$datos['nombre']   = $data->nombre;
			$datos['apellido']   = $data->apellido;
			$datos['fecha_desde'] = $this->formatearFecha($data->fecha_desde);
			$query = $model->agregar($datos);
			if(isset($query))
			{
			/// REGISTRO EN AUDITORIA LA CREACION DE LA NOTA DE ENTREGA 
			$auditoria['accion']   = 'SE REGISTRO UN NUEVO AUTORIZADOR :  ' . '  ' . '  ' . $data->nombre . $data->apellido;
			$Auditoria_sistema_Model = $model_Auditoria_sistema_Model->agregar($auditoria);
			}
		}else{
			//hacemos un update de las vigencias a falso 
			$query_vigencia = $model->cambiar_vigencia_autorizador();
			$datos_fecha = $this->formatearFecha($data->fecha_desde);
			$datos_id['fecha_desde'] = date("Y-m-d", strtotime($datos_fecha . " -1 days"));
			$datos_id['id']=$fila->max;
			// hacemos un update al la fecha hasta del registro anterior
			


			$query_update_fecha_hasta = $model->update_fecha_hasta($datos_id);
			//Insertamos el nuevo registro
			$datos['cedula']   = $data->cedula;
			$datos['nombre']   = $data->nombre;
			$datos['apellido']   = $data->apellido;
			$datos['fecha_desde']   = $this->formatearFecha($data->fecha_desde);
			$query = $model->agregar($datos);	
			/// REGISTRO EN AUDITORIA LA CREACION DE LA NOTA DE ENTREGA 
			$auditoria['accion']   = 'SE REGISTRO UN NUEVO AUTORIZADOR :  ' . '  ' . '  ' . $data->nombre . $data->apellido;
			$Auditoria_sistema_Model = $model_Auditoria_sistema_Model->agregar($auditoria);
		}
		if (isset($query)) {
			$mensaje = 1;
		} else {
			$mensaje = 0;
		}
		return json_encode($mensaje);
	}
	/*
      * Función para obtener los datos de un Rol
      */
	public function getDatosRol()
	{
		if ($this->request->isAJAX()) {
			$data = json_decode(base64_decode($this->request->getGet('data')));
			$datos['id'] = $data->aide;
			$modelo = new Autorizador_Model();
			$query = $modelo->getDatosRol($datos['id']);
			$respuesta = [];
			if (empty($query->getResult())) {
				$respuesta[] = '0';
			} else {
				foreach ($query->getResult() as $fila) {
					$respuesta['id']      = $fila->id;
					$respuesta['rol']     = $fila->rol;
					$respuesta['activo']  = $fila->activo;
				}
			}
		} else {
			redirect()->to('/403');
		}
		return json_encode($respuesta);
	}
	/*
      * Método que actualiza el registro
      */
	public function actualizar()
	{
		$modelo = new Autorizador_Model();
		$model_auditoria = new Auditoria_sistema_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		if ($data->vigencia=='true' ) {
			$query_vigencia = $modelo->cambiar_vigencia_autorizador();
			$datos['id']   = $data->id;
			$datos['cedula']   = $data->cedula;
			$datos['nombre']   = $data->nombre;
			$datos['apellido']   = $data->apellido;
			$datos['fecha_desde'] = $this->formatearFecha($data->fecha_desde);
			$datos['vigencia']       = $data->vigencia;
			$datos_aut['Autorizador']   = $data->nombre_anterior.' '.$data->apellido_anterior;
			$datos_modificados['datos_modificados']       = $data->datos_modificados;
			$datos_modificados['datos_modificados'] = strtoupper($datos_modificados['datos_modificados']);
			$query = $modelo->actualizar($datos);
			if (isset($query)) {
				$mensaje = 1;
            	$auditoria['accion'] = 'SE MODIFICARON LOS SIGUENTES DATOS DEL AUTORIZADOR '.' '.$datos_aut['Autorizador'].','.' '.$datos_modificados['datos_modificados'];
				$Auditoria_sistema_Model = $model_auditoria->agregar($auditoria);
			} else {
				$mensaje = 0;
			}
		}else
		{
			$datos['id']   = $data->id;
			$datos['cedula']   = $data->cedula;
			$datos['nombre']   = $data->nombre;
			$datos['apellido']   = $data->apellido;
			$datos['fecha_desde'] = $this->formatearFecha($data->fecha_desde);
			$datos['vigencia']       = $data->vigencia;
			$datos_modificados['datos_modificados']       = $data->datos_modificados;
			$datos_modificados['datos_modificados'] = strtoupper($datos_modificados['datos_modificados']);
			$query = $modelo->actualizar($datos);
			if (isset($query)) {
				$mensaje = 1;
			} else {
				$mensaje = 0;
			}
		}
		
		
	
		//$mensaje=$datos;
		return json_encode($mensaje);
	}
}
