<?php

namespace App\Controllers;
use App\Models\Auditoria_sistema_Model;
use App\Models\Presentacion_model;
use CodeIgniter\API\ResponseTrait;

use CodeIgniter\RESTful\ResourceController;

class Presentacion_producto extends BaseController
{
	use ResponseTrait;
	public function index()
	{
		return ('Esta es la Página de tipo de Medicamentos ...');
	}
	public function vistapresentacion()
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		echo view('/listarPresentacion/content_P');
		echo view('/listarPresentacion/footer_P');
	}
	/*
      * Función parar cargar los registros del Módulo en el Data Table o en las Persianas
      */
	public function getAll()
	{
		$model = new Presentacion_model();
		$query = $model->getAll();
		if (empty($query->getResult())) {
			$presentacion = [];
		} else {
			$presentacion = $query->getResultArray();
		}
		echo json_encode($presentacion);
	}
	/*
      * Método que guarda el registro nuevo
      */
	//public function save()

	public function getAllActivos()
	{
		$model = new Presentacion_model();
		$query = $model->getAllActivos();

		if (empty($query->getResult())) {
			$presentacion = [];
		} else {
			$presentacion = $query->getResultArray();
		}
		echo json_encode($presentacion);
	}

	public function agregar()
	{
		$model = new Presentacion_model();
		$model_auditoria=new Auditoria_sistema_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['descripcion']   = $data->presentacion;
		$datos['fecha_creacion'] = $this->formatearFecha($data->fechaRegistro);
		$query = $model->agregar($datos);
		if (isset($query)) {
				$mensaje = 1;
				$auditoria['accion'] = 'REGISTRÓ LA PRESENTACION  '.' '.$datos['descripcion'];
				$Auditoria_sistema_Model = $model_auditoria->agregar($auditoria);
		} else {
			$mensaje = 0;
		}
		return json_encode($mensaje);
	}
	/*
      * Función para obtener los datos de un Rol
      */
	public function getDatosRol()
	{
		if ($this->request->isAJAX()) {
			$data = json_decode(base64_decode($this->request->getGet('data')));
			$datos['id'] = $data->aide;
			$modelo = new Presentacion_model();
			$query = $modelo->getDatosRol($datos['id']);
			$respuesta = [];
			if (empty($query->getResult())) {
				$respuesta[] = '0';
			} else {
				foreach ($query->getResult() as $fila) {
					$respuesta['id']      = $fila->id;
					$respuesta['rol']     = $fila->rol;
					$respuesta['activo']  = $fila->activo;
				}
			}
		} else {
			redirect()->to('/403');
		}
		return json_encode($respuesta);
	}
	/*
      * Método que actualiza el registro
      */
	public function actualizar()
	{
		$modelo = new Presentacion_model();
		$model_auditoria=new Auditoria_sistema_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['id']            = $data->id;
		$datos['descripcion']   = $data->presentacion;
		$datos['fecha_creacion'] = $this->formatearFecha($data->fechaRegistro);
		$datos['borrado']       = $data->borrado;
		$datos_modificados['datos_modificados']       = $data->datos_modificados;
		$datos_modificados['datos_modificados'] = strtoupper($datos_modificados['datos_modificados']);
		$datos_descr_anterior['decripcion_anterior']= $data->descripcion_anterior;
		$query = $modelo->actualizar($datos);
		if (isset($query)) {
			$mensaje = 1;
            $auditoria['accion'] = 'SE MODIFICARON LOS SIGUENTES DATOS DEL TIPO DE PRESENTACION '.' '.$datos_descr_anterior['decripcion_anterior'].','.' '.$datos_modificados['datos_modificados'];
			$Auditoria_sistema_Model = $model_auditoria->agregar($auditoria);
		} else {
			$mensaje = 0;
		}
		//$mensaje=$datos;
		return json_encode($mensaje);
	}
}
