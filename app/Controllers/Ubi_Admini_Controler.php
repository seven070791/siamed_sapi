<?php

namespace App\Controllers;

use App\Models\Ubi_Admini_Model;
use App\Models\Auditoria_sistema_Model;
use CodeIgniter\API\ResponseTrait;

use CodeIgniter\RESTful\ResourceController;

class Ubi_Admini_Controler extends BaseController
{
	use ResponseTrait;


	public function listar_Ubicacion_Administrativa()
	{
		$model = new Ubi_Admini_Model();
		$query = $model->listar_Ubicacion_Administrativa();
		if (empty($query->getResult())) {
			$ubicacion = [];
		} else {
			$ubicacion = $query->getResultArray();
		}
		echo json_encode($ubicacion);
	}
}
