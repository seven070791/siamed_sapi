<?php

namespace App\Controllers;

use App\Models\Medicamentos_model;
use CodeIgniter\API\ResponseTrait;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use \PhpOffice\PhpSpreadsheet\IOFactory;
use CodeIgniter\RESTful\ResourceController;

class Medicamento_Controllers extends BaseController
{
	use ResponseTrait;

	public function GenerarReportesPorFecha()
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		$pdf = new \FPDF('P', 'mm', 'letter');
		$pdf->AddPage();
		$pdf->Header_Medicamentos();
		$model = new Medicamentos_model();
		$desde = $_POST['desde'];
		$hasta = $_POST['hasta'];
		//print_r (explode("-",  $hasta));
		if (empty($desde) || empty($hasta)) {
			$medicamentos = $model->getAll();
		} else {
			$arrdesde = explode("-",  $desde);
			$anid = $arrdesde[0];
			$mesd = $arrdesde[1];
			$diad = $arrdesde[2];
			$f_desde = ($diad . '/' . $mesd . '/' . $anid);

			$arrhasta = explode("-",  $hasta);
			$anih = $arrhasta[0];
			$mesh = $arrhasta[1];
			$diah = $arrhasta[2];
			$f_hasta = ($diah . '/' . $mesh . '/' . $anih);
			$medicamentos = $model->getRangoFechas($f_desde, $f_hasta);
		}
		//echo($medicamentos);
		//die();
		$i = 0;
		foreach ($medicamentos as $medicamento) {

			$descripcion = $medicamento->descripcion;
			$fecha_creacion = $medicamento->fecha_creacion;
			$estatus = $medicamento->estatus;
			$control = $medicamento->control;
			$stock = $medicamento->stock;
			$pdf->Cell(87, 5, $descripcion, 1, 0, 'L');
			$pdf->Cell(35, 5, $fecha_creacion, 1, 0, 'L');
			$pdf->Cell(26, 5, $estatus, 1, 0, 'L');
			$pdf->Cell(23, 5, $control, 1, 0, 'L');
			$pdf->Cell(25, 5, $stock, 1, 1, 'L');
			$i++;
			if ($i == 42) {
				$pdf->AddPage();
				$pdf->Header_Medicamentos();
				$i = 0;
			}
		}
		$this->response->setHeader('Content-Type', 'application/pdf');
		$pdf->Output("medicamento_pdf.pdf", "I");
	}

	/*
      * Función para mostrar el listado de Roles
      */
	public function vistamedicamentos()
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		echo view('/medicamento/content_M');
		echo view('/medicamento/footer_M');
	}

	public function vista_stock_minimo()
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		echo view('/medicamento/content_Stock_Minimo');
		echo view('/medicamento/footer_stock_minimo');
	}
	/*
      * Función parar cargar los registros del Módulo en el Data Table o en las Persianas
      */
	public function getAll($id_categoria = 0, $cronicos = false)
	{
		$model = new Medicamentos_model();
		$query = $model->getAll($id_categoria, $cronicos);
		if (empty($query)) {
			$registroMedicamentos = [];
		} else {
			$registroMedicamentos = $query;
		}
		echo json_encode($registroMedicamentos);
	}


	public function listar_stock_minimo($id_categoria = 0, $cronicos = false)
	{
		$model = new Medicamentos_model();
		$query = $model->listar_stock_minimo($id_categoria, $cronicos);
		if (empty($query)) {
			$stock_minimo = [];
		} else {
			$stock_minimo = $query;
		}
		echo json_encode($stock_minimo);
	}

	public function VerMedicamentosPdf()
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		$pdf = new \FPDF('P', 'mm', 'letter');
		$pdf->AddPage();
		$pdf->Header_Medicamentos();

		$model = new Medicamentos_model();
		$medicamentos = $model->getAll();

		if (empty($medicamentos)) {
			$pdf->cell(196, 5, utf8_decode('Sin Información Coincidente'), 1, 1, 'C', 1);
		} else {
			$i = 0;
			foreach ($medicamentos as $medicamento) {
				$descripcion = $medicamento->descripcion;
				$fecha_creacion = $medicamento->fecha_creacion;
				$estatus = $medicamento->estatus;
				$control = $medicamento->control;
				$stock = $medicamento->stock;

				$pdf->Cell(87, 5, $descripcion, 1, 0, 'L');
				$pdf->Cell(35, 5, $fecha_creacion, 1, 0, 'L');
				$pdf->Cell(26, 5, $estatus, 1, 0, 'L');
				$pdf->Cell(23, 5, $control, 1, 0, 'L');
				$pdf->Cell(25, 5, $stock, 1, 1, 'L');
				$i++;
				if ($i == 42) {
					$pdf->AddPage();
					$pdf->Header_Medicamentos();
					$i = 0;
				}
			}
		}

		$this->response->setHeader('Content-Type', 'application/pdf');


		$pdf->Output("medicamento_pdf.pdf", "I");
	}
	/*
      * Método que guarda el registro nuevo
      */
	//public function save()
	public function agregar()
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}

		$model = new Medicamentos_model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['descripcion']   = $data->descripcion;
		$datos['id_tipo_medicamento ']   = $data->medicamento;
		$datos['id_control']   = $data->control;
		$datos['id_presentacion']   = $data->presentacion;
		$datos['fecha_creacion'] = $this->formatearFecha($data->fechaRegistro);
		$datos['stock_minimo']   = $data->stock_minimo;
		$datos['med_cronico']   = $data->med_cronico;
		$query = $model->agregar($datos);
		if (isset($query)) {
			$mensaje = 1;
		} else {
			$mensaje = 0;
		}
		return json_encode($mensaje);
	}
	/*
      * Función para obtener los datos de un Rol
      */
	public function getDatosRol()
	{
		if ($this->request->isAJAX()) {
			$data = json_decode(base64_decode($this->request->getGet('data')));
			$datos['id'] = $data->aide;
			$modelo = new Medicamentos_model();
			$query = $modelo->getDatosRol($datos['id']);
			$respuesta = [];
			if (empty($query->getResult())) {
				$respuesta[] = '0';
			} else {
				foreach ($query->getResult() as $fila) {
					$respuesta['id']      = $fila->id;
					$respuesta['rol']     = $fila->rol;
					$respuesta['activo']  = $fila->activo;
				}
			}
		} else {
			redirect()->to('/403');
		}
		return json_encode($respuesta);
	}
	/*
      * Método que actualiza el registro
      */
	public function actualizar()
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		$modelo = new Medicamentos_model();

		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['id']                 = $data->id;
		$datos['descripcion']        = $data->descripcion;
		$datos['id_tipo_medicamento'] = $data->medicamento;
		$datos['id_presentacion']    = $data->presentacion;
		$datos['id_control']         = $data->control;
		$datos['fecha_creacion']     = $this->formatearFecha($data->fechaRegistro);
		$datos['borrado']            = $data->borrado;
		$datos['stock_minimo']       = $data->stock_minimo;
		$datos['med_cronico']       = $data->cronico;

		$query = $modelo->actualizar($datos);
		if (isset($query)) {
			$mensaje = 1;
		} else {
			$mensaje = 0;
		}
		return json_encode($mensaje);
	}
	/*
	 *
	 * */
	public function getDatosDelMedicamento()
	{
		$data = json_decode(base64_decode($this->request->getGet('data')));
		$datos['id'] = $data->id;
		$modelo = new Medicamentos_model();
		$query = $modelo->getDatosMedicamento($datos['id']);
		//$mensaje=$query;
		$respuesta = [];
		if (empty($query->getResult())) {
			$respuesta = 'no hay registros';
			//$respuesta[]='0';
		} else {
			foreach ($query->getResult() as $fila) {
				$respuesta['id']                 = $fila->id;

				$respuesta['tipo_medicamento'] = $fila->tipo_medicamento;
				$respuesta['control']         = $fila->control;
				$respuesta['presentacion']    = $fila->presentacion;
				$respuesta['descripcion']        = $fila->medicamento;
				$respuesta['fecha_creacion']     = $fila->fecha_creacion;

				$respuesta['borrado']            = $fila->borrado;
			}
		}
		return json_encode($respuesta);
	}

	public function VerMedicamentoExcel($desde = null, $hasta = null)
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		$model = new Medicamentos_model();


		if ($desde == '' || $hasta == '') {
			$medicamentos = $model->getAll();
		} else {


			$arrdesde = explode("-",  $desde);
			$anid = $arrdesde[0];
			$mesd = $arrdesde[1];
			$diad = $arrdesde[2];
			$f_desde = ($diad . '/' . $mesd . '/' . $anid);
			$arrhasta = explode("-",  $hasta);
			$anih = $arrhasta[0];
			$mesh = $arrhasta[1];
			$diah = $arrhasta[2];
			$f_hasta = ($diah . '/' . $mesh . '/' . $anih);


			$medicamentos = $model->getRangoFechas($f_desde, $f_hasta);
		}
		//echo($medicamentos);
		//die();


		$phpExcel = new Spreadsheet();
		$hoja = $phpExcel->getActiveSheet();
		$hoja = $phpExcel->setActiveSheetIndex(0);
		// ***drawing Se usa para imagenes***
		$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
		$drawing->setName('Logo');
		$drawing->setPath('img/sapi.png');
		$drawing->setHeight(100);
		$drawing->setCoordinates('A1');
		$drawing->setOffsetX(10);
		$drawing->setWorksheet($hoja);
		$hoja->mergeCells("A1:E1");
		// *****-** Fondo de Celdas********
		$hoja->getStyle('A2:E4')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('FF696969');
		//**********************************//
		$hoja->mergeCells("A2:E4");
		$hoja->getStyle('A2')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
		$hoja->getStyle('A2')->getFont()->setSize(14);
		$hoja->getStyle('A2')->getFont()->setName('Arial');
		$hoja->setCellValue('A2', 'Reporte Control de Inventario');
		// ***Centrar texto***
		$hoja->getStyle('A5:E5')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
		// ***Ancho del  texto***
		$hoja->getColumnDimension('A')->setWidth(55);
		// *****-** Fondo de Celdas********
		$hoja->getStyle('A5:E5')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('FFA9A9A8');
		$hoja->setCellValue('A5', 'Descripcion');
		$hoja->getColumnDimension('B')->setWidth(20);
		$hoja->setCellValue('B5', 'Fecha de Registro');
		$hoja->getColumnDimension('C')->setWidth(10);
		$hoja->setCellValue('C5', 'Estatus');
		$hoja->getColumnDimension('D')->setWidth(10);
		$hoja->setCellValue('D5', 'Control ');
		$hoja->getColumnDimension('E')->setWidth(10);
		$hoja->setCellValue('E5', 'Stock');
		$hoja->getStyle('A5:E5')->getFont()->setBold(true);
		$fila = 6;
		foreach ($medicamentos as $medicamento) {
			$descripcion = $medicamento->descripcion;
			$fecha_creacion = $medicamento->fecha_creacion;
			$estatus = $medicamento->estatus;
			$control = $medicamento->control;
			$stock = $medicamento->stock;
			$hoja->setCellValue('A' . $fila, $descripcion);
			$hoja->setCellValue('B' . $fila, $fecha_creacion);
			$hoja->setCellValue('C' . $fila, $estatus);
			$hoja->setCellValue('D' . $fila, $control);
			$hoja->setCellValue('E' . $fila, $stock);
			$fila++;
		}
		$ultimafila = $fila - 1;
		$styleArray = array(
			'borders' => array(
				'outline' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
					'color' => array('argb' => 'FF778899'),

				),
			),
		);
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="MedicamentosExcel.xlsx"');
		header('Cache-Control: max-age=0');

		$writer = IOFactory::createWriter($phpExcel, 'Xlsx');
		$writer->save('php://output');
	}
}
