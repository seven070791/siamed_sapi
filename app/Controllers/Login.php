<?php

namespace App\Controllers;

use App\Models\UsuarioModel;
use App\Models\AuditoriaModel;
use App\Models\EntradaModel;
use App\Models\Medicos_Model;
//Importamos la libreria PHPMailer, que ya tenemos la configuracion para enviar y recibir correos, gracias a la Intranet
require_once APPPATH . '/ThirdParty/PHPMailer/PHPMailer.php';
require_once APPPATH . '/ThirdParty/PHPMailer/Exception.php';
require_once APPPATH . '/ThirdParty/PHPMailer/SMTP.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
class Login extends BaseController
{
    private $usuario;
    public function index()
    {
        return view('login/loginpantalla');
    }

    public function validarIngreso()
    {
        $usuario = $this->request->getPost("usuario");
        /*         $this->usuario= new UsuarioModel();       
        $resultado= $this->usuario->buscarUsuario($usuario);
 */
        $modelo = new UsuarioModel();
        $resultado = $modelo->buscarUsuario($usuario);

        if ($resultado) {
            if (
                $this->request->getMethod() === 'post' && $this->validate(
                    [
                        'nombre' => 'required|min_length[7]|max_length[58]',
                        'contrasena'  => 'required',
                    ]
                )
            );

            // Obtener el password ingresado por el usuario
            $passwordIngresado = $this->request->getPost('contrasena');
            // Obtener el password encriptado almacenado en la base de datos
            $passwordEncriptado = $resultado->password;
            // Verificar si el password ingresado coincide con el password encriptado almacenado en la base de datos
            if (password_verify($passwordIngresado, $passwordEncriptado)) {
                // El password ingresado es correcto
                $data =
                [
                    "nombreUsuario" => $resultado->nombre . ' ' . $resultado->apellido,
                    "id_user" => $resultado->id,
                    "nivel_usuario" => $resultado->tipousuario,
                    "cedula_usuario" => $resultado->cedula,  
                ];
    
                if ($resultado->tipousuario == '3') {
                    //Es un médico:

                    $modelo_medico = new Medicos_Model();
                    $resultado_medico = $modelo_medico->Buscarmedicos($resultado->cedula);
                    $data['medico_id'] = $resultado_medico[0]->id;
                    $data['descripcion'] = $resultado_medico[0]->especialidad;
                    $data['id_especialidad'] = $resultado_medico[0]->id_especialidad;
                    $data['acceso_citas'] = $resultado_medico[0]->acceso_citas;
                   
                } else if ($resultado->tipousuario != '3') {
                    //NO ES un médico:

                    $modelo_medico = new Medicos_Model();
                    $resultado_medico = $modelo_medico->Buscarmedicos($resultado->cedula);
                    // $data['medico_id']=$resultado_medico[0]->id;    
                    //$data['descripcion']=$resultado_medico[0]->especialidad;    
                    //$data['id_especialidad']=$resultado_medico[0]->id_especialidad; 
                    // $data['acceso_citas']=false;


                }
               
                // Coloco en sesión lo que tiene $data:
                session()->set($data);
                $modelo_auditoria = new AuditoriaModel();
                $resultado_aditoria = $modelo_auditoria->fecha_auditoria($resultado->id);
              
                echo view('menu/supermenu', $data);
               // echo view('menu/footer.php'); 
                
            } else {
                $data = ['tipo' => 'danger', 'mensaje' => 'La Contraseña es Incorrecta'];
                return view('login/loginpantalla', $data);
            }
        } else {
            $data = ['tipo' => 'danger', 'mensaje' => 'Usuario incorrecto o inactivo'];
            return view('login/loginpantalla', $data);
        }
    }


//Envio de correo de recuperacion
public function sendRecoverEmail()
{
    $mail = new PHPMailer();
    $model = new UsuarioModel();
    $dataEmail = array();
    if ($this->request->isAJAX()) {
        $data = json_decode(base64_decode($this->request->getPost('data')));
        $datos['email']   = $data->email;
        $query = $model->getUserByMail($datos["email"]);
        //Si lo esta, generamos un JSON con los datos para generar la recuperacion
        if (isset($query)) {
            //Grabamos el nombre del usuario
            foreach ($query as $row) {
                $dataEmail["name"] = $row->nombre . ' ' . $row->apellido;
            }
            $dataEmail["email"] = $datos["email"];
            $dataEmail["timestamp_generate"] = strtotime(date('Y-m-d H:i:s'));
            $dataEmail["timestamp_expire"] = strtotime("5 minutes", $dataEmail["timestamp_generate"]);
            //Codificamos el JSON y lo encriptamos
            $urlData = base64_encode(json_encode($dataEmail));
            $dataEmail["urldata"] = $urlData;
            $el_servidor  = "172.16.0.5";
            $el_puerto    = "587";
            $el_remitente = "adminsigedoc@sapi.gob.ve";
            $el_pass      = "37d3g8Z6D2";
            try {
                $smtpOptions = array(
                    'ssl' => array(
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                        'allow_self_signed' => true
                    )
                );
                $io_mail = new PHPMailer();
                $io_mail->isSMTP();
                $io_mail->Host = $el_servidor;
                $io_mail->Port = $el_puerto;
                $io_mail->SMTPAuth = true;
                $io_mail->Username = $el_remitente;
                $io_mail->Password = $el_pass;
                $io_mail->SMTPOptions = $smtpOptions;
                $io_mail->setFrom($el_remitente);
                $io_mail->AddAddress($dataEmail["email"], $dataEmail["name"]); // Agrega la dirección de correo de destino
                $io_mail->FromName = "No Reply";
                $io_mail->Subject = utf8_decode("Recuperación de Contraseña de SIAMED");
                $io_mail->Body = view('mail/recover', $dataEmail);
                $io_mail->AltBody = 'Este es un mensaje de prueba enviado desde el servidor SMTP';
                if ($io_mail->send()) {
                    $url = base_url('mail/recover');
                    $link = "<a href='$url' </a>";
                    $mensaje=1;
                    return json_encode($mensaje);
                    //return $this->respond(["message" => "Revisa tu correo para seguir los pasos de recuperación. $link"], 200);
                } else {
                    $mensaje=2;
                    return json_encode($mensaje);
                    //return $this->respond(["message" => "No se pudo enviar el correo, pongase en contacto con el administrador del sistema para más información"], 404);
                }

            } catch (Exception $e) {
                echo 'Error al establecer la conexión SMTP: ' . $e->getMessage();
            }
        }
    } else {
        return redirect()->to('/');
    }
}


//Metodo que carga la vista para recuperar la contraseña
public function settingNewPass($datos = NULL)
{
    //$data = $datos;
    $cadena_codificada = $datos;
    $cadena_decodificada = base64_decode($cadena_codificada);
    $cadena = $cadena_decodificada;
    $arreglo = explode(",", $cadena);
    $data['email'] = str_replace('"', '', explode(":", $arreglo[1])[1]);

    if (is_null($arreglo)) {
        return redirect()->to('/');
    } else {
        
        echo view("set_password/content", $data);
        echo view("set_password/footer");
    }
}

    public function cerrarSesion()

    {
        session()->destroy();
        return redirect()->to('/');
    }
}
