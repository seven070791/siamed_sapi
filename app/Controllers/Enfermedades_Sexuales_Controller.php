<?php

namespace App\Controllers;

use App\Models\Enfermedades_Sexuales_Model;
use CodeIgniter\API\ResponseTrait;
use App\Models\Medicamentos_model;
use CodeIgniter\RESTful\ResourceController;

class Enfermedades_Sexuales_Controller extends BaseController
{
	use ResponseTrait;


	public function ListarEnfermedadesSexuales()
	{
		$model = new Enfermedades_Sexuales_Model();
		$query = $model->ListarEnfermedadesSexuales();

		if (empty($query->getResult())) {
			$enfermedades = [];
		} else {
			$enfermedades = $query->getResultArray();
		}
		echo json_encode($enfermedades);
	}
}
