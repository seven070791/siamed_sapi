<?php

namespace App\Controllers;

use App\Models\Medicos_Model;
use App\Models\Notas_Entregas_Model;
use App\Models\Especialidad_Model;
use CodeIgniter\API\ResponseTrait;
use App\Models\Detalles_Nota_Entrega_Model;
use App\Models\Salida_Model;
use App\Models\Auditoria_sistema_Model;
use App\Models\Autorizador_Model;
use CodeIgniter\RESTful\ResourceController;

class Notas_Entregas_Controllers extends BaseController
{
	use ResponseTrait;
	/*
      * Función para mostrar el listado de Roles
      */
	/*
      * Función parar cargar los registros del Módulo en el Data Table o en las Persianas
      */


	public function vistaNotasEntregas()
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		echo view('/Notas_de_Entregas/vistanotasentregas');
		echo view('/Notas_de_Entregas/footer_notasentregas');
	}

	public function listar_notas_entregas($desde = null, $hasta = null)
	{
		$model = new Notas_Entregas_Model();
		$query = $model->listar_notas_entregas($desde, $hasta);
		

		if (empty($query)) {
			$notas_entregas = [];
		} else {
			$notas_entregas = $query;
		}
		echo json_encode($notas_entregas);
	}



	/*
      * Método que guarda los datos del Medico 
      */
	//public function save()
	public function agregar_nota_entrega()
	{
		$model = new Notas_Entregas_Model();
		$model_salidas = new Salida_Model();
		$model_detalle_nota_entrega = new Detalles_Nota_Entrega_Model();
		$model_Auditoria_sistema_Model = new Auditoria_sistema_Model();
		$model_autorizador = new Autorizador_Model();
		$query_autorizador = $model_autorizador->getAllActivos();
		if (empty($query_autorizador)) {
			$mensaje['procesado'] = 4;
			return json_encode($mensaje);
		} else {
			///BUSCO AL AUTORIZADOR ACTIVO
			foreach ($query_autorizador as $fila) {
				$datos['id_autorizador'] = $fila->id;
			}

		}
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['fecha_registro']   = $data->fechaconvertida;
		$datos['n_historial']   = $data->n_historial;
		$datos['usuario']   = $data->usuario;
		$datos_2['cedula_beneficiario']   = $data->cedula_beneficiario;
		$cedula_beneficiario = $data->cedula_beneficiario;
		$tipobeneficiario = $data->tipobeneficiario;
		$datos_2['fecha_registro']   = $data->fechaconvertida;
		$query_buscar_salidas = $model_salidas->buscar_salidas_notas_entregas($datos_2);
		$n_historial = $data->n_historial;
		$fecha_registro = $data->fechaconvertida;
		if (empty($query_buscar_salidas)) {
			$mensaje['procesado'] = 3;
			$mensaje['autorizador'] = $datos['id_autorizador'];
		} else {
			///BUSCO EL ID DE LA SALIDA PARA PODER GENERAL LA NOTA DE ENTREGA
			$cont = 0;

			foreach ($query_buscar_salidas as $fila) {

				$array_salidas_fecha_historia[$cont]['salida_id'] = $fila->id;
				$cont++;
				//$datos_detalle_notaentrega['salida_id']       = $fila->id;
			}
			///CREO LA NOTA DE ENTREGA
			$query = $model->agregar_nota_entrega($datos);
			if (isset($query)) {
				/// SI TODO ESTA BIEN , MUESTRO REGISTRO INCORPORADO
				$mensaje['procesado'] = 1;
				$mensaje['autorizador'] = $datos['id_autorizador'];
				/// REGISTRO EN AUDITORIA LA CREACION DE LA NOTA DE ENTREGA 
				$auditoria['accion']   = 'CREO UNA NOTA DE ENTREGA  PARA  ' . '  ' . '  ' . $tipobeneficiario . $cedula_beneficiario;
				$Auditoria_sistema_Model = $model_Auditoria_sistema_Model->agregar($auditoria);
				/// LUEGO BUSCO EL ID DE LA NOTA DE ENTREGA
				$buscar_id_notas_entregas = $model->buscar_id_notas_entregas($n_historial, $fecha_registro);

				//$prueba = var_dump($buscar_id_notas_entregas);

				///BUSCO EL ID DE LA NOTA DE ENTREGA 

				foreach ($buscar_id_notas_entregas as $fila) {
					$nota_entrega_id      = $fila->id;
				}

				//$prueba = $datos_detalle_notaentrega;
				//LUEGO REGISTRO EN LA TABLA DETALLES_NOTASDE_ENTREGAS EL ID_SALIDAS Y ID_NOTAS_ENREGAS
				$contDetalles = 0;
				foreach ($array_salidas_fecha_historia as $fila) {
					$datos_detalle_notaentrega[$contDetalles]['nota_entrega_id'] = $nota_entrega_id;
					$datos_detalle_notaentrega[$contDetalles]['salida_id'] = $array_salidas_fecha_historia[$contDetalles]['salida_id'];
					$contDetalles++;
					//$query_agregar_detalle_notaentrega = $model_detalle_nota_entrega->agregar($datos_detalle_notaentrega, $contDetalles);
				}
				$query_agregar_detalle_notaentrega = $model_detalle_nota_entrega->agregar($datos_detalle_notaentrega, $contDetalles);
			} else {
				/// SI NO SE CREO LA NOTA , MUESTRO ERROR EN REGISTO
				$mensaje['procesado'] = 0;
				$mensaje['autorizador'] = $datos['id_autorizador'];
			}
		}
		return json_encode($mensaje);
	}


	public function Reversar_nota_entrega()
	{
		$model_nota_entrega = new Notas_Entregas_Model();
		$model_detalle_nota_entrega = new Detalles_Nota_Entrega_Model();
		$model_salidas = new Salida_Model();
		$model_Auditoria_sistema_Model = new Auditoria_sistema_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['id']   = $data->nota_entrega_id;
		$nota_entrega_id = $data->nota_entrega_id;
		$datos['borrado']   = $data->borrado;
		$auditoria['accion']   = 'REVERSO LA NOTA DE ENTREGA  Nº ' . ' ' . ' ' . $nota_entrega_id;
			//BUSCO EL DETALLE DE LA NOTA DE ENTREGA EN FUNCION DEL ID DE LA NOTA DE ENTREGA
		$query_detalle_nota_entrega = $model_detalle_nota_entrega->buscar_detalle_nota_entrega($datos);
		if (empty($query_detalle_nota_entrega)) 
		{
			//NO SE ENCONTRARON DETALLES DE SALIDAS ASOCIADAS A LA NOTA DE ENTREGA 
			$mensaje = 0;
		}else
		{
		    //BORRO LOS DETALLES DE  LA NOTA DE ENTREGA EN FUNSION DE SU ID 
			$query_borrar_detalle_nota_entrega = $model_detalle_nota_entrega->borrar_detalle_nota_entrega($datos);
			
			if ($query_borrar_detalle_nota_entrega === false)
			{
				//HUBO UN ERROR AL BORRAR LOS DETALLES DE SALIDAS ASOCIADAS A LA NOTA DE ENTREGA 
				$mensaje = 1;
			}
			else
			{
				//BORRO LAS SALIDAS  ASOCiADAS A LA NOTA DE ENTREGA
				$contDetalles = 0;
				//RECORRO LOS ID Y LOS METO EN UN ARREGLO
				foreach ($query_detalle_nota_entrega as $fila) 
				{
					$array_salidas[$contDetalles]['borrado'] = true;
					$array_salidas[$contDetalles]['salida_id'] = $fila->salida_id;
					$contDetalles++;
				}
				$borrar_salidas_nota_entrega = $model_salidas->borrar_salidas_nota_entrega($array_salidas, $contDetalles);
				if ($borrar_salidas_nota_entrega === false)
				{
					//HUBO UN ERROR AL BORRAR LAS SALIDAD DE LA NOTA DE ENTREGA
					$mensaje = 2;
				}
				else
				{
					//BORRO  LA NOTA DE ENTREGA EN FUNCION DE SU ID 
					$query_borrar_nota_entrega = $model_nota_entrega->borrar_nota_entrega($datos);
				 	if ($query_borrar_nota_entrega)
					{
						//REVERSO REALIZADO OK
						$Auditoria_sistema_Model = $model_Auditoria_sistema_Model->agregar($auditoria);
				 		$mensaje = 3;
				 	}else 
					{
						//HUBO UN ERROR AL BORRAR LA NOTA DE ENTREGA 
				 		$mensaje = 4;
				 	}
					
				}    	
			}	
		}
		return json_encode($mensaje);
	}
}












	// public function listar_especialidad()
	// {
	// 	$model = new Especialidad_Model();
	// 	$query = $model->listar_especialidad();
	// 	if (empty($query)) {
	// 		$categoria = [];
	// 	} else {
	// 		$especialidad = $query;
	// 	}
	// 	echo json_encode($especialidad);
	// }

	// public function listar_medicos()
	// {
	// 	$model = new Medicos_Model();
	// 	$query = $model->listar_medicos();;
	// 	if (empty($query)) {
	// 		$especialidad = [];
	// 	} else {
	// 		$especialidad = $query;
	// 	}
	// 	echo json_encode($especialidad);
	// }
	// public function Buscarmedicos()
	// {
	// 	$model = new Medicos_Model();
	// 	$query = $model->Buscarmedicos();;
	// 	if (empty($query)) {
	// 		$Buscarmedicos = [];
	// 	} else {
	// 		$Buscarmedicos = $query;
	// 	}
	// 	echo json_encode($Buscarmedicos);
	// }




	// public function listar_medicos_activos()
	// {
	// 	$model = new Medicos_Model();
	// 	$query = $model->listar_medicos_activos();;
	// 	if (empty($query)) {
	// 		$especialidad = [];
	// 	} else {
	// 		$especialidad = $query;
	// 	}
	// 	echo json_encode($especialidad);
	// }




	// public function listar_especialidades_activas()
	// {
	// 	$model = new Especialidad_Model();
	// 	$query = $model->listar_especialidades_activas();
	// 	if (empty($query)) {
	// 		$categoria = [];
	// 	} else {
	// 		$especialidad = $query;
	// 	}
	// 	echo json_encode($especialidad);
	// }




	// public function Vita_Especialidad()
	// {
	// 	if (!session('nombreUsuario')) {
	// 		return redirect()->to(base_url() . '/index.php');
	// 	}
	// 	echo view('/medicos/especialidades');
	// 	echo view('/medicos/footer_Especialidades');
	// }

	// public function medico_Citas()
	// {

	// 	echo view('/medicos/medico_Citas');
	// 	echo view('/medicos/footer_medico_Citas');
	// }
	// public function medico_Consultas()
	// {
	// 	if (!session('nombreUsuario')) {
	// 		return redirect()->to(base_url() . '/index.php');
	// 	}
	// 	echo view('/medicos/medico_Consultas');
	// 	echo view('/medicos/footer_medico_Consultas');
	// }


	// /*
	//   * Método que guarda los datos del Medico 
	//   */
	// //public function save()
	// public function agregar_Medicos()
	// {
	// 	$model = new Medicos_Model();
	// 	$data = json_decode(base64_decode($this->request->getPost('data')));
	// 	$datos['cedula']   = $data->cedula;
	// 	$datos['nombre']   = $data->nombre;
	// 	$datos['apellido']   = $data->apellido;
	// 	$datos['telefono']   = $data->telefono;
	// 	$datos['especialidad'] = $data->especialidad;

	// 	$query = $model->agregar_medico($datos);
	// 	if (isset($query)) {
	// 		$mensaje = 1;
	// 	} else {
	// 		$mensaje = 0;
	// 	}
	// 	return json_encode($mensaje);
	// }
	// public function actualizar_Medicos()
	// {
	// 	$modelo = new Medicos_Model();
	// 	$data = json_decode(base64_decode($this->request->getPost('data')));
	// 	$datos['id']   = $data->id_medico;
	// 	$datos['cedula']   = $data->cedula;
	// 	$datos['nombre']   = $data->nombre;
	// 	$datos['apellido']   = $data->apellido;
	// 	$datos['telefono']   = $data->telefono;
	// 	$datos['especialidad'] = $data->especialidad;
	// 	$datos['borrado'] = $data->borrado;
	// 	$query = $modelo->actualizar_Medicos($datos);
	// 	if (isset($query)) {
	// 		$mensaje = 1;
	// 	} else {
	// 		$mensaje = 0;
	// 	}
	// 	//$mensaje=$datos;
	// 	return json_encode($mensaje);
	// }



	/*
      * Método que guarda el registro de la Especialidad
      */
	//public function save()
	// public function agregar_Especialidad()
	// {
	// 	$model = new Especialidad_Model();
	// 	$data = json_decode(base64_decode($this->request->getPost('data')));
	// 	$datos['descripcion']   = $data->descripcion;
	// 	$query = $model->agregar_especialidad($datos);
	// 	if (isset($query)) {
	// 		$mensaje = 1;
	// 	} else {
	// 		$mensaje = 0;
	// 	}
	// 	return json_encode($mensaje);
	// }

	// public function actualizar_especialidad()
	// {
	// 	$modelo = new Especialidad_Model();
	// 	$data = json_decode(base64_decode($this->request->getPost('data')));
	// 	$datos['id_especialidad'] = $data->id_especialidad;
	// 	$datos['descripcion'] = $data->descripcion;
	// 	$datos['borrado']       = $data->borrado;
	// 	$query = $modelo->actualizar_especialidad($datos);
	// 	if (isset($query)) {
	// 		$mensaje = 1;
	// 	} else {
	// 		$mensaje = 0;
	// 	}
	// 	//$mensaje=$datos;
	// 	return json_encode($mensaje);
	// }
