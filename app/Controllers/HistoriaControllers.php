<?php namespace App\Controllers;
use App\Models\Beneficiarios_Model;
use App\Models\HistorialModel;
use CodeIgniter\API\ResponseTrait;
use CodeIgniter\Exceptions\AlertError;
use CodeIgniter\RESTful\ResourceController;

class HistoriaControllers extends BaseController
{
         
    public function  datos_titular($cedula_trabajador)
    {
        if (!session('nombreUsuario')) {
            return redirect()->to(base_url().'/index.php'); 
        }
        $modelo = new Beneficiarios_model();
        $query=$modelo->getAll($cedula_trabajador);
    if(empty($query))
    {
       $datos_titular=[];
    }
    else
    {
       foreach($query as $fila)
       {
           $datos_titular['cedula_trabajador']       =$fila->cedula_trabajador;
           $datos_titular['nombre']                  =$fila->nombre;
           $datos_titular['apellido']				  =$fila->apellido;
           $datos_titular['ubicacion_administrativa']=$fila->ubicacion_administrativa;	
           $datos_titular['fecha_nacimiento']=$fila->fecha_nacimiento;
       }    
    }  
    if (!session('nombreUsuario')) {
        return redirect()->to(base_url().'/index.php'); 
    }
    echo view('/historia_clinica/vista_historial',$datos_titular);
    echo view('/historia_clinica/footer_vista_historial');
 
}  
public function  datos_titular_familiares($cedula_trabajador,$cedula_beneficiario)
{
    $modelo = new Beneficiarios_model();
  

    $query=$modelo->getAllInfoMedicamentosFamiliares($cedula_beneficiario);
    

if(empty($query))
{
   $datosfamiliar=[];
}
else
{
   foreach($query as $fila)
   {
       $datosfamiliar['cedula']                     =$fila->cedula;
       $datosfamiliar['nombre']                     =$fila->nombre;
       $datosfamiliar['apellido']				    =$fila->apellido;
       $datosfamiliar['fecha_nac_familiares']	    =$fila->fecha_nac_familiares;
       $datosfamiliar['cedula_trabajador']		    =$fila->cedula_trabajador;
       
       //$datosfamiliar['ubicacion_administrativa']=$fila->ubicacion_administrativa;	
       //$datosfamiliar['fecha_nacimiento']=$fila->fecha_nacimiento;
   }    
}  
//if (!session('nombreUsuario')) {
  //  return redirect()->to(base_url().'/index.php'); 
//}

echo view('/historia_clinica/vista_historialFamiliares',$datosfamiliar);
echo view('/historia_clinica/footer_vista_historial_familiares');

}  

public function  datos_titular_cortesia($cedula_trabajador,$cedula_beneficiario)
{
   
    $modelo = new Beneficiarios_model();
    $query=$modelo->getAllInfoMedicamentosCortesia($cedula_beneficiario);
   
if(empty($query))
{
   $dotasCortesia=[];
}
else
{
   foreach($query as $fila)
   {
       $dotasCortesia['cedula']                  =$fila->cedula;
       $dotasCortesia['nombre']                  =$fila->nombre;
       $dotasCortesia['apellido']				 =$fila->apellido;
       $dotasCortesia['fecha_nac_cortesia']		 =$fila->fecha_nac_cortesia;
       $dotasCortesia['cedula_trabajador']		 =$fila->cedula_trabajador;
    
   }    
}  
if (!session('nombreUsuario')) {
    return redirect()->to(base_url().'/index.php'); 
}
echo view('/historia_clinica/vista_historialCortesia',$dotasCortesia);
echo view('/historia_clinica/footer_vista_historial_cortesia');

} 




public function vista_habitos($n_historial,$cedula,$historial_id,$tipo_beneficiario,$cedulat)
{
    $modelo = new Beneficiarios_model();  
   
if ($tipo_beneficiario=='T') 
{
   
    $query=$modelo->getAll($cedula);
    if(empty($query))
    {
       $datos_titular=[];
    }
    else
    {
        foreach($query as $fila)
        {
            $datos_titular['cedula_trabajador']       =$fila->cedula_trabajador;
            $datos_titular['nombre']                  =$fila->nombre;
            $datos_titular['apellido']				  =$fila->apellido;
            $datos_titular['ubicacion_administrativa']=$fila->ubicacion_administrativa;	
            $datos_titular['fecha_nacimiento']=$fila->fecha_nacimiento;
            $datos_titular['edad_actual']=$fila->edad_actual;
            $datos_titular['historial_id']=$historial_id;
            $datos_titular['n_historial']=$n_historial;
        }    
        if (!session('nombreUsuario')) {
            return redirect()->to(base_url().'/index.php'); 
        }
        echo view('/historia_clinica/vista_habitos',$datos_titular);
        echo view('/historia_clinica/footer_vista_habitos');
    } 
}
else if ($tipo_beneficiario=='F') 

{
    $query_familiar=$modelo->getFamiliar($cedula);
   
    if(empty($query_familiar))
    {
       $datos_titular=[];
    }
    else
    {
        foreach($query_familiar as $fila)
        {
            $datos_titular['cedula_trabajador']       =$fila->cedula;
            $datos_titular['nombre']                  =$fila->nombre;
            $datos_titular['fecha_nacimiento']=$fila->fecha_nac_familiares;
            $datos_titular['edad_actual']=$fila->edad_actual;
            $datos_titular['historial_id']=$historial_id;
            $datos_titular['n_historial']=$n_historial;
            $datos_titular['cedulat']=$cedulat;

        }   
       
        if (!session('nombreUsuario')) {
            return redirect()->to(base_url().'/index.php'); 
        }
        echo view('/historia_clinica/vista_habitos_familiares',$datos_titular);   
        echo view('/historia_clinica/footer_vista_habitos');
    }
    
}
else if ($tipo_beneficiario=='C') 

{
    $query_cortesia=$modelo->getCortersia($cedula);
   
    if(empty($query_cortesia))
    {
       $datos_titular=[];
    }
    else
    {
        foreach($query_cortesia as $fila)
        {
            $datos_titular['cedula_trabajador']       =$fila->cedula;
            $datos_titular['nombre']                  =$fila->nombre;
            $datos_titular['fecha_nacimiento']=$fila->fecha_nac_cortesia;
            $datos_titular['edad_actual']=$fila->edad_actual;
            $datos_titular['historial_id']=$historial_id;
            $datos_titular['n_historial']=$n_historial;
            $datos_titular['cedulat']=$cedulat;
        }    
        if (!session('nombreUsuario')) {
            return redirect()->to(base_url().'/index.php'); 
        }
        echo view('/historia_clinica/vista_habitos_cortesia',$datos_titular);   
        echo view('/historia_clinica/footer_vista_habitos');
    }
    
}


}

public function agregar_historial_medico($cedulabeneficiario)
{
    $modelo = new  HistorialModel();
    $query=$modelo->getHistorial($cedulabeneficiario);
    if(empty($query))
    {
        $data=json_decode(base64_decode($this->request->getPost('data')));
        $datos['n_historial']   =$data->numero_historial;
        $datos['cedula']   =$data->cedula;
        $datos['fecha_regist']=$data->fecha_registro;

        $query = $modelo->agregar_historial_medico($datos);
        
        if(isset($query))
        {
            $mensaje=1;
        }
        else
        {
            $mensaje=0;
        }
     
    }
    else
    {
        $mensaje=2;
    }

    // $model = new HistorialModel();

    return json_encode($mensaje);  

}   

public function borrar_historial()
{
   
   $modelo = new  HistorialModel();
   $data=json_decode(base64_decode($this->request->getPost('data')));
   $datos['id']   =$data->historial_id;
   $datos['borrado']=$data->borrado;
   $query = $modelo->borrar_historial($datos);
   if(isset($query))
     {
      $mensaje=1;
     }
     else
     {
      $mensaje=0;
     }
     //$mensaje=$datos;
 return json_encode($mensaje);
}

public function getAllHistorialFamiliar($n_historial,$cedula)
     {
        
	  $model = new HistorialModel ();
	  $query = $model->getAllHistorialFamiliar($n_historial,$cedula);
     
	  if(empty($query))
	  {
        
		$historial=[];
	  }
	  else
	  {
       
		$historial= $query->getResultArray();
	  }
	  echo json_encode($historial);
     }

    
     public function getAllHistorialCortesia($n_historial,$cedula)
     {  
	  $model = new HistorialModel ();
	  $query = $model->getAllHistorialCortesia($n_historial,$cedula);
     
	  if(empty($query))
	  {
        
		$historial=[];
	  }
	  else
	  {
       
		$historial= $query->getResultArray();
	  }
	  echo json_encode($historial);
     }




     public function getAllHistorial($cedulabeneficiario)
     {
	  $model = new HistorialModel ();
	  $query = $model->getAllHistorial($cedulabeneficiario);
	  if(empty($query))
	  {
		$historial=[];
	  }
	  else
	  {
		$historial= $query->getResultArray();
	  }
	  echo json_encode($historial);
     }
    
     

    public function  historia_P_pediatrico()
    {
        if (!session('nombreUsuario')) {
            return redirect()->to(base_url().'/index.php'); 
        }
        return view ('historia_clinica/historia_P_pediatrico');

    } 
    public function   historia_Psicologia_p()
    {
        if (!session('nombreUsuario')) {
            return redirect()->to(base_url().'/index.php'); 
        }
        return view ('historia_clinica/historia_Psicologia_p');

    } 
    public function    historia_M_interna()
    {
        if (!session('nombreUsuario')) {
            return redirect()->to(base_url().'/index.php'); 
        }
        return view ('historia_clinica/historia_M_interna');

    } 
    public function     historia_M_general()
    {
        if (!session('nombreUsuario')) {
            return redirect()->to(base_url().'/index.php'); 
        }
        return view ('historia_clinica/historia_M_general');

    } 
    public function  morbilidad()
    {
        if (!session('nombreUsuario')) {
            return redirect()->to(base_url().'/index.php'); 
        }
        return view ('historia_clinica/morbilidad');

    } 

   

}
