<?php

namespace App\Controllers;

use App\Models\Medicos_Model;
use App\Models\Especialidad_Model;
use CodeIgniter\API\ResponseTrait;
use App\Models\Auditoria_sistema_Model;
use CodeIgniter\RESTful\ResourceController;
use PhpOffice\PhpSpreadsheet\Worksheet\Row;
use App\Models\Historial_info_Model;



class Medicos_Controllers extends BaseController
{
	use ResponseTrait;
	/*
      * Función para mostrar el listado de Roles
      */
	/*
      * Función parar cargar los registros del Módulo en el Data Table o en las Persianas
      */
	public function listar_especialidad()
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		$model = new Especialidad_Model();
		$query = $model->listar_especialidad();
		if (empty($query)) {
			$categoria = [];
		} else {
			$especialidad = $query;
		}
		echo json_encode($especialidad);
	}



	public function listar_medicos()
	{
		$model = new Medicos_Model();
		$query = $model->listar_medicos();;
		if (empty($query)) {
			$especialidad = [];
		} else {
			$especialidad = $query;
		}
		echo json_encode($especialidad);
	}
	public function Buscarmedicos()
	{
		$model = new Medicos_Model();
		$query = $model->Buscarmedicos();;
		if (empty($query)) {
			$Buscarmedicos = [];
		} else {
			$Buscarmedicos = $query;
		}
		echo json_encode($Buscarmedicos);
	}




	public function listar_medicos_activos()
	{
		$model = new Medicos_Model();
		$query = $model->listar_medicos_activos();;
		if (empty($query)) {
			$especialidad = [];
		} else {
			$especialidad = $query;
		}
		echo json_encode($especialidad);
	}


	public function medicos_activos_por_especialidad($id_especialidad = null)
	{
		$model = new Medicos_Model();
		$query = $model->medicos_activos_por_especialidad($id_especialidad);
		if (empty($query)) {
			$especialidad = [];
		} else {
			$especialidad = $query;
		}
		echo json_encode($especialidad);
	}




	public function listar_especialidades_activas()
	{
		$model = new Especialidad_Model();
		$query = $model->listar_especialidades_activas();
		if (empty($query)) {
			$categoria = [];
		} else {
			$especialidad = $query;
		}
		echo json_encode($especialidad);
	}




	public function listar_acceso_citas_especialidades()
	{
		$model = new Especialidad_Model();
		$query = $model->listar_acceso_citas_especialidades();
		if (empty($query)) {
			$categoria = [];
		} else {
			$especialidad = $query;
		}
		echo json_encode($especialidad);
	}


	public function listar_especialidades_activas_sin_filtro()
	{
		$model = new Especialidad_Model();
		$query = $model->listar_especialidades_activas_sin_filtro();
		if (empty($query)) {
			$categoria = [];
		} else {
			$especialidad = $query;
		}
		echo json_encode($especialidad);
	}



	public function listar_especialidades_activas_sin_filtro_caja_chica()
	{
		$model = new Especialidad_Model();
		$query = $model->listar_especialidades_activas_sin_filtro_caja_chica();
		if (empty($query)) {
			$categoria = [];
		} else {
			$especialidad = $query;
		}
		echo json_encode($especialidad);
	}











	public function vistaMedicos()
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		echo view('/medicos/vistamedicos');
		echo view('/medicos/footer_Medicos');
	}

	public function Vita_Especialidad()
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		echo view('/medicos/especialidades');
		echo view('/medicos/footer_Especialidades');
	}

	public function medico_Citas()
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		echo view('/medicos/medico_Citas');
		echo view('/medicos/footer_medico_Citas');
	}
	public function medico_Consultas()
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		echo view('/medicos/medico_Consultas');
		echo view('/medicos/footer_medico_Consultas');
	}


	/*
      * Método que guarda los datos del Medico 
      */
	//public function save()
	public function agregar_Medicos()
	{
		$model = new Medicos_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['cedula']   = $data->cedula;
		$datos['nombre']   = $data->nombre;
		$datos['apellido']   = $data->apellido;
		$datos['telefono']   = $data->telefono;
		$datos['especialidad'] = $data->especialidad;
		$query = $model->agregar_medico($datos);
		if (isset($query)) {
			$mensaje = 1;
		} else {
			$mensaje = 0;
		}
		return json_encode($mensaje);
	}

	public function actualizar_Medicos()
	{
		$modelo = new Medicos_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['id']   = $data->id_medico;
		$datos['cedula']   = $data->cedula;
		$datos['nombre']   = $data->nombre;
		$datos['apellido']   = $data->apellido;
		$datos['telefono']   = $data->telefono;
		$datos['especialidad'] = $data->especialidad;
		$datos['borrado'] = $data->borrado;
		$query = $modelo->actualizar_Medicos($datos);
		if (isset($query)) {
			$mensaje = 1;
		} else {
			$mensaje = 0;
		}
		//$mensaje=$datos;
		return json_encode($mensaje);
	}



	/*
      * Método que guarda el registro de la Especialidad
      */
	//public function save()
	public function agregar_Especialidad()
	{
		$model = new Especialidad_Model();
		$model_auditoria=new Auditoria_sistema_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['descripcion']   = $data->descripcion;
		$datos['acceso_citas']   = $data->acceso;
		$query = $model->agregar_especialidad($datos);
		if (isset($query)) {

			$mensaje = 1;
            $auditoria['accion'] = 'REGISTRÓ LA ESPECIALIDAD DE  '.' '.$datos['descripcion'];
			$Auditoria_sistema_Model = $model_auditoria->agregar($auditoria);
		} else {
			$mensaje = 0;
		}
		return json_encode($mensaje);
	}

	public function actualizar_especialidad()
	{
		$modelo = new Especialidad_Model();
		$model = new Historial_info_Model();
		$model_auditoria=new Auditoria_sistema_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		
		$check_borrados=[];
		$check_borrados=$data->deletedElements;
		$selectedValues = $data->selectedValues;
		$especialidad_id=$data->id_especialidad;
					$datos['id_especialidad'] = $data->id_especialidad;
					$datos['descripcion'] = $data->descripcion;
					$datos['borrado']       = $data->borrado;
					$datos['acceso_citas']       = $data->acceso;
					$datos['decripcion_anterior']= $data->descripcion_anterior;
					if ($data->datos_modificados!=false)
					{
						$datos_modificados['datos_modificados']       = $data->datos_modificados;
						$datos_modificados['datos_modificados'] = strtoupper($datos_modificados['datos_modificados']);	
		}	

	
		/// SI NO SELECCIONO NINGUNO , ENTONCES ELIMINAMOS LOS REGISTROS DE LA TABLA especialidad_hist_informativo
		if (empty($selectedValues)) 
		{
			//VERIFICO SI TIENE PATOLOGIAS ASOCIADAS
			$query_histo_info_Asociadas=$model->especialidad_hist_informativo($especialidad_id);	
			if (empty($query_histo_info_Asociadas)) {
				$mensaje = 2;
				
			}else
			{
				
				$query_Historial_infor_existentes=$model->Eliminar_historial_infor($check_borrados,$especialidad_id);	
				$mensaje = 1;
			}
			
			return json_encode($mensaje);
		}
		else
		{
			$results=[];
				// SI HAY REGISTROS SELECCIONADOS ENTONCES RECORREMOS ($selectedValues) 
				$datosHistoInfo = [];
				foreach ($selectedValues as $seleccionado)
					{
						$datos = array(
							'especialidad_id' => $especialidad_id,
							'histo_info_id' => $seleccionado
						);
						// BUSCAMOS SI YA EXISTE LOS HISTORIAL INFORMAITVOS PARA ESA ESPECIALIDAD
						$query_histo_info_existentes = $model->histo_info_existentes($datos);
						
						// SI ,NO HAY UN REGISTRO PARA HISTORIAL INFORMAITVOS  AGREGAMOS EL INSERT 
						if (empty($query_histo_info_existentes)) 
						{	
							if (!empty($check_borrados)) 
							{
								$query_actualizar_historial_existentes=$model->Eliminar_historial_infor($check_borrados,$especialidad_id);	
							}
							if (!empty($check_borrados)) 
							{
								$query_Historial_infor_existentes=$model->Eliminar_historial_infor($check_borrados,$especialidad_id);	
							}

							// AGREGAMOS EL INSERT 
							$results[] = $model->agregar($datos);
						}else
						{

							
							// BUSCAMOS HISTORIAL INFORMATIVO  CON SU ESTATUS DE BORRADO
							foreach ($query_histo_info_existentes as $existentes)
							{
								$historial['id']     =   $existentes->id;
								$historial['especialidad_id']     =   $existentes->especialidad_id;
								$historial['histo_info_id']     =   $existentes->histo_info_id;
								$historial['borrado']         = $existentes->borrado;

							}
								
							
							
							// VERIFICAMOS SI HAY HISTORIAL INFORMATIVO  BORRADOS
							if ($historial['borrado']=='t') 
							{
								$query_Historial_infor_existentes=$model->Activar_historial_infor($historial);
							}

							if (!empty($check_borrados)) 
							{
								$query_Historial_infor_existentes=$model->Eliminar_historial_infor($check_borrados,$especialidad_id);	
							}
						}

					}		
			}
			
	

			// Si todas las inserciones fueron exitosas, establezca el mensaje en 1
			if (count(array_filter($results)) === count($results)) {
				$mensaje = 1;
			} else {
				// Si alguna inserción falló, establezca el mensaje en 0
				$mensaje = 0;
			}

			
			

		return json_encode($mensaje);
	}



					
				
					// 	else 
					// 	{	
					// 		// BUSCAMOS HISTORIAL INFORMATIVO  CON SU ESTATUS DE BORRADO
					// 		foreach ($especialidad_histo_info as $existentes)
					// 		{
					// 			$historial['id']     =   $existentes->id;
					// 			$historial['especialidad_id']     =   $existentes->especialidad_id;
					// 			$historial['borrado']         = $existentes->borrado;
					// 		}
								
					// 		// VERIFICAMOS SI HAY HISTORIAL INFORMATIVO  BORRADOS
					// 		if ($historial['borrado']=='t') 
					// 		{
					// 			$query_Historial_infor_existentes=$model->Activar_historial_infor($historial);
					// 		}

					// 		if (!empty($check_borrados)) 
					// 		{
					// 			$query_Historial_infor_existentes=$model->Eliminar_historial_infor($check_borrados,$id_especialida);	
					// 		}

					// 		 }
					
							
					// }
					// 	// Si todas las inserciones fueron exitosas, establezca el mensaje en 1
					// 	if (count(array_filter($results)) === count($results)) {
					// 		$mensaje = 1;
					// 	} else {
					// 		// Si alguna inserción falló, establezca el mensaje en 0
					// 		$mensaje = 0;
					// 	}
					
					// 	return json_encode($mensaje);	

				// 	$query = $modelo->actualizar_especialidad($datos);
				// 	if (isset($query)) {
				// 		$mensaje = 1;
				//         $auditoria['accion'] = 'SE MODIFICARON LOS SIGUENTES DATOS DE LA ESPECIALIDAD  '.' '.$datos['decripcion_anterior'].','.' '.$datos_modificados['datos_modificados'];
				// 		$Auditoria_sistema_Model = $model_auditoria->agregar($auditoria);
				// 	} else {
				// 		$mensaje = 0;
				// 	}
				// 	//$mensaje=$datos;
				// 	return json_encode($mensaje);
 
}
