<?php

namespace App\Controllers;

use App\Models\Categoria_Model;
use CodeIgniter\API\ResponseTrait;
use App\Models\Enfermedad_Actual_Model;
use CodeIgniter\RESTful\ResourceController;

class Enfermedad_Actual_Controllers extends BaseController
{
	use ResponseTrait;

	public function agregar()
	{
		$model = new Enfermedad_Actual_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));

		$datos['n_historial']   = $data->n_historial;
		$datos['id_consulta']   = $data->id_consulta;
		$datos['descripcion']   = $data->enfermedad_actual;

		$query2 = $model->buscar_consulta($datos['id_consulta']);

		if ($query2) {
			$mensaje = 2;
		} else if (empty($query2)) {
			$query_agragar = $model->agregar($datos);
			if (isset($query_agragar)) {
				$mensaje = 1;
			} else {
				$mensaje = 0;
			}
		}


		return json_encode($mensaje);
	}

	public function actualizar_enfermedad_actual()
	{
		$model = new Enfermedad_Actual_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['id']   = $data->id;
		$datos['descripcion'] = $data->observacion;
		$datos['fecha_actualizacion'] = $data->today;
		$query = $model->actualizar_enfermedad_actual($datos);

		if (isset($query)) {
			$mensaje = 1;
		} else {
			$mensaje = 0;
		}

		return json_encode($mensaje);
	}





	public function listar_enfermedad_actual($n_historial, $id_consulta)
	{

		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		$model = new  Enfermedad_Actual_Model();

		$query = $model->listar_enfermedad_actual($n_historial, $id_consulta);
		if (empty($query)) {
			$enfermedad_actual = [];
		} else {
			$enfermedad_actual = $query;
		}
		echo json_encode($enfermedad_actual);
	}
}
