<?php

namespace App\Controllers;

use App\Models\Metodo_Anticonceptivo_Model;
use CodeIgniter\API\ResponseTrait;
use App\Models\Medicamentos_model;
use CodeIgniter\RESTful\ResourceController;

class Metodo_Anticonceptivo_Controllers extends BaseController
{
	use ResponseTrait;


	public function ListarMetodosAnticonceptivos_Activos()
	{
		$model = new Metodo_Anticonceptivo_Model();
		$query = $model->ListarMetodosAnticonceptivos_Activos();

		if (empty($query->getResult())) {
			$metodosac = [];
		} else {
			$metodosac = $query->getResultArray();
		}
		echo json_encode($metodosac);
	}











	/*
      * Función para mostrar el listado de Roles
      */
	public function vistacompuestos($id_medicamento)
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		$model = new Metodo_Anticonceptivo_Model();
		$query = $model->getDatosMedicamento($id_medicamento);

		//Obtengo Detalle del Medicamento para mostarlo
		//en el encabezdo de la vista
		if (empty($query->getResult())) {
			$infomedicamento = [];
		} else {
			foreach ($query->getResult() as $fila) {
				$infomedicamento['medicamento'] = $fila->medicamento;
				$infomedicamento['control']    = $fila->descripcion;
				$infomedicamento['id']    = $fila->id;
			}
		}
		echo view('/listarCompuesto/content_Compuestos', $infomedicamento);
		echo view('/listarCompuesto/footer_Compuestos');
	}
	/*
      * Método que guarda el registro nuevo
      */
	//public function save()
	public function agregar_compuesto()
	{
		$model = new Metodo_Anticonceptivo_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['id_medicamento']   = $data->id_medicamento;
		$datos['descripcioncompuesta']   = $data->descripcioncompuesta;
		$datos['cantidad']   = $data->cantidad;
		$datos['id_unidad_medida'] = $data->unidadmedida;
		$datos['id_tipo_medicamento'] = $data->id_tipo_medicamento;
		$query = $model->agregar_compuesto($datos);
		if (isset($query)) {
			$mensaje = 1;
		} else {
			$mensaje = 0;
		}
		return json_encode($mensaje);
	}
	/*
      * Función parar cargar los registros del Módulo en el Data Table o en las Persianas
      */
	public function getAll($id_medicamento)
	{
		$model = new Metodo_Anticonceptivo_Model();
		$query = $model->getAll($id_medicamento);
		if (empty($query)) {
			$compuestos = [];
		} else {
			$compuestos = $query;
		}
		echo json_encode($compuestos);
	}

	public function actualizar_compuestos()
	{
		$modelo = new Metodo_Anticonceptivo_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['id_tipo_medicamento'] = $data->id_tipo_medicamento;
		$datos['id_compuesto'] = $data->id_compuesto;
		$datos['id_unidad_medida '] = $data->unidadmedida;
		$datos['borrado']       = $data->borrado;
		$datos['cantidad']       = $data->cantidad;
		$query = $modelo->actualizar_compuestos($datos);
		if (isset($query)) {
			$mensaje = 1;
		} else {
			$mensaje = 0;
		}
		//$mensaje=$datos;
		return json_encode($mensaje);
	}
}
