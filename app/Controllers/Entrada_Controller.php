<?php

namespace App\Controllers;

use App\Models\Entrada_Model;
use App\Models\Medicamentos_model;
use App\Models\Salida_Model;

use App\Models\AuditoriaModel;
use CodeIgniter\API\ResponseTrait;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use \PhpOffice\PhpSpreadsheet\IOFactory;
use CodeIgniter\RESTful\ResourceController;

class Entrada_Controller extends BaseController
{
	use ResponseTrait;
	public function index($id)
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		return ('Esta es la Pagina de entrada  ...' . $id);
	}
	public function vistaEntradas($id_medicamento)
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		$model = new Medicamentos_model();
		$query = $model->getDatosMedicamento($id_medicamento);

		if (empty($query->getResult())) {
			$infomedicamento = [];
		} else {
			foreach ($query->getResult() as $fila) {
				$infomedicamento['medicamento'] = $fila->medicamento;
				$infomedicamento['control']    = $fila->descripcion;
				$infomedicamento['id']    = $fila->id;
			}
		}
		$totales = $model->getTotalesParaVistaEntradas($id_medicamento);
		$infomedicamento['total_entradas'] = $totales['total_entradas'];
		$infomedicamento['total_salidas'] = $totales['total_salidas'];
		$infomedicamento['total_stock']   = $totales['total_stock'];
		echo view('/entradas/content_Entradas', $infomedicamento);
		echo view('/entradas/footer_Entradas');
	}
	public function getAll($id_medicamento = null)
	{
		$model = new Entrada_Model();
		$query = $model->getAll($id_medicamento);

		//if(empty($query->getResult()))
		if (empty($query)) {
			$entradas = [];
		} else {
			$entradas = $query;
		}
		echo json_encode($entradas);
	}
	public function agregar()
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		date_default_timezone_set('America/Caracas');
		$hora = date("H:i:s A");

		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['id_medicamento']    = $data->id_medicamento;
		$datos['user_id']           = session('id_user');
		$datos['fecha_entrada']     = $this->formatearFecha($data->fecha_entrada);
		$datos['fecha_vencimiento'] = $this->formatearFecha($data->fecha_vencimiento);
		$datos['cantidad']          = $data->cantidad;
		$auditoria['id_medicamento']    = $data->id_medicamento;
		$auditoria['user_id']           = session('id_user');
		$auditoria['fecha_entrada']     = $this->formatearFecha($data->fecha_entrada);
		$auditoria['fecha_vencimiento'] = $this->formatearFecha($data->fecha_vencimiento);
		$auditoria['cantidad']          = $data->cantidad;
		$auditoria['hora'] = $hora;
		$model = new Entrada_model();
		$model2 = new AuditoriaModel();
		$query = $model->agregar($datos);
		$query2 = $model2->agregarAccion_Entrada($auditoria);
		if (isset($query)) {
			$mensaje = 1;
		} else {
			$mensaje = 0;
		}
		return json_encode($mensaje);
	}

	/*
      * Método que actualiza la entrada
      */
	public function actualizar_entrada()
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		date_default_timezone_set('America/Caracas');
		$hora = date("H:i:s A");
		$model_salidas = new Salida_Model();
		$modelo = new Entrada_model();
		$model2 = new AuditoriaModel();

		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['id']       = $data->id_entrada;
		$id_entrada = $data->id_entrada;
		$datos['user_id']           = session('id_user');
		$datos['fecha_entrada']     = $this->formatearFecha($data->fecha_entrada);
		$datos['fecha_vencimiento'] = $this->formatearFecha($data->fecha_vencimiento);
		$datos['id_medicamento']    = $data->id_medicamento;
		$datos['cantidad']          = $data->cantidad;
		$datos['borrado']       = $data->estatus;

		$auditoria['user_id']           = session('id_user');
		$auditoria['fecha_entrada']     = $this->formatearFecha($data->fecha_entrada);
		$auditoria['fecha_vencimiento'] = $this->formatearFecha($data->fecha_vencimiento);
		$auditoria['id_medicamento']    = $data->id_medicamento;
		$auditoria['cantidad']          = $data->cantidad;
		$auditoria['hora'] = $hora;

		$query_salidas = $model_salidas->verificar_salidas_para_la_entrada($id_entrada);
		//$mensaje=$query_salidas;


		if (empty($query_salidas)) {
			$query = $modelo->Reverso_entrada($datos);
			$query2 = $model2->auditoria_reverso($auditoria);
			if (isset($query)) {
				$mensaje = 1;
			} else {
				$mensaje = 2;
			}
		} else {
			$mensaje = 0;
		}




		return json_encode($mensaje);
	}


	public function actualizar_fecha_v()
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		date_default_timezone_set('America/Caracas');
		$hora = date("H:i:s A");
		$modelo = new Entrada_model();
		$model2 = new AuditoriaModel();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['id']       = $data->id_entrada;
		$datos['user_id']           = session('id_user');
		$datos['fecha_entrada']     = $this->formatearFecha($data->fecha_entrada);
		$datos['fecha_vencimiento'] = $this->formatearFecha($data->fecha_vencimiento);
		$datos['id_medicamento']    = $data->id_medicamento;
		$datos['cantidad']          = $data->cantidad;
		$datos['borrado']       = $data->estatus;
		$auditoria['user_id']           = session('id_user');
		$auditoria['fecha_entrada']     = $this->formatearFecha($data->fecha_entrada);
		$auditoria['fecha_vencimiento'] = $this->formatearFecha($data->fecha_vencimiento);
		$auditoria['id_medicamento']    = $data->id_medicamento;
		$auditoria['cantidad']          = $data->cantidad;
		$auditoria['hora'] = $hora;
		$query = $modelo->actualizar_fecha_v($datos);
		$query2 = $model2->auditoria_actualizacion_fv($auditoria);

		if (isset($query)) {
			$mensaje = 1;
		} else {
			$mensaje = 0;
		}
		//$mensaje=$datos;
		return json_encode($mensaje);
	}










	// ***********ESTE METODO MUESTRA UN REPORTE DE LAS ENTRADAS EN GENERAL************
	public function  VerEntradasMedicamentosPdf($id_categoria = 0, $cronicos = 0, $desde = null, $hasta = null, $nombre_categoria = null, $id_estatus = 0, $nombre_estatus = null)
	{

		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		$model = new Entrada_model();
		$entradas = $model->VerEntradasMedicamentosPdf($id_categoria, $cronicos, $desde, $hasta, $nombre_categoria, $id_estatus, $nombre_estatus);
		if (empty($entradas)) {
			$entradas = [];
		} else {
			$entradas = $entradas;
		}
		echo json_encode($entradas);
	}

	// ***********ESTE METODO MUESTRA UN REPORTE FPDF DE LAS ENTRADAS ************
	public function GenerarReportesEntradaPorFecha($desde = null, $hasta = null, $id_medicamento = null, $nombre_categoria = null)
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}

		$model = new Entrada_model();
		if ($desde != 'null' and $hasta != 'null') {
			$entradas = $model->GenerarReportesEntradaPorFecha($desde, $hasta, $id_medicamento);
		} else {
			$entradas = $model->getAllEntradas($id_medicamento);
		}

		if (empty($entradas)) {
			$entradas = [];
		} else {
			$entradas = $entradas;
		}
		echo json_encode($entradas);
	}


	public function Relacion_entradas_medicamentos()
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		} else {
			echo view('/entradas/Relacion_entradas_medicamentos');
			echo view('/entradas/footer_Relacion_entradas_medicamentos');
		}
	}
}
