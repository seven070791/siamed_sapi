<?php

namespace App\Controllers;

use App\Models\Categoria_Model;
use CodeIgniter\API\ResponseTrait;
use App\Models\Alergias_Medicamentos_Model;
use CodeIgniter\RESTful\ResourceController;

class Alergias_Medicamentos_Controllers extends BaseController
{
	use ResponseTrait;

	public function agregar_alergias_medicamentos()
	{
		$model = new Alergias_Medicamentos_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['n_historial']   = $data->n_historial;
		$datos['descripcion']   = $data->alergias;
		$datos['id_consulta']   = $data->id_consulta;
		$query2 = $model->buscar_consulta($datos['id_consulta']);

		if ($query2) {
			$mensaje = 2;
		} else if (empty($query2)) {
			$query = $model->agregar_alergias_medicamentos($datos);
			if (isset($query)) {
				$mensaje = 1;
			} else {
				$mensaje = 0;
			}
		}

		return json_encode($mensaje);
	}



	public function actualizar_alergias_medicamentos()
	{
		$model = new Alergias_Medicamentos_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['id']   = $data->id;
		$datos['descripcion'] = $data->alergias;
		$datos['fecha_actualizacion'] = $data->today;
		$query = $model->actualizar_alergias_medicamentos($datos);

		if (isset($query)) {
			$mensaje = 1;
		} else {
			$mensaje = 0;
		}

		return json_encode($mensaje);
	}


	public function listar_alergias_medicamentos($n_historial)
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		$model = new Alergias_Medicamentos_Model();

		$query = $model->listar_alergias_medicamentos($n_historial);
		if (empty($query)) {
			$enfermedad_actual = [];
		} else {
			$enfermedad_actual = $query;
		}
		echo json_encode($enfermedad_actual);
	}
}
