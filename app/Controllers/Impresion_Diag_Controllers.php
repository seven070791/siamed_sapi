<?php

namespace App\Controllers;

use App\Models\Categoria_Model;
use CodeIgniter\API\ResponseTrait;
use App\Models\Impresion_Diag_Model;
use CodeIgniter\RESTful\ResourceController;

class Impresion_Diag_Controllers extends BaseController
{
	use ResponseTrait;

	public function agregar_impresiondiagnostica()
	{
		$model = new Impresion_Diag_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['n_historial']   = $data->n_historial;
		$datos['descripcion']   = $data->impresiondiagnostica;
		$datos['id_consulta']   = $data->id_consulta;

		$query2 = $model->buscar_consulta($datos['id_consulta']);
		if ($query2) {
			$mensaje = 2;
		} else if (empty($query2)) {
			$query = $model->agregar($datos);
			if (isset($query)) {
				$mensaje = 1;
			} else {
				$mensaje = 0;
			}
		}
		return json_encode($mensaje);
	}


	public function listar_impresion_diag($n_historial)
	{

		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		$model = new Impresion_Diag_Model();

		$query = $model->listar_impresion_diag($n_historial);
		if (empty($query)) {
			$impresion_diag = [];
		} else {
			$impresion_diag = $query;
		}
		echo json_encode($impresion_diag);
	}


	public function actualizar_impresiondiagnostica()
	{
		$model = new Impresion_Diag_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['id']   = $data->id;
		$datos['descripcion'] = $data->impresiondiagnostica;
		$datos['fecha_actualizacion'] = $data->today;
		$query = $model->actualizar_impresiondiagnostica($datos);

		if (isset($query)) {
			$mensaje = 1;
		} else {
			$mensaje = 0;
		}

		return json_encode($mensaje);
	}
}
