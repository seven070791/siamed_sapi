<?php

namespace App\Controllers;

use App\Models\Categoria_Model;
use CodeIgniter\API\ResponseTrait;
use App\Models\Antecendentes_Patologicosp_Model;
use CodeIgniter\RESTful\ResourceController;

class Antecedentes_Patologicosp_Controllers extends BaseController
{
	use ResponseTrait;

	public function agregar_antecedentes_patologicosp()
	{
		$model = new Antecendentes_Patologicosp_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['n_historial']   = $data->n_historial;
		$datos['descripcion']   = $data->antecedentes_patologicosp;
		$datos['id_consulta']   = $data->id_consulta;
		$query2 = $model->buscar_consulta($datos['id_consulta']);

		if ($query2) {
			$mensaje = 2;
		} else if (empty($query2)) {
			$query = $model->agregar_antecedentes_patologicosp($datos);
			if (isset($query)) {
				$mensaje = 1;
			} else {
				$mensaje = 0;
			}
		}
		return json_encode($mensaje);
	}



	public function listar_antecedentes_patologicosp($n_historial)
	{

		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		$model = new Antecendentes_Patologicosp_Model();

		$query = $model->antecedentes_patologicosp($n_historial);
		if (empty($query)) {
			$antecedentes_personales = [];
		} else {
			$antecedentes_personales = $query;
		}
		echo json_encode($antecedentes_personales);
	}





	public function actualizar_antecedentes_patologicosp()
	{
		$model = new Antecendentes_Patologicosp_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['id']   = $data->id;
		$datos['descripcion'] = $data->antecedentes_patologicosp;
		$datos['fecha_actualizacion'] = $data->today;
		$query = $model->actualizar_antecedentes_patologicosp($datos);

		if (isset($query)) {
			$mensaje = 1;
		} else {
			$mensaje = 0;
		}

		return json_encode($mensaje);
	}
}
