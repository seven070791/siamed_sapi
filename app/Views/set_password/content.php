<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
  <meta charset="utf-8">
  <title>SIAMED </title>
  <link rel="icon" type="image/png" href="<?= base_url() ?>/img/siamedcorazon.jpg" style="width: 50px; height: 50px;">

  <link href="<?php echo base_url(); ?>/css/login2.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <!-- /.login-logo -->
    <div class="card">
    <div class="container-sm " style="text-align: right;  ">
      <img class="img_center" src="<?php echo base_url(); ?>/img/sapi.png" width="220" alt="Avatar">
    </div>
    <section class="form-login">
      <div class="card-body login-card-body">
        <p class="login-box-msg"><h3>Introduzca la contraseña nuevamente para volver a iniciar sesion</h3></p>
        <br>
        
          <input type="hidden" name="correo" id="correo" value="<?php echo $email; ?>">
          <div class="input-group mb-6">
            <input type="password"  id="newpass" name="newpass" class="controls" placeholder="Introduzca la nueva contraseña" autocomplete="off">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-6">
            <input type="password" id="confirmpass" name="confirmpass" class="controls" placeholder="Confirme la nueva contraseña" autocomplete="off">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-12">
              <button type="button" id="recoverPassword"class="buttons">Cambiar Clave</button>
            </div>
            <!-- /.col -->
          </div>
       

        <!-- <p class="mt-3 mb-1">
          <a href="/">Iniciar Sesion</a>
        </p> -->
      </div>
      <!-- /.login-card-body -->
      </section>
    </div>
  </div>
  <!-- /.login-box -->