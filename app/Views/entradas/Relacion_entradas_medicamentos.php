<?= $this->extend('menu/supermenu') ?>
<?= $this->section('content') ?>
<link href="/css/relacion_entradas_medicamento.css" rel="stylesheet" type="text/css" />
<div class="container">
	<br>
	<div class="row ">
		<div class="col-3">
		</div>
		<div class="col-8">
			<h5 class="center"> RELACION ENTRADAS DE MEDICAMENTOS </h5>
		</div>
		<input type="hidden" id="usuario" autocomplete="off" style="width:102px;" value='<?= session('nombreUsuario'); ?>'> &nbsp;
	</div>
	<div class="row">
  <div class="col-md-3">
    <input type="hidden" id="categoria" autocomplete="off" value="">
    <label for="cmbCategoria" class="label-categoria">Categoria</label>
    <select class="select-categoria" id="cmbCategoria" autocomplete="off" required>
      <option value="0">Seleccione</option>
    </select>
  </div>
  <div class="col-md-4">
    <label for="desde">Desde</label>
    <input type="date" class="bodersueve" value="<?php echo date('y-m-d'); ?>" name="desde" id="desde">
    <label for="hasta">Hasta</label>
    <input type="date" class="bodersueve" value="<?php echo date('y-m-d'); ?>" name="hasta" id="hasta">
  </div>
  <div class="col-md-5">
    <div class="medic_cronico" id="medic_cronico">
      <label for="cronico" class="label-cronico">Crónico</label>

      <select class="select" id="cronico" name="" data-style="btn-primary">
        <option value="0" selected disabled>Seleccione</option>
        <option value="1">SI</option>
        <option value="2">NO</option>
      </select>
    </div>
  </div>
</div>



	<div>
		&nbsp;&nbsp;<label class="labelestatus">Estatus</label>&nbsp;&nbsp;&nbsp;
		<select class="custom-select " style="width:180px;" id="id_estatus" name="estatus" data-style="btn-primary">
			<option value="0" selected disabled>seleccione</option>
			<option value="1">Activo</option>
			<option value="2">Vencido</option>
		</select>
	</div>

	<style>
		table.dataTable thead,
		table.dataTable tfoot {
			background: linear-gradient(to right, #4a779c, #7e9ab1, #5f7a91);
		}
	</style>
	<div class="row">
		<div class="col-md-5">
		</div>
		<div class="col-md-4">
			<button id="btnfiltrar" class="btn btn-primary btn-sm">Filtrar</button>
			<button id="btnlimpiar" class="btn btn-primary btn-sm">Limpiar</button>
		</div>
	</div>
	<input type="hidden" class="border" name="fecha_del_dia" id="fecha_del_dia" style="width:100px;" autocomplete="off" style=" z-index: 1050 !important;">
	<div class="col-lg-12">
		<table class="display" id="table_relacion_entradas" style="width:100%" style="margin-top: 20px">
			<thead>
				<tr>
					<td class="text-center" style="width:7%">FECHA </td>
					<td class="text-center" style="width:30%">DESCRIPCION </td>
					<td class="text-center" style="width:7%">FECHA V </td>
					<td class="text-center" style="width:10%">CATEGORIA</td>
					<td class="text-center" style="width:0%">CRONICO</td>
					<td class="text-center" style="width:0%">CANTIDAD</td>
					<td class="text-center" style="width:2%">USUARIO</td>
					<td class="text-center" style="width:7%">ESTATUS </td>

				</tr>
			</thead>
			<tbody id="lista_notas_Entregas">
			</tbody>
		</table>


		<script>
			// ***CONFIGURACION DE DATAPICKER :( MOSTRAR AÑO Y DAR FORMATO DEFECHA D-M-A)*****
			$(function() {
				$("#fecha_del_dia").datepicker({
					dateFormat: 'dd/mm/yy',
					changeMonth: true,
					changeYear: true
				});
				$("#fecha_del_dia").datepicker({
					dateFormat: 'dd/mm/yy',
					changeMonth: true,
					changeYear: true
				});


			});
		</script>


		<script>
			$(document).ready(function() {

				var now = new Date();

				var day = ("0" + now.getDate()).slice(-2);
				var month = ("0" + (now.getMonth() + 1)).slice(-2);
				//var today = day + "/" + month + "/" + now.getFullYear();
				// var today= (day)+"-"+(month)+"-"+now.getFullYear();
				var today = now.getFullYear() + "-" + (month) + "-" + (day);
				$("#fecha_del_dia").val(today);
			});
		</script>

		<!-- ***** FUNCION PARA SOLO NUMEROS***-** -->
		<script type="text/javascript">
			function valideKey(evt) {

				// code is the decimal ASCII representation of the pressed key.
				var code = (evt.which) ? evt.which : evt.keyCode;

				if (code == 8) { // backspace.
					return true;
				} else if (code >= 48 && code <= 57) { // is a number.
					return true;
				} else { // other keys.
					return false;
				}
			}
		</script>
		<?= $this->endSection(); ?>