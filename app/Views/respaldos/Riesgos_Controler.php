<?php

namespace App\Controllers;

use App\Models\Categoria_Model;
use CodeIgniter\API\ResponseTrait;
use App\Models\Plan_Model;
use App\Models\Riesgos_Model;
use CodeIgniter\RESTful\ResourceController;

use function PHPSTORM_META\type;

class Riesgos_Controler extends BaseController
{
	use ResponseTrait;

	public function agregar_Riesgos()

	{
	

		$model = new Riesgos_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$nHistorials = $data->n_historials;
		$n_historia= $data->n_historia;
		$check_borrados=[];
		$check_borrados=$data->valores_borrados;
		$selectedValues = $data->selectedValues;

		
		if (empty($selectedValues)) 
		{
			$query_actualizar_riesgos_existentes=$model->actualizar_riesgos_existentes($check_borrados,$n_historia);	
			$mensaje = 1;
			return json_encode($mensaje);
		}else
		{
			// Inicialice una matriz vacía para almacenar los resultados de cada inserción
			$results = [];
			// Recorra el arreglo nHistorials e inserte una nueva fila para cada uno
			foreach ($nHistorials as $index => $nHistorial)
			{
			
				$datos = array(
					'n_historial' => $nHistorial,
					'id_riesgo' => $selectedValues[$index]
				);
				// BUSCAMOS SI YA EXISTE ESA PATOLOGIA EN EL HISTORIAL ACTUAL
				$query_buscar_riesgos_existentes = $model->buscar_riesgos_existentes($datos);
				// SI , LA PATOLOGIA NO EXISTE LA AGREGAMOS 
				if (empty($query_buscar_riesgos_existentes)) {
					$results[] = $model->agregar($datos);
				}

					// VERIFICAMOS SI HAY riesgos BORRADAS
					if (!empty($check_borrados)) 
					{
						$query_actualizar_riesgos_existentes=$model->actualizar_riesgos_existentes($check_borrados,$n_historia);	
					}		
			}
			
				// Si todas las inserciones fueron exitosas, establezca el mensaje en 1
				if (count(array_filter($results)) === count($results)) {
					$mensaje = 1;
				} else {
					// Si alguna inserción falló, establezca el mensaje en 0
					$mensaje = 0;
				}

		}

		return json_encode($mensaje);
	
	}


	public function Listar_riesgos()
    {
		
        $modelo_Riesgos = new  Riesgos_Model();
        $query_Riesgos = $modelo_Riesgos->Listar_riesgos();

        if (empty($query_Riesgos)) {
            $Riesgos = [];
        } else {
            $Riesgos = $query_Riesgos;
        }
        echo json_encode($Riesgos);
    }


	public function listar_riesgos_Historial($numeroHistorial=null)
    {
        $modelo_riesgos = new  Riesgos_Model();
        $query_riesgos = $modelo_riesgos->listar_riesgos_Historial($numeroHistorial);

        if (empty($query_riesgos)) {
            $riesgos = [];
        } else {
            $riesgos = $query_riesgos;
        }
        echo json_encode($riesgos);
    }
	

	public function actualizar_plan()
	{
		$model = new Plan_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['id']   = $data->id;
		$datos['descripcion'] = $data->plan;
		$datos['fecha_actualizacion'] = $data->today;
		$query = $model->actualizar_plan($datos);

		if (isset($query)) {
			$mensaje = 1;
		} else {
			$mensaje = 0;
		}

		return json_encode($mensaje);
	}
}
