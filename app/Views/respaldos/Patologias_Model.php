<?php namespace App\Models;
use CodeIgniter\Model;
use PhpOffice\PhpSpreadsheet\Worksheet\Row;

class Patologias_Model extends BaseModel
{
	
	public function agregar($data)
	{
		$builder = $this->dbconn('historial_clinico.patologia_historial');
		$query = $builder->insert($data);
		return $query ? true : false;
	
	}
	
	public function listar_patologias()
	{
 
	   $db      = \Config\Database::connect();
	   $strQuery ="";
	   $strQuery .="SELECT";
	   $strQuery .=" patologias.id";  
	   $strQuery .=",patologias.descripcion "; 
	   $strQuery .="FROM ";
	   $strQuery .="  historial_clinico.patologias ";	
	  // return  $strQuery;
	  $query = $db->query($strQuery);
	  $resultado=$query->getResult(); 
	  return $resultado;
	}
	public function listar_patologias_Historial($numeroHistorial=null)
	{
 
	   $db      = \Config\Database::connect();
	   $strQuery ="";
	   $strQuery .="SELECT";
	   $strQuery .=" patologias.id_patologia,patologias.borrado ";  
	   $strQuery .="FROM ";
	   $strQuery .="  historial_clinico.patologia_historial as patologias ";	
	   $strQuery .="  where patologias.n_historial='$numeroHistorial' ";	
	   $strQuery .="  and patologias.borrado='false'";
	   //return  $strQuery;
	  $query = $db->query($strQuery);
	  $resultado=$query->getResult(); 
	 return $resultado;
	}

	public function buscar_patologias_existentes($datos=null)
	{
 
		$n_historial = $datos['n_historial'];
		$id_patologia = $datos['id_patologia'];
	   $db      = \Config\Database::connect();
	   $strQuery ="";
	   $strQuery .="SELECT";
	   $strQuery .=" patologias.id_patologia ";  
	   $strQuery .="FROM ";
	   $strQuery .="  historial_clinico.patologia_historial as patologias ";	
	   $strQuery .="  where patologias.n_historial='$n_historial' ";	
	   $strQuery .="  and patologias.id_patologia=$id_patologia ";	
	   $strQuery .="  and patologias.borrado='false'";
	   $query = $db->query($strQuery);
	   $resultado=$query->getResult(); 
	 return $resultado;
	}
	public function patologias_existentes($datos=null)
	{
 
		$n_historial = $datos['n_historial'];
		$id_patologia = $datos['id_patologia'];
	   $db      = \Config\Database::connect();
	   $strQuery ="";
	   $strQuery .="SELECT";
	   $strQuery .=" patologias.id_patologia ";  
	   $strQuery .="FROM ";
	   $strQuery .="  historial_clinico.patologia_historial as patologias ";	
	   $strQuery .="  where patologias.n_historial='$n_historial' ";	
	   $strQuery .="  and patologias.id_patologia=$id_patologia ";	
	   $query = $db->query($strQuery);
	   $resultado=$query->getResult(); 
	 return $resultado;
	}

	

	public function actualizar_patologias_existentes($check_borrados,$n_historia)
	{
		$builder = $this->dbconn(' historial_clinico.patologia_historial as patologias');
		foreach ($check_borrados as $Row)
		{	
			$builder->where('n_historial',$n_historia);
			$builder->where('id_patologia',$Row);
			$query = $builder->update([
				'borrado' => true,
			]);
		}
		return $query;
	   //return  $strQuery;
	}
	
	
}

	