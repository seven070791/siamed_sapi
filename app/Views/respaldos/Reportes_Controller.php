<?php namespace App\Controllers;
use App\Models\Entrada_Model;
use App\Models\Salida_Model;
use App\Models\Medicamentos_model;
use CodeIgniter\API\ResponseTrait;

use CodeIgniter\RESTful\ResourceController;

class Reportes_Controller extends BaseController
{
     use ResponseTrait;
    public function Vistareportes($id_medicamento)
    {
		if (!session('nombreUsuario')) {
            return redirect()->to(base_url().'/index.php'); 
        }
		$model = new Medicamentos_model();
		$query=$model->getDatosMedicamento($id_medicamento);

		//Obtengo Detalle del Medicamento para mostarlo
		//en el encabezdo de la vista
		if(empty($query->getResult()))
		{
			$infomedicamento=[];
		}
		else
		{
			foreach($query->getResult() as $fila)
			{
				$infomedicamento['medicamento']=$fila->medicamento;
				$infomedicamento['control']    =$fila->control;
				$infomedicamento['descripcion']=$fila->descripcion;
				$infomedicamento['id']    =$fila->id;
			
			}
		}
	
		//var_dump($infomedicamento);
		echo view('/reportes/reportes',$infomedicamento);
		echo view('/reportes/footer_Reportes');
	} 
	public function entradasFpdfMedicamento($id_medicamento)
	{
		$pdf =new \FPDF('P','mm','letter');
		$pdf->AddPage();
		$pdf->Header_Entradas();
			$pdf->target="_new"; 
	
		$pdf->SetMargins(10,10,10);
 	   $model = new Entrada_model(); 
 	   $entradas=$model->getAllEntradas($id_medicamento);
 	   if(empty($entradas))
	   {
		   $pdf->cell(196, 5, utf8_decode('Sin Información Coincidente'), 1, 1, 'C', 1);
	   }
	   else
	   {  
		   $i=0;
		   foreach($entradas as $entradas)
		   {
			   $descripcion=$entradas->descripcion;
			   $fecha_entrada=$entradas->fecha_entrada;
			   $fecha_vencimiento=$entradas->fecha_vencimiento;
			   $cantidad=$entradas->cantidad;
			   $nombre=$entradas->nombre;
			   
			   
			   $pdf->Cell(61,5,$descripcion,1,0,'L');
 			   $pdf->Cell(36,5,$fecha_entrada,1,0,'L');
 			   $pdf->Cell(42,5,$fecha_vencimiento,1,0,'L');
			   $pdf->Cell(27,5,$cantidad,1,0,'L');
			   $pdf->Cell(30,5,$nombre,1,1,'L'); 			
			   $i++;
			   if ($i==40) {
				   $pdf->AddPage();
				   $pdf->Header_Entradas();
 				   $i=0;	
 			   }
					   
			}
		   
	   }
	   $this->response->setHeader('Content-Type','application/pdf');
	   $pdf->Output("entradas_pdf.pdf","I");
   // echo view( 'reportes/reportes');

 	 }
	  public function salidasFpdfMedicamento($id_medicamento)
	 {
		$pdf =new \FPDF('P','mm','letter');
		$pdf->AddPage();
		$pdf->Header_Salidas();
	    $model = new Salida_model(); 
	    $salidas=$model->getAllSalidas($id_medicamento);
		if(empty($salidas))
		{
			$pdf->cell(196, 5, utf8_decode('Sin Información Coincidente'), 1, 1, 'C', 1);
		}
		else
		{
			$i=0;
			foreach($salidas as $salidas)
			{
				$descripcion=$salidas->descripcion;
				$fecha_salida=$salidas->fecha_salida;
				$cantidad=$salidas->cantidad;
				$nombre=$salidas->nombre;
				$tipo_beneficiario=$salidas->tipo_beneficiario;
				$beneficiario=$salidas->beneficiario;
				$pdf->Cell(20,5,$nombre,1,0,'L');
				$pdf->Cell(64,5,$descripcion,1,0,'L');
				$pdf->Cell(26,5,$fecha_salida,1,0,'L');
				$pdf->Cell(20,5,$cantidad,1,0,'L');
				$pdf->Cell(40,5,$beneficiario,1,0,'L');
				$pdf->Cell(30,5,$tipo_beneficiario,1,1,'L');

				$i++;
				if ($i==30) {
					$pdf->AddPage();
					$pdf->Header_Salidas();
					$i=0;	
				}
		 	}
		}
			 $this->response->setHeader('Content-Type','application/pdf');
		 $pdf->Output("salidas_pdf.pdf","I");

	 }

	
}
