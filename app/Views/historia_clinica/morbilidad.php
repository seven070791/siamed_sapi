<?= $this->extend('menu/supermenu') ?>
<!-- INICIO DE LA SECION CONTENT -->
<?= $this->section('content') ?>
<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->
  <link href="/css/morbilidad.css" rel="stylesheet" type="text/css" />
  <!-- Bootstrap CSS -->
  <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"> -->

  <!--    Datatables  -->
  <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.css" /> -->
  <title></title>
</head>

<body>
  <BR>
  <BR>
  <BR>
  <h3 class="text-center">HOJA DE MORBILIDAD </h3>


  <div class="container">
    <div class="row">
      <div class="col-lg-13">
        <table id="example" class="table table-striped table-bordered" style="width:110%">
          <thead>
            <tr>
              <th class="text-center">Nª</th>
              <th class="text-center">Nombre </th>
              <th class="text-center">Apellido </th>
              <th class="text-center">Edad </th>
              <th class="text-center">Sexo </th>
              <th class="text-center">Cedula </th>
              <th class="text-center">Estatus </th>
              <th class="text-center">Area o Departamento </th>
              <th class="text-center">Diagnostico </th>
              <th class="text-center">Morivo de Consulta </th>
              <th class="text-center">Fecha </th>
              <th class="text-center">Medico de Guardia </th>
            </tr>
          </thead>
          <tbody>
      </div>

    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
 -->

    <!--    Datatables-->
    <!-- <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.js"></script> -->



    <!-- <script>
      //Idiomas con el 1er método   
      $(document).ready(function() {
        $('#example').DataTable({
          "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.20/i18n/Spanish.json"
          }

        });
      });
    </script> -->


</body>

</html>
<?= $this->endSection() ?>