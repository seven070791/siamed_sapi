<?= $this->extend('menu/supermenu') ?>
<!-- INICIO DE LA SECION CONTENT -->
<?= $this->section('content') ?>
<!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->
<link href="/css/vista_habitos.css" rel="stylesheet" type="text/css" />
<br>
<div class="container">

	<input type="hidden" id="cedulatitular" disabled="disabled" class="control" style="width:100px;" value='<?php echo $cedula_trabajador; ?>'>
	<div class="row ">
		<div class="col-4">
		</div>
		<div class="col-4">

			<h3 class="center">Registro /habitos </h3>
		</div>
		<br>
		<div class="col-3">
			<button id="btnAgregar" class=" btn-primary btnAgregar">Agregar</button> &nbsp;&nbsp;
			<button id="btnRegresar" class=" btn-secondary btnRegresar">Regresar</button>
		</div>
	</div>
	<br>
	<input type="hidden" id="cedula" disabled="disabled" class="control" style="width:100px;" value='<?php echo $cedula_trabajador; ?>'>
	<input type="hidden" id="historial_id" class="control" style="width:100px;" value='<?php echo $historial_id; ?>'>
	<input type="hidden" id="n_historial" class="control" style="width:100px;" value='<?php echo  $n_historial; ?>'>
	<input type="hidden" id="tipo_beneficiario" class="control" style="width:100px;" value='T'>

	<style>
		table.dataTable thead,
		table.dataTable tfoot {
			background: linear-gradient(to right, #4a779c, #7e9ab1, #5f7a91);
		}
	</style>
	<div class="row ">
		<div class="col-2">
		</div>
		<div class="col-md-8">
			<table class="display" id="table_historial" style="width:100%">
				<thead>
					<tr>
						<td class="text-center" style="width:10%">Nº</td>
						<td class="text-center" style="width:45%">Descripcion</td>
						<td class="text-center" style="width:15%">Acciones</td>
					</tr>
				</thead>
				<tbody id="historial_habitos">
				</tbody>
			</table>
		</div>
	</div>

	<!-- Ventana Modal -->

	<div class=" modal fade" id="modal_citas" tabindex="-1" data-backdrop="static">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="row col-md-12">
					<div class="form-group has-feedback" id="cajas1">
						<div class="modal-header text-center">
							<h5>Registro de Habitos del Paciente</h5>
							<label class="labelfecha">Fecha</label>
							<input class="sinborder" type="text" id="fecha_del_dia" style="width:90px;" disabled="disabled" value="" />
							<br>
							<br>
							<br>
							<br>
							<label class="labelhabitos">Habitos Psicosociales</label>
							<select class="form-control habitos_psicosociales " id="habitos_psicosociales" name="especialidad" data-style="btn-primary" style="width:300px;">
								<option value="seleccione">seleccione</option>
							</select>
							<br>
							<br>
						</div>

						<div class="modal-footer">

							<button id="btnGuardar" class="btn-primary btn-sm">Guardar</button>
							<button id="btnCerrar" type="button" class="btn-primary btn-sm" data-dismiss="modal" id="btnClose" name="btnCerrar">Cerrar</button>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>






	<!-- ***** FUNCION PARA SOLO NUMEROS***-** -->
	<script type="text/javascript">
		function valideKey(evt) {

			// code is the decimal ASCII representation of the pressed key.
			var code = (evt.which) ? evt.which : evt.keyCode;

			if (code == 8) { // backspace.
				return true;
			} else if (code >= 48 && code <= 57) { // is a number.
				return true;
			} else { // other keys.
				return false;
			}
		}
	</script>

	<!-- <script src="https://code.jquery.com/jquery-3.1.0.js"></script> -->
	<script>
		$(document).ready(function() {

			var now = new Date();

			var day = ("0" + now.getDate()).slice(-2);
			var month = ("0" + (now.getMonth() + 1)).slice(-2);
			var today = day + "-" + month + "-" + now.getFullYear();
			// var today= (day)+"-"+(month)+"-"+now.getFullYear();
			// var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
			$("#fecha_del_dia").val(today);
		});
	</script>

	<script>
		// ***CONFIGURACION DE DATAPICKER :( MOSTRAR AÑO Y DAR FORMATO DEFECHA D-M-A)*****
		$(function() {
			// $("#modal_editar").modal("show");

			$("#fecha").datepicker({
				dateFormat: 'dd/mm/yy',
				changeMonth: true,
				changeYear: true,
				yearRange: '1930:2022',
				maxDate: '+30Y',
				inline: true
			});
			$("#fecha1").datepicker({
				dateFormat: 'dd/mm/yy',
				changeMonth: true,
				changeYear: true,
				yearRange: '1930:2022',
				maxDate: '+30Y',
				inline: true
			});


		});
	</script>
	<?= $this->endSection(); ?>