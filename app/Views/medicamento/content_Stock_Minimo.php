<?= $this->extend('menu/supermenu')?>
<!-- INICIO DE LA SECION CONTENT -->
<?= $this->section('content')?>
<!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">    
 
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->
	<link href="/css/vista_stock_minimo.css" rel="stylesheet" type="text/css" />	 
<div class="container">  
	<br>
	<div class="row ">
  		<div class="col-4">
  		</div>
		<div class="col-5">
		<h4 class="center">	Inventario por Debajo del Stock Minimo</h4>
		</div>		
		<div class="col-2">
		<input type="hidden" id="usuario"   autocomplete="off" style="width:100px;"value='<?= session('nombreUsuario');?>'> &nbsp;
  		</div>
  	</div>
	
	<div class="row">
		<div class="col-md-1">
		</div>
		<div class="col-md-5">
			<input type="hidden" id="categoria"   autocomplete="off" style="width:100px;"value=> &nbsp;
			<div class="from-group">
				&nbsp;&nbsp;&nbsp;<label id=""><h5>Categoria</h5></label>&nbsp;&nbsp;			    
				<select class="custom-select" style="width:200px;" id="cmbCategoria" autocomplete="off" required >
				<option value="0">Seleccione</option>
				</select>&nbsp;
				
			</div>				
		</div>
		<div class="col-md-1">
			<div class="medic_cronico" id="medic_cronico">
			&nbsp;<label class="labelcronico" id="labelcronico">Crónico</label>
		 	<select class="custom-select" style="width:120px;"  id="cronico" name= "cronico" data-style="btn-primary" >
			<option value="0"selected disabled>Seleccione</option> 
			<option value="1">SI</option> 
			<option value="2">NO</option> 
			</select>		  				
			</div>
		</div>	
		<div class="col-md-4 botones" id="botones">
			<button id="btnfiltrar" class="btn btn-primary btn-sm">Filtrar</button>
			<button id="btnlimpiar" class="btn btn-primary btn-sm">Limpiar</button>	 
		</div>
		
	</div>	

<style>
   table.dataTable thead, table.dataTable tfoot {
           background: linear-gradient(to right, #4a779c,#7e9ab1,#5f7a91);
       }
   </style>
	<div class="row">
		<div class="col-md-1">
		</div>
		<div class="col-11">

		<table class="display" id="table_medicamentos" style="width:100%" >
			<thead>
				<tr> 
					<td >Descripcion</td>
					<td >Categoria</td>
					<td >Cronico</td>
					<td >Control</td>
					<td >Stock Minimo</td>
					<td >Stock</td> 
				</tr>
			</thead>
			<tbody id="lista_medicamento">
			</tbody>
		</table>
		</div>				
	</div>	

</div>
<section class="form-login">

<!-- Ventana Modal -->
<div class="row">
	<div class="modal fade" id="modal"role="dialog" aria-labelledby="exampleModalLabel">
    	<div class="modal-dialog" role="document">
      		<div class="modal-content">
					<div class="col-xs-8">
           				<h4 class="modal-title center" id="modal">&nbsp;&nbsp;&nbsp;Registro   Medicamento&nbsp;&nbsp;</h4>
					</div>
					<div class="col-xs-4">
					   <button type="button" class="btn btn-success  btncompuestos" id="btncompuestos">(+)Compuestos</button> 
					</div>

 				<div class="modal-body">
   					<div id="datos_ajax_register"></div>
					<div>
						<input type="hidden" id='id' name='id'>
						<input type="hidden" id='prueba' name='id_unidadmedida'>
					</div>
      				<div class="form-group">
					  <label for="nombre0" class="control-label">Descripcion</label>					  
					  <input type="text" class="form-control"onkeyup="mayus(this);" id="descripcion"  name="descripcion" style="width:450px;"  placeholder="" autocomplete="off" required >
					  <br>
        				<label for="nombre0" class="control-label">Tipo Medicamento</label>					
						<select class="custom-select vm" style="width:200px;" id="cmbTipoMedicamento"autocomplete="off" data-placeholder="Seleccione"  autocomplete="off" required  >
						<option value="0">seleccione</option>
					    </select>	
						<br>	
						<br>			
						<div> 
							<label for="medicamento" class="control-label">Presentacion</label>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<select class="custom-select vm" style="width:200px;" id="cmbPresentacion">
							<option value="0" >seleccione</option>
							</select>						
						</div>
						<br>
						<div> 						    		
						<label for="control" class="control-label"><span>*</span>Control</label>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<select class="custom-select vm" style="width:200px;" id="cmbControl" autocomplete="off" required >
						<option value="seleccione">seleccione</option>
						</select>
						</div>
						<br>
						<div>
						<label class="labeledad">Stock Minimo:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="text" class="sinborder" min="0" onkeypress="return valideKey(event);" id="stock_minimo" style="width:50px;" value='0'>&nbsp;&nbsp;&nbsp; 	
					
							<label class="labelfecha">Fecha Registro</label>       
							<div class="input-group date" for="fecha" >
								<input type="text"class="control fecha" disabled="disabled" name="fecha" autocomplete="off"id="fecha" style="width:103px;" style=" z-index: 1050 !important;">  
								<div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker"> 
							</div> 
						</div>	  
					 	      				
					</div>
				
					
					<div>
						<label class="labelborrado" for="borrado" id ="activo" >Activo</label>
						<input type="checkbox" class="borrado" id="borrado" name="borrado" value='false'>  
					</div>
					
					<div >
						<label class="labelcronico" id ="labelcronico" >Medicamento Crónico</label>
						<input type="checkbox" class="cronico" id="cronico"  value='false'>  
					</div>
					<br>
					<br>
					
				
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
					<button type="button" class="btn btn-primary btnGuardar" id="btnGuardar">Guardar datos</button>
					<button type="button" class="btn btn-primary" id="btnActualizar">Actualizar datos</button>
					</div>
					

</section>







<?= $this->endSection(); ?>