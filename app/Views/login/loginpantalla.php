<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
  <meta charset="utf-8">
  <title>SIAMED </title>
  <link rel="icon" type="image/png" href="<?= base_url() ?>/img/siamedcorazon.jpg" style="width: 50px; height: 50px;">

  <link href="<?php echo base_url(); ?>/css/login2.css" rel="stylesheet" type="text/css" />
</head>

<body>

  <form action="<?php echo base_url(); ?>/Login/validarIngreso" method="post">
    <div class="container-sm " style="text-align: right;  ">
      <img class="img_center" src="<?php echo base_url(); ?>/img/sapi.png" width="220" alt="Avatar">
    </div>
    </div>
    <section class="form-login">
      <h5>Inicie sesion para usar la aplicacion </h5>
      <input class="controls" type="text" name="usuario" value="" placeholder="Usuario" autocomplete="off" required>
      <input class="controls" type="password" name="contrasena" value="" placeholder="Contraseña" autocomplete="off" required>
      <input class="buttons" type="submit" name="" value="Ingresar">
      <p></p>
      <p class="mb-1">
        <a href="/forget">¿Olvidó su contraseña?</a>
      </p>
    </section>
  </form>

  </div>

  </div>
</body>
<!-- EL MENSAJE VIENE DEL CONTROLADO LOGIN -->
<br>
<div class="row">
  <div class="col-12" style="text-align: center;">
    <br>
    <?php if (isset($mensaje)) { ?>
      <div class="alert alert-<?= $tipo; ?> ">
        <?= $mensaje; ?>
      </div>
    <?php } ?>
  </div>
</div>
<script src="<?php echo base_url(); ?>/plugins/jQuery/jQuery-2.1.3.min.js"></script>
<script src="<?php echo base_url(); ?>/js/bootstrap.min.js" type="text/javascript"></script>
</body>

</html>