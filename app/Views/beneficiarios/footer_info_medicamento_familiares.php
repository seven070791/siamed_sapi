<!-- jQuery esto lo comente para los select -->
<!-- <script src="<=base_url();?>/js/jquery.js"></script> -->
<!-- jQuery-UI -->
<script src="<?=base_url();?>/js/jquery-ui.js"></script>
<!-- Bootstrap -->
<script src="<?=base_url();?>/js/bootstrap4.3.1.min.js"></script>
<script src="<?php  echo base_url();?>/plugins/moment/moment.min.js"></script>
<!-- Date Picker -->
<script src="<?php echo base_url();?>/js/fecha_datapicker.js"></script>
<!-- DataTables -->
<script src="<?=base_url();?>/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url();?>/js/dataTables.bootstrap4.min.js"></script>
<script src="<?=base_url();?>/js/dataTables.responsive.min.js"></script>
<!-- Bootstrap Responsive -->
<!-- Para el Módulo -->


<script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.print.min.js"></script>
<!-- Para los estilos en Excel     -->
<script src="https://cdn.jsdelivr.net/npm/datatables-buttons-excel-styles@1.1.1/js/buttons.html5.styles.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/datatables-buttons-excel-styles@1.1.1/js/buttons.html5.styles.templates.min.js"></script>
<!-- Para el Módulo -->
<script type="text/javascript" src="<?php echo base_url();?>/js/beneficiarios/info_medicamento_familiares.js"></script>
<!-- ******ESTO ES PARA INSERTAR LA IMAGEN EN EL PDF,******* -->
<?php
    $path = ROOTPATH.'public/img/cintillo2022.jpg';//this is the image path
    $type = pathinfo($path, PATHINFO_EXTENSION);
    $data = file_get_contents($path);
    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
?>
<script>
    var rootpath='<?php echo($base64);?>'  
</script>
</body>
</html>
