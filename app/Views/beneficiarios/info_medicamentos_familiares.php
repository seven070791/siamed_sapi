<?= $this->extend('menu/supermenu') ?>
<!-- INICIO DE LA SECION CONTENT -->
<?= $this->section('content') ?>
<!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->
<link href="/css/info_medicamentos_familiares.css" rel="stylesheet" type="text/css" />
<div class="container">
	<div class="row col-md-12">
		<div class=" col-md-3">
		</div>
		<div class="col-md-7">
			<h4>Registro Historico de Entrega de Medicamentos </h4>
		</div>
		<input type="hidden" class="control" disabled="disabled" name="fecha" autocomplete="off" id="fecha" style="width:103px;" style=" z-index: 1050 !important;">
	</div>
	<div class="row col-md-12">
		<h5>Titular:</h5>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="text" class="sinborder" readonly="readonly" class="form-control" style="width:400px;" value='<?php echo $nombretitular . $apellidotitular; ?>'>
		<h5>&nbsp;&nbsp;Cedula:&nbsp;</h5>
		<input type="text" class="sinborder" readonly="readonly" id="cedula_titular" class="form-control" style="width:200px;" value='<?php echo $cedulatitular; ?>'>
	</div>
	<div class="row col-md-12">
		<h5>Beneficiario:&nbsp;&nbsp;&nbsp;</h5>
		<input type="text" class="sinborder" readonly="readonly" id="nombre_beneficiario" class="form-control" style="width:400px;" value='<?php echo $nombre . ' ' . ' ' . $apellido; ?>'>
		<!-- <h5>&nbsp;&nbsp;Cedula:&nbsp;</h5> -->
		<input type="text" class="sinborder" readonly="readonly" id="cedula_familiar" class="form-control" style="width:200px;" value='<?php echo $cedula; ?>'>
	</div>

	<div>
	</div>

	<br>

	<style>
		table.dataTable thead,
		table.dataTable tfoot {
			background: linear-gradient(to right, #4a779c, #7e9ab1, #5f7a91);
		}
	</style>
	<div class="row">
		<div class="col-md-0">
		</div>
		<div class="col-md-4.3">
			<div class="from-group">
				&nbsp;&nbsp;&nbsp;&nbsp;<label for="min">Desde</label>
				<input type="date" value="<?php echo date('y-m-d'); ?>" name="desde" id="desde">&nbsp;&nbsp;
				<label for="hasta">Hasta</label>&nbsp;&nbsp;
				<input type="date" value="<?php echo date('y-m-d'); ?>" name="hasta" id="hasta">&nbsp;&nbsp;
			</div>
		</div>
		<div class="col-md-6">
			<label class="labelmedicotratante" class="control-label">Medico &nbsp;&nbsp; </label>
			<select class="custom-select" style="width:270px;" id="cmbmedicos" autocomplete="off" required>
				<option value="0">seleccione</option>
			</select>&nbsp;&nbsp;&nbsp;&nbsp;
			<button id="btnfiltrar" class="btn btn-primary btn-sm">Filtrar</button>
			<button id="btnlimpiar" class="btn btn-primary btn-sm">Limpiar</button>
		</div>
	</div>

	<!--table class="table table-hover table-bordered" id="table_roles" style="margin-top: 20px"-->
	<div class="row">
		<div class="col-11">
			<table class="display" id="info_medicamento" width="102%">
				<thead>
					<tr>

						<td style="width: 30%;">Medicamentos</td>
						<td style="width: 2px;">Cronico</td>
						<td style="width: 10px;">Cantidad</td>
						<td style="width: 20px;">Control</td>
						<td style="width: 100px;">Fecha de Entrega</td>
						<td style="width: 115px;">Recetado Por</td>
					</tr>
				</thead>
				<tbody id="lista_info_medicamento">
				</tbody>
			</table>
		</div>
	</div>
</div>
<!-- <script src="https://code.jquery.com/jquery-3.1.0.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script> -->
<script>
	$(document).ready(function() {

		var now = new Date();

		var day = ("0" + now.getDate()).slice(-2);
		var month = ("0" + (now.getMonth() + 1)).slice(-2);
		var today = day + "/" + month + "/" + now.getFullYear();
		$("#fecha").val(today);
	});
</script>

<script>
	// ***CONFIGURACION DE DATAPICKER :( MOSTRAR AÑO Y DAR FORMATO DEFECHA D-M-A)*****
	$(function() {
		// $("#modal_editar").modal("show");            
		$("#fecha").datepicker({
			dateFormat: 'dd/mm/yy',
			changeMonth: true,
			changeYear: true
		});
		$("#fecha1").datepicker({
			dateFormat: 'dd/mm/yy',
			changeMonth: true,
			changeYear: true
		});
	});
</script>
<?= $this->endSection(); ?>