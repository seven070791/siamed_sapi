<?= $this->extend('menu/supermenu') ?>
<!-- INICIO DE LA SECION CONTENT -->
<?= $this->section('content') ?>
<!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->
<link href="/css/info_medicamentos_titulares.css" rel="stylesheet" type="text/css" />
<div class="container">
	<br />

	<div class="row col-md-12">
		<div class=" col-md-3">
		</div>
		<div class=" col-md-7">
			<h4>Registro Historico de Entrega de Medicamentos </h4>
		</div>
		<div class=" col-md-2">
		</div>
	</div>
	<input type="hidden" class="control" disabled="disabled" name="fecha" autocomplete="off" id="fecha" style="width:103px;" style=" z-index: 1050 !important;">
	<div class="row col-md-12">
		<h5>Titular:</h5>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="text" class="sinborder" readonly="readonly" class="form-control" id="nombre_beneficiario" style="width:600px;" value='<?php echo $nombre . $apellido; ?>'>
		<h5>&nbsp;&nbsp;Cedula:&nbsp;</h5>
		<input type="text" class="sinborder" readonly="readonly" id="cedula_trabajador " class="form-control" style="width:200px;" value='<?php echo $cedula_trabajador; ?>'>
	</div>
	<br>
	<div class="row">
		<div class="col-md-0">
		</div>
		<div class="col-md-4.3">
			<div class="from-group">
				<label for="min">Desde</label>&nbsp;&nbsp;
				<input type="date" value="<?php echo date('y-m-d'); ?>" name="desde" id="desde">&nbsp;&nbsp;
				<label for="hasta">Hasta</label>&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="date" value="<?php echo date('y-m-d'); ?>" name="hasta" id="hasta">

			</div>

		</div>


		<div class="col-md-7">
			<label class="labelmedicotratante" class="control-label">Medico &nbsp;&nbsp; </label>
			<select class="custom-select" style="width:270px;" id="cmbmedicos" autocomplete="off" required>
				<option value="0">seleccione</option>
			</select>&nbsp;&nbsp;&nbsp;&nbsp;
			<button id="btnfiltrar" class="btn btn-primary btn-sm">Filtrar</button>
			<button id="btnlimpiar" class="btn btn-primary btn-sm">Limpiar</button>
		</div>
	</div>

	<style>
		table.dataTable thead,
		table.dataTable tfoot {
			background: linear-gradient(to right, #4a779c, #7e9ab1, #5f7a91);
		}
	</style>

	<!--table class="table table-hover table-bordered" id="table_roles" style="margin-top: 20px"-->
	<div class="row">

		<div class="col-11">
			<table class="display" id="info_medicamento" width="102%">
				<thead>
					<tr>

						<td style="width: 30%;">Medicamentos</td>
						<td style="width: 2px;">Cronico</td>
						<td style="width: 10px;">Cantidad</td>
						<td style="width: 20px;">Control</td>
						<td style="width: 100px;">Fecha de Entrega</td>
						<td style="width: 115px;">Recetado Por</td>

					</tr>
				</thead>
				<tbody id="lista_info_medicamento">
				</tbody>
			</table>
		</div>
	</div>
</div>
<!-- Ventana Modal -->
<form action="" method="post" name="">
	<div class=" modal fade" id="modal_agregar_familiar" tabindex="-1" data-backdrop="static">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div class="row col-md-12">
						<div>
							<div class="form-group has-feedback" id="cajas1">
								<form>
									<h4>Datos del Titular </h4>
									<!-- ****************************DATOS DEL TITULAR*************************************	 -->
									<div>
										<label>Cedula</label>
										<input type="text" id="cedula_trabajador" disabled="disabled" name="cedula_trabajador" readonly="readonly" value='<?php echo $cedula_trabajador; ?>'>
										<label for="name">Nombre</label>&nbsp;
										<input type="text" id="nombre" readonly="readonly" disabled="disabled" name="nombre" value='<?php echo $nombre . ' ' . ' ' . ' ' . ' ' . $apellido; ?>' style="width:250px;"> &nbsp;
										<label for="mail">Ubicacion Administrativa</label>
										<input type="text" id="ubicacion_administrativa" disabled="disabled" readonly="readonly" name="ubicacion_administrativa" value='<?php echo $ubicacion_administrativa; ?>' style="width:350px;">
									</div>
							</div>
							<br>
							<!-- ****************************DATOS DE PERSONAL DE CORTESIA *************************************	 -->
							<div id="cajas2">
								<h5>Datos del Beneficiario </h5>
								<div>
									<label for="mail">Cedula</label>
									&nbsp; &nbsp; <input type="text" id="cedula" name="cedula" style="width:470px;" value=''>
									<label for="name">Nacionalidad</label>
									<input type="text" id="nacionalidad" name="nacionalidad" value="V" style="width:41px; ">
									<label for="mail">Fecha de Nacimiento</label>;
									<input type="text" name="fecha" id="fecha" style=" z-index: 1050 !important;">
									<div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
									</div>
									<div>
										<label for="name">Nombre</label>
										&nbsp; <input type="text" id="nombre_cortesia" name="nombre_cortesia" value='' style="width:468px;">
										<!-- ******SELECT DEL SEXO******* -->
										&nbsp;<label for="name">Sexo</label>
										&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; <select class="selectpicker" id="sexo" name="sexo" data-style="btn-primary">
											<option value="1">Masculino</option>
											<option value="2">Femenino</option>
										</select>
									</div>
									<div>
										<label for="mail">Apellido</label>
										&nbsp; <input type="text" id="apellido_cortesia" name="apellido_cortesia" value='' style="width:468px;">
										<label for="mail">Telefono</label>
										&nbsp; <input type="text" id="telefono" name="telefono" style="width:400px;" placeholder="04143222222" value=''>
									</div>
									<div class="span">
										<SPAN class="texto" style="position: absolute; top: 200 px; left: 400 px;">
											Observacion
										</SPAN>
										<div class="row col-10">
											<textarea id="observacion" name="observacion"> </textarea>
										</div>


										<br>
										<div class="row col-md-6">
											<button id="btnAgregarCortesia" class="btn-primary btn-sm">Agregar</button>
											&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" class="btn-primary btn-sm" data-dismiss="modal" id="btnClose" name="btnCerrar">Cerrar</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
</form>
<!-- <script src="https://code.jquery.com/jquery-3.1.0.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script> -->
<script>
	$(document).ready(function() {

		var now = new Date();

		var day = ("0" + now.getDate()).slice(-2);
		var month = ("0" + (now.getMonth() + 1)).slice(-2);
		var today = day + "/" + month + "/" + now.getFullYear();
		$("#fecha").val(today);
	});
</script>
<script>
	// ***CONFIGURACION DE DATAPICKER :( MOSTRAR AÑO Y DAR FORMATO DEFECHA D-M-A)*****
	$(function() {
		// $("#modal_editar").modal("show");            
		$("#fecha").datepicker({
			dateFormat: 'dd/mm/yy',
			changeMonth: true,
			changeYear: true
		});
		$("#fecha1").datepicker({
			dateFormat: 'dd/mm/yy',
			changeMonth: true,
			changeYear: true
		});
	});
</script>
<?= $this->endSection(); ?>