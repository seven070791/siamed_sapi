<?= $this->extend('menu/supermenu') ?>
<!-- INICIO DE LA SECION CONTENT -->
<?= $this->section('content') ?>
<!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->

<link href="/css/titulares.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>/datatable_responsive/css/fixedHeader.bootstrap.min.css">
   <link rel="stylesheet" href="<?php echo base_url(); ?>/datatable_responsive/css/responsive.bootstrap.min.css">

<div class="container"><br />
	<div class="row ">
		<div class="col-md-4 col-sm-4 col-lg-4 col-xl-4">
		</div>
		<div class="col-md-4 col-sm-4 col-lg-4 col-xl-4">
			<h4 class="center">Titulares/ Informacion </h4>
		</div>
		<div class="col-md-4 col-sm-4 col-lg-4 col-xl-4">

			<button id="btnAgregar" class=" btn-primary btnAgregar">Agregar</button>
			<button id="btnRegresar" class="  btn-secondary btnRegresar">Regresar</button>
		</div>

	</div>
	<style>
		table.dataTable thead,
		table.dataTable tfoot {
			background: linear-gradient(to right, #4a779c, #7e9ab1, #5f7a91);
		}
	</style>

</div>
<div class="row ">
	<div class="col-md-1 col-sm-1 col-lg-1 col-xl-1">
	</div>
	<div class="col-md-10 col-sm-10 col-lg-10 col-xl-10">
		<table class="display" id="table_titulares" style="width:100%">
			<thead>
				<tr>
					<td class="text-center">Cedula</td>
					<td class="text-center" style="width: 15%;">Nombre</td>
					<td class="text-center" style="width: 15%;">Apellido</td>
					<td class="text-center" style="width: 18%;">Ubicacion Administrativa</td>
					<td class="text-center" style="width: 2%;">Sexo</td>
					<td class="text-center" style="width: 9%;">Telefono</td>
					<td class="text-center" style="width: 4%;">Fecha de nacimiento </td>
					<!-- <td class="text-center"style="width: 3%;" >Estatus</td>	 -->
					<td class="text-center" style="width: 18%;">Acciones</td>
				</tr>
			</thead>
			<tbody id="lista_titulares">
			</tbody>
		</table>
	</div>
</div>
<!-- Ventana Modal -->
<form action="" method="post" name="">
	<div class=" modal fade" id="modal" tabindex="-1" data-backdrop="static">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<h5 class="modal-title text-center">Registro de Titulares</h5>
				<div class="modal-header">
					<div class="modal-body ">
						<div class="row ">
							<div class="col-md-12">
								<div class="form-group has-feedback">
									<fieldset>
										<legend class="legend">Datos del Usuario</legend>

										<label>Cedula</label>
										<input type="text" id="cedula" autocomplete="off" minlength="7" maxlength="8" required pattern="[0-9]+" name="cedula">
										<button id="btnBuscar" class="btn-primary btn-sm">Buscar</button>
										<button type="button" class="btn-primary btn-sm" data-dismiss="modal" id="btnClose" name="btnCerrar">Cerrar</button>

									</fieldset>
									<br>
									<div class="datos">
										<fieldset>
											<div>
												<label for="name">Nombre</label>
												&nbsp; <input class="sinborder" type="text" id="nombre" disabled="disabled" autocomplete="off" readonly="readonly" name="nombre">
												<label for="mail">Apellido</label>
												&nbsp; <input class="sinborder" type="text" disabled="disabled" id="apellido" autocomplete="off" readonly="readonly" name="apellido">
												<label for="name">Nacionalidad</label>
												&nbsp; <input class="sinborder" type="text" id="nacionalidad" style="width:130px;" name="nombre">
											</div>
											<div>
												<label for="mail">Ubicacion Administrativa</label>
												&nbsp; <input class="sinborder" type="text" disabled="disabled" id="ubicacionadministrativa" style="width:270px;" readonly="readonly" name="ubicacionadministrativa">
												<label for="mail">Fecha de Nacimiento</label>
												<input type="text" class="border" name="fecha" id="fecha" style="width:100px;" autocomplete="off" style=" z-index: 1050 !important;">
												<div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
												</div>
												<div>
													<label for="mail">Tipo de Personal </label>&nbsp;&nbsp;
													<input class="sinborder" type="text" disabled="disabled" id="tipo_de_personal" readonly="readonly" name="tipopersonal">
													<label for="mail">Telefono</label>
													&nbsp; &nbsp; <input class="sinborder" disabled="disabled" style="width:130px;" type="text" id="telefono" autocomplete="off" name="telefono" placeholder="04143222222" value="">

												</div>


												<div>
													<!-- ******Estado cicil******* -->
													<label for="name">Estado Civil </label>
													<select class="classic" id="estadocivil" style="width:130px;" name="estadocivil" data-style="btn-primary" style="width:115px;">
														<option value="seleccione">seleccione</option>
													</select>&nbsp;&nbsp;



													<!-- ******Estado Tipo de sangre******* -->
													<label for="name">Tipo de Sangre </label>
													<select class="classic" id="tipodesangre" style="width:130px;" name="tipodesangre" data-style="btn-primary" style="width:115px;">
														<option value="seleccione">seleccione</option>
													</select>&nbsp;&nbsp;

													<!-- ******SELECT DEL OCUPACION******* -->
													<label for="name">Ocupacion</label>
													<select class="classic" id="ocupacion" style="width:130px;" name="ocupacion" data-style="btn-primary">
														<option value="seleccione">seleccione</option>
													</select>

												</div>

												<div>
													<!-- ******Grado de Instruccion******* -->
													<label for="name">Grado.Instru</label>
													<select class="classic" id="gradointruccion" style="width:129px;" name="gradointruccion" data-style="btn-primary">
														<option value="seleccione">seleccione</option>
													</select>

													<!-- ******SELECT DEL SEXO******* -->&nbsp;&nbsp;&nbsp;&nbsp;
													<label for="name">Genero</label>&nbsp;&nbsp;&nbsp;&nbsp;
													<select class="classic" id="sexo" style="width:150px;" name="sexo" data-style="btn-primary">
													<option value="0" selected>Seleccione</option>
														<option value="1">Masculino</option>
														<option value="2">Femenino</option>
													</select>
												</div>
											</div>

										</fieldset>
										<div>
										</div>
										<div>
										</div>

										<div class="modal-footer">
											<!-- <button type="button" class="btn-primary btn-sm" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button> -->
											<button id="btnGuardar" class="btn-primary btn-sm">Guardar</button>
										</div>

									</div>
								</div>
							</div>

</form>
</div>
</div>

<!-- <script src="https://code.jquery.com/jquery-3.1.0.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script> -->
<script>
	// ***CONFIGURACION DE DATAPICKER :( MOSTRAR AÑO Y DAR FORMATO DEFECHA D-M-A)*****
	$(function() {
		$("#fecha").datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: '1920:2050',
			dateFormat: 'dd/mm/yy',
		})
		$("#fecha1").datepicker({
			dateFormat: 'dd/mm/yy',
			changeMonth: true,
			changeYear: true
		});


	});
</script>


<?php if (session('nivel_usuario') === '2' || session('nivel_usuario') === '3') : ?>

	<script>
		$('.btnAgregar').prop('disabled', true);
	</script>

	<style>
		.btnAgregar {

			pointer-events: none;
			opacity: 0.4;
		}
	</style>

<?php endif; ?>






<style>
	.disabled {
		color: #808080
	}
</style>
<!--  Datatables JS-->
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.js"></script>
<!-- SUM()  Datatables-->
<script src="https://cdn.datatables.net/plug-ins/1.10.20/api/sum().js"></script>







<?= $this->endSection(); ?>