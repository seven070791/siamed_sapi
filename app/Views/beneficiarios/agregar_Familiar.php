<?= $this->extend('menu/supermenu') ?>
<!-- INICIO DE LA SECION CONTENT -->
<?= $this->section('content') ?>

<link href="/css/familiares.css" rel="stylesheet" type="text/css" />
<div class="container">
	<br />
	<div class="row col-md-12">
		<div class="row col-md-12">
			<h3>Beneficiarios / Familiares</h3>
		</div>
		<h5>Titular:&nbsp;&nbsp;&nbsp;</h5>
		<div>
			<input type="text" class="sinborder" disabled="disabled" class="form-control" style="width:600px;" value='<?php echo $nombre . ' ' . ' ' . ' ' . ' ' . $apellido; ?>'>
		</div>
		<h5>&nbsp;&nbsp;Cedula:&nbsp;&nbsp;&nbsp;&nbsp;</h5>
		<div>
			<input type="text" class="sinborder" disabled="disabled" id="cedula_trabajador " class="form-control" style="width:200px;" value='<?php echo $cedula_trabajador; ?>'>
		</div>
	</div>

	<div class="row">
		<div class="col-md-1">
			<button id="btnAgregar_familiar" class="btn btn-primary">Agregar</button>
		</div>
		<div class="col-md-2">
			<button id="btnRegresar_hacia_titulares" class="btn btn-secondary">Regresar</button>
		</div>
	</div>
	<style>
		table.dataTable thead,
		table.dataTable tfoot {
			background: linear-gradient(to right, #4a779c, #7e9ab1, #5f7a91);
		}
	</style>

	<div class="row col-md-12 fami">
		<!--table class="table table-hover table-bordered" id="table_roles" style="margin-top: 20px"-->
		<table class="display" id="table_familiares" style="width:100%">
			<thead>
				<tr>
					<td class="text-center" style="width: 80px;">Cedula</td>
					<td class="text-center" style="width: 220px;">Nombre</td>
					<td class="text-center" style="width: 220px;">Apellido</td>
					<td class="text-center" style="width: 100px;">Parentesco</td>
					<td class="text-center" style="width: 10px;">Sexo</td>
					<td class="text-center">Telefono</td>
					<td class="text-center" style="width: 120px;">F.Nacimiento</td>
					<!-- <td class="text-center"style="width: 100px;">Estatus</td>	  -->
					<td class="text-center" style="width: 130px;">Acciones</td>
				</tr>
			</thead>
			<tbody id="lista_familiares">
			</tbody>
		</table>
	</div>
</div>
<!-- Ventana Modal -->

<div class=" modal fade" id="modal_agregar_familiar" tabindex="-1" data-backdrop="static">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<h5 class="modal-title text-center">Registro de Familiares</h5>
			<div class="modal-header">


				<div class="col-xs-6 ">
				</div>

				<div class="col-xs-6 ">
					<div>
						<div class="form-group has-feedback" id="cajas1">


							<fieldset>
								<legend class="legend">Datos del Titular </legend>

								<div>
									<label>Cedula</label>&nbsp;&nbsp;
									<input type="text" id="cedula_trabajador" disabled="disabled" name="cedula_trabajador" readonly="readonly" value='<?php echo $cedula_trabajador; ?>'>
									&nbsp;&nbsp;
									<label for="name">Nombre y Apellido</label>&nbsp;
									<input type="text" id="nombre" readonly="readonly" disabled="disabled" style="width:350px;" name="nombre" value='<?php echo $nombre . ' ' . ' ' . ' ' . ' ' . $apellido; ?>'> &nbsp;
								</div>

								<div>
									<label for="mail">Ubicacion Administrativa</label>&nbsp;&nbsp;
									<input type="text" id="ubicacion_administrativa" disabled="disabled" readonly="readonly" name="ubicacion_administrativa" value='<?php echo $ubicacion_administrativa; ?>' style="width:310px;">
								</div>
							</fieldset>
							<div id="cajas2">
								<div>
									<br>
									<fieldset class="familiares">
										<legend class="legend">Datos Del Beneficiario </legend>
										<label for="mail">Cedula</label>&nbsp;&nbsp;&nbsp;&nbsp;
										<input class="conborder" type="text" id="cedula" onkeypress="return valideKey(event);" autocomplete="off" minlength="7" maxlength="11" name="cedula" value=''>
										<button id="btnBuscar" class="btn-primary sm">Buscar</button>
										<div>
											&nbsp;<label for="name">Nombre</label>
											&nbsp; <input class="conborder" type="text" disabled="disabled" onkeyup="mayus(this);" id="nombre_familiar" style="width:293px;" autocomplete="off" name="nombre_familiar" value=''>
											&nbsp; <label for="mail">Apellido</label>
											<input class="conborder" type="text" disabled="disabled" id="apellido" onkeyup="mayus(this);" style="width:293px;" autocomplete="off" name="apellido" value=''>

											<div>
												<label for="mail">Telefono</label>
												&nbsp; <input class="conborder" type="text" id="telefono" onkeypress="return valideKey(event);" name="telefono" style="width:292px;" autocomplete="off" placeholder="04143222222" value=''>
												&nbsp;
												<label for="mail">Fecha de Nacimiento</label>
												<input type="text" name="fecha" id="fecha" disabled="disabled" style="width:210px;" style=" z-index: 1050 !important;">
												<div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
													<div>
														<!-- ******SELECT DEL Parentesco******* -->
														<label for="name">Parentesco</label>
														&nbsp; <select class="classic" id="parentesco" name="parentesco" data-style="btn-primary" style="width:150px;">
															<option value="seleccione">seleccione</option>
														</select> &nbsp;

														<!-- ******SELECT DEL SEXO******* -->&nbsp;
													<label for="name">Genero</label>&nbsp;&nbsp;&nbsp;&nbsp;
													<select class="classic" id="sexo" style="width:150px;" name="sexo" data-style="btn-primary">
													<option value="0" selected>Seleccione</option>
														<option value="1">Masculino</option>
														<option value="2">Femenino</option>
													</select>&nbsp;&nbsp;&nbsp;

														<!-- ******SELECT DEL Parentesco******* -->
														<label for="name">Ocupacion</label>&nbsp;
														<select class="classic" id="ocupacion" name="ocupacion" data-style="btn-primary" style="width:150px;">
															<option value="seleccione">seleccione</option>
														</select> &nbsp;
														
														<div>
														<!-- ******Estado Civl******* -->
														<label for="name">Estado Civl</label>
														&nbsp;&nbsp;<select class="classic" id="estadocivil" name="estadocivil" data-style="btn-primary" style="width:150px;">
															<option value="seleccione">seleccione</option>
														</select>

															<!-- ******Estado Civl******* -->
															<label for="name">Tipo de Sangre </label>&nbsp;
															<select class="classic" id="tipodesangre" name="tipodesangre" data-style="btn-primary" style="width:150px;">
																<option value="seleccione">seleccione</option>
															</select>


															
														</div>
														<div>
															<!-- ******grado de instruccion******* -->
															<label for="name">Grado de Instruccion</label>
															&nbsp;<select class="classic" id="gradointruccion" name="gradointruccion" data-style="btn-primary" style="width:190px;">
																<option value="seleccione">seleccione</option>
															</select>

														</div>
														<!-- &nbsp;&nbsp; <label for="name"></label>
														&nbsp; <input class="conborder" type="TEXT" id="sexo" disabled="disabled" readonly="readonly" name="sexo" value='' style="width:60px;"> -->
													
									
													</fieldset>
								</div>

							</div>
							<div class="modal-footer">
								<button id="btnAgregarFamiliar" class="btn-primary btn-sm">Agregar</button>
								<button type="button" class="btn-primary btn-sm" data-dismiss="modal" id="btnClose" name="btnCerrar">Cerrar</button>
							</div>
						</div>
						<script>
							function mayus(e) {
								e.value = e.value.toUpperCase();
							}
						</script>
						<!-- <script src="https://code.jquery.com/jquery-3.1.0.js"></script>
						<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script> -->
						<script>
							// ***CONFIGURACION DE DATAPICKER :( MOSTRAR AÑO Y DAR FORMATO DEFECHA D-M-A)*****
							$(function() {
								$("#fecha").datepicker({
									changeMonth: true,
									changeYear: true,
									yearRange: '1920:2050',
									dateFormat: 'dd/mm/yy',
								})
								$("#fecha1").datepicker({
									dateFormat: 'dd/mm/yy',
									changeMonth: true,
									changeYear: true
								});


							});
						</script>


						<script>
							$(document).ready(function() {

								var now = new Date();

								var day = ("0" + now.getDate()).slice(-2);
								var month = ("0" + (now.getMonth() + 1)).slice(-2);

								var today = now.getFullYear() + "-" + (month) + "-" + (day);
								$("#fecha").val(today);
							});
						</script>

						<!-- ***** FUNCION PARA SOLO NUMEROS***-** -->
						<script type="text/javascript">
							function valideKey(evt) {

								// code is the decimal ASCII representation of the pressed key.
								var code = (evt.which) ? evt.which : evt.keyCode;

								if (code == 8) { // backspace.
									return true;
								} else if (code >= 48 && code <= 57) { // is a number.
									return true;
								} else { // other keys.
									return false;
								}
							}
						</script>

						<?php if (session('nivel_usuario') === '2' || session('nivel_usuario') === '3') : ?>

							<script>
								$('#btnAgregar_familiar').prop('disabled', true);
							</script>

							<style>
								#btnAgregar_familiar {

									pointer-events: none;
									opacity: 0.4;
								}
							</style>

						<?php endif; ?>

						<?= $this->endSection(); ?>