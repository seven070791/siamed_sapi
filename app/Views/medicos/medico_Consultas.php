<?= $this->extend('menu/supermenu') ?>
<!-- INICIO DE LA SECION CONTENT -->
<?= $this->section('content') ?>

<link href="/css/vista_citas.css" rel="stylesheet" type="text/css" />
<link href="/css/card.css" rel="stylesheet" type="text/css" />
<div class="container">
	<br>
	<br>


	<div class="row ">
		<div class="col-5">
			<input type="text" autocomplete="off" disabled="disabled" class="descripcion_especialidad" id="descripcion_especialidad" value="<?= session('descripcion'); ?>" name="descripcion" placeholder="">
			<input type="hidden" autocomplete="off" class="id_especialidad" id="id_especialidad" value="<?= session('id_especialidad'); ?>" name="descripcion" style="width:200px;" placeholder="">


		</div>
		<div class="col-5">
			<h6 class="center">REGISTRO /CONSULTAS </h6>
		</div>

		<input type="hidden" autocomplete="off" id="medico_id" value="<?= session('medico_id'); ?>" name="medico_id" style="width:100px;" placeholder="">
		<input type="hidden" id="usuario" autocomplete="off" style="width:102px;" value='<?= session('nombreUsuario'); ?>'>
		<div class="col-2">
			<!-- <button id="btnAgregar" class=" btn-primary btnAgregar">Agregar</button>&nbsp; 
	<button id="btnRegresar" class=" btn-secondary btnRegresar">Regresar</button><br /><br /> -->

		</div>
	</div>
	<div class="row">
		<div class="col--1 ">
		</div>
		<div class="col-md-4    card">
			<!-- small box -->
			<div class="small-box bg-info">
				<div class="inner">

					<h1>Pacientes Atendidos </h1>
					<br>

					<input type="text" disabled="disabled " id="P_atendidos" name="" value="0">
					<p>HOY</p>
				</div>
				<div class="icon">
					<i class="fas fa-user " style='color:#f1f0e9'></i>
				</div>
			</div>
		</div>
		<div class="col-md-3 ">
		</div>
		<div class="col-md-4  card">
			<!-- small box -->
			<div class="small-box bg-primary card_amarillo">
				<div class="inner">
					<h1>Pacientes por Atender</h1>
					<br>
					<input type="text" disabled="disabled " id="P_no_atendidos" name="" value="0">
					<p>HOY</p>
				</div>
				<div class="icon">
					<i class="fas fa-users" style='color:#f1f0e9'></i>
				</div>

			</div>
		</div>
	</div>
	<style>
		table.dataTable thead,
		table.dataTable tfoot {
			background: linear-gradient(to right, #4a779c, #7e9ab1, #5f7a91);
		}
	</style>

	<div class="row">
		<div class="col-0">

		</div>
		<div class="col-lg-11">
			<!--table class="table table-hover table-bordered" id="table_roles" style="margin-top: 20px"-->
			<table class="display" id="table_especialidad" style="width:100%" style="margin-top: 20px">
				<thead>
					<tr>
						<td class="text-center" style="width: 20px;">Aten</td>
						<td class="text-center" style="width: 80px;">Acciones</td>
						<!-- <td class="text-center" style="width: 20px;">ID</td> -->
						<td class="text-center" style="width: 110px;">Nº Historial</td>
						<td class="text-center" style="width: 70px;">Cedula</td>
						<td class="text-center" style="width: 740px;">Nombre</td>
						<td class="text-center" style="width: 190px;">Fecha Creacion</td>
						<td class="text-center" style="width: 200px;">Fecha Asistencia</td>
						<td class="text-center" style="width: 160px;">Telefono</td>

					</tr>
				</thead>
				<tbody id="lista_de_categoria">
				</tbody>
			</table>
		</div>
	</div>


	<!-- Ventana Modal -->
	<form action="" method="post" name="">
		<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<div class="row">
							<div class="col-12">

								<label class="labelespecialidad">Especialidad&nbsp;&nbsp;&nbsp;&nbsp; </label>
								<select class="classic" style="width:250px;" id="especialidad" autocomplete="off" required>
									<option value="seleccione">seleccione</option>
									</h5>
								</select>
								<br>
								<br>
								<label class="labelmedicotratante" class="control-label">Medico Tratant.</label>
								<select class="classic" style="width:250px;" id="cmbmedicosreferidos" autocomplete="off" required>
									<option value="0">seleccione</option>
								</select>
							</div>
						</div>
						<input type="hidden" autocomplete="off" class="form-control categoria" id="id_especialidad" value="" name="id_especialidad" style="width:80px;" placeholder="">
					</div>
					<div class="modal-body">
						<div id="datos_ajax_register"></div>

						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
							<button type="button" class="btn btn-primary" id="btnGuardar">Guardar </button>

						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
	<script>
		$(function() {
			//$( "#fecha" ).datepicker({changeMonth:true, changeYear:true});	
			$("#fecha").datepicker({
				dateFormat: 'dd/mm/yy',
				changeMonth: true,
				changeYear: true
			});
		});
	</script>
	<?= $this->endSection(); ?>