<?= $this->extend('menu/supermenu') ?>
<!-- INICIO DE LA SECION CONTENT -->
<?= $this->section('content') ?>
<!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->
<link href="/css/vista_citas.css" rel="stylesheet" type="text/css" />
<link href="/css/card.css" rel="stylesheet" type="text/css" />
<div class="container">
	<br>
	<br>

	<style>
		table.dataTable thead,
		table.dataTable tfoot {
			background: linear-gradient(to right, #4a779c, #7e9ab1, #5f7a91);
		}
	</style>

	<div class="row ">
		<div class="col-5">
			<input type="text" autocomplete="off" disabled="disabled" class="descripcion_especialidad" id="descripcion_especialidad" value="<?= session('descripcion'); ?>" name="descripcion" placeholder="">
			<input type="hidden" autocomplete="off" class="id_especialidad" id="id_especialidad" value="<?= session('id_especialidad'); ?>" name="descripcion" style="width:200px;" placeholder="">
			<input type="hidden" id="usuario" autocomplete="off" style="width:102px;" value='<?= session('nombreUsuario'); ?>'>

		</div>
		<div class="col-5">
			<h5 class="center">REGISTRO /CITAS </h5>
		</div>

		<input type="hidden" autocomplete="off" id="medico_id" value="<?= session('medico_id'); ?>" name="medico_id" style="width:100px;" placeholder="">

		<div class="col-2">
			<!-- <button id="btnAgregar" class=" btn-primary btnAgregar">Agregar</button>&nbsp; 
	<button id="btnRegresar" class=" btn-secondary btnRegresar">Regresar</button><br /><br /> -->

		</div>
	</div>

	<div class="row">
		<div class="col--1 ">
		</div>
		<div class="col-md-4    card">
			<!-- small box -->

			<div class="small-box bg-info">
				<div class="inner">

					<h1>Pacientes Atendidos</h1>
					<br>

					<input type="text" disabled="disabled " id="P_atendidos" name="" value="0">
					<p>HOY</p>
				</div>
				<div class="icon">
					<i class="fas fa-user " style='color:#f1f0e9'></i>
				</div>
			</div>
		</div>
		<div class="col-md-3 ">
		</div>
		<div class="col-md-4    card">
			<!-- small box -->
			<div class="small-box bg-primary card_amarillo">
				<div class="inner">
					<h1>Pacientes por Atender</h1>
					<br>
					<input type="text" disabled="disabled " id="P_no_atendidos" name="" value="0">
					<p>HOY</p>
				</div>
				<div class="icon">

					<i class="fas fa-users" style='color:#f1f0e9'></i>

				</div>


			</div>
		</div>
	</div>
	<style>
		table.dataTable thead,
		table.dataTable tfoot {
			background: linear-gradient(to right, #4a779c, #7e9ab1, #5f7a91);
		}
	</style>

	<div class="row">
		<div class="col-0">

		</div>
		<div class="col-lg-11">
			<!--table class="table table-hover table-bordered" id="table_roles" style="margin-top: 20px"-->
			<table class="display" id="table_especialidad" style="width:105%" style="margin-top: 20px">
				<thead>
					<tr>
						<td class="text-center" style="width: 20px;">Aten</td>
						<td class="text-center" style="width: 80px;">Acciones</td>
						<td class="text-center" style="width: 110px;">Nº Historial</td>
						<td class="text-center" style="width: 70px;">Cedula</td>
						<td class="text-center" style="width: 740px;">Nombre</td>
						<td class="text-center" style="width: 190px;">Fecha Creacion</td>
						<td class="text-center" style="width: 200px;">Fecha Asistencia</td>
						<td class="text-center" style="width: 160px;">Telefono</td>

					</tr>
				</thead>
				<tbody id="lista_de_categoria">
				</tbody>
			</table>
		</div>
	</div>


	<input type="hidden" disabled="disabled" id="numeroHistorialDatatable" class="form-control" style="width:100px;">
	<!-- Ventana Modal -->
	<input type="hidden" id="tipo_beneficiario" disabled="disabled" value=' <php echo $tipo_beneficiario;?> ' />
	<div class=" modal fade " id="modal_citas" tabindex="-1" data-backdrop="static">
		<div class="modal-dialog  modal-sm " role="document">
			<div class="modal-content ">
				<div class="modal-header ">
					<h5><u>Registro de Citas</u></h5>
				</div>
				<div class="row ">
					<div class="col-sm-1">

					</div>
					<div class="col-lg-10" id="consultas">
						<div>

							<input type="hidden" id="apellido" disabled="disabled" value='<php echo $apellido;?> ' />
							<input type="hidden" id="nombre" disabled="disabled" value='<php echo $nombre;?> ' />
							<br>
							<fieldset>
								<legend class="legend">Datos del Beneficiario </legend>
								<label class="labelCedula">Cedula</label>
								<input type="text" id="cedula" disabled="disabled" style="width:80px;" value='<php echo $cedula_trabajador;?>'>&nbsp;&nbsp;
								<label class="labelnombreApellido">Nombre y Apellido </label>
								<input type="text" id="nombrepellido" disabled="disabled" style="width:365px;" value=''>
								<div>
									<label class="labelhistorial"><u>Nº Historial</u> </label>&nbsp;&nbsp;
									<input type="text" disabled="disabled" id="numeroHistorial" style="width:100px;" value=''>&nbsp;&nbsp;
									<label class="labelfechaNacimiento">Fecha De Nacimiento</label>
									<input type="text" id="fecha_nacimiento" disabled="disabled" style="width:100px;" value='<php echo $fecha_nacimiento;?> ' />&nbsp;&nbsp;

									<label class="labelEdad">Edad</label>&nbsp;&nbsp;
									<input type="text" id="edad" disabled="disabled" style="width:100px;" value='<php echo $edad_actual;?>'>
									<label class="labelsexo">Sexo</label>&nbsp;&nbsp;
									<input type="text" disabled="disabled" id="sexo" style="width:50px;" value='<php echo  $sexo;?>'>
									<label class="labeldepartamento">Departamento</label>
									<input type="text" id="departamento" disabled="disabled" style="width:320px;" value='<php echo $ubicacion_administrativa;?>'>

									<label class="labeltelefono">Nº Telefono</label>&nbsp;&nbsp;
									<input type="text" disabled="disabled" id="telefono" class="control" style="width:115px;" value='<php echo  $telefono;?>'>&nbsp;&nbsp;


								</div>

								<label class="labelespecialidad">Especialidad&nbsp;&nbsp; </label>
								<select class="custom-select vm" style="width:250px;" id="especialidad" autocomplete="off" required>
									<option value="seleccione">seleccione</option>
									</h5>
								</select>
								<label class="labelmedicotratante" class="control-label">Medico Tratante &nbsp;&nbsp; </label>
								<select class="custom-select vm" style="width:200px;" id="cmbmedicosreferidos" autocomplete="off" required>
									<option value="0">seleccione</option>
								</select>
								<br>
								<div>
							</fieldset>
							<br>
							<fieldset>
								<legend class="legend">Signos vitales </legend>
								<div>
									<label class="labelpeso">PESO:</label>
									<input type="number" id="peso" class="control" min="0" value="0" name="peso" style="width:50px;">
									<label class="labelkg">(KG)</label>&nbsp;&nbsp;
									<label class="labeltalla">TALLA:</label>
									<input type="number" id="talla" class="control" min="0" value="0" name="talla" style="width:50px;">
									<label class="labelcm">(CM)</label>&nbsp;&nbsp;
									<label class="labelspo2">SPO2:</label>
									<input type="number" id="spo2" class="control" min="0" value="0" name="spo2" style="width:50px;">&nbsp;&nbsp;
									<label class="labelfc">FC:</label>
									<input type="number" id="frecuencia_c" class="control" min="0" value="0" name="fc" style="width:50px;">
									<label class="labelfr">(X) &nbsp;&nbsp;FR:</label>&nbsp;&nbsp;
									<input type="number" id="frecuencia_r" class="control" min="0" value="0" name="fr" style="width:50px;">
									<label class="labelx">(X)</label>
								</div>

								<label class="labeltemperatura">TEMPERATURA:</label>
								<input type="number" id="temperatura" class="control" min="0" value="0" name="temperatura" style="width:50px;">&nbsp;&nbsp;
								<label class="labelc">(ºC)</label>&nbsp;&nbsp;
								<label class="labelta">T/A:</label>
								<input type="number" id="ta_alta" class="control" min="0" placeholder="t.a" name="ta" style="width:50px;">
								<input type="number" id="ta_baja" class="control" min="0" placeholder="t.b" name="ta" style="width:50px;">
								<label class="labelmmhg">(MMHg)</label>
								<input type="text" id="Tbenficiario" disabled="disabled" style="width:100px;" value=''> &nbsp;&nbsp;

								<label class="labelfecha">Fecha de Asistencia</label>
								<input type="text" id="fecha_del_dia" style="width:120px;" value="" />


							</fieldset>

							<div class="modal-footer">
								<button id="btnGuardar" class="btn-primary btn-sm">Guardar</button>
								<button id="btnCerrar" class="btn-primary btn-sm" data-dismiss="modal" id="btnClose" name="btnCerrar">Cerrar</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

</div>
</div>

<script>
	$(function() {
		//$( "#fecha" ).datepicker({changeMonth:true, changeYear:true});	
		$("#fecha").datepicker({
			dateFormat: 'dd/mm/yy',
			changeMonth: true,
			changeYear: true
		});
	});
</script>
<?= $this->endSection(); ?>