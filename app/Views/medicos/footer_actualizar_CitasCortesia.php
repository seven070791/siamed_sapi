<!-- jQuery -->
<script src="<?=base_url();?>/js/jquery.js"></script>
<!-- jQuery-UI -->
<script src="<?=base_url();?>/js/jquery-ui.js"></script>
<!-- Bootstrap -->
<script src="<?=base_url();?>/js/bootstrap4.3.1.min.js"></script>
<script src="<?php  echo base_url();?>/plugins/moment/moment.min.js"></script>
<!-- Date Picker -->
<script src="<?php echo base_url();?>/js/fecha_datapicker.js"></script>
<!-- DataTables -->
<script src="<?=base_url();?>/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url();?>/js/dataTables.bootstrap4.min.js"></script>
<script src="<?=base_url();?>/js/dataTables.responsive.min.js"></script>
<!-- Bootstrap Responsive -->
<script src="<?=base_url();?>/js/responsive.bootstrap4.min.js"></script>
<!-- Para el Módulo -->

<script type="text/javascript" src="<?php echo base_url();?>/js/medicos/footer_actualizar_CitasCortesia.js"></script>
</body>
</html>
