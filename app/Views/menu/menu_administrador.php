
<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="true">
      <li class="nav-item menu-close">
        <a href="../supermenu" class="nav-link active"><i class="nav-icon fas fa-tachometer-alt"></i>
          <p>Administrador</p>
        </a>
      </li>  
      <li class="nav-item">
         <a href="#" class="nav-link"><i class="nav-icon fas fa-copy"></i>
           <p> Control de Usuarios</p>  
           <i class="right fas fa-angle-left"></i>   
         </a>
         <ul class="nav nav-treeview">
           <li class="nav-item">
              <a href="../User_controllers" class="nav-link">
              <i class="far fa-circle nav-icon"></i>
              <p>Usuarios</p>
              </a>
            </li>
           <li class="nav-item">
              <a href="../Grupousuario" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Grupo Usuarios</p>
              </a>
            </li>
            <li class="nav-item">
                <a href="../AuditoriaController" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Auditoria</p>
                </a>       
            </li>
        </ul>
      </li>
