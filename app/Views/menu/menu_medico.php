<!-- ******************** METODO PARA OCULTAR UN ELEMENTO DEL MENU************** -->

<?php if (session('acceso_citas') === 'f') : ?>

  <style>
    #menu_citas {
      display: none !important;
    }
  </style>


<?php endif; ?>


<?php if (session('id_especialidad') === '5' || session('id_especialidad') === '2') : ?><style>
    #menu_consultas {
      display: none !important;
    }
  </style>

<?php endif; ?>



<!-- 
******************** METODO PARA BLOQUEAR UN ELEMENTO DEL MENU**************
      <php if (session('acceso_citas') === 'f'):?>
        
        <style>
          #menu_citas:not(:disabled):not(.disabled)
           {
            cursor: not-allowed;
            pointer-events: none;
            opacity: 0.6;
          }
         </style>


      <php endif;?> 

      <php if (session('id_especialidad') === '5' ||session('id_especialidad') === '2'):?>
        
        <style>
          #menu_consultas:not(:disabled):not(.disabled)
           {
            cursor: not-allowed;
            pointer-events: none;
            opacity: 0.6;
          }
         </style>
   
      <php endif;?>  -->

<link rel="stylesheet" href="<?php echo base_url(); ?>/css/menumedico.css">
<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu1">
  <li class="nav-item ">
    <a href="<?php echo base_url(); ?>/supermenu" class="nav-link active"><i class="nav-icon fas fa-tachometer-alt"></i>
      <p>Personal Medico </p>
    </a>
  </li>
  <li class="nav-item">
    <a href="<?php echo base_url(); ?>/medico_Citas" class="nav-link" id="menu_citas"><i class="nav-icon fas fa-copy" id="menu_citas"></i>
      <p> Citas</p>

    </a>
  </li>

  <li class="nav-item">
    <a href="<?php echo base_url(); ?>/medico_Consultas" class="nav-link" id="menu_consultas"><i class="nav-icon fas fa-copy"></i>
      <p> Consultas</p>

    </a>
  </li>

  <li class="nav-item">
    <a href="<?php echo base_url(); ?>/medico_reposo" class="nav-link" id="menu_reposos"><i class="nav-icon fas fa-copy"></i>
      <p> Reposos</p>
    </a>
  </li>
</ul>
<script src="https://code.jquery.com/jquery-3.1.0.js"></script>
<script>
  $(document).ready(function() {
    $('.usuarios').prop('disabled', true);
    $('.auditoria').prop('disabled', true);
    $('.reportes').prop('disabled', true);
    $('.inventario').prop('disabled', true);
    $('.historial').prop('disabled', true);

  });
</script>