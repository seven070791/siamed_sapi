<?= $this->extend('menu/supermenu') ?>
<!-- INICIO DE LA SECION CONTENT -->
<?= $this->section('content') ?>
<!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->
<link href="/css/relacion_movimientos_medicamento.css" rel="stylesheet" type="text/css" />
<div class="container">
	<br>
	<div class="row ">
		<div class="col-3">
		</div>
		<div class="col-8">
			<h5 class="center"> RELACION MOVIMIENTOS DE MEDICAMENTO</h5>
		</div>
	</div>
	<div class="row ">
		<div class="col-1">
		</div>
		<div class="col-11">
			&nbsp;&nbsp;<label>DESCRIPCION</label>&nbsp;
			<input type="hidden" id="id_medicamento" class="control control medicamento" style="width:20px;" value='<?php echo $id; ?>'>
			<input type="text" id="medicamento" readonly="readonly" class="control control medicamento" style="width:420px;" value='<?php echo $medicamento; ?>'>
			&nbsp;&nbsp;<label>CONTROL </label>&nbsp;&nbsp;
			<input type="text" id="control" readonly="readonly" class="control control " value='<?php echo $descripcion; ?>'>
		</div>
		<div class="col-md-9">
			&nbsp;
			<label for="min">Desde</label>
			<input type="date" value="<?php echo date('y-m-d'); ?>" name="desde" id="desde">&nbsp;&nbsp;
			<label for="hasta">Hasta</label>&nbsp;&nbsp;
			<input type="date" value="<?php echo date('y-m-d'); ?>" name="hasta" id="hasta">&nbsp;
			<!-- ******SELECT DEL TIPO DE MOVIMIENTO******* -->
			&nbsp;<label class="labelsexo">Tipo Movimiento</label>
			<select class="accion" style="width:120px;" id="accion" name="accion" data-style="btn-primary">
				<option value="0" selected disabled>seleccione</option>
				<option value="1">Entradas</option>
				<option value="2">Salidas</option>
			</select>&nbsp;&nbsp;
			<button id="btnfiltrar" class="btn btn-primary btn-sm">Filtrar</button>
			<button id="btnlimpiar" class="btn btn-primary btn-sm" onclick="location.reload()">Limpiar</button>
		</div>
	</div>
	<style>
		table.dataTable thead,
		table.dataTable tfoot {
			background: linear-gradient(to right, #4a779c, #7e9ab1, #5f7a91);
		}
	</style>
	<div class="row ">
		<div class="col-lg-0">
		</div>
		<div class="col-lg-10">
			<table class="display" id="table_movimientos_medicamentos" style="width:100%" style="margin-top: 20px">
				<thead>
					<tr>
						<td class="text-center">DESCRIPCION DEL MEDICAMENTO</td>
						<td class="text-center">FECHA</td>
						<td class="text-center" style="width:0%">CANTIDAD</td>
						<td class="text-center" style="width:4%">USUARIO</td>
					</tr>
					<tr>
						<td class="fila1" colspan="4">
							<div class="sin_datos" id="sin_datos" style=display:block>Ningun dato disponible en la tabla</div>
						</td>
					</tr>
				</thead>

				<tbody id="lista_notas_Entregas">


				</tbody>

			</table>
		</div>
	</div>


	<!-- Ventana Modal -->

	<div class=" modal fade" id="modal" tabindex="-1" data-backdrop="static">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<h5 class="modal-title text-center">Ficha Retiro de Medicamento</h5>
				<div class="modal-header">
					<input type="hidden" id="usuario" autocomplete="off" style="width:100px;" value='<?= session('nombreUsuario'); ?>'> &nbsp;
					<input type="hidden" id="n_historial" autocomplete="off" style="width:100px;" value=''>
					<div class="row ">

						<div class="col-lg-6">
							<input type="hidden" class="tipobeneficiario" id="tipobeneficiario" disabled="disabled" readonly="readonly" name="tipobeneficiario">
							<fieldset>

								<legend class="legend">Busqueda General</legend>
								<div>
									<input class="form-check-input radio" type="radio" style="width:25px;" name="inlineRadioOptions" id="Radio1" value="">
									&nbsp;<label class="titular">TITULAR</label>&nbsp;
									</button>
									&nbsp;&nbsp;<input class="form-check-input radio" type="radio" style="width:25px;" name="inlineRadioOptions" id="Radio2" value="">
									&nbsp;<label class="familiar">FAMILIAR</label>&nbsp;
									</button>
									&nbsp;&nbsp;<input class="form-check-input radio" type="radio" style="width:25px;" name="inlineRadioOptions" id="Radio3" value="">
									&nbsp;<label class="cortesia">CORTESIA</label>&nbsp;
									</button>

								</div>
								<form id="formulario">
									&nbsp; <label class="labelcedulabeneficiario">Cedula</label> &nbsp;
									<input type="text" id="cedula_beneficiario" autocomplete="off" style="width:100px;" value=''> &nbsp;


									<button type="button" class=" btn-primary btnbotones" id="btntitulares">Buscar</TItle> </button>
									<button type="button" class=" btn-primary btnbotones" id="btnfamiliares">Buscar</button>
									<button type="button" class=" btn-primary btnbotones" id="btncortesia">Buscar</button>
							</fieldset>
						</div>


						<div class="col-lg-4">
							<img src="<?= base_url() ?>/img/n1.jpeg" width="100" alt="Avatar" id="n1">
						</div>
						<div class="col-lg-12">
							<label class="labelbuscarfecha">Fecha</label>
							<div class="input-group date" for="fecha">
								<input type="text" class="fecha" disabled="disabled" name="fecha" id="fecha" style="width:90px;" style=" z-index: 1050 !important;">

								<div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
								</div>
							</div>
							<fieldset>

								<legend class="legend">Datos del Trabajador</legend>


								<div>
									<label for="name">Nombre y Apellido</label>
									<input type="text" class="nombretitular" id="nombretitular" readonly="readonly" disabled="disabled" name="nombretitular"> &nbsp;
								</div>
								<div>
									<label>Fecha De Nacimiento</label>
									<input type="text" class="fecha_nacimientotitular" readonly="readonly" disabled="disabled" id="fecha_nacimientotitular" name="fecha_nacimientotitular">
									<label>CI</label>
									<input type="text" class="cedulatitular" id="cedulatitular" disabled="disabled" name="cedulatitular" readonly="readonly">
									<label>Edad</label>
									<input type="text" class="edadtitular" style="width:130px;" id="edadtitular" disabled="disabled" name="edadtitular" readonly="readonly">
								</div>

								<div>
									<label for="mail">Unidad Administrativa</label>
									<input type="text" class="unidadtitular" id="unidadtitular" readonly="readonly" disabled="disabled" name="unidadtitular">
									<label for="mail">Telefono</label>
									<input type="text" class="telefonotitular" readonly="readonly" disabled="disabled" id="telefonotitular" name="telefonotitular">
								</div>
							</fieldset>
							<div id="cajas2">
								<div>
									<br>
									<fieldset>
										<legend class="legend">Datos Del Beneficiario </legend>
										<div>
											<label for="name">Nombre y Apellido</label>
											<input type="text" class="nombreb" id="nombreb" readonly="readonly" disabled="disabled" name="nombre"> &nbsp;
										</div>
										<div>
											<label>Fecha De Nacimiento</label>
											<input type="text" id="fecha_nacimientob" disabled="disabled" name="cedula_trabajador" readonly="readonly">
											<label>CI</label>
											<input type="text" id="cedulab" disabled="disabled" name="cedula_trabajador" readonly="readonly">
											<label>Edad</label>
											<input type="text" id="edadb" style="width:140px;" disabled="disabled" name="cedula_trabajador" readonly="readonly">
										</div>

										<div>
											<label for="mail">Parentesco</label>
											<input type="text" id="parentescob" disabled="disabled" readonly="readonly" name="ubicacion_administrativa">
											<label for="mail">Telefono</label>
											<input type="text" id="telefonob" disabled="disabled" readonly="readonly" name="ubicacion_administrativa">
										</div>
									</fieldset>
								</div>

							</div>
							<div class="modal-footer">
								<button id="btnPDF" class="btn-success btn-sm ">Generar PDF</button>
								<button type="button" class="btn-primary btn-sm" data-dismiss="modal" id="btnClose" name="btnCerrar">Cerrar</button>
							</div>
						</div>
						</form>





						<!-- <script src="https://code.jquery.com/jquery-3.1.0.js"></script>
						<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script> -->
						<script>
							// ***CONFIGURACION DE DATAPICKER :( MOSTRAR AÑO Y DAR FORMATO DEFECHA D-M-A)*****
							$(function() {
								$("#fecha").datepicker({
									dateFormat: 'dd/mm/yy',
									changeMonth: true,
									changeYear: true
								});
								$("#fecha1").datepicker({
									dateFormat: 'dd/mm/yy',
									changeMonth: true,
									changeYear: true
								});


							});
						</script>


						<script>
							$(document).ready(function() {

								var now = new Date();

								var day = ("0" + now.getDate()).slice(-2);
								var month = ("0" + (now.getMonth() + 1)).slice(-2);
								var today = day + "/" + month + "/" + now.getFullYear();
								// var today= (day)+"-"+(month)+"-"+now.getFullYear();
								// var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
								$("#fecha").val(today);
							});
						</script>

						<!-- ***** FUNCION PARA SOLO NUMEROS***-** -->
						<script type="text/javascript">
							function valideKey(evt) {

								// code is the decimal ASCII representation of the pressed key.
								var code = (evt.which) ? evt.which : evt.keyCode;

								if (code == 8) { // backspace.
									return true;
								} else if (code >= 48 && code <= 57) { // is a number.
									return true;
								} else { // other keys.
									return false;
								}
							}
						</script>
						<?= $this->endSection(); ?>