<?= $this->extend('menu/supermenu') ?>
<!-- INICIO DE LA SECION CONTENT -->
<?= $this->section('content') ?>
<!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->
<br>
<link href="/css/combos.css" rel="stylesheet" type="text/css" />
<div class="container">
	<div class="row ">
		<div class="col-5">
		</div>
		<div class="col-5">
			<h3 class="center">Autorizadores</h3>
		</div>
		<div class="col-2">
			<button id="btnAgregar" class="btn btn-primary">Agregar</button><br /><br />
		</div>
	</div>

	<style>
		table.dataTable thead,
		table.dataTable tfoot {
			background: linear-gradient(to right, #4a779c, #7e9ab1, #5f7a91);
		}
	</style>
	<div class="row ">
		<div class="col-1">
		</div>
		<!--table class="table table-hover table-bordered" id="table_roles" style="margin-top: 20px"-->
		<div class="col-lg-10">
			<!--table class="table table-hover table-bordered" id="table_roles" style="margin-top: 20px"-->

			<table class="display" id="table_autorizador" style="width:100%" style="margin-top: 20px">
				<thead>
					<tr>
						<td>Id</td>
						<td>Cedula</td>
						<td>Nombre</td>
						<td>Fecha desde</td>
						<td>Fecha hasta</td>
						<td>Estatus</td>
						<td class="text-center" style="width: 90px;">Acciones</td>
					</tr>
				</thead>
				<tbody id="lista_autorizador">
				</tbody>
			</table>
		</div>
	</div>


	<!-- Ventana Modal -->
	<form action="" method="post" name="">
		<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
		<div class="modal-dialog modal-xs" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="modal"> Autorizador</h4>
					</div>
					<div class="modal-body">
						<input type="hidden" id='id' name='id'>		
						
						<div class="form-group">
						<label for="nombre0" class="control-label">CEDULA </label>
								<input type="text" class="form-control "  onkeypress="return valideKey(event);" style="width:330px;"id="cedula" autocomplete="off" minlength="7" maxlength="8" required pattern="[0-9]+" name="cedula" require>							
						</div>

						<div class="form-group">
								<label for="nombre0" class="control-label">NOMBRE </label>
								<input type="text" onkeyup="mayus(this);" style="width:330px;" class="form-control " autocomplete="off" id="nombre" name="nombre" value='' require>
						</div>
					
						<div class="form-group">
							<label for="nombre0" class="control-label">APELLIDO </label>
							<input type="text" onkeyup="mayus(this);" style="width:330px;"class="form-control " autocomplete="off" id="apellido" name="nombre" value=''require>
						</div>
						<label>Fecha </label>
						<div class="input-group date" for="fecha">
							<input type="text" class="form-control " disabled="disabled" style="width:100px;" autocomplete="off" name="fecha" id="fecha" style=" z-index: 1600 !important;">
							<div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
						</div>
						</div>
						<br>
						<label for="borrado" id="activo">Activo</label>
						<input type="checkbox" style="display: none;" class="vigencia" id="vigencia" name="vigencia" value='false'>	
      				</div> 

						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
							<button type="button" class="btn btn-primary" id="btnGuardar">Guardar datos</button>
							<button type="button" class="btn btn-primary" id="btnActualizar">Actualizar datos</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>


	<input type="hidden" class="form-control "  onkeypress="return valideKey(event);" style="width:330px;"id="cedula_anterior" autocomplete="off" minlength="7" maxlength="8" required pattern="[0-9]+" name="cedula" require>							
	<input type="hidden" onkeyup="mayus(this);" style="width:330px;" class="form-control " autocomplete="off" id="nombre_anterior" name="nombre" value='' require>
	<input type="hidden" onkeyup="mayus(this);" style="width:330px;"class="form-control " autocomplete="off" id="apellido_anterior" name="nombre" value=''require>
	<input type="hidden" class="vigencia" id="vigencia_anterior" name="vigencia" value='false'>	




	<script>
		function mayus(e) {
			e.value = e.value.toUpperCase();
		}
	</script>

	<script>
		$(document).ready(function() {

			var now = new Date();

			var day = ("0" + now.getDate()).slice(-2);
			var month = ("0" + (now.getMonth() + 1)).slice(-2);
			var today = day + "/" + month + "/" + now.getFullYear();
			// var today= (day)+"-"+(month)+"-"+now.getFullYear();
			// var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
			$("#fecha").val(today);
		});
	</script>

	<script>
		$(function() {	
			$("#fecha").datepicker({
				dateFormat: 'dd/mm/yy',
				changeMonth: true,
				changeYear: true
			});
		});
	</script>

	<!-- ***** FUNCION PARA SOLO NUMEROS***-** -->
<script type="text/javascript">
	function valideKey(evt) {

		// code is the decimal ASCII representation of the pressed key.
		var code = (evt.which) ? evt.which : evt.keyCode;

		if (code == 8) { // backspace.
			return true;
		} else if (code >= 48 && code <= 57) { // is a number.
			return true;
		} else { // other keys.
			return false;
		}
	}
</script>

	<?= $this->endSection(); ?>