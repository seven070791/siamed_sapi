<?= $this->extend('menu/supermenu') ?>
<!-- INICIO DE LA SECION CONTENT -->
<?= $this->section('content') ?>
<!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->
<link href="/css/reposos.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>/datatable_responsive/css/fixedHeader.bootstrap.min.css">
   <link rel="stylesheet" href="<?php echo base_url(); ?>/datatable_responsive/css/responsive.bootstrap.min.css">

<div class="container ">
	<br>
	<br>

	<input type="hidden" class="sinborder" id="nivel_usuario" disabled="disabled" value='<?= session('nivel_usuario'); ?> ' />

	<?php if (session('nivel_usuario') === '1' || session('nivel_usuario') === '2') : ?>
		<style>
			#btnMostrar_reposo {
				display: none;
			}
		</style>
	<?php endif; ?>






	<style>
		table.dataTable thead,
		table.dataTable tfoot {
			background: linear-gradient(to right, #4a779c, #7e9ab1, #5f7a91);
		}
	</style>

	<div class="row">

		<div class="col-sm-4 ">
		</div>
		<div class="col-md-6 ">
			<h3 class="center">CONTROL DE REPOSOS (MEDICO) </h3>

		</div>

		<div class="col-md-2 col-sm-2 col-lg-2 col-xl-2">

			<input type="hidden" id="usuario" autocomplete="off" style="width:102px;" value='<?= session('nombreUsuario'); ?>'> &nbsp;
		</div>
	</div>
	&nbsp;&nbsp;<button type="button" class="btn  btn-xs btn-primary btnMostrar" id="btnMostrar_reposo">Agregar(+)</button>
	<div class="col-md-11 col-sm-11 col-lg-11 col-xl-11">
		<label for="min">Desde</label>
		<input type="date" value="<?php echo date('y-m-d'); ?>" name="desde" id="desde">&nbsp;&nbsp;
		<label for="hasta">Hasta</label>&nbsp;&nbsp;
		<input type="date" value="<?php echo date('y-m-d'); ?>" name="hasta" id="hasta">&nbsp;&nbsp;

	</div>

	<input type="hidden" class="sinborder" id="nombre_medico" disabled="disabled" value='<?= session('nombreUsuario'); ?> ' />
	<input type="hidden" class="sinborder" id="medico_id" disabled="disabled" value='<?= session('medico_id'); ?> ' />



	<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">


		<!--table class="table table-hover table-bordered" id="table_roles" style="margin-top: 20px"-->
		<table class="display" id="table_reposos" style="width:100%" style="margin-top: 20px">
			<thead>
				<tr>
					<td class="text-center" style="width: 5%;">ACCIONES: </td>
					<td class="text-center" style="width: 5%;">N_HISTORIAL</td>
					<td class="text-center" style="width: 40%;">TITULAR</td>
					<td class="text-center" style="width: 5%;">DESDE</td>
					<td class="text-center" style="width: 5%;">HASTA</td>
					<td class="text-center" style="width: 30%;">MOTIVO: </td>
					<td class="text-center" style="width: 20%;">MEDICO: </td>
					<td class="text-center" style="width: 5%;">F.CREACION: </td>
					<td class="text-center" style="width: 5%;">ESTATUS: </td>


				</tr>
			</thead>
			<tbody id="lista_reposos">
			</tbody>
		</table>

		<!-- Ventana Modal REPOSOS -->
		<div class="modal fade" id="modal_reposos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<h5 class="modal-title text-center">REPOSO MEDICO</h5>
					<div class="modal-body">
						<div id="datos_ajax_register"></div>
						<div class="span">
							<input type="hidden" id="id" value='' />
							<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
							<input type="hidden" class="sinborder" id="id_consulta" value='<php echo $id_consulta; ?>'>
							<input type="hidden" id="n_historialT" value='' />
							<input type="hidden" id="id_psicologiap" value='' />

							<input type="hidden" id="id_reposo" value='' />


							<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<php echo $tipo_beneficiario; ?> '>
						</div>
						<fieldset>

							<div id="busqueda">
								<legend class="legend">Datos del Trabajador</legend>
								<label id="label_r_cedula" for=" mail">Cedula</label>&nbsp;&nbsp;&nbsp;&nbsp;
								<input class="conborder " type=" text" id="cedulatitular" style="width:120px;" onkeypress="return valideKey(event);" autocomplete="off" minlength="7" maxlength="8" required pattern="[0-9]+" name="cedula">
								<button id="btnBuscar" class="btn-primary sm">Buscar</button>
							</div>

							<div>
								<label for="name">Nombre y Apellido</label>&nbsp;
								<input type="text" class="nombretitular" id="nombretitular" readonly="readonly" disabled="disabled" name="nombretitular"> &nbsp;
							</div>
							<div>
								<label>Fecha De Nacimiento</label>
								<input type="text" class="fecha_nacimientotitular" readonly="readonly" disabled="disabled" id="fecha_nacimientotitular" name="fecha_nacimientotitular">
								&nbsp;&nbsp;<label>Edad</label>&nbsp;&nbsp;
								<input type="text" class="edadtitular" style="width:110px;" id="edadtitular" disabled="disabled" name="edadtitular" readonly="readonly">
								&nbsp;&nbsp;<label for="mail">Telefono</label>&nbsp;&nbsp;
								<input type="text" class="telefonotitular" style="width:100px;" readonly="readonly" disabled="disabled" id="telefonotitular" name="telefonotitular">
							</div>

							<div>
								<label for="mail">Unidad Administrativa</label>
								<input type="text" class="unidadtitular" id="unidadtitular" readonly="readonly" disabled="disabled" style="width:51%;" name="unidadtitular">

							</div>

							
							<div>
								<label for="mail">Tipo de Reposo</label>
								<select class="custom-select vm" style="width:100px;" id="tipo_reposo" autocomplete="off" data-placeholder="Seleccione" autocomplete="off" required>
									<option value="0" disabled="disabled">seleccione</option>
									<option value="1">Dias</option>
									<option value="2">Horas</option>
								</select>
								&nbsp;&nbsp;
								<label for=" min" id="label_inicio">Inicio</label>
								<input type="date" value="<?php echo date('y-m-d'); ?>" name="desde" id="r_desde">&nbsp;&nbsp;

								<label id="label_retorno">Retorno</label>&nbsp;&nbsp;
								<input type="date" disabled="disabled" value="<?php echo date('y-m-d'); ?>" name="hasta" id="r_hasta">&nbsp;&nbsp;

								<label id="labe_horas" style="display: none;">Horas</label>&nbsp;&nbsp;
								<input type="text" class="dia_en_horas" style="display: none;" disabled="disabled" style=" width:10px;" name="dia_en_horas" min="0" id="dia_en_horas">&nbsp;&nbsp;
								<input type="text" style="display: none;" class="horas" style="width:30px;" name="horas" value="0" min="0" id="horas">&nbsp;&nbsp;

								<label id="labe_dias" disabled="disabled">Dias</label>&nbsp;&nbsp;
								<input type=" text" class="dias" disabled="disabled" style=" width:40px;" name="dias" value="0" min="0" id="dias">&nbsp;&nbsp;




							</div>


							&nbsp;<label>Motivo de Reposo :</label>
							<textarea class="observacion_reposos" id="motivo_reposo" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>

						</fieldset>

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" id="btnagregar_reposos">Guardar</button>
						<button type="button" class="btn btn-primary" id="btnActualizar_reposos">Actualizar</button>
						<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
					</div>
				</div>
			</div>
		</div>
	</div>


	<!-- <script src="https://code.jquery.com/jquery-3.1.0.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script> -->

	<script>
		function mayus(e) {
			e.value = e.value.toUpperCase();
		}
	</script>


	<script>
		// ***CONFIGURACION DE DATAPICKER :( MOSTRAR AÑO Y DAR FORMATO DEFECHA D-M-A)*****
		$(function() {
			$("#fecha").datepicker({
				dateFormat: 'dd/mm/yy',
				changeMonth: true,
				changeYear: true
			});
			$("#fecha1").datepicker({
				dateFormat: 'dd/mm/yy',
				changeMonth: true,
				changeYear: true
			});


		});
	</script>
	<!-- ***** FUNCION PARA SOLO NUMEROS***-** -->
	<script type="text/javascript">
		function valideKey(evt) {

			// code is the decimal ASCII representation of the pressed key.
			var code = (evt.which) ? evt.which : evt.keyCode;

			if (code == 8) { // backspace.
				return true;
			} else if (code >= 48 && code <= 57) { // is a number.
				return true;
			} else { // other keys.
				return false;
			}
		}
	</script>

	<script>
		$(document).ready(function() {

			var now = new Date();

			var day = ("0" + now.getDate()).slice(-2);
			var month = ("0" + (now.getMonth() + 1)).slice(-2);
			var today = day + "/" + month + "/" + now.getFullYear();
			// var today= (day)+"-"+(month)+"-"+now.getFullYear();
			// var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
			$("#fecha").val(today);
		});
	</script>


	<?= $this->endSection(); ?>