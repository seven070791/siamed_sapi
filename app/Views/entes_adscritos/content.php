<?= $this->extend('menu/supermenu') ?>
<!-- INICIO DE LA SECION CONTENT -->
<?= $this->section('content') ?>
<!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->
<link href="/css/tipomedicamentos.css" rel="stylesheet" type="text/css" />
<br>
<div class="container">


	<div class="row ">
		<div class="col-5">
		</div>
		<div class="col-5">
			<h3 class="center">ENTES ADSCRITOS </h3>
		</div>

		<div class="col-2">
			<button id="btnAgregar" class="btn btn-primary">Agregar</button><br /><br />

		</div>

	</div>
	<style>
		table.dataTable thead,
		table.dataTable tfoot {
			background: linear-gradient(to right, #4a779c, #7e9ab1, #5f7a91);
		}
	</style>
	<div class="row ">
		<div class="col-2">
		</div>
		<!--table class="table table-hover table-bordered" id="table_roles" style="margin-top: 20px"-->
		<div class="col-lg-9">
			<table class="display" id="table_entes_adscritos" style="width:100%" style="margin-top: 20px">
				<thead>
					<tr>
						<td>Id</td>
						<td>Rif</td>
						<td>Descripcion</td>
						<td>Estatus</td>
						<td class="text-center" style="width: 90px;">Acciones</td>
					</tr>
				</thead>
				<tbody id="lista_entes_adscritos">
				</tbody>
			</table>
		</div>
	</div>

	<!-- Ventana Modal -->
	<form action="" method="post" name="">
		<div class="modal fade" id="modalagregar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="modalagregar"> Registro de Entes</h4>
					</div>
					<div class="modal-body">
						<div id="datos_ajax_register"></div>
						<div>
							<input type="hidden" id='id' name='id'>
						</div>
						<div class="form-group">
							<label for="nombre0" class="control-label">Rif </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="text" class="control sinborder" onkeyup="mayus(this);" autocomplete="off" id="rif" style="width:280px;" required pattern="[aA-Za-z]+" autocomplete="off" name="tipomedicamento" value=''>
						</div>

						<div class="form-group">
							<label for="nombre0" class="control-label">Entes Adscritos</label>&nbsp;
							<input type="text" class="control sinborder" onkeyup="mayus(this);" autocomplete="off" id="descripcion" style="width:280px;" required pattern="[aA-Za-z]+" autocomplete="off" name="tipomedicamento" value=''>
						</div>
						<div>
							<label>Fecha Registro</label>&nbsp;&nbsp;&nbsp;

							<input type="text" class="sinborder" disabled="disabled" autocomplete="off" id="fecha" value=''>
							&nbsp;&nbsp;&nbsp;
						</div>
						<label for="borrado" id="activo">Activo</label>
						<input type="checkbox" class="" id="borrado" name="borrado" value='false'>



						<!-- <div class="form-check">
        				
        				
      				</div> -->
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
						<button type="button" class="btn btn-primary" id="btnGuardar">Guardar datos</button>
						<button type="button" class="btn btn-primary" id="btnActualizar">Actualizar datos </button>

					</div>
				</div>
			</div>
	</form>
<!-- METODO QUE TOMA LOS DATOS ANTERIORES DEL FORMULARIO -->
<input type="hidden" class="control sinborder" onkeyup="mayus(this);" autocomplete="off" id="rif_anterior" style="width:280px;" required pattern="[aA-Za-z]+" autocomplete="off" name="tipomedicamento" value=''>
<input type="hidden" class="control sinborder" onkeyup="mayus(this);" autocomplete="off" id="descripcion_anterior" style="width:280px;" required pattern="[aA-Za-z]+" autocomplete="off" name="tipomedicamento" value=''>
<input type="hidden" class="" id="borrado_anterior" name="borrado" value='false'>
	
	<script>
		function mayus(e) {
			e.value = e.value.toUpperCase();
		}
	</script>
	<script>
		$(document).ready(function() {

			var now = new Date();

			var day = ("0" + now.getDate()).slice(-2);
			var month = ("0" + (now.getMonth() + 1)).slice(-2);
			var today = day + "/" + month + "/" + now.getFullYear();
			// var today= (day)+"-"+(month)+"-"+now.getFullYear();
			// var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
			$("#fecha").val(today);
		});
	</script>
	<script>
		$(function() {
			//$( "#fecha" ).datepicker({changeMonth:true, changeYear:true});	
			$("#fecha").datepicker({
				dateFormat: 'dd/mm/yy',
				changeMonth: true,
				changeYear: true
			});
		});
	</script>

	<?= $this->endSection(); ?>