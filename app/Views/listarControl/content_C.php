<?= $this->extend('menu/supermenu')?>
<!-- INICIO DE LA SECION CONTENT -->
<?= $this->section('content')?>
	<!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">    
  
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->
	<link href="/css/combos.css" rel="stylesheet" type="text/css" />
<div class="container">	
	<BR>
	<div class="row ">
  		<div class="col-5">
  		</div>
  		<div class="col-5">
			<h3 class="center">Control de Inventario</h3>
		</div>	
		<div class="col-2">
			<button id="btnAgregar" class="btn btn-primary">Agregar</button><br /><br />		
  		</div>
  	</div>
	  <style>
   table.dataTable thead, table.dataTable tfoot {
           background: linear-gradient(to right, #4a779c,#7e9ab1,#5f7a91);
       }
   </style>
	<div class="row ">
		<div class="col-1">	
		</div>
		<!--table class="table table-hover table-bordered" id="table_roles" style="margin-top: 20px"-->
		<div class="col-lg-10">
		<!--table class="table table-hover table-bordered" id="table_roles" style="margin-top: 20px"-->

		<table class="display" id="table_control" style="width:100%" style="margin-top: 20px">
		
			<thead>
				<tr>
					<td >Id</td>
					<td>Descripcion</td>
					<td>Fecha Registro</td>				
					<td>Estatus</td>
					<td class="text-center" style="width: 90px;">Acciones</td>
				</tr>
			</thead>
			<tbody id="lista_de_unida_control">
			</tbody>
		</table>
	</div>
</div>

<!-- Ventana Modal -->
<form action="" method="post" name ="">
	<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop= "static">
    	<div class="modal-dialog modal-xs" role="document">
      		<div class="modal-content">
        		<div class="modal-header">
           			<h4 class="modal-title" id="modal"> Control de Inventario </h4>
        		</div>
 				<div class="modal-body">
   					<div id="datos_ajax_register"></div>
					<div>
						<input type="hidden" id='id' name='id'>
					</div>
      				<div class="form-group">
        				<label for="nombre0" class="control-label">Medida </label>
        				<input type="text"  onkeyup="mayus(this);" class="form-control" id="control" name="control" value= ''>
      				</div>

				    <label>Fecha </label>
          
				    <div class="input-group date" for="fecha" >
          				<input type="text" disabled="disabled" class="form-control "autocomplete="off" name="fecha" id="fecha"   style=" z-index: 1600 !important;">  
	      				<div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">    
          				</div>
				    </div>
				
				<br>
					<label for="borrado"id ="activo" >Activo</label>
					<input type="checkbox" class="" id="borrado" name="borrado" value='false'>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
					<button type="button" class="btn btn-primary" id="btnGuardar">Guardar datos</button>
					<button type="button" class="btn btn-primary" id="btnActualizar">Actualizar datos</button>
		      </div>
    		</div>
  		</div>
	</div>
</form>
<!-- METODO QUE TOMA LOS DATOS ANTERIORES DEL FORMULARIO -->
<input type="hidden"  onkeyup="mayus(this);" class="form-control" id="control_anterior" name="control" value= ''>
<input type="hidden" class="" id="borrado_anterior" name="borrado" value='false'>
<script>
  function mayus(e) {
    e.value = e.value.toUpperCase();
}
</script>
<!-- <script src="https://code.jquery.com/jquery-3.1.0.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script> -->
<script>
	$(document).ready(function() {

var now = new Date();

var day = ("0" + now.getDate()).slice(-2);
var month = ("0" + (now.getMonth() + 1)).slice(-2);
var today= day+"/"+month+"/"+now.getFullYear();
// var today= (day)+"-"+(month)+"-"+now.getFullYear();
// var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
$("#fecha").val(today);
});
</script>


<script>
			$( function() {
				//$( "#fecha" ).datepicker({changeMonth:true, changeYear:true});	
				$( "#fecha" ).datepicker({dateFormat: 'dd/mm/yy',changeMonth:true, changeYear:true});			
			} );
		</script>

<?= $this->endSection(); ?>