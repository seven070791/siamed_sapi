<?= $this->extend('menu/supermenu') ?>
<!-- INICIO DE LA SECION CONTENT -->
<?= $this->section('content') ?>
<!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->
<link href="/css/compuestos.css" rel="stylesheet" type="text/css" />
<style>
	table.dataTable thead,
	table.dataTable tfoot {
		background: linear-gradient(to right, #4a779c, #7e9ab1, #5f7a91);
	}
</style>
<div class="container"><br><br>
	<div class="row">
		<div class="col-5">
		</div>
		<div class="col-6">
			<h4>Compuestos </h4>
		</div>
	</div>
	<div class="row">
		<div class="col-1">
		</div>
		<label for="nombre0" class="control-label">
			<h4>Descripcion</h4>
		</label>&nbsp;&nbsp;&nbsp;
		<input type="text" class="form-control" readonly="readonly" id=" descripcion " name="descripcion" style="width:450px;" value='<?php echo $medicamento; ?>'>
		<div class="col-1">
		</div>
		<div class="col-1">
			<button id="btnAgregar" class=" btn-success btnAgregar">Agregar</button>
		</div>
		<div class="col-1">
			<button id="btnRegresar" class=" btn-secondary btnRegresar">Regresar</button>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-1">

		</div>
		<div class="col-lg-10">
			<!--table class="table table-hover table-bordered" id="table_roles" style="margin-top: 20px"-->
			<table class="display" id="table_compuestos" style="width:100%" style="margin-top: 20px">

				<thead>
					<tr>
						<td>Id</td>
						<td style="width:40%">Compuesto Activo</td>
						<td>Cantidad</td>
						<td style="width:5%">U.Medida </td>
						<td>Estatus </td>
						<td class="text-center" style="width: 90px;">Acciones</td>
					</tr>
				</thead>
				<tbody id="lista_de_compuestos">
				</tbody>
			</table>
		</div>
	</div>


	<!-- Ventana Modal -->
	<form action="" method="post" name="">
		<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
			<div class="modal-dialog " role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title " id="modal"> Descripcion</h5>
						<input type="text" class="sinborder" class="form-control  descripcion" readonly="readonly" id="descripcioncompuesta" name=" descripcioncompuesta " value='<?php echo $medicamento; ?>'>
					</div>
					<div class="modal-body">
						<div id="datos_ajax_register"></div>
						<div>
							<input type="hidden" id='id_medicamento' name='id' value='<?php echo $id; ?>'>
							<input type="hidden" id='id_compuesto' name='id_compuesto' value=''>

						</div>
						<br>
						<div>
							<label for="nombre0" class="control-label">Principio Activo</label>
							&nbsp;&nbsp;&nbsp;&nbsp;<select class="custom-select" style="width:200px;" id="cmbTipoMedicamento">
								<option value="0">seleccione</option>
							</select>
						</div>
						<br>
						<div>
							<label class="activo" id="activo">Activo</label>
							<label for="nombre0" class="control-label">Unidad De Medida</label>
							<select class="custom-select" style="width:200px;" id="cmbUnidadMedida">
								<option value="0">seleccione</option>
							</select>
						</div>
						<br>
						<div>
							<label for="nombre0" class="control-label">Cantidad</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="text" class="sinborder" autocomplete="off" id="cantidad" value="" name="cantidad" style="width:100px;" placeholder="Cantidad">
						</div>


						<input type="checkbox" class="borrado" id="borrado" name="borrado" value='false'>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
							<button type="button" class="btn btn-primary" id="btnGuardar">Guardar datos</button>
							<button type="button" class="btn btn-primary" id="btnActualizar">Actualizar datos</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
	<script>
		$(function() {
			//$( "#fecha" ).datepicker({changeMonth:true, changeYear:true});	
			$("#fecha").datepicker({
				dateFormat: 'dd/mm/yy',
				changeMonth: true,
				changeYear: true
			});
		});
	</script>
	<?= $this->endSection(); ?>