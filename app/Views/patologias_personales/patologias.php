<?= $this->extend('menu/supermenu')?>
<!-- INICIO DE LA SECION CONTENT -->
<?= $this->section('content')?>

	<link href="/css/combos.css" rel="stylesheet" type="text/css" />
<div class="container">
	<br>
	<br>


	<div class="row ">
  		<div class="col-4">
  		</div>
  		<div class="col-5">
			<h3 class="center">Registro /Patologías Personales  </h3>
		</div>	
	</div>
	<div class="row ">
		<div class="col-9">
		</div>
		<div class="col-3">
		<button id="btnAgregar" class=" btn-primary btnAgregar">Agregar</button>&nbsp;&nbsp;
		<button id="btnRegresar" class=" btn-secondary btnRegresar">Regresar</button>
		</div>
	</div>	
  
	<br>

  <style>
   table.dataTable thead, table.dataTable tfoot {
           background: linear-gradient(to right, #4a779c,#7e9ab1,#5f7a91);
       }
   </style>
		<div class="row">   
			<div class="col-2">
				
			</div>
			<div class="col-lg-9">
				<!--table class="table table-hover table-bordered" id="table_roles" style="margin-top: 20px"-->
				<table class="display" id="table_especialidad" style="width:100%" style="margin-top: 20px">
					<thead>
						<tr>
							<td style="width:10%"  >ID</td>				
							<td style="width:40%"  >DESCRIPCIÓN  </td>		
							<td style="width:10%"  >BORRADO</td>			
							<td class="text-center" style="width:10%">Acciones</td>
						</tr>
					</thead>
					<tbody id="lista_de_patologias">
					</tbody>
				</table>
			</div>
		</div>


		<!-- Ventana Modal -->
		<form action="" method="post" name ="">
			<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop= "static">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<div class="row">
									<h4 class="modal-title " id="modal">Patologias Personales </h4>								
							</div>
							<input type="hidden"autocomplete="off" class="form-control categoria" id="id_patologia" value="" name="id_especialidad" style="width:80px;" placeholder="">
						</div>
						<div class="modal-body">
							<div id="datos_ajax_register"></div>
							<label for="nombre0" class="control-label">Descripcion</label>
							<input type="text"class="sinborder"   autocomplete="off" id="descripcion" value="" name="descripcion" style="width:330px;" placeholder="">
							<div>
								<br>
								<div class="check_borrado" style="display: none;">
								<label class="activo"id="activo" >Activo</label>	
									<input type="checkbox" class="borrado" id="borrado" name="borrado" value=''>	&nbsp;&nbsp;&nbsp;&nbsp; 	
								</div>
							
						</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
						<button type="button" class="btn btn-primary" id="btnGuardar">Guardar datos</button>
						<button type="button" class="btn btn-primary btnActualizar" id="btnActualizar">Actualizar datos</button> 		      
					</div>
					</div>
					
				</div>
			</div>
</form>

<!-- METODO QUE TOMA EL VALOR ANTERIOR DEL LOS CAMPOS DEL FORMULARIO  -->
<!-- DECRIPCION -->
<input type="hidden"class="sinborder" onkeyup="mayus(this);"  autocomplete="off" id="descripcion_anterior" value="" name="descripcion" style="width:330px;" placeholder="">
<!-- BORRADO -->
<input type="hidden" class="borrado" id="borrado_anterior" name="borrado" value=''>	&nbsp;&nbsp;&nbsp;&nbsp; 
<!-- ACCESO A CITAS -->
<input type="hidden" class="acceso_citas" id="acceso_citas_anterior" name="acceso_citas" value=''>
<script>
  function mayus(e) {
    e.value = e.value.toUpperCase();
}
</script>
		<!-- <script>
			$( function() {
				//$( "#fecha" ).datepicker({changeMonth:true, changeYear:true});	
				$( "#fecha" ).datepicker({dateFormat: 'dd/mm/yy',changeMonth:true, changeYear:true});			
			} );
		</script> -->

		
<?= $this->endSection(); ?>