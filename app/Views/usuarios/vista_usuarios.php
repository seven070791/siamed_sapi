<?= $this->extend('menu/supermenu') ?>
<!-- INICIO DE LA SECION CONTENT -->
<?= $this->section('content') ?>

<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link href="/css/editaruser.css" rel="stylesheet" type="text/css" />
  <title></title>
</head>
<link rel="stylesheet" href="<?php echo base_url(); ?>/datatable_responsive/css/fixedHeader.bootstrap.min.css">
   <link rel="stylesheet" href="<?php echo base_url(); ?>/datatable_responsive/css/responsive.bootstrap.min.css">

<div class="container"><br />
    <div class="row ">
      <div class="col-md-4 col-sm-4 col-lg-4 col-xl-4">
      </div>
      <div class="col-md-4 col-sm-4 col-lg-4 col-xl-4">
        <h4 class="center">Usuarios-Informacion </h4>
      </div>
      <div class="col-md-4 col-sm-4 col-lg-4 col-xl-4">
        <a href="User_controllers/create" id="agregar" class="btn btn-xs btn-primary ">(+) Agregar</a>
      </div>
    </div>
	<style>
		table.dataTable thead,
		table.dataTable tfoot {
			background: linear-gradient(to right, #4a779c, #7e9ab1, #5f7a91);
		}
	</style>



</style>
<div class="container">
  <div class="row">
    <div class="col-lg-1">
    </div>
    <div class="col-lg-11">
      <table class="display" id="table_usuarios" cellspacing="2" width="100%">
        
        <thead>
          <tr>
            <th class="text-center">Id </th>
            <th class="text-center">Cedula </th>
            <th class="text-center">Nombre </th>
            <th class="text-center">Apellido </th>
            <th class="text-center">User</th>
            <th class="text-center">Correo</th>
            <th class="text-center" style="width: 15%;">Rol de usuario</th>
            <th class="text-center" style="width: 10%;">Estatus</th>
            <th class="text-center" style="width: 100px;">Acciones</th>
          </tr>
        </thead>
        <tbody id="lista_usuarios">
				</tbody>
    </div>
  </div>
</div>
</div>

<!-- Ventana Modal -->
<form action="" method="post" name="">
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
		<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="modal">Edicion Usuario</h4>
					</div>
					<div class="modal-body">
						<div id="datos_ajax_register"></div>
						<div>
							<input type="hidden" id='id' name='id'>
						</div>
            <div class="input-group">
                <label class="input-fill">
                  <i class="		fas fa-shield-alt"></i>   
                  <input type="text" id="cedula" style="width: 300px;" onkeypress="return valideKey(event);" name="cedula"  placeholder=" Cedula: 99999999" autocomplete="off" minlength="7" maxlength="8" required pattern="[0-9]+" />
                </label>
              </div>
              <div class="input-group">
                <label class="input-fill">
                  <input type="text" id="nombre" name="nombre" style="width: 300px;" onkeyup="mayus(this);" placeholder=" Nombre:" required pattern="[aA-Za-z]+" autocomplete="off"  />
                  <i class="	fas fa-portrait"></i>   
                  </label>
                </div>
              <div class="input-group">
                <label class="input-fill">
                  <input type="text" id="apellido" name="apellido"style="width: 300px;" onkeyup="mayus(this);"placeholder=" Apellido:" required pattern="[aA-Za-z]+" autocomplete="off" />
                  <i class="far fa-id-badge"></i>   
                  </label>
              </div>
              <div class="input-group">
                <label class="input-fill">
                  <input type="text" id="usuario" name="usuario" style="width: 300px;" placeholder=" Usuario:" required pattern="[aA-Za-z0-9]+"autocomplete="off" />
                  <i class="	fas fa-user-circle"></i>   
                  </label>
                </div>

                <div class="input-group">
                  <label class="input-fill">
                    <input type="email" id="email" name="email"style="width: 300px;" placeholder=" Email:" autocomplete="off" required />
                    <i class="fas fa-user-circle"></i>
                  </label>
                </div>
              <div class="input-group">
                <label class="input-fill">
                  <input type="password" id="password" name="password"style="width: 300px;" placeholder=" Clave:" required autocomplete="off" />
                  <i class="	fas fa-lock"></i>   
                </label>
              </div>

              <div class="input-group">
                  <label ><H6>Nivel  Usuario </H6> </label> 
                <select class="form-control"  id="tipousuario" name="tipousuario" data-style="btn-primary">
                  <option value="0" selected disabled>Seleccione</option>
                </select>
              </div>
                
              <div class="input-group">
              <label ><H6>Bloqueado:&nbsp; &nbsp; <input type="checkbox" class="check_borrado" id="borrado" name="borrado" value='false'> </H6> </label> 
              </div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
							<button type="button" class="btn btn-primary" id="btnGuardar">Guardar datos</button>
							<button type="button" class="btn btn-primary" id="btnActualizar">Actualizar datos</button>
						</div>
					</div>
				</div>
			</div>
	</form>









</div>


  <input type="hidden" class="check_borrado" id="borrado_anterior" name="borrado_anterior" value='false'>
  <input type="hidden" id="cedula_anterior" onkeypress="return valideKey(event);" name="cedula"  placeholder=" Cedula: 99999999" autocomplete="off" minlength="7" maxlength="8" required pattern="[0-9]+" />
  <input type="hidden" id="nombre_anterior"   onkeyup="mayus(this);" placeholder=" Nombre:" required pattern="[aA-Za-z]+" autocomplete="off"  />
  <input type="hidden" id="apellido_anterior"  onkeyup="mayus(this);"placeholder=" Apellido:" required pattern="[aA-Za-z]+" autocomplete="off" />
  <input type="hidden" id="usuario_anterior"   placeholder=" Usuario:" required pattern="[aA-Za-z0-9]+"autocomplete="off" />
  <input type="hidden" id="password_anterior" name="password" placeholder=" Clave:" required autocomplete="off" />
  <input type="hidden" id="email_anterior" name="email" placeholder=" Emil:" required autocomplete="off" />
  <select class="form-control"  style="display: none;" id="tipousuario_anterior" name="tipousuario" data-style="btn-primary">
  <option value="0" selected disabled>Seleccione</option>
  </select>



<script>
  function mayus(e) {
    e.value = e.value.toUpperCase();
}
</script>
<script src="js-password-hash.min.js"></script>
<!-- <script>
          $("#cedula").keyup(function()
        {              
          var ta      =   $("#cedula");
          letras      =   ta.val().replace(/ /g, "");
          ta.val(letras)
        });
          $("#nombre").keyup(function()
        {              
          var ta      =   $("#nombre");
          letras      =   ta.val().replace(/ /g, "");
          ta.val(letras)
        });

          $("#apellido").keyup(function()
        {              
          var ta      =   $("#apellido");
          letras      =   ta.val().replace(/ /g, "");
          ta.val(letras)
        });
          $("#username").keyup(function()
        {              
          var ta      =   $("#username");
          letras      =   ta.val().replace(/ /g, "");
          ta.val(letras)
        });
        </script> -->


<?= $this->endSection() ?>