<?= $this->extend('menu/supermenu')?>
<!-- INICIO DE LA SECION CONTENT -->
<?= $this->section('content')?>
<script>
$("#imagencentral").hide();
</script>

  <link rel="stylesheet" href="<?php echo base_url();?>/css/cdn/jquery-ui.css">
  <link rel="stylesheet" href="<?php echo base_url();?>/css/cdn/bootstrap.min.css">
  <!-- Este controla la Estructura del formulario  -->
  <link href="/css/creargrupo.css" rel="stylesheet" type="text/css" />
 
  

<body>
<div class="row">
  <div class="col-lg-3">   
  </div>
  <div class="col-lg-6"> 
    <form action="/Grupousuario/create" method="post">
      <div class="modal fade" id="crear" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header"> 
              <h4 class="modal-title" id="crear"> Grupos de Usuarios</h4>
            </div>
              <div class="modal-body">
			          <div id="datos_ajax_register"></div>    
		              <div class="form-group">
                    <label for="nombre0" class="control-label">Nombre del Grupo:</label>
                    <input type="text" class="form-control" onkeyup="mayus(this);" autocomplete="off"id="nivel_usuario" name="nivel_usuario" required maxlength="45">
                  </div> 
                </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
                <button type="submit" class="btn btn-primary">Guardar datos</button>
              </div>
          </div>
        </div>
      </div>
    </form>
  </div>
  <div class="col-lg-3">   
  </div>
</div> 

<!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script> -->




<script>
  function mayus(e) {
    e.value = e.value.toUpperCase();
}
</script>
<script>
 $("#crear").modal("show");
 $('#btnCerrar').click(function()
 {
  //  alert('Hiciste click');
   $(location).attr('href','/Grupousuario',); 
 });

 </script>
 
</body>

<!-- /.modal -->
<?= $this->endSection()?>