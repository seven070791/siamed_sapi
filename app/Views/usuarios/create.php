<?= $this->extend('menu/supermenu')?>
<!-- INICIO DE LA SECION CONTENT -->
<?= $this->section('content')?>
<link href="/css/crearuser.css" rel="stylesheet" type="text/css" /> 

        <?php  
          if (empty($_GET['alert'])) 
          {
            echo "";
          } 
          elseif ($_GET['alert'] == 1)
          {
            echo "<div class='alert alert-danger alert-dismissable'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                <h4>  <i class='icon fa fa-times-circle'></i> Error al entrar!</h4>
               Usuario o la contraseña es incorrecta, vuelva a verificar su nombre de usuario y contraseña.
              </div>";
          }
          elseif ($_GET['alert'] == 2)
          {
            echo "<div class='alert alert-success alert-dismissable'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                <h4>  <i class='icon fa fa-check-circle'></i> Exito!!</h4>
              Has salido con éxito.
              </div>";
            }?>

  <div class="row">  
    <div class="col-4">
    </div>
    <div class="col-lg-4">
      <br>
      <h3> <p class="login-box-msg"><i class="fa fa-user icon-title"></i>Registro de  usuario</p></h3>  
      <section class="form-login"> 
      <form  action="/User_controllers/create" class="formulario" method="post">
              <?= session()->getFlashdata('error') ?>
              <?= service('validation')->listErrors() ?>
              <?= csrf_field() ?>  
              <div class="input-group">
                <label class="input-fill">
                  <!-- <span class="input-label">Cedula:</span> -->
                  <i class="		fas fa-shield-alt"></i>   
                  <input type="text" id="cedula" onkeypress="return valideKey(event);" name="cedula"  placeholder=" Cedula: 99999999" autocomplete="off" minlength="7" maxlength="8" required pattern="[0-9]+" />
                </label>
              </div>
              <div class="input-group">
                <label class="input-fill">
                  <input type="text" id="nombre" name="nombre"  onkeyup="mayus(this);" placeholder=" Nombre:" required pattern="[aA-Za-z]+" autocomplete="off"  />
                  <!-- <span class="input-label">Nombre</span> -->
                  <i class="	fas fa-portrait"></i>   
                  </label>
                </div>
              <div class="input-group">
                <label class="input-fill">
                  <input type="text" id="apellido" name="apellido" onkeyup="mayus(this);"placeholder=" Apellido:" required pattern="[aA-Za-z]+" autocomplete="off" />
                  <!-- <span class="input-label">Apellido</span> -->
                  <i class="far fa-id-badge"></i>   
                  </label>
              </div>
              <div class="input-group">
                <label class="input-fill">
                  <input type="text" id="username" name="usuario"  placeholder=" Usuario:" required pattern="[aA-Za-z0-9]+"autocomplete="off" />
                  <!-- <span class="input-label">Username</span> -->
                  <i class="	fas fa-user-circle"></i>   
                  </label>
                </div>

                <div class="input-group">
                  <label class="input-fill">
                    <input type="email" id="email" name="email" placeholder=" Email:" autocomplete="off" required />
                    <i class="fas fa-user-circle"></i>
                  </label>
                </div>


              <div class="input-group">
                <label class="input-fill">
                  <input type="password" id="password" name="password" placeholder=" Clave:" required autocomplete="off" />
                  <!-- <span class="input-label">Password</span> -->
                  <i class="	fas fa-lock"></i>   
                </label>
              </div>
              <!-- <div class="input-group">
                <label class="input-fill">
                  <input type="email" id="correo" name="correo"  pattern=".+@sapi.gob\.ve" placeholder=" Correo Electronico: aaaaa@sapi.gob.ve"autocomplete="off" required  />
                  <span class="input-label">Correo Electronico</span> -->
                  <!-- <i class="fas fa-envelope"></i>   
                </label>
              </div>  -->
              <div class="row">
                <div class="col-5">   
                  <label ><H6>Nivel  Usuario </H6> </label> 
                </div>
                <div class="col-6">   
                <div class="input-group-fluid">
                  <label class="input-fill">
                  <select class="form-control tipousuario " name="tipousuario" id="tipousuario" required>
                    <?php foreach($tipos as $tipo): ?>
                      <option value="<?= $tipo['id']; ?>"><?= $tipo['nivel_usuario']; ?></option>                              
                    <?php endforeach; ?>   
                  </select>
               </div>
              </div>
              
             

          
              </div>
              <br>
              <div class="row">
                  <div class="col-lg-4">
                  </div>
                  <div class="col-lg-8">
                  <input type="submit" value="Registrar" class="btn-login" /> 
                  </div> 
                 
                </div>
            
    </form>


  
      </section>
    </div> 
    </div> 
   
  </div> 


     

 <script>
         function sanear(e) {
  let contenido = e.target.value;
  e.target.value = contenido.toUpperCase().replace(" ", "");
}
 </script>

<script>
  function mayus(e) {
    e.value = e.value.toUpperCase();
}
</script>



<script>
         function sanear2(e) {
  let contenido = e.target.value;
  e.target.value = contenido.replace(" ", "");   
                  }
                                 
  document.getElementById("cedula").addEventListener('keyup', sanear);
  document.getElementById("nombre").addEventListener('keyup', sanear);
  document.getElementById("apellido").addEventListener('keyup', sanear);
  document.getElementById("username").addEventListener('keyup', sanear2);
 // document.getElementById("correo").addEventListener('keyup', sanear2);
  document.getElementById("password").addEventListener('keyup', sanear2);
</script>

<script type="text/javascript">
function valideKey(evt){
    
    // code is the decimal ASCII representation of the pressed key.
    var code = (evt.which) ? evt.which : evt.keyCode;
    
    if(code==8) { // backspace.
      return true;
    } else if(code>=48 && code<=57) { // is a number.
      return true;
    } else{ // other keys.
      return false;
    }
}
</script> 






    <!-- jQuery 2.1.3 -->
    <script src="/plugins/jQuery/jQuery-2.1.3.min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="/js/bootstrap.min.js" type="text/javascript"></script>

  </body>
</html>
              
<?= $this->endSection()?>
