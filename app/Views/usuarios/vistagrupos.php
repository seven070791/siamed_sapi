<?= $this->extend('menu/supermenu') ?>
<!-- INICIO DE LA SECION CONTENT -->
<?= $this->section('content') ?>
<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->
  <link href="/css/tablagrupos.css" rel="stylesheet" type="text/css" />
  <!-- Bootstrap CSS -->
  <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"> -->
  <style>
    table.dataTable thead,
    table.dataTable tfoot {
      background: linear-gradient(to right, #5f7a91, #4a779c, #5f7a91);
    }
  </style>
  <!--    Datatables  -->
  <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.css" /> -->
  <title></title>
</head>
<strong>
  <br>
  <h4 class="text-center ">Grupos de Usuarios </h4>
</strong>
<div class="container">

  <div class="row">
    <style>
      table.dataTable thead,
      table.dataTable tfoot {
        background: linear-gradient(to right, #4a779c, #7e9ab1, #5f7a91);
      }
    </style>
    <div class="col-lg-2">
    </div>
    <div class="col-lg-9">
      <br>

      <a href="creargrupo" class="btn btn-primary agregar">(+) Agregar </a>
      <br>
      <br>
      <table class="display" id="example" style="width:100%">
        <thead>
          <tr>
            <th class="text-center" style="width: 50px;">Id</th>
            <th class="text-center">Nombre Grupos</th>
            <th class="text-center" style="width: 80px;">Estatus </th>
            <th class="text-center" style="width:20%">Acciones </th>
          </tr>
        </thead>

    </div>
  </div>
</div>

<!-- --------------AQUI FINALIZA----------  -->


<?php foreach ($Grupousuario as $grupo) : ?>
  <tr>
    <td><?php echo ($grupo['id']) ?></td>
    <td class="text-center"> <?php echo ($grupo['nivel_usuario']) ?></td-->
    <td>
      <?php

      if ($grupo['borrado'] == 't') {
        // echo("<input type='text' class='label label-success' value='Inactivo' style='background-color:#d9534f;'");
        echo ('<span class="label label-danger">Inactivo</span>');
      } else {
        echo ('<span class="label label-success">-Activo-</span>');
        // echo("  <input type='text'  value='Activo' style='background-color: #5cb85c;'");
      }
      //echo($grupo['borrado'])
      ?>
    </td>

    <td class="text-center">
      <div class="btn-group">

        <a href="./Grupousuario/editgrupo1/<?= esc($grupo['id'], 'url') ?>" class=" btn-xs btn-secondary" data-toggle="tooltip" title="Editar">
          <i class="large material-icons">create</i>


          <!-- <a href="./Grupousuario/editgrupo1/<= esc($grupo['id'], 'url') ?>"  data-toggle="tooltip" title="Block">
              <i class="large material-icons">person</i>secondary
              -->
    </td>
  </tr>
  <div>
  </div>
<?php endforeach; ?>

<a href="supermenu" class="btn btn-secondary regresar"> Regresar </a>

</table>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<!-- <script src="https://code.jquery.com/jquery-3.4.1.js"></script>

<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script> -->


<!--    Datatables-->
<!-- <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.js"></script> -->



<!-- <script>
  //Idiomas con el 1er método   
  $(document).ready(function() {
    $('#example').DataTable({
      "language": {
        "url": "https://cdn.datatables.net/plug-ins/1.10.20/i18n/Spanish.json"
      }

    });
  });
</script> -->


</body>

</html>
<?= $this->endSection() ?>