/*
 *Este es el document ready
 */
 $(function()
 {
    // e.preventDefault;
     var medico_id=$('#medico_id').val();
     consultas_atendidas(medico_id);
     consultas_not_atendidas(medico_id);
     listar_consultas(medico_id);
    
     
  

     document.getElementById("cmbmedicosreferidos").disabled=true;
     // Actualiza la tabla con las filas ordenadas
    // table.find('tbody').html(sortedRows);
     //Lleno la persiana de la unidad de medida
 });


 function consultas_atendidas(medico_id)
 {
   
       url=base_url+'/listar_consultas_atendidas/'+medico_id;
        $.ajax
       ({
            url:url,
            method:'GET',
           dataType:'json',
           beforeSend:function(data)
           {
           },
           success:function(data)
           {    
              
           $('#P_atendidos').val(data[0].total_atendidos)
              
           },
          error:function(xhr, status, errorThrown)
          {
               alert(xhr.status);
               alert(errorThrown);
          }
     }); 
 }


 function consultas_not_atendidas(medico_id)
 {

    
       url='/listar_consultas_not_atendidas/'+medico_id;
        $.ajax
       ({
            url:url,
            method:'GET',
           dataType:'json',
           beforeSend:function(data)
           {
           },
           success:function(data)
           {    
               //alert(data);
           $('#P_no_atendidos').val(data[0].total_no_atendidos)
              
           },
          error:function(xhr, status, errorThrown)
          {
               alert(xhr.status);
               alert(errorThrown);
          }
     }); 
 }
 function  llenar_combo_especialidad(e,id_especialidad)
 {

     e.preventDefault;
     
       url='/listar_especialidades_activas';
        $.ajax
       ({
            url:url,
            method:'GET',
           dataType:'JSON',
           beforeSend:function(data)
           {
           },
           success:function(data)
           {   
 if(data.length>=1)
 {
      $('#especialidad').empty();
      $('#especialidad').append('<option value=0>Seleccione</option>');     
      if(id_especialidad===undefined)
     {
          
          $.each(data, function(i, item)
          {
               //console.log(data)
               $('#especialidad').append('<option value='+item.id_especialidad+'>'+item.descripcion+'</option>');

          });
     }
     else
     {
          $.each(data, function(i, item)
          {
              

               if(item.id_especialidad===id_especialidad)
              
               {
                    $('#especialidad').append('<option value='+item.id_especialidad+' selected>'+item.descripcion+'</option>');
               }
               else
               {
                    $('#especialidad').append('<option value='+item.id_especialidad+'>'+item.descripcion+'</option>');
               }
          });
     }
 }      
 },
 error:function(xhr, status, errorThrown)
 {
     alert(xhr.status);
     alert(errorThrown);
 }
 });
 }
 function  llenar_combo_MedicoTratante(e,id)
 {

     e.preventDefault;
     
       url='/listar_medicos_activos';
        $.ajax
       ({
            url:url,
            method:'GET',
           dataType:'JSON',
           beforeSend:function(data)
           {
           },
           success:function(data)
           {

 if(data.length>=1)
 {
      $('#cmbmedicotratante').empty();
      $('#cmbmedicotratante').append('<option value=0>Seleccione</option>');     
      if(id===undefined)
     {
          
          $.each(data, function(i, item)
          {
               //console.log(data)
               $('#cmbmedicotratante').append('<option value='+item.id+'>'+item.nombre+'  '+item.apellido+'</option>');

          });
     }
     else
     {
          
          $.each(data, function(i, item)
          {
              

               if(item.id===id)
              
               {
                    $('#cmbmedicotratante').append('<option value='+item.id+' selected>'+item.nombre+'  '+item.apellido+'</option>');
               }
               else
               {
                    $('#cmbmedicotratante').append('<option value='+item.id+'>'+item.nombre+'  '+item.apellido+'</option>');
               }
          });
     }
 }      
 },
 error:function(xhr, status, errorThrown)
 {
     alert(xhr.status);
     alert(errorThrown);
 }
 });
 }




 
 function  llenar_combo_MedicoReferido(e,id_especialidad)
 {

     e.preventDefault;
     
       url='/listar_medicos_activos';
        $.ajax
       ({
            url:url,
            method:'GET',
           //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
           dataType:'JSON',
           beforeSend:function(data)
           {
           },
           success:function(data)
           {
           
 if(data.length>=1)
 {
      $('#cmbmedicosreferidos').empty();
      $('#cmbmedicosreferidos').append('<option value=0  selected disabled>Medicos</option>');   
      verificar=7;  
      if(id_especialidad===undefined)
     {
          
          $.each(data, function(i, item)
          {
               //console.log(data)
               $('#cmbmedicosreferidos').append('<option value='+item.id+'>'+item.nombre+'  '+item.apellido+'</option>');

          });
     }
     else
     {
        data=data.filter(dato=>dato.id_especialidad==id_especialidad);
        //console.log(buscar);
           $.each(data, function(i, item)
           {
            $('#cmbmedicosreferidos').append('<option value='+item.id+'>'+item.nombre+'  '+item.apellido+'</option>');

              
          });
     }
 }      
 },
 error:function(xhr, status, errorThrown)
 {
     alert(xhr.status);
     alert(errorThrown);
 }
 });
 }

 $("#especialidad").on('change', function(e)
 {
     document.getElementById("cmbmedicosreferidos").disabled=false;
    id_especialidad=$('#especialidad option:selected').val(); 

    llenar_combo_MedicoReferido(e,id_especialidad)
 }); 

 /*
  * Función para definir datatable:
  */

 
 function listar_consultas(medico_id)
 {
     let prueba='false'
    // alert('hola');
      $('#table_especialidad').DataTable
      (
       {
            "order":[[0,"desc"]],					
            "paging":true,
            "info":true,
            "filter":true,
            "responsive": true,
            "autoWidth": true,					
            //"stateSave":true,
            "ajax":
            {
             "url":"/listar_consultas_medicos/"+medico_id,
             "type":"GET",
             dataSrc:''
            },
            "columns":
            [

               {orderable: true,
                    render:function(data, type, row)
                    {
                     if(row.atendido=='ATENDIDO')
                   
                      {
                         return '<input type="checkbox" checked onclick = "this.checked=!this.checked" >';
                         
                      }
                      else
                      {
                         return '<input type="checkbox" disabled>';
                       
                      } 
                   
                    }
               },

                 

              

               {orderable: true,
                    render:function(data, type, row)
                    {
                       return '<a href="javascript:;" class="btn btn-xs btn-secondary  Detalles" style=" font-size:1px" data-toggle="tooltip" title="Detalles" id='+row.id+' n_historial='+row.n_historial+' cedula='+row.cedula+' nombre='+row.nombre+'  telefono='+row.telefono+'  fecha_asistencia='+row.fecha_asistencia+'   tipo_beneficiario='+row.tipo_beneficiario+'  id_historial_medico='+row.id_historial_medico+' > <i class="material-icons " >create</i></a>'+'  '+'<a href="javascript:;" class="btn btn-xs btn-primary Imprimir" style=" font-size:1px" data-toggle="tooltip" title="Imprimir" id='+row.id+' n_historial='+row.n_historial+' cedula='+row.cedula+' nombre='+row.nombre+'  telefono='+row.telefono+'  fecha_asistencia='+row.fecha_asistencia+'   tipo_beneficiario='+row.tipo_beneficiario+'  id_historial_medico='+row.id_historial_medico+' > <i class="material-icons " >print</i></a>'
                    }
               },    
              
               {data:'n_historial'},  
               {data:'cedula'},   
               {data:'nombre'},             
               {data:'fecha_creacion'},
               {data:'fecha_asistencia'},    
               {data:'telefono'},      
               
               
            ],
            "language":
            {
             "sProcessing":    "Procesando...",
             "sLengthMenu":    "Mostrar _MENU_ registros",
             "sZeroRecords":   "No se encontraron resultados",
             "sEmptyTable":    "Ningún dato disponible en esta tabla",
             "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
             "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
             "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
             "sInfoPostFix":   "",
             "sSearch":        "Buscar:",
             "sUrl":           "",
             "sInfoThousands":  ",",
             "sLoadingRecords": "Cargando...",
             "oPaginate": 
             {
              "sFirst":    "Primero",
              "sLast":    "Último",
              "sNext":    "Siguiente",
              "sPrevious": "Anterior"
             },
             "oAria":
             {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
             },
             "columnDefs": 
             [
                {
                 "targets": [ 0 ],
                 "visible": false,
                 "searchable": false
             }
             ],			
            }
       });
 }
 $('#btnAgregar').on('click',function(e)
 { 
     $('#categoria').val('');
    
      $('#modal').find('#btnGuardar').show();
      $('#modal').find('#btnActualizar').hide();
      $('#modal').find('#borrado').hide();

      $('#modal').find('#activo').hide();

      $("#modal").modal("show");
   

 });
 
 $('#lista_de_categoria').on('click','.Editar', function(e)
 {
     $('#modal').find('#btnGuardar').hide();
     $('#modal').find('#btnActualizar').show();
     var id_especialidad           =$(this).attr('id_especialidad');  
     var descripcion   =$(this).attr('descripcion'); 
     var estatus       =$(this).attr('estatus');
     $('#descripcion').val(descripcion);
     $('#id_especialidad').val(id_especialidad);

     $("#modal").modal("show");

     if(estatus=='Activo')
     {
          $('#borrado').attr('checked','checked');
          $('#borrado').val('false');
     }
     if(estatus=='Eliminado')
     {
          $('#borrado').removeAttr('checked')
          $('#borrado').val('true')
     }


 });


$(document).on('click','#btnGuardar', function(e)
 {
      e.preventDefault();
      var id_especialidad           =$('#id_especialidad ').val();
      var descripcion   =$('#descripcion').val();
      var url='/agregar_Especialidad';
      var data=
      {
        id_especialidad  :id_especialidad  ,
          descripcion  :descripcion,        
      }
     
      $.ajax
      ({
          url:url,
          method:'POST',
          data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
          },
          success:function(data)
          {
            
               if(data=='1')
               {
                     alert('Registro Incorporado');
                     window.location = '/Vita_Especialidad'; 
               }
               else
               {
                    alert('Error en la Incorporación del Registro');
               }
          },
          error:function(xhr, status, errorThrown)
          {
               alert(xhr.status);
               alert(errorThrown);
           }
       });

});

$(document).on('click','#btnActualizar', function(e)
{
     var id_especialidad =$('#id_especialidad ').val();        
     var descripcion   =$('#descripcion ').val();

    
   
     var borrado='false';

     if($('#borrado').is(':checked'))
     {
          borrado='false';
     }
     else
     {
          borrado='true';
     }

   
     var data=
     {
          id_especialidad :id_especialidad ,
          descripcion :descripcion ,
          borrado        :borrado
     }
 
     var url='/actualizar_especialidad';
     $.ajax
     ({
          url:url,
          method:'POST',
          data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
               //alert('Procesando Información ...');
          },
          success:function(data)
          {
             //alert(data);
             if(data===1)
             {
                alert('Registro Actualizado');
             }
             else
             {
               alert('Error en la Incorporación del registro');
             }
             window.location = '/Vita_Especialidad';             
          },
          error:function(xhr, status, errorThrown)
          {
             alert(xhr.status);
             alert(errorThrown);
          }
     });
 });
 $(document).on('click','#btnRegresar', function(e)
{
     window.location = '/vistamedicamentos/';
})

$('#lista_de_categoria').on('click','.Detalles', function(e)
{
     var id_especialidad=$('#id_especialidad').val();
     var id_historial_medico=$(this).attr('id_historial_medico');
     var fecha_asistencia2=$(this).attr('fecha_asistencia');  

     let fechaconvertida_hasta=moment(fecha_asistencia2,'DD/MM/YYYY'); 
     fecha_asistencia=fechaconvertida_hasta.format('YYYY-MM-DD');
///alert(fecha_asistencia);
     let id_consulta=$(this).attr('id');
     var n_historial =$(this).attr('n_historial');
     var cedula      =$(this).attr('cedula');
     var tipobeneficiario  =$(this).attr('tipo_beneficiario');
   window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia2+'/'+id_historial_medico+'/'+id_consulta; 

});


$('#lista_de_categoria').on('click','.Imprimir', function(e)
{
     e.preventDefault
     var id_historial_medico=$(this).attr('id_historial_medico');
     let cedula =$(this).attr('cedula');
     let usuario=$('#usuario').val();  
     var n_historial =$(this).attr('n_historial');
     var tipobeneficiario  =$(this).attr('tipo_beneficiario');
     var fecha_asistencia=$(this).attr('fecha_asistencia');  
     let id_consulta=$(this).attr('id');
     let id_especialidad=$('#id_especialidad').val();
    if (id_especialidad=='1') {
         window.open('PDF_Medicina_General/'+cedula+'/'+n_historial+'/'+tipobeneficiario+'/'+usuario+'/'+fecha_asistencia+'/'+id_consulta+'/'+id_historial_medico,'_blank');
    }
    else if (id_especialidad=='6'){     
     window.open('PDF_Paciente_Pediatrico/'+cedula+'/'+n_historial+'/'+tipobeneficiario+'/'+usuario+'/'+fecha_asistencia+'/'+id_consulta+'/'+id_historial_medico+'/'+id_especialidad,'_blank');
    }
   else if (id_especialidad=='4') {
     window.open('PDF_Ginecologia_Obtertricia/'+cedula+'/'+n_historial+'/'+tipobeneficiario+'/'+usuario+'/'+fecha_asistencia+'/'+id_consulta+'/'+id_historial_medico+'/'+id_especialidad,'_blank');        
     }
     else if (id_especialidad=='10') {
          window.open('PDF_Odontologia/'+cedula+'/'+n_historial+'/'+tipobeneficiario+'/'+usuario+'/'+fecha_asistencia+'/'+id_consulta+'/'+id_historial_medico+'/'+id_especialidad,'_blank');        
          }

});


