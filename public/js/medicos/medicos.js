/*
 *Este es el document ready
 */
 $(function()
 {
    
    listar_Medicos();
     
    
 });

 /*
  * Función para definir datatable:
  */
 function  listar_Medicos()
 {
      $('#table_medicos').DataTable
      (
       {
            "order":[[0,"asc"]],					
            "paging":true,
            "info":true,
            "filter":true,
            "responsive": true,
            "autoWidth": true,					
            //"stateSave":true,
            "ajax":
            {
             "url":base_url+"/listar_medicos",
             "type":"GET",
             dataSrc:''
            },
            "columns":
            [
                {data:'id'},
                {data:'cedula'},
                {data:'nombre'},                
                {data:'apellido'},     
                {data:'telefono'},
                {data:'especialidad'}, 
 
              
               
                
                {
                    data: null,
                    orderable: true,
                    render:function(data, type, row)
                    {
                     if(row.borrado=='Bloqueado')
                   
                      {
                         return '<button id="btnbloqueado"class=" btn-danger btnbloqueado "disabled="disabled">'+row.borrado+'</button>'
                      }
                      else
                      {
                         return '<button id="btnactivo"class=" btn-success btnactivo"disabled="disabled">'+row.borrado+'</button>'
                       
                      } 
                   
                    }
               },

                {
                    data: null,
                    orderable: true,
                 render:function(data, type, row)
                 {
                    return '<a href="javascript:;" class="btn btn-xs btn-secondary  Editar" style=" font-size:1px" data-toggle="tooltip" title="Editar"   cedula='+row.cedula+' id_medico='+row.id+' id_especialidad='+row.id_especialidad+' especialidad='+row.especialidad+' nombre ="'+row.nombre+'" apellido ="'+row.apellido+'"telefono ="'+row.telefono+'" estatus='+row.borrado+'> <i class="material-icons " >create</i></a>'
                    }
                }
            ],
            "language":
            {
             "sProcessing":    "Procesando...",
             "sLengthMenu":    "Mostrar _MENU_ registros",
             "sZeroRecords":   "No se encontraron resultados",
             "sEmptyTable":    "Ningún dato disponible en esta tabla",
             "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
             "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
             "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
             "sInfoPostFix":   "",
             "sSearch":        "Buscar:",
             "sUrl":           "",
             "sInfoThousands":  ",",
             "sLoadingRecords": "Cargando...",
             "oPaginate": 
             {
              "sFirst":    "Primero",
              "sLast":    "Último",
              "sNext":    "Siguiente",
              "sPrevious": "Anterior"
             },
             "oAria":
             {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
             },
             "columnDefs": 
             [
                {
                 "targets": [ 0 ],
                 "visible": false,
                 "searchable": false
             }
             ],			
            }
       });
 }
 $('#btnAgregar').on('click',function(e)
 { 

    
      $('#modal').find('#btnGuardar').show();
      $('#modal').find('#btnActualizar').hide();
      $('#modal').find('#borrado').hide();

      $('#modal').find('#activo').hide();

      $("#modal").modal("show");

      $('#cedula').val('');
      $('#nombre').val('');
      $('#apellido').val('');
      $('#telefono').val('');
      
   
      llenar_combo_especialidad(Event)

 });
 function  llenar_combo_especialidad(e,id_especialidad)
 {

     e.preventDefault;
     
       url='/listar_especialidades_activas_sin_filtro';
        $.ajax
       ({
            url:url,
            method:'GET',
           dataType:'JSON',
           beforeSend:function(data)
           {
           },
           success:function(data)
           {   
 if(data.length>1)
 {
      $('#especialidad').empty();
      $('#especialidad').append('<option value=0>Seleccione</option>');     
      if(id_especialidad===undefined)
     {
          
          $.each(data, function(i, item)
          {
               //console.log(data)
               $('#especialidad').append('<option value='+item.id_especialidad+'>'+item.descripcion+'</option>');

          });
     }
     else
     {
          $.each(data, function(i, item)
          {
              

               if(item.id_especialidad===id_especialidad)
              
               {
                    $('#especialidad').append('<option value='+item.id_especialidad+' selected>'+item.descripcion+'</option>');
               }
               else
               {
                    $('#especialidad').append('<option value='+item.id_especialidad+'>'+item.descripcion+'</option>');
               }
          });
     }
 }      
 },
 error:function(xhr, status, errorThrown)
 {
     alert(xhr.status);
     alert(errorThrown);
 }
 });
 }

 $(document).on('click','#btnGuardar', function(e)
 {
      e.preventDefault();
      var cedula   =$('#cedula').val();
      var nombre  =$('#nombre').val();
      var apellido  =$('#apellido').val();
      var telefono  =$('#telefono').val();

      var especialidad=$('#especialidad').val();

if (cedula==''||nombre==''||apellido==''||especialidad=='0') {
   alert('Los Campos: Cedula , Nombre , Apellido y especialidad son Obligatorios');
}else
{
      var url='/agregar_Medicos';
      var data=
      {
        cedula :cedula ,
         nombre   :nombre ,  
         apellido: apellido,  
         telefono:telefono, 
         especialidad:especialidad   
      }
     
      $.ajax
      ({
          url:url,
          method:'POST',
          data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
          },
          success:function(data)
          {
            
               if(data=='1')
               {
                     alert('Registro Incorporado');
                     window.location = '/vistaMedicos'; 
               }
               else
               {
                    alert('Error en la Incorporación del Registro');
               }
          },
          error:function(xhr, status, errorThrown)
          {
               alert(xhr.status);
               alert(errorThrown);
           }
      });
    }
});

$('#lista_de_categoria').on('click','.Editar', function(e)
 {
     
     $('#modal').find('#btnGuardar').hide();
     $('#modal').find('#btnActualizar').show();

     var id_medico =$(this).attr('id_medico'); 
     var id_especialidad  =$(this).attr('id_especialidad'); 
     var especialidad  =$(this).attr('especialidad');  
     var nombre   =$(this).attr('nombre'); 
     var apellido       =$(this).attr('apellido');
     var telefono       =$(this).attr('telefono');
     var cedula       =$(this).attr('cedula');
     var estatus       =$(this).attr('estatus');
     $('#cedula').val(cedula);
     $('#nombre').val(nombre);
     $('#apellido').val(apellido);
     $('#telefono').val(telefono);
     $('#especialidad').val(especialidad);
     $('#id_especialidad').val(id_especialidad);
     $('#id_medico').val(id_medico);

     llenar_combo_especialidad(e,id_especialidad)
     $("#modal").modal("show");

     if(estatus=='Activo')
     {
          $('#borrado').attr('checked','checked');
          $('#borrado').val('false');
     }
     if(estatus=='Eliminado')
     {
          $('#borrado').removeAttr('checked')
          $('#borrado').val('true')
     }


 });

 $(document).on('click','#btnActualizar', function(e)
 {
     var id_medico=$('#id_medico').val();  
     var id_especialidad =$('#id_especialidad').val();        
     var nombre   =$('#nombre').val();
     var apellido   =$('#apellido').val();
     var telefono   =$('#telefono').val();
     var cedula   =$('#cedula').val();
     var especialidad =$('#especialidad').val();
 
      var borrado='false';
 
      if($('#borrado').is(':checked'))
      {
           borrado='false';
      }
      else
      {
           borrado='true';
      }
 
    
      var data=
      {
          id_medico:id_medico,
          nombre:nombre,
          apellido:apellido,
          telefono:telefono,
          cedula:cedula,
          id_especialidad :id_especialidad ,
          especialidad:especialidad,
          borrado        :borrado
      }
  
      var url='/actualizar_Medicos';
      $.ajax
      ({
           url:url,
           method:'POST',
           data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
           dataType:'JSON',
           beforeSend:function(data)
           {
                //alert('Procesando Información ...');
           },
           success:function(data)
           {
              //alert(data);
              if(data===1)
              {
                 alert('Registro Actualizado');
              }
              else
              {
                alert('Error en la Incorporación del registro');
              }
              window.location = '/vistaMedicos';             
           },
           error:function(xhr, status, errorThrown)
           {
              alert(xhr.status);
              alert(errorThrown);
           }
      });
  });

  $(document).on('click','#btnRegresar', function(e)
  {

   
      e.preventDefault();
       window.location = '/supermenu/';
      
  });
 