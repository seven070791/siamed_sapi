/*
 *Este es el document ready
 */
 $(function() {
  
    let desde = $('#desde').val();
    let hasta = $('#hasta').val();
    let medico_id = $('#medico_id').val();

    if (medico_id == ' ') {
        id_medico = 0;
    } else {
        id_medico = medico_id;
    }

    let cedula_trabajador = $('#cedula_cortesia').val();
    if (desde == '' && hasta == '') {
        desde = 'null'
        hasta = 'null'
    }
    $('#medico').val('0')
    $('#especialidad').val('0')
    let especialidad = 0
    let nombre_especialidad = 'null'
    let nombre_medico = 'null'
    medico = 0;
    id_especialidad = null
    llenar_combo_especialidad(Event, id_especialidad)
   // document.getElementById("medico").disabled = true;
    listar_Reposos(desde, hasta, id_medico, especialidad, nombre_medico, nombre_especialidad);
   // $('#formulario')[0].reset();
   // document.getElementById("btnMostrar_reposo").disabled = true;
});

function listar_Reposos(desde, hasta, id_medico, especialidad, nombre_medico, nombre_especialidad) {
    let medico_id = $('#medico_id').val();
    medico_id = medico_id.trim();
    let nivel_usuario = $('#nivel_usuario').val();
    var now = new Date();
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var today = now.getFullYear() + "-" + month + "-" + day;
    var today = today.trim();
    let encabezado = '';
    var fechaOriginal = desde;
    var dataFormatada_desde = moment(fechaOriginal).format("DD-MM-YYYY");
    var fechaOriginal2 = hasta;
    var dataFormatada_hasta = moment(fechaOriginal2).format("DD-MM-YYYY");
    if (dataFormatada_desde != 'NaN/NaN/NaN' && dataFormatada_hasta != 'NaN/NaN/NaN') {
        encabezado = encabezado + 'FECHA' + ' ' + 'DESDE:' + ' ' + dataFormatada_desde + ' ' + ' ' + 'HASTA' + ' ' + ' ' + dataFormatada_hasta + ' ' + ' ';
    }
    if (nombre_especialidad != ' ' && nombre_especialidad != 'null' && nombre_especialidad != 'Seleccione') {
        encabezado = encabezado + 'ESPECIALIDAD :' + ' ' + ' ' + nombre_especialidad + ' ' + ' ';
    }
    if (nombre_medico != 'Medicos' && nombre_medico != 'null' && nombre_medico != 'Seleccione') {
        encabezado = encabezado + 'MEDICO :' + ' ' + ' ' + nombre_medico + ' ' + ' ';;
    }
    let ruta_imagen = rootpath
    var table = $('#table_reposos').DataTable({

        dom: "Bfrtip",
        buttons: {
            dom: {
                button: {
                    className: 'btn-xs-xs'
                },

            },

            buttons: [{
                    //definimos estilos del boton de pdf
                    extend: "pdf",
                    text: 'PDF',
                    orientation: 'landscape',
                    pageSize: 'LETTER',
                    className: 'btn-xs btn-dark',
                    title: 'REPOSOS',
                    download: 'open',
                    exportOptions: {
                        columns: [1, 2, 3, 4, 5, 6, 7],

                    },
                    alignment: 'center',
                    customize: function(doc) {
                        //Remove the title created by datatTables
                        doc.content.splice(0, 1);

                        doc.styles.title = {
                            color: '#4c8aa0',
                            fontSize: '18',
                            alignment: 'center'
                        }
                        doc.styles['td:nth-child(2)'] = {
                                width: '100px',
                                'max-width': '100px'
                            },
                            doc.styles.tableHeader = {
                                fillColor: '#4c8aa0',
                                color: 'white',
                                alignment: 'center'
                            },
                            // Create a header
                            doc.pageMargins = [10, 85, 0, 30];
                        doc['header'] = (function(page, pages) {
                            doc.styles.title = {
                                color: '#4c8aa0',
                                fontSize: '18',
                                alignment: 'center',
                            }
                            return {
                                columns: [{
                                        margin: [170, 3, 40, 40],
                                        image: ruta_imagen,
                                        width: 500,

                                    },
                                    {
                                        margin: [-330, 40, -25, 0],
                                        color: '#4c8aa0',
                                        fontSize: '18',
                                        alignment: 'center',
                                        text: 'CONTROL DE REPOSOS',
                                        fontSize: 18,
                                    },
                                    {
                                        margin: [-517, 70, -25, 0],
                                        text: encabezado,
                                    },
                                ],
                            }
                        });
                        // Create a footer
                        doc['footer'] = (function(page, pages) {
                            return {
                                columns: [{
                                    alignment: 'center',
                                    text: ['pagina ', { text: page.toString() }, ' of ', { text: pages.toString() }]
                                }],
                            }
                        });

                    },
                },

                {
                    //definimos estilos del boton de excel
                    extend: "excel",
                    text: 'Excel',
                    className: 'btn-xs btn-dark',
                    title: 'NOTAS DE ENTREGAS',

                    download: 'open',
                    exportOptions: {
                        columns: [1, 2, 3, 4, 5, 6, 7],
                    },
                    excelStyles: {
                        "template": [
                            "blue_medium",
                            "headerN_HISTORIAL_blue",
                            "title_medium"
                        ]
                    },

                }
            ]
        },


        "order": [
            [0, "desc"]
        ],
        "paging": true,
        "lengthChange": true,

        dom: 'Blfrtip',
        "searching": true,
        "lengthMenu": [
            [10, 25, 50, -1],
            ['10', '25', '50', 'Todos']
        ],

        "ordering": true,
        "info": true,
        "autoWidth": true,
        //"dom": 'Bfrt<"col-md-6 inline"i> <"col-md-6 inline"p>',
        "ajax": {

            "url": base_url+"/listar_reposos/" + desde + '/' + hasta + '/' + id_medico + '/' + especialidad,
            "type": "GET",
            dataSrc: ''
        },
        "columns": [

            {
                orderable: true,
                render: function(data, type, row) {

                    return '<a href="javascript:;"  class="btn btn-xs btn-primary Editar  " style=" font-size:1px" data-toggle="tooltip" title="Editar" tipo_beneficiario=' + row.tipo_beneficiario + ' horas=' + row.horas + '  id_medico=' + row.id_medico + ' id_especialidad=' + row.id_especialidad + '  cedula_trabajador=' + row.cedula_trabajador + '  id_reposo=' + row.id + '  fecha_desde=' + row.fecha_desde + ' motivo="' + row.motivo + '" fecha_hasta=' + row.fecha_hasta + ' n_historial="' + row.n_historial + '"> <i class="material-icons " >create</i>' + ' ' + ' ' + '<a href="javascript:;" class="btn btn-xs btn-success Imprimir " style=" font-size:1px" data-toggle="tooltip" title="Imprimir"    tipo_beneficiario=' + row.tipo_beneficiario + ' horas=' + row.horas + ' cedula_trabajador=' + row.cedula_trabajador + ' id_reposo=' + row.id + '  fecha_desde=' + row.fecha_desde + ' motivo="' + row.motivo + '" fecha_hasta=' + row.fecha_hasta + ' n_historial="' + row.n_historial + '"> <i class="material-icons " >print</i>' + ' ' + ' ' + '<a href="javascript:;" class="btn btn-xs btn-secondary  Bloquear  " style="font-size:1px" data-toggle="tooltip" title="Reversar"  cedula_trabajador=' + row.cedula_trabajador + ' id_reposo=' + row.id + ' fecha_desde=' + row.fecha_desde + ' motivo="' + row.motivo + '" fecha_hasta=' + row.fecha_hasta + '  nombre="' + row.nombre + '" n_historial="' + row.n_historial + '">   <i class="material-icons  " > delete_forever</i></a>'

                }
            },

            { data: 'n_historial' },
            { data: 'nombre' },
            { data: 'fecha_desde' },
            { data: 'fecha_hasta' },
            { data: 'motivo' },
            { data: 'medico' },
            { data: 'fecha_creacion_c' },
            { data: 'estatus' },




        ],

        "language": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": [{
                "targets": [0],
                "visible": false,
                "searchable": false
            }, ]

        },
    });
}


$(document).on('click', '#btnfiltrar ', function(e) {
    e.preventDefault();
    var desde = $('#desde').val();
    var hasta = $('#hasta').val();
    let id_medico = $('#medico').val();
    let nombre_medico = $('#medico option:selected').text();
    let especialidad = $('#especialidad').val();
    let nombre_especialidad = $('#especialidad option:selected').text();
    if (desde == '') {
        desde = 'null'
    }
    if (hasta == '') {
        hasta = 'null'
    }
    $("#table_reposos").dataTable().fnDestroy();
    listar_Reposos(desde, hasta, id_medico, especialidad, nombre_medico, nombre_especialidad);

    if (desde == 'null' && hasta != 'null') {
        alert('DEDE INDICAR EL CAMPO DESDE');

    } else if (hasta == 'null' && desde != 'null') {
        alert('DEDE INDICAR EL CAMPO HASTA');
    } else if (hasta < desde) {
        alert('EL CAMPO DESDE ES MAYOR AL CAMPO HASTA')
    }

});
$(document).on('click', '#btnlimpiar', function(e) {
    e.preventDefault();
    $('input[type="date"]').val('');
    let desde = $('#desde').val();
    let hasta = $('#hasta').val();
    $('#medico').val('0');
    $('#especialidad').val('0');
    let id_medico = 0;
    let especialidad = 0;
    if (desde == '' && hasta == '') {
        desde = 'null'
        hasta = 'null'

    }
    let nombre_medico = 'null';
    let nombre_especialidad = 'null';
    document.getElementById("medico").disabled = true;
    $("#table_reposos").dataTable().fnDestroy();
    listar_Reposos(desde, hasta, id_medico, especialidad, nombre_medico, nombre_especialidad);

});

$('#lista_reposos').on('click', '.Imprimir', function(e) {
    e.preventDefault();
    var now = new Date();
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var today = day + "-" + month + "-" + now.getFullYear();
    let id_reposo = $(this).attr('id_reposo');
    let n_historial = $(this).attr('n_historial');
    let cedula = $(this).attr('cedula_trabajador');
    let tipo_beneficiario = $(this).attr('tipo_beneficiario');
    var nombre_medico = $('#nombre_medico').val();
    let horas_d = $(this).attr('horas');
    
    window.open('../../../../../../../../PDF_Reposos/' + cedula + '/' + n_historial + '/' + id_reposo + '/' + nombre_medico + '/' + today + '/' + tipo_beneficiario + '/' + horas_d, '_blank');


});

$('#lista_reposos').on('click', '.Bloquear', function(e)

    {
        Swal.fire({
            title: 'Reversar el Registro?',
            icon: 'warning',
            showCancelButton: true,
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirmar',

        }).then((result) => {
            if (result.isConfirmed) {

                Swal.fire(

                    'REVERSO!!',
                    'Registro Actualizado.',
                    'success', {

                    }
                )

                let id_reposo = $(this).attr('id_reposo');
                let nombret = $(this).attr('nombre');

                id_reposo = (isNaN(parseInt(id_reposo))) ? 0 : parseInt(id_reposo);
                let n_historial = $(this).attr('n_historial');
                let borrado = true;
                let accion = 'REVERSO EL REPOSO DE ' + ' ' + nombret;




                var data = {
                    id_reposo: id_reposo,
                    n_historial: n_historial,
                    borrado: borrado,
                    accion: accion

                }
                var url = '/Reversar_reposo';
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: { data: btoa(unescape(encodeURIComponent(JSON.stringify(data)))) },
                    dataType: 'JSON',
                    beforeSend: function(data) {
                        //alert('Procesando Información ...');
                    },
                    success: function(data) {



                        if (data == 0) {
                            alert('Error al Reversar el Registro');

                        } else if (data == 2) {
                            alert('Hubo un error durante el reverso del la nota de entrega');
                        }
                        setTimeout(function() {
                            window.location = '/medico_reposo/';
                        }, 1500);

                    },
                    error: function(xhr, status, errorThrown) {
                        alert(xhr.status);
                        alert(errorThrown);
                    }
                });

            }

        })


    });

function llenar_combo_especialidad(e, id_especialidad = null) {

    e.preventDefault;

    url = '/listar_especialidades_activas_sin_filtro';
    $.ajax({
        url: url,
        method: 'GET',
        dataType: 'JSON',
        beforeSend: function(data) {},
        success: function(data) {
            if (data.length >= 1) {
                $('#especialidad').empty();
                $('#especialidad').append('<option value=0>Seleccione</option>');
                $('#r_especialidad').empty();
                $('#r_especialidad').append('<option value=0>Seleccione</option>');
                if (id_especialidad === undefined) {

                    $.each(data, function(i, item) {
                        //console.log(data)
                        $('#especialidad').append('<option value=' + item.id_especialidad + '>' + item.descripcion + '</option>');
                        $('#r_especialidad').append('<option value=' + item.id_especialidad + '>' + item.descripcion + '</option>');

                    });
                } else {
                    $.each(data, function(i, item) {


                        if (item.id_especialidad === id_especialidad)

                        {
                            $('#especialidad').append('<option value=' + item.id_especialidad + ' selected>' + item.descripcion + '</option>');
                            $('#r_especialidad').append('<option value=' + item.id_especialidad + ' selected>' + item.descripcion + '</option>');
                        } else {
                            $('#especialidad').append('<option value=' + item.id_especialidad + '>' + item.descripcion + '</option>');
                            $('#r_especialidad').append('<option value=' + item.id_especialidad + '>' + item.descripcion + '</option>');
                        }
                    });
                }
            }
        },
        error: function(xhr, status, errorThrown) {
            alert(xhr.status);
            alert(errorThrown);
        }
    });
}


function llenar_combo_MedicoReferido_edicion(e, id_especialidad = null, id_medico = null) {

    e.preventDefault;

    url = '/medicos_activos_por_especialidad/' + id_especialidad;
    $.ajax({
        url: url,
        method: 'GET',
        //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
        dataType: 'JSON',
        beforeSend: function(data) {},
        success: function(data) {

            if (data.length >= 1) {
                $('#r_medico').empty();
                $('#r_medico').append('<option value=0  selected disabled>Medicos</option>');
                verificar = 7;
                if (id_medico === undefined) {
                    $.each(data, function(i, item) {
                        $('#r_medico').append('<option value=' + item.id + '>' + item.nombre + '  ' + item.apellido + '</option>');
                    });
                } else {
                    $.each(data, function(i, item) {
                        if (item.id == id_medico) {
                            $('#r_medico').append('<option value=' + item.id + ' selected>' + item.nombre + '  ' + item.apellido + '</option>');
                        } else {
                            $('#r_medico').append('<option value=' + item.id + '>' + item.nombre + '  ' + item.apellido + '</option>');
                        }
                    });
                }

            }
        },
        error: function(xhr, status, errorThrown) {
            alert(xhr.status);
            alert(errorThrown);
        }
    });
}
/// este es para chando ha el change
function llenar_combo_MedicoReferido_edicion_2(e, id_especialidad = null, id_medico = null) {

    e.preventDefault;

    url = '/medicos_activos_por_especialidad/' + id_especialidad;
    $.ajax({
        url: url,
        method: 'GET',
        //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
        dataType: 'JSON',
        beforeSend: function(data) {},
        success: function(data) {

            if (data.length >= 1) {

                $('#r_medico').empty();
                $('#r_medico').append('<option value=0  selected disabled>Medicos</option>');
                verificar = 7;
                if (id_medico === undefined) {

                    $.each(data, function(i, item) {
                        //console.log(data)    
                        $('#r_medico').append('<option value=' + item.id + '>' + item.nombre + '  ' + item.apellido + '</option>');
                    });
                } else {

                    data = data.filter(dato => dato.id_especialidad == id_especialidad);
                    //console.log(buscar);
                    $.each(data, function(i, item) {

                        $('#r_medico').append('<option value=' + item.id + ' selected>' + item.nombre + '  ' + item.apellido + '</option>');

                    });


                }
            }
        },

        error: function(xhr, status, errorThrown) {
            alert(xhr.status);
            alert(errorThrown);
        }
    });
}


function llenar_combo_MedicoReferido(e, id_especialidad) {

    e.preventDefault;

    url = '/listar_medicos_activos';
    $.ajax({
        url: url,
        method: 'GET',
        //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
        dataType: 'JSON',
        beforeSend: function(data) {},
        success: function(data) {

            if (data.length >= 1) {
                $('#medico').empty();
                $('#medico').append('<option value=0  selected disabled>Medicos</option>');
                $('#r_medico').empty();
                $('#r_medico').append('<option value=0  selected disabled>Medicos</option>');
                verificar = 7;
                if (id_especialidad === undefined) {

                    $.each(data, function(i, item) {
                        //console.log(data)
                        $('#medico').append('<option value=' + item.id + '>' + item.nombre + '  ' + item.apellido + '</option>');
                        $('#r_medico').append('<option value=' + item.id + '>' + item.nombre + '  ' + item.apellido + '</option>');

                    });
                } else {
                    data = data.filter(dato => dato.id_especialidad == id_especialidad);
                    //console.log(buscar);
                    $.each(data, function(i, item) {
                        $('#medico').append('<option value=' + item.id + '>' + item.nombre + '  ' + item.apellido + '</option>');
                        $('#r_medico').append('<option value=' + item.id + '>' + item.nombre + '  ' + item.apellido + '</option>');


                    });
                }
            }
        },
        error: function(xhr, status, errorThrown) {
            alert(xhr.status);
            alert(errorThrown);
        }
    });
}


///*****************************************/ APARTADO DE REPOSOS********************************

$(document).on('click', '#btnMostrar_reposo', function(e)

    {
        // $('#formulario')[0].reset();
        $("#busqueda").show();
        $('#cedulatitular').val('');
        $('#motivo_reposo').val('');
        $('#r_medico').val('0');
        $('#r_especialidad').val('0');
        $("#modal_reposos").modal("show");
        $("#btnActualizar_reposos").hide();
        $("#btnagregar_reposos").show();
        $(".observacion").val("");
        let nombre = $("#nombrepellidoT").val();
        let fecha_nacimiento = $("#fecha_nacimiento").val();
        let edad = $("#edad").val();
        let departamento = $("#departamento").val();
        let telefono = $("#telefono").val();
        let cedulaT = $("#cedulaT").val();
        let numeroHistorial = $("#numeroHistorial").val();
        $('#nombretitular').val(nombre);
        $('#fecha_nacimientotitular').val(fecha_nacimiento);
        $('#cedulatitular').val(cedulaT);
        $('#edadtitular').val(edad);
        $('#unidadtitular').val(departamento);
        $('#telefonotitular').val(telefono);
        $('#n_historialT').val(numeroHistorial);
        $('#r_desde').val('');
        $('#r_hasta').val('');
        $('#tipo_reposo').val(1);
        let tipo_reposo = $('#tipo_reposo').val();
        document.getElementById("btnagregar_reposos").disabled = true;



    });


$("#r_desde").on('change', function() {

    document.getElementById('r_hasta').disabled = false;

    fechaDesde_normal = $("#r_desde").val();
    fechaHasta_normal = $("#r_hasta").val();

    if (fechaDesde_normal == fechaHasta_normal) {
        alert('Error! Debe seleccionar el Tipo de Reposo en Horas ')
    } else {
        $("#dia_en_horas").val(0);
    }
});


$("#r_hasta").on('change', function() {
    fechaDesde_normal = $("#r_desde").val();
    fechaHasta_normal = $("#r_hasta").val();
});




$("#tipo_reposo").on('change', function() {
    let tipo_reposo = $("#tipo_reposo").val();
    if (tipo_reposo == 2) {

 
        $('#r_desde').val('');
        $('#r_hasta').val('');
        $("#label_inicio").hide();
        $("#r_desde").hide();
        $("#label_retorno").hide();
        $("#r_hasta").hide();
        $("#labe_dias").hide();
        $("#dias").hide();
        $("#dias").val('0');
        $("#horas").val('0');
        document.getElementById("r_desde").disabled = true;
        $("#labe_horas").show();
        $("#horas").show();
        let horas = $("#dia_en_horas").val();
        $("#dia_en_horas").val(horas);
        $("#dia_en_horas").hide();
    } else if (tipo_reposo == 1) {
        $("#dias").val('0');
        $("#horas").val('0');
        $('#r_desde').val('');
        $('#r_hasta').val('');
        $("#label_inicio").show();
        $("#r_desde").show();
        $("#label_retorno").show();
        $("#r_hasta").show();
        document.getElementById("r_desde").disabled = false;
        $("#labe_horas").hide();
        $("#horas").hide();
        $("#labe_dias").show();
        $("#dias").show();
    }

});

$("#horas").blur(function() {
    var valor = $(this).val();
    if (valor > 8) {
        alert('LAS HORAS DEBEN SER IGUAL O MENOR A 8')
    } else {
        $("#dia_en_horas").val(valor);
    }

});


/**********ESTE METODO AGREGA LOS DATOS DEL REPOSOS***** */
$(document).on('click', '#btnagregar_reposos', function(e) {
    let nombre = $("#nombretitular").val();
    var now = new Date();
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var today = day + "-" + month + "-" + now.getFullYear();
    var t_n_historial = $('#n_historialT').val();
    var t_id_medico = $('#medico_id').val();
    let motivo = $('#motivo_reposo').val();
    let accion = 'GENERO UN REPOSO PARA ' + ' ' + nombre;
    var r_especialidad = $('#r_especialidad').val();
    var r_especialidad = $('#r_medico').val();
    var r_desde = $('#r_desde').val();
    var r_hasta = $('#r_hasta').val();
    let fecha_formateada = now.getFullYear() + "-" + month + "-" + day;
    let fecha_desde = fecha_formateada;
    let fecha_hasta = fecha_formateada;
    let tipo_reposo = $('#tipo_reposo').val();
    function sumarUnDia(fecha) 
    {
        const [day, month, year] = fecha.split('-');
        const fechaObj = new Date(year, month - 1, day);
        fechaObj.setDate(fechaObj.getDate() + 1);
        const nuevoDia = `${fechaObj.getFullYear()}-${("0" + (fechaObj.getMonth() + 1)).slice(-2)}-${("0" + fechaObj.getDate()).slice(-2)}`;
        return nuevoDia;
    }
    const dia_siguiente = sumarUnDia(today);    
   if (tipo_reposo == 1) // VALIDACION PARA DIAS 
    {
       
        let horas = $('#horas').val();
        fecha_desde_normal = $("#r_desde").val();
        fecha_hasta_normal = $("#r_hasta").val();
        if (fecha_desde_normal == '' && fecha_hasta_normal != '') {
            alert('POR FAVOR INDICAR LA FECHA DE INICIO');
        } else if (fecha_hasta_normal == '' && fecha_desde_normal == '') {
            alert('POR FAVOR INGRESAR LA FECHA DE INICIO Y RETORNO');
        } else if (fecha_hasta_normal == '' && fecha_desde_normal != '') {
            alert('POR FAVOR INDICAR LA FECHA DE RETORNO');
        } 
        else if (r_desde>=dia_siguiente) 
        {
             alert('LA FECHA DE INICIO DEL REPOSO NO PUEDE SER MAYOR AL DIA DE HOY ');  
        } 
        
        else if (fecha_hasta_normal < fecha_desde_normal) {
            alert('ERRO! LA FECHA DE INICIO ES MAYOR A LA FECHA DE RETORNO');
        } else if (fecha_desde_normal == fecha_hasta_normal) {
            alert('Error! Debe seleccionar el Tipo de Reposo en Horas ')
        } else if (motivo.trim() === '') {
            alert('NO DEBE DEJAR EL MOTIVO DEL REPOSO  VACIO');
        } else {

            //CALCULO PARA OBTENER LA CANTIDAD DE HORAS ENTRE LOS CALENDARIOS
            fecha_desde = new Date($("#r_desde").val());
            fecha_hasta = new Date($("#r_hasta").val());
            var diff = fecha_hasta.valueOf() - fecha_desde.valueOf();
            var diffInHours = diff / 1000 / 60 / 60; // Convert milliseconds to hours
            $("#dia_en_horas").val(diffInHours);
            let horas_d = $("#dia_en_horas").val();
            let hora_atraso = "24";
            hora_atraso = parseInt(hora_atraso);
            horas_d = parseInt(horas_d);
            hora_atraso += horas_d;
            //CALCULO PARA OBTENER LA CANTIDAD DE DIAS EN FUNCION DE LAS HORAS
            var horast = hora_atraso;
            var dias = horast / 24;
            $("#dias").val(dias);
            var data = {
                    t_n_historial: t_n_historial,
                    // t_id_consulta:t_id_consulta,
                    t_id_medico: t_id_medico,
                    fecha_desde: fecha_desde_normal,
                    fecha_hasta: fecha_hasta_normal,
                    motivo: motivo,
                    accion: accion,
                    horas_d: hora_atraso,
                }
             // variables para regresar al la pagina central 
            var url = '/Agregar_Reposo';
            $.ajax({
                url: url,
                method: 'POST',
                data: { data: btoa(unescape(encodeURIComponent(JSON.stringify(data)))) },
                dataType: 'JSON',
                beforeSend: function(data) {
                    //alert('Procesando Información ...');
                },
                success: function(data) {

                    if (data == '1') {
                        alert('Registro Exitoso');
                    } else {
                        alert('Error en la Incorporación del registro');
                    }
                    window.location = '/medico_reposo'

                },
                error: function(xhr, status, errorThrown) {
                    alert(xhr.status);
                    alert(errorThrown);
                }
            });
        }
    } else if (tipo_reposo == 2) // VALIDACION PARA HORAS
    {
        let horas = $('#horas').val();
        if (horas == ' ' || horas == 0) {
            alert('DEBE INTRODUCIR LAS HORAS DEL REPOSO')
        } else {
            let horas_d = $('#dia_en_horas').val();
            if (motivo.trim() === '') {
                alert('NO DEBE DEJAR EL MOTIVO DEL REPOSO  VACIO');
            } else {
                var data = {
                    t_n_historial: t_n_historial,
                    // t_id_consulta:t_id_consulta,
                    t_id_medico: t_id_medico,
                    fecha_desde: today,
                    fecha_hasta: today,
                    motivo: motivo,
                    accion: accion,
                    horas_d: horas,
                }
                var url = '/Agregar_Reposo';
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: { data: btoa(unescape(encodeURIComponent(JSON.stringify(data)))) },
                    dataType: 'JSON',
                    beforeSend: function(data) {
                        //alert('Procesando Información ...');
                    },
                    success: function(data) {
                        if (data == '1') {
                            alert('Registro Exitoso');
                        } else {
                            alert('Error en la Incorporación del registro');
                        }
                        window.location = '/medico_reposo'

                    },
                    error: function(xhr, status, errorThrown) {
                        alert(xhr.status);
                        alert(errorThrown);
                    }
                });
            }
        }

   }
});



$('#lista_reposos').on('click', '.Editar', function(e) {
    e.preventDefault();
   
    $("#btnagregar_reposos").hide();
    $("#btnActualizar_reposos").show();
    let cedula_beneficiario = $(this).attr('cedula_trabajador');
    let fecha_hasta = $(this).attr('fecha_hasta');
    let fechaconvertida_hasta = moment(fecha_hasta, 'DD-MM-YYYY');
    fechaconvertida_hasta = fechaconvertida_hasta.format('YYYY-MM-DD');
    $('#hasta').val(fechaconvertida_hasta);
    let fecha_desde = $(this).attr('fecha_desde');
    let fechaconvertida_desde = moment(fecha_desde, 'DD-MM-YYYY');
    fechaconvertida_desde = fechaconvertida_desde.format('YYYY-MM-DD');
    $('#desde').val(fechaconvertida_desde);
    let motivo = $(this).attr('motivo');
    let id_medico = $(this).attr('id_medico');
    let id_especialidad = $(this).attr('id_especialidad');
    let id_reposo = $(this).attr('id_reposo');
    let horas_d = $(this).attr('horas');

    if (horas_d <= 8) {
        $('#tipo_reposo').val(2);
        $("#labe_dias").hide();
        $("#dias").hide();
        $("#r_desde").hide();
        $("#r_hasta").hide();
        $("#label_inicio").hide();
        $("#label_retorno").hide();
        $("#labe_horas").show();
        $("#dia_en_horas").val(horas_d);
        //$("#dia_en_horas").show();
        $("#horas").val(horas_d);
        $("#horas").show();

        document.getElementById("dia_en_horas").disabled = false;

    } else {
        $("#r_desde").show();
        $("#r_hasta").show();
        $("#label_inicio").show();
        $("#label_retorno").show();
        $('#tipo_reposo').val(1);
        $("#labe_horas").hide();
        $("#dia_en_horas").hide();
        $("#labe_dias").show();
        $("#dias").show();
        // //CALCULO PARA OBTENER LA CANTIDAD DE DIAS EN FUNCION DE LAS HORAS
        var horast = horas_d;
        var dias = horast / 24;
        $("#dias").val(dias);
    }
    let tipo_beneficiario = $(this).attr('tipo_beneficiario');


    data = {
        cedula_beneficiario: cedula_beneficiario,
    };

    if (tipo_beneficiario == 'T') {
        url = '/buscartitular_NotasEntregas';
        $.ajax({
            url: url,
            method: 'GET',
            data: { data: btoa(unescape(encodeURIComponent(JSON.stringify(data)))) },
            dataType: 'JSON',
            beforeSend: function(data) {
                //alert('Procesando');
            },
            success: function(data) {
                if (data.borrado == 't') {
                    alert('El BENEFICIARIO SE ENCUENTRA INACTIVO');
                } else {
                   // document.getElementById("r_medico").disabled = false;
                    document.getElementById("r_hasta").disabled = false;
                    llenar_combo_especialidad(Event, id_especialidad)
                    llenar_combo_MedicoReferido_edicion(Event, id_especialidad, id_medico)
                    $("#busqueda").hide();
                    $("#modal_reposos").modal("show");
                    $('#modal_reposos').find('#nombretitular').val(data.nombre + ' ' + ' ' + data.apellido);
                    let fecha_nac = data.fecha_nacimiento;
                    let fecha_convertida = moment(fecha_nac, 'YYYY-MM-DD');
                    fecha_convertida = fecha_convertida.format('DD-MM-YYYY');
                    $('#modal_reposos').find('#fecha_nacimientotitular').val(fecha_convertida);
                    $('#modal_reposos').find('#edadtitular').val(data.edad_actual);
                    $('#modal_reposos').find('#unidadtitular').val(data.ubicacion_administrativa);
                    $('#modal_reposos').find('#telefonotitular').val(data.telefono);
                    $('#modal_reposos').find('#n_historialT').val(data.n_historial);
                    $('#modal_reposos').find('#r_desde').val(fechaconvertida_desde);
                    $('#modal_reposos').find('#r_hasta').val(fechaconvertida_hasta);
                    $('#modal_reposos').find('#motivo_reposo').val(motivo);
                    $('#modal_reposos').find('#id_reposo').val(id_reposo);
                    $('#modal_reposos').find('#dia_en_horas').val(horas_d);
                }
            },
            error: function(xhr, status, errorThrown) {
                alert('NO SE ENCUENTRA LA CEDULA DEL BENEFICIARIO');
            }
        });
    } else if (tipo_beneficiario == 'F') {
        url = '/buscarfamiliar_NotasEntregas';
        $.ajax({
            url: url,
            method: 'GET',
            data: { data: btoa(unescape(encodeURIComponent(JSON.stringify(data)))) },
            dataType: 'JSON',
            beforeSend: function(data) {
                //alert('Procesando');
            },
            success: function(data) {
                if (data.borrado == 't') {
                    alert('El BENEFICIARIO SE ENCUENTRA INACTIVO');
                } else {
                    //document.getElementById("r_medico").disabled = false;
                    document.getElementById("r_hasta").disabled = false;
                    llenar_combo_especialidad(Event, id_especialidad)
                    llenar_combo_MedicoReferido_edicion(Event, id_especialidad, id_medico)
                    $("#busqueda").hide();
                    $("#modal_reposos").modal("show");
                    $('#modal_reposos').find('#nombretitular').val(data.nombre + ' ' + ' ' + data.apellido);
                    let fecha_nac = data.fecha_nacimiento;
                    let fecha_convertida = moment(fecha_nac, 'YYYY-MM-DD');
                    fecha_convertida = fecha_convertida.format('DD-MM-YYYY');
                    $('#modal_reposos').find('#fecha_nacimientotitular').val(fecha_convertida);
                    $('#modal_reposos').find('#edadtitular').val(data.edad_actual);
                    $('#modal_reposos').find('#unidadtitular').val('FAMILIAR');
                    $('#modal_reposos').find('#telefonotitular').val(data.telefono);
                    $('#modal_reposos').find('#n_historialT').val(data.n_historial);
                    $('#modal_reposos').find('#r_desde').val(fechaconvertida_desde);
                    $('#modal_reposos').find('#r_hasta').val(fechaconvertida_hasta);
                    $('#modal_reposos').find('#motivo_reposo').val(motivo);
                    $('#modal_reposos').find('#id_reposo').val(id_reposo);
                    $('#modal_reposos').find('#dia_en_horas').val(horas_d);
                }
            },
            error: function(xhr, status, errorThrown) {
                alert('NO SE ENCUENTRA LA CEDULA DEL BENEFICIARIO');
            }
        });
    } else {
        url = '/buscarcortesia_NotasEntregas';
        $.ajax({
            url: url,
            method: 'GET',
            data: { data: btoa(unescape(encodeURIComponent(JSON.stringify(data)))) },
            dataType: 'JSON',
            beforeSend: function(data) {
                //alert('Procesando');
            },
            success: function(data) {
                if (data.borrado == 't') {
                    alert('El BENEFICIARIO SE ENCUENTRA INACTIVO');
                } else {
                    //document.getElementById("r_medico").disabled = false;
                    document.getElementById("r_hasta").disabled = false;
                    llenar_combo_especialidad(Event, id_especialidad)
                    llenar_combo_MedicoReferido_edicion(Event, id_especialidad, id_medico)
                    $("#busqueda").hide();
                    $("#modal_reposos").modal("show");
                    $('#modal_reposos').find('#nombretitular').val(data.nombre + ' ' + ' ' + data.apellido);
                    let fecha_nac = data.fecha_nacimiento;
                    let fecha_convertida = moment(fecha_nac, 'YYYY-MM-DD');
                    fecha_convertida = fecha_convertida.format('DD-MM-YYYY');
                    $('#modal_reposos').find('#fecha_nacimientotitular').val(fecha_convertida);
                    $('#modal_reposos').find('#edadtitular').val(data.edad_actual);
                    $('#modal_reposos').find('#unidadtitular').val('FAMILIAR');
                    $('#modal_reposos').find('#telefonotitular').val(data.telefono);
                    $('#modal_reposos').find('#n_historialT').val(data.n_historial);
                    $('#modal_reposos').find('#r_desde').val(fechaconvertida_desde);
                    $('#modal_reposos').find('#r_hasta').val(fechaconvertida_hasta);
                    $('#modal_reposos').find('#motivo_reposo').val(motivo);
                    $('#modal_reposos').find('#id_reposo').val(id_reposo);
                    $('#modal_reposos').find('#dia_en_horas').val(horas_d);
                }
            },
            error: function(xhr, status, errorThrown) {
                alert('NO SE ENCUENTRA LA CEDULA DEL BENEFICIARIO');
            }
        });
    }


});


$(document).on('click', '#btnActualizar_reposos', function(e) {
    e.preventDefault();
    let tipo_reposo = $("#tipo_reposo").val();

   

    var now = new Date();
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var today = day + "-" + month + "-" + now.getFullYear();
    function sumarUnDia(fecha) {
        const [day, month, year] = fecha.split('-');
        const fechaObj = new Date(year, month - 1, day);
        fechaObj.setDate(fechaObj.getDate() + 1);
        const nuevoDia = `${fechaObj.getFullYear()}-${("0" + (fechaObj.getMonth() + 1)).slice(-2)}-${("0" + fechaObj.getDate()).slice(-2)}`;
        return nuevoDia;
      }
      
    
      const dia_siguiente = sumarUnDia(today);
    // Imprime la fecha con un día más
    var t_n_historial = $('#n_historialT').val();
    var t_id_medico = $('#medico_id').val();
    let fecha_desde = $('#r_desde').val();
    let fecha_hasta = $('#r_hasta').val();
    let motivo = $('#motivo_reposo').val();
    let horas_d = $('#dia_en_horas').val();
    let id_reposo = $('#id_reposo').val();
    let nombre = $("#nombretitular").val();
    let accion = 'EDITO UN REPOSO PARA ' + ' ' + nombre;
    if (fecha_desde>dia_siguiente) {
        alert('LA FECHA DE INICIO DEL REPOSO NO PUEDE SER MAYOR AL DIA DE HOY ');  
    } 
     else {

        if (tipo_reposo == 1) // VALIDACION PARA DIAS 
        {
            let horas = $('#horas').val();
            fecha_desde_normal = $("#r_desde").val();
            fecha_hasta_normal = $("#r_hasta").val();
            if (fecha_desde_normal == '' && fecha_hasta_normal != '') {
                alert('POR FAVOR INDICAR LA FECHA DE INICIO');
            } else if (fecha_hasta_normal == '' && fecha_desde_normal == '') {
                alert('POR FAVOR INGRESAR LA FECHA DE INICIO Y RETORNO');
            } else if (fecha_hasta_normal == '' && fecha_desde_normal != '') {
                alert('POR FAVOR INDICAR LA FECHA DE RETORNO');
            } else if (fecha_hasta_normal < fecha_desde_normal) {
                alert('ERRO! LA FECHA DE INICIO ES MAYOR A LA FECHA DE RETORNO');
            } else if (fecha_desde_normal == fecha_hasta_normal) {
                alert('Error! Debe seleccionar el Tipo de Reposo en Horas ')
            } else if (motivo.trim() === '') {
                alert('NO DEBE DEJAR EL MOTIVO DEL REPOSO  VACIO');
            } else {
                //CALCULO PARA OBTENER LA CANTIDAD DE HORAS ENTRE LOS CALENDARIOS
                fecha_desde = new Date($("#r_desde").val());
                fecha_hasta = new Date($("#r_hasta").val());
                var diff = fecha_hasta.valueOf() - fecha_desde.valueOf();
                var diffInHours = diff / 1000 / 60 / 60; // Convert milliseconds to hours
                $("#dia_en_horas").val(diffInHours);
                let horas_d = $("#dia_en_horas").val();
                let hora_atraso = "24";
                hora_atraso = parseInt(hora_atraso);
                horas_d = parseInt(horas_d);
                hora_atraso += horas_d;
                //CALCULO PARA OBTENER LA CANTIDAD DE DIAS EN FUNCION DE LAS HORAS
                var horast = hora_atraso;
                var dias = horast / 24;
                $("#dias").val(dias);
                var data = {
                        id_reposo: id_reposo,
                        t_n_historial: t_n_historial,
                        // t_id_consulta:t_id_consulta,
                        t_id_medico: t_id_medico,
                        fecha_desde: fecha_desde_normal,
                        fecha_hasta: fecha_hasta_normal,
                        motivo: motivo,
                        accion: accion,
                        horas_d: hora_atraso,
                    }
                    // variables para regresar al la pagina central 

                var url = '/Actualizar_Reposo';
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: { data: btoa(unescape(encodeURIComponent(JSON.stringify(data)))) },
                    dataType: 'text',
                    beforeSend: function(data) {
                        //alert('Procesando Información ...');
                    },
                    success: function(data) {

                        if (data == '1') {
                            alert('Registro Exitoso');
                        } else {
                            alert('Error en la Incorporación del registro');
                        }
                        window.location = '/medico_reposo'

                    },
                    error: function(xhr, status, errorThrown) {
                        alert(xhr.status);
                        alert(errorThrown);
                    }
                });
            }
        } else if (tipo_reposo == 2) // VALIDACION PARA HORAS
        {
           
            const fechaActual = new Date();
            const año = fechaActual.getFullYear();
            const mes = fechaActual.getMonth() + 1; // Enero es 0, asi que le sumamos 1
            const dia = fechaActual.getDate();
            const fecha_today = `${año}-${mes.toString().padStart(2, '0')}-${dia.toString().padStart(2, '0')}`;

            let horas = $('#horas').val();
            if (horas == ' ' || horas == 0) {
                alert('DEBE INTRODUCIR LAS HORAS DEL REPOSO')
            } else {
                let horas_d = $('#dia_en_horas').val();
                if (motivo.trim() === '') {
                    alert('NO DEBE DEJAR EL MOTIVO DEL REPOSO  VACIO');
                } else {
                    var data = {
                        id_reposo: id_reposo,
                        t_n_historial: t_n_historial,
                        // t_id_consulta:t_id_consulta,
                        t_id_medico: t_id_medico,
                        fecha_desde: fecha_today,
                        fecha_hasta: fecha_today,
                        motivo: motivo,
                        accion: accion,
                        horas_d: horas,
                    }
               
               
                    var url = '/Actualizar_Reposo';
                    $.ajax({
                        url: url,
                        method: 'POST',
                        data: { data: btoa(unescape(encodeURIComponent(JSON.stringify(data)))) },
                        dataType: 'JSON',
                        beforeSend: function(data) {
                            //alert('Procesando Información ...');
                        },
                        success: function(data) {
                            if (data == '1') {
                                alert('Registro Exitoso');
                            } else {
                                alert('Error en la Incorporación del registro');
                            }
                            window.location = '/medico_reposo'

                        },
                        error: function(xhr, status, errorThrown) {
                            alert(xhr.status);
                            alert(errorThrown);
                        }
                    });
                }
            }
        }

   }


});





$(document).on('click', '#btnBuscar', function(e) {
    e.preventDefault('');

    var cedula_beneficiario = $('#cedulatitular').val();
    if (cedula_beneficiario.trim() == '') {
        alert('Debe Introducir la Cedula');
    } else {
        data = {
            cedula_beneficiario: cedula_beneficiario
        };
        url = '/buscartitular_NotasEntregas';
        $.ajax({
            url: url,
            method: 'GET',
            data: { data: btoa(unescape(encodeURIComponent(JSON.stringify(data)))) },
            dataType: 'JSON',
            beforeSend: function(data) {
                //alert('Procesando');
            },
            success: function(data) {


                if (data.borrado == 't') {
                    alert('El TITULAR SE ENCUENTRA INACTIVO');
                } else {

                    llenar_combo_especialidad(Event)


                    $('#modal_reposos').find('#nombretitular').val(data.nombre + ' ' + ' ' + data.apellido);
                    let fecha_nac = data.fecha_nacimiento;
                    let fecha_convertida = moment(fecha_nac, 'YYYY-MM-DD');
                    fecha_convertida = fecha_convertida.format('DD-MM-YYYY');
                    $('#modal_reposos').find('#fecha_nacimientotitular').val(fecha_convertida);
                    $('#modal_reposos').find('#edadtitular').val(data.edad_actual);

                    $('#modal_reposos').find('#unidadtitular').val(data.ubicacion_administrativa);
                    $('#modal_reposos').find('#telefonotitular').val(data.telefono);
                    $('#modal_reposos').find('#n_historialT').val(data.n_historial);
                    document.getElementById("btnagregar_reposos").disabled = false;
                }
            },
            error: function(xhr, status, errorThrown) {
                alert('NO SE ENCUENTRA LA CEDULA DEL BENEFICIARIO');
            }
        });
    }


});
$("#r_medico").on('change', function(e) {
    document.getElementById("motivo_reposo").disabled = false;
    document.getElementById("btnagregar_reposos").disabled = false;

});



$(document).on('click', '#btnCerrar', function(e) {
    $("#tipo_reposo").val(0);


    $("#horas").hide();
    $("#dias").hide();



})