/*
 *Este es el document ready
 */
 $(function()
 {
    //Este es el código que se ejecuta cuando el documento está listo
    let desde          =$('#desde').val();
    let hasta          =$('#hasta').val();
    let id_medico         =$('#cmbmedicos').val();
    let cedula_trabajador  =$('#cedula_cortesia').val(); 
    if (desde==''&& hasta=='')
    {
    
        desde='null'
        hasta='null'
         
    } 

    
    listar_NotasEntregas(desde,hasta);
    $('#formulario')[0].reset();
   
    
 });

 function  listar_NotasEntregas(desde,hasta)
 {
   
  
    var now = new Date();
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var today= day+"/"+month+"/"+now.getFullYear(); 
    let encabezado='';
    data=new Date(desde);
    let dataFormatada_desde = ((data.getDate() + 1 )) + "/" + ((data.getMonth() + 1)) + "/" + data.getFullYear(); 
    data=new Date(hasta);
    let dataFormatada_hasta = ((data.getDate() + 1 )) + "/" + ((data.getMonth() + 1)) + "/" + data.getFullYear();     
    if(dataFormatada_desde!='NaN/NaN/NaN' && dataFormatada_hasta!='NaN/NaN/NaN') 
        {
            encabezado= encabezado +'FECHA'+' '+ 'DESDE:'+' '+dataFormatada_desde+' '+' '+'HASTA'+' '+' '+dataFormatada_hasta+' '+' ';
        }
     


     let ruta_imagen =rootpath
     var table = $('#table_notas_entregas').DataTable( {
        responsive :true,
         dom: "Bfrtip",
         buttons:{
             dom: {
                 button: {
                     className: 'btn-xs-xs'
                 },
                 
             },
             
             buttons: [
             {
                 //definimos estilos del boton de pdf
                 extend: "pdf",
                 text:'PDF',
                 className:'btn-xs btn-dark',
                 title:'NOTAS DE ENTREGAS',
                 download: 'open',
                 exportOptions: {
                             columns: [ 0,1,2,3,4],
                             
                 },   
                 alignment: 'center',                 
                 customize:function(doc)
                      {                       
                      //Remove the title created by datatTables
                           doc.content.splice(0,1);
                         
                           doc.styles.title= 
                           { 
                                color: '#4c8aa0',
                                fontSize: '18',
                                alignment: 'center'                     
                           }
                           doc.styles['td:nth-child(2)'] = 
                           {
                                width: '100px',
                                'max-width': '100px'
                           },
                           doc.styles.tableHeader = 
                           {
                                fillColor:'#4c8aa0',
                                color:'white',
                                alignment:'center'
                           }, 
                 // Create a header
                 doc.pageMargins = [48,85,0,30];
                 doc['header'] = (function (page, pages)
                 {                      
                      doc.styles.title = 
                      {    
                           color: '#4c8aa0',
                           fontSize: '18',
                           alignment: 'center',  
                      } 
                      return {  
                                columns:
                                [
                                     {                              
                                          margin: [ 40, 3, 40, 40 ],                                 
                                          image:ruta_imagen,
                                          width: 500,  
                                                                   
                                     },                        
                                     {
                                          margin :[ -470, 40, -25, 0 ],
                                          color: '#4c8aa0',
                                          fontSize: '18',
                                          alignment: 'center' , 
                                          text: 'NOTAS DE ENTREGAS',
                                          fontSize: 18,                             
                                     },
                                     {
                                     margin :[ -500, 70, -25, 0 ],     
                                     text:encabezado,
                                     },
                                ], 
                           }                               
                  }); 
                 // Create a footer
                 doc['footer'] = (function (page, pages)
                 {    
                      return {
                                columns:
                                [
                                     {
                                          alignment: 'center',
                                          text: ['pagina ', { text: page.toString() },	' of ',	{ text: pages.toString() }]
                                     }
                                ],               
                                }                            
                      });
        
                      },
                 },

             {    
                  //definimos estilos del boton de excel
                  extend: "excel",
                  text:'Excel',
                  className:'btn-xs btn-dark',
                  title:'NOTAS DE ENTREGAS', 
                 
                  download: 'open',
                  exportOptions: {
                              columns: [ 0,1,2,3,4]
                  },
                  excelStyles: {                                                
                    "template": [
                        "blue_medium",
                        "header_blue",
                        "title_medium"
                    ]                                  
                },

             }
             ]            
         }, 
        

         "order":[[0,"desc"]],					
         "paging": true,
         "lengthChange": true,
        
         dom: 'Blfrtip',
         "searching": true,
         "lengthMenu": [
              [ 10, 25, 50, -1 ],
              [ '10', '25', '50', 'Todos' ]
         ], 
        
         "ordering": true,
         "info": true,
         "autoWidth": true,
         //"dom": 'Bfrt<"col-md-6 inline"i> <"col-md-6 inline"p>',
         "ajax":
         {
           
            "url":base_url+"/listar_notas_entregas/"+desde+'/'+hasta,
          "type":"GET",
          dataSrc:''
         },
         "columns":
         [
             
            {data:'id'},
            {data:'n_historial'},
            {data:'nombre'},
           // {data:'ced_titular'},
            {data:'fecha_registro'},                
            {data:'fecha_creacion'},   
            {data:'usuario'},    
           // {data:'fecha'}, 

       

             {  
                data :null,
                orderable: true,
              render:function(data, type, row)
              {
                 return '<a href="javascript:;" class="btn btn-xs btn-success  Notas" style="font-size:1px" data-toggle="tooltip" title="Notas de Entrega" nota_entrega_id='+row.id+' id_autorizador='+row.id_autorizador+' tipobeneficiario='+row.tipobeneficiario+' fecha_registro='+row.fecha_registro+' cedula_beneficiario='+row.cedula+'>   <i class="material-icons  " > print</i></a>'+' '+' '+'<a href="javascript:;" class="btn btn-xs btn-secondary  Bloquear" style="font-size:1px" data-toggle="tooltip" title="Reversar"  nota_entrega_id='+row.id+' tipobeneficiario='+row.tipobeneficiario+' fecha_registro='+row.fecha_registro+' cedula_beneficiario='+row.cedula+'>   <i class="material-icons  " > delete_forever</i></a>'
                 }
             }
            
           
         ],
         
   "language":
       {
           "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
              {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
              },
          ]
       
    },
});
 }
  



  $(document).on('click','#btnfiltrar ', function(e)
  {
      e.preventDefault(); 
      var desde           =$('#desde').val();
      var hasta          =$('#hasta').val();
      if (desde=='') 
      {
          desde='null' 
      }
       if (hasta=='') 
      {
          hasta='null' 
      }
      $("#table_notas_entregas").dataTable().fnDestroy();
      listar_NotasEntregas(desde,hasta); 
      
      if (desde=='null'&&hasta!='null') 
      {
          alert('DEDE INDICAR EL CAMPO DESDE');
          
      }
      else if (hasta=='null'&&desde!='null') 
      {
          alert('DEDE INDICAR EL CAMPO HASTA');
      } 
      else if (hasta<desde) {
        alert('EL CAMPO DESDE ES MAYOR AL CAMPO HASTA')
      }
      
  });
  $(document).on('click','#btnlimpiar', function(e)
  {
    e.preventDefault();
    $('input[type="date"]').val('');
    let desde          =$('#desde').val();
    let hasta          =$('#hasta').val(); 
    if (desde==''&&hasta=='')
    {  
        desde='null'
        hasta='null'
         
    }  
      $("#table_notas_entregas").dataTable().fnDestroy();
      listar_NotasEntregas(desde,hasta);
        
  });


 /*
  * Función para definir datatable:
  */

 $('#btnAgregar').on('click',function(e)
 { 
   
    
     
      $('#modal').find('.labelcedulabeneficiario').hide();
      $('#modal').find('#cedula_beneficiario').hide();
      $('#modal').find('.btnbotones').hide();
      $("#modal").modal("show");
      $("#cedula_beneficiario").val('');
      document.getElementById("cedula_beneficiario").value = "";
      document.getElementById("btntitulares").disabled=true;
      document.getElementById("btnfamiliares").disabled=true;
      document.getElementById("btncortesia").disabled=true;
      document.getElementById("cedula_beneficiario").disabled=true;
      document.getElementById("btnPDF").disabled=true;
      var now = new Date();
     var day = ("0" + now.getDate()).slice(-2);
     var month = ("0" + (now.getMonth() + 1)).slice(-2);
     var today= day+"/"+month+"/"+now.getFullYear();
     $("#fecha").val(today);
    

 });


 $(document).on('click','#Radio1', function(e)
 {
     $('#formulario')[0].reset();
     var now = new Date();
     var day = ("0" + now.getDate()).slice(-2);
     var month = ("0" + (now.getMonth() + 1)).slice(-2);
     var today= day+"/"+month+"/"+now.getFullYear();
     $("#fecha").val(today);
     document.getElementById("cedula_beneficiario").disabled=false;
     document.getElementById("btntitulares").disabled=false;
     $('#modal').find('.labelcedulabeneficiario').show();
     $('#modal').find('#cedula_beneficiario').show();
     $('#modal').find('.btnbotones').show();
     $("#cedula_beneficiario").val('');
     $("#btntitulares").show();
     $("#btnfamiliares").hide();  
     $("#btncortesia").hide();  
     $("#tipobeneficiario").val('T');
    
 });

 $(document).on('click','#Radio2', function(e)
 {
     $('#formulario')[0].reset();
     var now = new Date();
     var day = ("0" + now.getDate()).slice(-2);
     var month = ("0" + (now.getMonth() + 1)).slice(-2);
     var today= day+"/"+month+"/"+now.getFullYear();
     $("#fecha").val(today);
     document.getElementById("cedula_beneficiario").disabled=false;
     document.getElementById("btnfamiliares").disabled=false;
     $('#modal').find('.labelcedulabeneficiario').show();
     $('#modal').find('#cedula_beneficiario').show();
     $('#modal').find('.btnbotones').show();
      // $("#beneficiarios").css("display", "none"); 
      $("#cedula_beneficiario").val('');
     // $("#verificar").show();  
      $("#btntitulares").hide();  
      $("#btnfamiliares").show(); 
      $("#btncortesia").hide();    
      $("#tipobeneficiario").val('F'); 
 });

 $(document).on('click','#Radio3', function(e)
 {
     $('#formulario')[0].reset();
     var now = new Date();
     var day = ("0" + now.getDate()).slice(-2);
     var month = ("0" + (now.getMonth() + 1)).slice(-2);
     var today= day+"/"+month+"/"+now.getFullYear();
     $("#fecha").val(today);
     document.getElementById("cedula_beneficiario").disabled=false;
     document.getElementById("btncortesia").disabled=false;
     $('#modal').find('.labelcedulabeneficiario').show();
     $('#modal').find('#cedula_beneficiario').show();
     $('#modal').find('.btnbotones').show();
    // $("#beneficiarios").css("display", "none"); 
     $("#cedula_beneficiario").val('');
     //$("#verificar").show();  
     $("#btncortesia").show();
     $("#btnfamiliares").hide();  
     $("#btntitulares").hide();  
     $("#tipobeneficiario").val('C'); 
 });







 $(document).on('click','#btntitulares', function(e)
 {
     var cedula_beneficiario = $('#cedula_beneficiario').val();
     url='/buscartitular_NotasEntregas';
     data=
     {
          cedula_beneficiario:cedula_beneficiario
     };

     if(cedula_beneficiario=='')
     {
         alert('POR FAVOR INGRESE UNA CEDULA');    
     }
     else
     {
 
       $.ajax(
            {
                 url:url,
                 method:'GET',
                data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
                dataType:'JSON',
                beforeSend:function(data)
                {
                     //alert('Procesando');
                },
                success:function(data)
                {
                    if(data.borrado=='t')
                    {
                        alert('El TITULAR SE ENCUENTRA INACTIVO');
                    }
               else
               {
                //console.log(data);
                   //alert(data);
                    $("#cajas2").css("display", "none"); 
                    document.getElementById("btnPDF").disabled=false;
                    //document.getElementById("fecha").disabled=false;
                    $('#modal').find('#nombretitular').val(data.nombre+' '+data.apellido); 
                    $('#modal').find('#cedulatitular').val(data.cedula_trabajador); 
                    $('#modal').find('#telefonotitular').val(data.telefono);
                    $('#modal').find('#fecha_nacimientotitular').val(data.fecha_nacimiento);                  
                    $('#modal').find('#edadtitular').val(data.edad_actual); 
                    $('#modal').find('#unidadtitular').val(data.ubicacion_administrativa); 
                    $('#modal').find('#telefonotitular').val(data.telefono); 
                    $('#modal').find('#n_historial').val(data.n_historial); 
                   
               }

                },
                error:function(xhr, status, errorThrown)
                {
                    alert('EL BENEFICIARIO NO POSEE HISTORIAL');
                }
           });
     }
 });

 $(document).on('click','#btnfamiliares', function(e)
 {
     var cedula_beneficiario = $('#cedula_beneficiario').val();
     url='/buscarfamiliar_NotasEntregas';
     data=
     {
          cedula_beneficiario:cedula_beneficiario
     };
     if(cedula_beneficiario=='')
     {
         alert('POR FAVOR INGRESE UNA CEDULA');    
     }
     else
     {
        
       $.ajax(
            {
                 url:url,
                 method:'GET',
                data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
                dataType:'JSON',
                beforeSend:function(data)
                {
                     //alert('Procesando');
                },
                success:function(data)
                {
                // console.log(data);
               //alert(data);
               if(data.borrado=='t')
               {
                   alert('El TITULAR SE ENCUENTRA INACTIVO');
               }
               else
               {
                    $("#cajas2").css("display", "block"); 
                    document.getElementById("btnPDF").disabled=false; 
                   // document.getElementById("fecha").disabled=false;  
                    $('#modal').find('#nombretitular').val(data.nombre_titular + ' ' + data.apellido_titular); 
                    $('#modal').find('#nombreb').val(data.nombre+' '+data.apellido); 
                    $('#modal').find('#fecha_nacimientob').val(data.fecha_nac_familiares); 
                    $('#modal').find('#edadb').val(data.edad_actual); 
                    $('#modal').find('#cedulab').val(data.cedula); 
                    $('#modal').find('#telefonob').val(data.telefono); 
                    $('#modal').find('#parentescob').val(data.descripcionparentesco); 
                    $('#modal').find('#unidadtitular').val(data.departamento); 
                    $('#modal').find('#fecha_nacimientotitular').val(data.fecha_nacimiento);  
                    $('#modal').find('#edadtitular').val(data.edad_actualt);  
                    $('#modal').find('#cedulatitular').val(data.cedula_trabajador); 
                    $('#modal').find('#telefonotitular').val(data.telefonot);
                    $('#modal').find('#n_historial').val(data.n_historial);
               }   
                },
                error:function(xhr, status, errorThrown)
                {
                   alert('EL BENEFICIARIO NO POSEE HISTORIAL');
                }
           });   
          }    
 });
 $(document).on('click','#btncortesia', function(e)
 {
   
     var cedula_beneficiario = $('#cedula_beneficiario').val();
     url='/buscarcortesia_NotasEntregas';
     data=
     {
          cedula_beneficiario:cedula_beneficiario
     };
     if(cedula_beneficiario=='')
     {
         alert('POR FAVOR INGRESE UNA CEDULA');    
     }
     else
     {
         
       $.ajax(
            {
                 url:url,
                 method:'GET',
                data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
                dataType:'JSON',
                beforeSend:function(data)
                {
                     //alert('Procesando');
                },
                success:function(data)
                {
                // console.log(data);
                if(data.borrado=='t')
               {
                   alert('El TITULAR SE ENCUENTRA INACTIVO');
               }
               else
               {
                    $("#cajas2").css("display", "block"); 
                    document.getElementById("btnPDF").disabled=false;
                    //document.getElementById("fecha").disabled=false;
                    $('#modal').find('#nombreb').val(data.nombre+' '+data.apellido); 
                    $('#modal').find('#fecha_nacimientob').val(data.fecha_nac_cortesia); 
                    $('#modal').find('#edadb').val(data.edad_actual); 
                    $('#modal').find('#cedulab').val(data.cedula); 
                    $('#modal').find('#telefonob').val(data.telefono); 
                    //$('#modal').find('#parentescob').val(data.descripcionparentesco); 
                    $('#modal').find('#n_historial').val(data.n_historial);
                    $('#modal').find('#nombretitular').val(data.nombre_titular + ' ' + data.apellido_titular); 
                    $('#modal').find('#unidadtitular').val(data.departamento); 
                    $('#modal').find('#fecha_nacimientotitular').val(data.fecha_nacimiento);  
                    $('#modal').find('#edadtitular').val(data.edad_actualt);  
                    $('#modal').find('#cedulatitular').val(data.cedula_trabajador); 
                    $('#modal').find('#telefonotitular').val(data.telefonot);
               }   
                },
                error:function(xhr, status, errorThrown)
                {
                    alert('EL BENEFICIARIO NO POSEE HISTORIAL');
                }
           });   
          }
 });


 $('#lista_notas_Entregas').on('click','.Notas', function(e)
{
     e.preventDefault
     let usuario=$('#usuario').val();  
     let tipobeneficiario    =$(this).attr('tipobeneficiario');
     let fecha_registro=$(this).attr('fecha_registro');
     let cedula_beneficiario =$(this).attr('cedula_beneficiario');
     let id_autorizador =$(this).attr('id_autorizador');
    
      // ***SE USO EL  moment PARA CONVERTIR LA FECHA******
      let fechaconvertida=moment(fecha_registro,'DD-MM-YYYY'); 
      fechaconvertida=fechaconvertida.format('YYYY-MM-DD');
     window.open('Nota_Entrega_medicamento/'+cedula_beneficiario+'/'+tipobeneficiario+'/'+fechaconvertida+'/'+id_autorizador,'_blank');
     window.location = '/vistaNotasEntregas';
    });



 $('#btnPDF').on('click',function(e)
 {  
     e.preventDefault
     var usuario=$('#usuario').val();  
     var fechanotas=$('#fecha').val(); 
     var n_historial=$('#n_historial').val();  
     var cedula_beneficiario=$('#cedula_beneficiario').val();
     var tipobeneficiario=$('#tipobeneficiario').val();
     // ***SE USO EL  moment PARA CONVERTIR LA FECHA******
     let fechaconvertida=moment(fechanotas,'DD-MM-YYYY'); 
     fechaconvertida=fechaconvertida.format('YYYY-MM-DD');
     var url='/agregar_nota_entrega';
    
     var data=
     {
        fechaconvertida :fechaconvertida ,
        n_historial:n_historial,
        usuario:usuario,
        cedula_beneficiario:cedula_beneficiario,
        tipobeneficiario:tipobeneficiario
     }
      $.ajax
      ({
          url:url,
          method:'POST',
          data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
          },
          success:function(data)
          {    
             
           
               if(data.procesado=='1')
               {
                    alert('Registro Incorporado');
                    
                    window.open('Nota_Entrega_medicamento/'+cedula_beneficiario+'/'+tipobeneficiario+'/'+fechaconvertida+'/'+data.autorizador,'_blank');
               }

              else if(data.procesado=='3')
               {
                    alert('ERROR! NO EXISTE SALIDAS ACTIVAS PARA ESTA FECHA');
                    //window.location = '/vistapresentacion';
               }
               else if(data.procesado=='4')
               {
                    alert('ERROR! DEBE HABILITAR UN AUTORIZADOR PARA LA NOTA DE ENTREGA');
                    window.location = '/vistaNotasEntregas';
               }
               else
               {
                    alert('Error en la Incorporación del Registro');
               }
               
          },
          error:function(xhr, status, errorThrown)
          {
               alert(xhr.status);
               alert(errorThrown);
          }
      });
       
  });

  $('#lista_notas_Entregas').on('click','.Bloquear', function(e)    

{
     Swal.fire({
          title: 'Reversar el Registro?',
          icon:'warning',
          showCancelButton: true,
          cancelButtonColor:'#d33',
          confirmButtonText:'Confirmar',
                   
          }).then((result)=> {
                 if(result.isConfirmed){ 
     
                   
                       let nota_entrega   =$(this).attr('nota_entrega_id');
                       nota_entrega_id = (isNaN(parseInt(nota_entrega)))? 0 : parseInt(nota_entrega);
                       let borrado    =true;
                       var data=
                       {
                            nota_entrega_id:nota_entrega_id,
                            borrado:borrado
                           
                       }            
                       var url='/Reversar_nota_entrega';
                      $.ajax
                       ({
                            url:url,
                            method:'POST',
                            data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
                            dataType:'json',
                            beforeSend:function(data)
                            {
                                 //alert('Procesando Información ...');
                            },
                            success:function(data)
                            {
                              
                                if(data==0)
                               {
                                   alert('NO HAY DETALLES DE LA NOTA DE ENTREGA , COMUNIQUESE CON EL ADMINISTRADOR DEL SISTEMA');  
                                 
                               }
                               else  if(data==1)
                               {
                                   alert('HUBO UN ERROR AL BORRAR LOS DETALLES DE SALIDAS ASOCIADAS A LA NOTA DE ENTREGA ');   
                               }
                               else if(data==2)
                               {
                                   alert('HUBO UN ERROR AL BORRAR LAS SALIDAD DE LA NOTA DE ENTREGA');    
                                 
                               }
                               else if(data==3)
                               {
                                      //alert(' SE REVERSO LA NOTA DE ENTREGA DE MANERA EXITOSA ');   
                                      Swal.fire(  
                                                 
                                        'REVERSO!!',
                                        'Registro Actualizado.',
                                        'success',
                                        {
                                             
                                        }
                                      )
                                      setTimeout(function () 
                                      {
                                        window.location = '/vistaNotasEntregas/';  
                                      }, 1500); 
                                 
                               }
                               else if(data==4)
                               {
                                   alert(' HUBO UN ERROR AL REVERSAR LA NOTA DE ENTREGA  ');    
                                 
                               }
                                      
                            },
                            error:function(xhr, status, errorThrown)
                            {
                               alert(xhr.status);
                               alert(errorThrown);
                            }
                      });  
                      
                   }
                   
               })  
     
        
 });














