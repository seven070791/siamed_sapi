/*
 *Este es el document ready
 */
 $(function()
 {
     
     let descripcioncompleta;
     descripcioncompleta='';  //Del medicamento
     cronicos=null;
     nombre_categoria='Seleccione'
     $('#categoria').val('0')
     var id_categoria =$('#cmbCategoria').val();
     llenar_combo_categoria(Event);
     listarmedicamento(id_categoria,cronicos,nombre_categoria); 
     $('#modal').find('#cantidad').hide();
     $('#modal').find('#btnGuardar').hide();
     $('#modal').find('#btncompuestos').hide();
    document.getElementById("cmbPresentacion").disabled=true;
    document.getElementById("descripcion").disabled=true;   
    document.getElementById("cmbControl").disabled=true;  
 });


 /*
  * Función para definir datatable:
  */
function listarmedicamento(id_categoria=0,cronicos=null,nombre_categoria=null)
{

     let ruta_imagen =rootpath
     var now = new Date();
     var day = ("0" + now.getDate()).slice(-2);
     var month = ("0" + (now.getMonth() + 1)).slice(-2);
     var today= day+"/"+month+"/"+now.getFullYear();
     let encabezado=today;

     if(nombre_categoria!='Seleccione') 
     { 
          encabezado=encabezado+' '+'CATEGORIA:'+' '+' '+nombre_categoria+' '+' ';  
     }


          if(cronicos!=null) {
               if (cronicos=='true') {
               descr_cronico='SI' 
               }
               else {
                    descr_cronico='NO'    
               }
               encabezado=encabezado+' '+'CRONICO:'+' '+' '+descr_cronico+' '+' ';  
          }
        
     var titulo='CONTROL DE INVENTARIO'
     var table = $('#table_medicamentos').DataTable( {     
     responsive :true,     
     dom: "Bfrtip",
     buttons:{
     dom:
     {
          button: {
                    className: 'btn-xs-xs'
                  },       
     },
     buttons:[
               {
               //definimos estilos del boton de pdf
               extend: "pdf",
               text:'PDF',
               className:'btn-xs btn-dark',
               orientation: 'landscape',
               pageSize: 'LETTER',
               header:true,
               footer: true,
               download: 'open',
               exportOptions: 
               {
                    columns: [ 0,1,2,3,4,5,6,7,8],                          
               },   
               
               alignment: 'center',                 
               customize:function(doc)
                    {                       
                    //Remove the title created by datatTables
                         doc.content.splice(0,1);
                       
                         doc.styles.title= 
                         { 
                              color: '#4c8aa0',
                              fontSize: '18',
                              alignment: 'center'                     
                         }
                         doc.styles['td:nth-child(2)'] = 
                         {
                              width: '100px',
                              'max-width': '100px'
                         },
                         doc.styles.tableHeader = 
                         {
                              fillColor:'#4c8aa0',
                              color:'white',
                              alignment:'center'
                         }, 
               // Create a header
               doc.pageMargins = [20,95,0,70];
               doc['header'] = (function (page, pages)
               {                      
                    doc.styles.title = 
                    {    
                         color: '#4c8aa0',
                         fontSize: '18',
                         alignment: 'center',  
                    } 
                    return {  
                              columns:
                              [
                                   {                              
                                        margin: [ 170, 3, 40, 40 ],                                  
                                        image:ruta_imagen,
                                        width: 500,  
                                                                 
                                   },                        
                                   {
                                        margin :[ -380, 40, -25, 0 ],
                                        color: '#4c8aa0',
                                        fontSize: '18',
                                        alignment: 'center' , 
                                        text: 'CONTROL DE INVENTARIO',
                                        fontSize: 18,                             
                                   },
                                   {
                                   margin :[ -528, 70, -25, 0 ],     
                                   text:'FECHA:'+' '+encabezado,
                                   },
                              ], 
                         }                               
                }); 
               // Create a footer
               doc['footer'] = (function (page, pages)
               {    
                    return {
                              columns:
                              [
                                   {
                                        alignment: 'center',
                                        text: ['pagina ', { text: page.toString() },	' of ',	{ text: pages.toString() }]
                                   }
                              ],               
                              }                            
                    });
      
                    },
               },
               {    
               //definimos estilos del boton de excel
               extend: "excel",
               text:'Excel',
               className:'btn-xs btn-dark',
               title:'CONTROL DE INVENTARIO', 
               download: 'open',
               exportOptions: {
               columns: [ 0,1,2,3,4,5,6,7,8],
               excelStyles: 
               {                                                
                    "template": [
                    "blue_medium",
                    "header_blue",
                    "title_medium"
                     ]                                  
               },                              
                         },        
                    }
                    ]            
                    }, 
                    "order":[[0,"asc"]],					
                    "paging": true,
                    "lengthChange": true,
               
                    dom: 'Blfrtip',
                    "searching": true,
                    "lengthMenu": [
                         [ 10, 25, 50, -1 ],
                         [ '10', '25', '50', 'Todos' ]
                    ], 
               
                    "ordering": true,
                    "info": true,
                    "autoWidth": true,
                    //"dom": 'Bfrt<"col-md-6 inline"i> <"col-md-6 inline"p>',
                    
                    "ajax":
                    {
                         "url":"/listar_medicamentos/"+id_categoria+'/'+cronicos,
                    
                    "type":"GET",
                    dataSrc:''
                    },

               
                    
                    "columns":
                    [
                         {data:'descripcion'},  
                        {data:'med_cronico'}, 
                         {data:'fecha_creacion'}, 
                         {data:'control'},
                         {data:'categoria'}, 
                         {data:'stock_minimo'}, 
                         {data:'entradas'}, 
                         {data:'salidas'}, 

                         {
                              data :null,
                              orderable: true,
                              render:function(data, type, row)
                              {
                              if(parseInt(row.stock)<=parseInt(row.stock_minimo))
                              {
                                   return '<button id="btnalerta"class="  btn-warning btnalerta "disabled="disabled">'+row.stock+'</button>'
                              }
                              else 
                              {
                                   return '<button id="btnsolvente"class="btnsolvente "disabled="disabled">'+row.stock+'</button>'
                              
                              } 
                         
                              }
                         },
                    
                         {
                         data :null,     
                         orderable: true,
                         render:function(data, type, row)
                         {
                    //    return '<a href="javascript:;" class="btn btn-xs btn-success editar_lista_medicamento" style=" font-size:2px" data-toggle="tooltip" title="Editar Medicamento" id='+row.id+' descripcion="'+row.descripcion+'"> <i class="material-icons " >create</i></a>'
                         return '<a href="javascript:;" class="btn btn-xs btn-primary  editar_lista_medicamento" style=" font-size:1px" data-toggle="tooltip" title="Editar Medicamento" id='+row.id+' descripcion="'+row.descripcion+'" fecha_creacion='+row.fecha_creacion+' estatus='+row.estatus+' med_cronico='+row.med_cronico+' stock_minimo='+row. stock_minimo+'  > <i class="material-icons " >create</i></a>'+' '+'<a href="javascript:;" class="btn btn-xs btn-success Entrada"  style=" font-size:1px" data-toggle="tooltip" title="Entradas" id='+row.id+' rol='+row.descripcion+' estatus="'+row.estatus+'"><i class="material-icons ">add_circle</i></a>'+' '+'<a href="javascript:;" class="btn btn-xs btn-secondary Salidas" style=" font-size:1px" data-toggle="tooltip" title="Salidas" id='+row.id+' rol='+row.descripcion+' estatus="'+row.estatus+'"><i class="material-icons ">remove_circle</i></a>'+' '+'<a href="javascript:;" class="btn btn-xs btn-info Reportes" style=" font-size:1px" data-toggle="tooltip" title="Reportes" id='+row.id+' rol='+row.descripcion+' estatus="'+row.estatus+'"><i class="material-icons ">print</i></a>'
                         }
                         }
                         
                    
                    ],
          "language":
               {
                    "sProcessing":    "Procesando...",
                    "sLengthMenu":    "Mostrar _MENU_ registros",
                    "sZeroRecords":   "No se encontraron resultados",
                    "sEmptyTable":    "Ningún dato disponible en esta tabla",
                    "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":   "",
                    "sSearch":        "Buscar:",
                    "sUrl":           "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": 
                    {
                    "sFirst":    "Primero",
                    "sLast":    "Último",
                    "sNext":    "Siguiente",
                    "sPrevious": "Anterior"
                    },
                    "oAria":
                    {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    },
                    "columnDefs": 
                    [
                         {
                         "targets": [ 0 ],
                         "visible": false,
                         "searchable": false
                         },
                    ]
               
               },
          });
          }




/*
* Código para el botón Agregar:
*/
$('#btnAgregar').on('click',function(e)
{  
   
     //Botones de la ventana modal
     //$('#modal')[0].reset();
     $('#modal').modal();     
    $('#modal').find('#btnGuardar').show();
    //document.getElementById("btnGuardar").disabled=true;  
     $('#modal').find('#btnActualizar').hide();
     $('#modal').find('#borrado').hide();
     $('#modal').find('#activo').hide();   
     //Despliego la ventana Modal      
     $("#modal").modal("show");
     //Lleno la persiana de tipo de medicamento
     llenar_combo_tipo_medicamento(Event);
     //Lleno la persiana de tipo de presentación

    llenar_combo_compuesto(Event);
     //Lleno la persiana de la unidad de medida
     llenar_combo_presentacion(Event);
     //Lleno la persiana del Control Medicamento
     llenar_combo_Control(Event);

     llenar_combo_categoria(Event);

    
     document.getElementById("descripcion").disabled=true;
 });




 $('#btnCerrar').on('click',function(e)
{  
$('#descripcion').val('');  
document.getElementById("cmbPresentacion").disabled=true;
 });
/*
* Función para ...
*/
function llenar_combo_tipo_medicamento(e, id)
{
     let id_cmbTipoMedicamento='null'
     
     e.preventDefault
    
     url='/listar_medicamentos_activos'+'/'+id_cmbTipoMedicamento;
     $.ajax
     ({
         url:url,
         method:'GET',
         //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
         dataType:'JSON',
         beforeSend:function(data)
         {
         },
         success:function(data)
         {   
          if(data.length>1)
          { 
               $('#cmbTipoMedicamento').empty();
               $('#cmbTipoMedicamento').append('<option value=0  selected disabled>Seleccione</option>');                
               if(id===undefined)
               {      
                    $.each(data, function(i, item)
                    {
                         //console.log(data)
                         $('#cmbTipoMedicamento').append('<option value='+item.id+'>'+item.descripcion+'</option>');
     
                    });
               }
               else
               {
                  
                    $.each(data, function(i, item)
                    {
                         if(item.id===id)
                         {
                             $('#cmbTipoMedicamento').append('<option value='+item.id+' selected>'+item.descripcion+'</option>');
                        
                         }
                         else
                         {
                             
                              $('#cmbTipoMedicamento').append('<option value='+item.id+'>'+item.descripcion+'</option>');
                         }
                    });
               }
          }      
         },
         error:function(xhr, status, errorThrown)
         {
             // alert(xhr.status);
             // alert(errorThrown);
         }
     });
}

function llenar_combo_categoria(e, id)
{
    
     e.preventDefault
     url='/listar_Categoria_activas';
     $.ajax
     ({
         url:url,
         method:'GET',
         //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
         dataType:'JSON',
         beforeSend:function(data)
         {
         },
         success:function(data)
         {   
          /*
          console.log(data);
          alert(data[0].id)
          alert(data[0].descripcion)
          */
          if(data.length>1)
          { 
               
               $('#cmbCategoria').empty();
               $('#cmbCategoria').append('<option value=0  selected disabled>Seleccione</option>');                
               if(id===undefined)
               {      
                    $.each(data, function(i, item)
                    {
                         //console.log(data)
                         $('#cmbCategoria').append('<option value='+item.id+'>'+item.descripcion+'</option>');
     
                    });
               }
               else
               {
                  
                    $.each(data, function(i, item)
                    {
                         if(item.id===id)
                         {
                             $('#cmbCategoria').append('<option value='+item.id+' selected>'+item.descripcion+'</option>');
                        
                         }
                         else
                         {
                             
                              $('#cmbCategoria').append('<option value='+item.id+'>'+item.descripcion+'</option>');
                         }
                    });
               }
          }      
         },
         error:function(xhr, status, errorThrown)
         {
              //alert(xhr.status);
              //alert(errorThrown);
         }
     });
}
/*
* Función para ...
*/    
function llenar_combo_presentacion(e, id)
{
      e.preventDefault;
      url='/listar_presentaciones_activas';
      $.ajax
      ({
          url:url,
          method:'GET',
          //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
          },
          success:function(data)
          {

if(data.length>0)
{
     $('#cmbPresentacion').empty();
     $('#cmbPresentacion').append('<option value=0 selected disabled  >Seleccione</option>');                
     if(id===undefined)
     {
          $.each(data, function(i, item)
          {
          
               //console.log(data)
               $('#cmbPresentacion').append('<option value='+item.id+'>'+item.descripcion+'</option>');
          });
     }
     else
     {
          $.each(data, function(i, item)
          {
               if(item.id===id)
               {
                    $('#cmbPresentacion').append('<option value='+item.id+' selected>'+item.descripcion+'</option>');
               }
               else
               {
                    $('#cmbPresentacion').append('<option value='+item.id+'>'+item.descripcion+'</option>');
               }
          });
     }
}      
},
error:function(xhr, status, errorThrown)
{
    //alert(xhr.status);
    //alert(errorThrown);
}
});
}

/*
* Función para llenar combo ...
*/
function llenar_combo_compuesto(e, id)
{
     e.preventDefault
     url='/listar_Compuestos_activos';
     $.ajax
     ({
         url:url,
         method:'GET',
         //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
         dataType:'JSON',
         beforeSend:function(data)
         {
         },
         success:function(data)
         {    
          
         // console.log(data);
          //alert(data[0].id)
         // alert(data[0].descripcion)
     
          if(data.length>1)
          { 
               $('#cmbCompuesto').empty();
               $('#cmbCompuesto').append('<option value=0 selectd disabled>Seleccione</option>');               
               if(id===undefined)
               {
                    
                    $.each(data, function(i, item)
                    {
                         //console.log(data)
                         $('#cmbCompuesto').append('<option value='+item.id+'>'+item.descripcioncompuesta+'</option>');   
                    });
               }
               else
               {
                    $.each(data, function(i, item)
                    {
                         if(item.id===id_compuesto)
                         {
                              $('#cmbCompuesto').append('<option value='+item.id+' selected>'+item.descripcioncompuesta+'</option>');
                         }
                         else
                         {
                              $('#cmbCompuesto').append('<option value='+item.id+'>'+item.descripcioncompuesta+'</option>');
                         }
                    });
               }
          }      
         },
         error:function(xhr, status, errorThrown)
         {
             // alert(xhr.status);
             // alert(errorThrown);
         }
     });
}




/*
* Función para llenar el combo del control de medicamento
*/    
function llenar_combo_Control(e,id)
{
      e.preventDefault;
      url='/listar_controles_activos';
      $.ajax
      ({
          url:url,
          method:'GET',
          //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
          },
          success:function(data)
          {
           /*
//           console.log(data);
//           alert(data[0].id)
//           alert(data[0].descripcion)
//           */
if(data.length>1)
{
     $('#cmbControl').empty();
     $('#cmbControl').append('<option value=0 selected disabled >Seleccione</option>');     
     if(id===undefined)
     {
          $.each(data, function(i, item)
          {
               //console.log(data)
               $('#cmbControl').append('<option value='+item.id+'>'+item.descripcion+'</option>');

          });
     }
     else
     {
          $.each(data, function(i, item)
          {
               if(item.id===id)
               {
                    $('#cmbControl').append('<option value='+item.id+' selected>'+item.descripcion+'</option>');
               }
               else
               {
                    $('#cmbControl').append('<option value='+item.id+'>'+item.descripcion+'</option>');
               }
          });
     }
}      
},
error:function(xhr, status, errorThrown)
{
    //alert(xhr.status);
    //alert(errorThrown);
}
});
}





$("#cmbTipoMedicamento").on('change', function(e)
{
     e.preventDefault
     let id_cmbTipoMedicamento=$("#cmbTipoMedicamento").val();
     url='/listar_medicamentos_activos'+'/'+id_cmbTipoMedicamento;
     $.ajax
     ({
         url:url,
         method:'GET',
         //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
         dataType:'JSON',
         beforeSend:function(data)
         {
         },
         success:function(data)
         {   
          if (data[0].categoria_id!=1) 
          {
               $('.chk_cronico').hide();
          }
          else
          {
               $('.chk_cronico').show();
          }

            

         },
         error:function(xhr, status, errorThrown)
         {
              //alert(xhr.status);
             // alert(errorThrown);
         }
     });



     document.getElementById("descripcion").disabled=false; 
     descripcioncompleta=$('#cmbTipoMedicamento option:selected').text(); 
     document.getElementById("cmbPresentacion").disabled=false;
     document.getElementById("cmbControl").disabled=false; 
     document.getElementById("descripcion").disabled=false;  
     $('#descripcion').val(descripcioncompleta); 
       
     
    
      if($('#cmbPresentacion').val()>0)
     {
          
          descripcioncompleta=$('#cmbTipoMedicamento option:selected').text();     
          descripcioncompleta=descripcioncompleta + '/'+$('#cmbPresentacion option:selected').text()+"/"+$('#cmbCompuesto option:selected').text();
          $('#descripcion').val(descripcioncompleta);

     }

     $('#descripcion').val(descripcioncompleta);    
});

$("#cmbPresentacion").change(function()
{
     descripcioncompleta=$('#cmbTipoMedicamento option:selected').text(); 

     if($('#cmbTipoMedicamento').val()>0)
     {   
          descripcioncompleta=descripcioncompleta + '/'+$('#cmbPresentacion option:selected').text()+""+"";
          $('#descripcion').val(descripcioncompleta);
     }
             
 })


/*
* Código para el botón btnGuardar:
*/
$(document).on('click','#btnGuardar', function(e)
 {
      e.preventDefault();


    
      //var selected = $('#cmbCompuestos').select2("val

      var descripcion     =$('#descripcion').val();
      var medicamento     =$('#cmbTipoMedicamento').val();
      var presentacion    =$('#cmbPresentacion').val();
      var control         =$('#cmbControl').val();
      var fechaRegistro=$('#fecha').val();
      var stock_minimo=$('#stock_minimo').val();
      var med_cronico  ='false';
 
      if($('#cronico').is(':checked'))
      {
          med_cronico='true';
      }
      else
      {
          med_cronico='false';
      }



      if ( medicamento<=0)
      {    
          alert('DEBE  SELECCIONAR UN MEDICAMETNO');

      }
     else if ( control<=0)
      {    
          alert('DEBE  SELECCIONAR UN CONTROL');
          
      }
     else
     {

    
    
     {
     var url='/agregar_medicamento';
     var data=
     {    
          descripcion:descripcion,
          medicamento:medicamento,
          presentacion:presentacion,
          control:control,
          fechaRegistro  :fechaRegistro,
          stock_minimo:stock_minimo,
          med_cronico,med_cronico
     }
      $.ajax
      ({
          url:url,
          method:'POST',
          data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
          },
          success:function(data)
          {      
               if(data=='1')
               {
                    alert('Registro Incorporado');
                    window.location = '/vistamedicamentos';
               }
               else
               {
                    alert('Error en la Incorporación del Registro');
               }
               
          },
          error:function(xhr, status, errorThrown)
          {
               //alert(xhr.status);
              // alert(errorThrown);
          }
      });
     }
} 
});
/*
* 
*/
$('#lista_medicamento').on('click','.editar_lista_medicamento', function(e)
{
     $('#modal').find('#btncompuestos').show();
     document.getElementById("descripcion").disabled=false;
     var id            =$(this).attr('id');
     var estatus       =$(this).attr('estatus');
     var med_cronico=$(this).attr('med_cronico');
     var stock_minimo=$(this).attr('stock_minimo');
     $('#id').val(id);
     url='/datosdelmedicamento';
     //El Objeto con parameros:
     data=
     {
          id:id
     };
     $.ajax(
          {
               url:url,
               method:'GET',
               data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
               dataType:'JSON',
               beforeSend:function(data)
               {
                    //alert('Procesando');
               },
               success:function(data)
               {
                   //console.log(data);

                  
                    $("#modal").modal("show");
                    $('#modal').find('#btnGuardar').hide();
                    $('#modal').find('#btnActualizar').show();
                    $('#modal').find('#borrado').show();
                    $('#modal').find('#activo').show();   
                    $('#modal').find('#fecha_creacion').val(data.fecha_creacion); 
                    $('#modal').find('#descripcion').val(data.descripcion);
                    $('#modal').find('#stock_minimo').val(stock_minimo);
                    document.getElementById("cmbPresentacion").disabled=false;
                    document.getElementById("cmbControl").disabled=false;
                    $('#estatus').val(estatus );
                
                    //Lleno la persiana de tipo de medicamento
                    llenar_combo_tipo_medicamento(e, data.tipo_medicamento);
                    //Lleno la persiana de tipo de presentación
                    llenar_combo_presentacion(e,data.presentacion);

                    llenar_combo_compuesto(e);
                    //llenar_combo_categoria(e);
                    //Lleno la persiana del Control Medicamento
                    llenar_combo_Control(e,data.control);   
                    
                  
                    if(estatus=='Activo')
                    {
                         document.querySelector('.borrado').checked=true
                    }
                    if(estatus=='Eliminado')
                    {
                         document.querySelector('.borrado').checked=false
                    } 


                    if(med_cronico=='SI')
                    {
                         document.querySelector('.cronico').checked=true
                         $('#cronico').val('true');
                    }
                    if(med_cronico=='NO')
                    {
                         document.querySelector('.cronico').checked=false
                         $('#cronico').val('false');
                    } 

               }
          });
});
/*
*
*/
$(document).on('click','#btnActualizar', function(e)
{
     e.preventDefault();

     var id           =$('#id').val();
     var descripcion  =$('#descripcion').val();
     var medicamento  =$('#cmbTipoMedicamento').val();
     var presentacion =$('#cmbPresentacion').val();
     var control      =$('#cmbControl').val();
     var fechaRegistro=$('#fecha').val();
     var categoria =$('#cmbCategoria').val();
     var stock_minimo=$('#stock_minimo').val();
     var borrado      ='false';
     var cronico  ='false';

     if($('#borrado').is(':checked'))
     {
          borrado='false';
     }
     else
     {
          borrado='true';
     }

     if($('.cronico').is(':checked'))
     {
          cronico='true';
     }
     else
     {
          cronico='false';
     }

//alert(cronico);

     var data=
     {
          id             :id,
          descripcion    :descripcion,
          medicamento    :medicamento,
          presentacion   :presentacion,
          control        :control,
          fechaRegistro  :fechaRegistro,
          borrado        :borrado,
          cronico        :cronico,
          categoria :categoria,
          stock_minimo,stock_minimo
     }

    var url='/actualizar_medicamento';
    $.ajax
    ({
          url:url,
          method:'POST',
          data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
               //alert('Procesando Información ...');
          },
          success:function(data)
          {
             //alert(data);
             if(data===1)
             {
               alert('Registro Actualizado');
             }
             else
             {
               alert('Error en la Incorporación del registro');
             }
             window.location = '/vistamedicamentos';             
          },
          error:function(xhr, status, errorThrown)
          {
             alert(xhr.status);
             alert(errorThrown);
          }
     });
});




$(document).on('click','#btnFpdfEntradas', function(e)
{
     e.preventDefault();

     var id           =$('#id').val();
     var data=
     {
          id             :id,
     }

     window.open('entradasFpdfMedicamento/'+id, '_blank');
    // window.location = '/entradasFpdfMedicamento/'+id; 
   
    

});

$(document).on('click','#btnRegresar', function(e)
{
     e.preventDefault();
     window.location = '/vistamedicamentos'; 
});

$(document).on('click','#btnFpdfSalidas', function(e)
{
     e.preventDefault();

     var id           =$('#id').val();
     var data=
     {
          id             :id,
     }

     window.open('salidasFpdfMedicamento/'+id, '_blank');
    // window.location = '/salidasFpdfMedicamento/'+id; 

});


$('#lista_medicamento').on('click','.Entrada', function(e)
{
 var id_medicamento            =$(this).attr('id');    
window.location = '/vistaEntradas/'+id_medicamento ; 
});




$('#lista_medicamento').on('click','.Salidas', function(e)
{
 
 var id_medicamento          =$(this).attr('id');   

  window.location = '/vista_salidas/'+id_medicamento; 
});

$('#lista_medicamento').on('click','.Reportes', function(e)
{
 var id_medicamento          =$(this).attr('id');   
  window.location = '/vista_reportes/'+id_medicamento; 
});


$('#btnExcel').on('click',function(e)
 {  
     e.preventDefault
     var desde           =$('#desde').val();
     var hasta          =$('#hasta').val();



     
     if ((desde!='')&&(hasta==''))
     {
          alert('  Verificar  las fechas');  
     }
     else if ((hasta!='')&&(desde==''))
     {
          alert('  Verificar  las fechas');
     }else
     {
          window.location = '/Medicamento_Controllers/VerMedicamentoExcel/'+desde+'/'+hasta,'_blank';
     }
 
 
  });
/*
* Código para el botón  Compuestos:
*/
$(document).on('click','#btncompuestos', function(e)
 {
      e.preventDefault();
      var id_medicamento            =$('#id').val();
      var id_tipo_medicamento  =$('#cmbPresentacion option:selected').val(); 
      window.location = '/vistacompuestos/'+id_medicamento; 
 })

 


$("#cmbCategoria").on('change', function()
{
     $("#table_medicamentos").dataTable().fnDestroy();
     var id_categoria =$('#cmbCategoria').val();
     $('#cmbcronico').val('0');
     let cronico=0;

     let nombre_categoria=$('#cmbCategoria option:selected').text();
     
     listarmedicamento(id_categoria,cronicos,nombre_categoria);   
      if (id_categoria==1) {
          //***DESELECCIONAR EL CHECKBOX**
          $("input[type=checkbox]").prop("checked",false);
          $('.medic_cronico').css('display', 'block');  
          $('.botones').css('display', 'block'); 
      }else if(id_categoria!=1) {
   
          $('.medic_cronico').css('display', 'none');  
          $('.botones').css('display', 'none'); 
      }
             
});



$(document).on('click','#btnfiltrar ', function(e)
  {
      e.preventDefault(); 
     let id_categoria =$('#cmbCategoria').val();
     let cronicos =$('#cmbcronico').val();
      if(cronicos=='null')
      {
      cronicos='null';
      }
      else
        if(cronicos=='1')
        {
       
        cronicos='true';
        //alert(cronicos);
        }
        else if(cronicos=='2')
        {
        cronicos='false';
        //alert(cronicos);
       
        } 
          $("#table_medicamentos").dataTable().fnDestroy();
          let nombre_categoria=$('#cmbCategoria option:selected').text();
     listarmedicamento(id_categoria,cronicos,nombre_categoria); 
      
   
      
  });

  $(document).on('click','#btnlimpiar', function(e)
  {
    e.preventDefault();
     $('.medic_cronico').css('display', 'none');  
     $('.botones').css('display', 'none'); 
     cronicos=null;
     $('#cmbcronico').val('0');
     $('.cronico').val('0');
     $('#cmbCategoria').val('0')
     var id_categoria =$('#cmbCategoria').val();
     
    $("#table_medicamentos").dataTable().fnDestroy();
    let nombre_categoria=$('#cmbCategoria option:selected').text();
    listarmedicamento(id_categoria,cronicos,nombre_categoria); 
        
  });
