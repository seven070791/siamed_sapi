/*
 *Este es el document ready
 */
 $(function()
 {
     //$(".medic_cronico").hide();
       //$('.medic_cronico').css('display', 'none');
     //$('.medic_cronico').css('visibility', 'hidden')
    
     descripcioncompleta='';  //Del medicamento
     cronicos=null;
     nombre_categoria='Seleccione'
     $('#categoria').val('0')
     var id_categoria =$('#cmbCategoria').val();
     llenar_combo_categoria(Event);
     listarmedicamento(id_categoria,cronicos,nombre_categoria); 
     
    
 });

 /*
  * Función para definir datatable:
  */
function listarmedicamento(id_categoria=0,cronicos=null,nombre_categoria=null)
{
     
    var now = new Date();
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var today= day+"/"+month+"/"+now.getFullYear();
    let encabezado=today;

    if(nombre_categoria!='Seleccione') 
    { 
         encabezado=encabezado+' '+'CATEGORIA:'+' '+' '+nombre_categoria+' '+' ';  
    }


     if(cronicos!=null) {
          if (cronicos=='true') {
             descr_cronico='SI' 
          }
          else {
               descr_cronico='NO'    
          }
          encabezado=encabezado+' '+'CRONICO:'+' '+' '+descr_cronico+' '+' ';  
     }
          
   

    let ruta_imagen =rootpath
     var table = $('#table_medicamentos').DataTable( {
          
          dom: "Bfrtip",
          buttons:{
              dom: {
                  button: {
                      className: 'btn-xs'
                  },
                  
              },
              
              buttons: [
              {
                  //definimos estilos del boton de pdf
                  extend: "pdf",
                  text:'PDF',
                  className:'btn-xs btn-dark',
                  //title:'Inventario Segun Stock Minimo',
                  header:true,
                  footer: true,
                  download: 'open',
                  exportOptions: {
                              columns: [ 0,1,2,3],
                              
                  },   
                  alignment: 'center',
                  
                  customize:function(doc)
                  {                       
                  //Remove the title created by datatTables
                       doc.content.splice(0,1);
                       doc.styles.title= 
                       { 
                            color: '#4c8aa0',
                            fontSize: '18',
                            alignment: 'center'                     
                       }
                       doc.styles['td:nth-child(2)'] = 
                       {
                            width: '100px',
                            'max-width': '100px'
                       },
                       doc.styles.tableHeader = 
                       {
                            fillColor:'#4c8aa0',
                            color:'white',
                            alignment:'center'
                       }, 
             // Create a header
             doc.pageMargins = [40,95,15,30];
             doc['header'] = (function (page, pages)
             {                      
                  doc.styles.title = 
                  {    
                       color: '#4c8aa0',
                       fontSize: '18',
                       alignment: 'center',  
                  } 
                  return {  
                            columns:
                            [
                                 {                              
                                      margin: [ 40, 3, 40, 40 ],                                 
                                      image:ruta_imagen,
                                      width: 500,  
                                                               
                                 },                        
                                 {
                                      margin :[ -470, 50, -25, 0 ],
                                      color: '#4c8aa0',
                                      fontSize: '18',
                                      alignment: 'center' , 
                                      text: 'INVENTARIO EN STOCK MINIMO',
                                      fontSize: 18,                             
                                 },
                                 {
                                 margin :[ -468, 80, -25, 0 ],     
                                 text:'Fecha'+' '+encabezado,
                                 },
                            ], 
                       }                               
              }); 
             // Create a footer
             doc['footer'] = (function (page, pages)
             {    
                  return {
                            columns:
                            [
                                 {
                                      alignment: 'center',
                                      text: ['pagina ', { text: page.toString() },	' of ',	{ text: pages.toString() }]
                                 }
                            ],               
                            }                            
                  });
    
                  },
             },
              {    
                   //definimos estilos del boton de excel
                   extend: "excel",
                   text:'Excel',
                   className:'btn-xs btn-dark',
                   title:'Inventario Segun Stock Minimo', 
                   download: 'open',
                   exportOptions: {
                               columns: [ 0,1,2,3],
                               excelStyles: {                                                
                                   "template": [
                                       "blue_medium",
                                       "header_blue",
                                       "title_medium"
                                   ]                                  
                               },
                                        
                   },
                  
              }
              ]            
          }, 
         

          "order":[[0,"asc"]],					
          "paging": true,
          "lengthChange": true,
         
          dom: 'Blfrtip',
          "searching": true,
          "lengthMenu": [
               [ 10, 25, 50, -1 ],
               [ '10', '25', '50', 'Todos' ]
          ], 
         
          "ordering": true,
          "info": true,
          "autoWidth": true,
          //"dom": 'Bfrt<"col-md-6 inline"i> <"col-md-6 inline"p>',
            
          "ajax":
          {
             "url":base_url+"/listar_stock_minimo/"+$('#cmbCategoria').val()+'/'+cronicos,
           
             "type":"GET",
             dataSrc:''
          },

        
          
          "columns":
          [
               //{data:'id'}, 
                {data:'descripcion'}, 
                {data:'categoria'},
                {data:'cronico'}, 
                {data:'control'},
                {data:'stock_minimo'},   

              
                
                {orderable: true,
                    render:function(data, type, row)
                    {
                     if(parseInt(row.stock)<=parseInt(row.stock_minimo))
                      {
                         return '<button id="btnalerta"class="  btn-warning btnalerta "disabled="disabled">'+row.stock+'</button>'
                      }
                      else 
                      {
                         return '<button id="btnsolvente"class="btnsolvente "disabled="disabled">'+row.stock+'</button>'
                       
                      } 
                   
                    }
               },
               
              
          ],
    "language":
        {
            "sProcessing":    "Procesando...",
             "sLengthMenu":    "Mostrar _MENU_ registros",
             "sZeroRecords":   "No se encontraron resultados",
             "sEmptyTable":    "Ningún dato disponible en esta tabla",
             "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
             "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
             "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
             "sInfoPostFix":   "",
             "sSearch":        "Buscar:",
             "sUrl":           "",
             "sInfoThousands":  ",",
             "sLoadingRecords": "Cargando...",
             "oPaginate": 
             {
              "sFirst":    "Primero",
              "sLast":    "Último",
              "sNext":    "Siguiente",
              "sPrevious": "Anterior"
             },
             "oAria":
             {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
             },
             "columnDefs": 
             [
               {
                 "targets": [ 0 ],
                 "visible": false,
                 "searchable": false
               },
           ]
        
     },
});
}

/*
* Código para el botón Agregar:
*/
$('#btnAgregar').on('click',function(e)
{  
     //Botones de la ventana modal
     //$('#modal')[0].reset();
     $('#modal').modal();     
    $('#modal').find('#btnGuardar').show();
    //document.getElementById("btnGuardar").disabled=true;  
     $('#modal').find('#btnActualizar').hide();
     $('#modal').find('#borrado').hide();
     $('#modal').find('#activo').hide();   
     //Despliego la ventana Modal      
     $("#modal").modal("show");
     //Lleno la persiana de tipo de medicamento
     llenar_combo_tipo_medicamento(Event);
     //Lleno la persiana de tipo de presentación

    llenar_combo_compuesto(Event);
     //Lleno la persiana de la unidad de medida
     llenar_combo_presentacion(Event);
     //Lleno la persiana del Control Medicamento
     llenar_combo_Control(Event);

     llenar_combo_categoria(Event);

    
     document.getElementById("descripcion").disabled=true;
 });

 $('#btnCerrar').on('click',function(e)
{  
     $('#descripcion').val('');  
     document.getElementById("cmbPresentacion").disabled=true;
 });


function llenar_combo_categoria(e, id)
{
    
     e.preventDefault
     url='/listar_Categoria_activas';
     $.ajax
     ({
         url:url,
         method:'GET',
         //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
         dataType:'JSON',
         beforeSend:function(data)
         {
         },
         success:function(data)
         {   
          /*
          console.log(data);
          alert(data[0].id)
          alert(data[0].descripcion)
          */
          if(data.length>=1)
          { 
               
               $('#cmbCategoria').empty();
               $('#cmbCategoria').append('<option value=0  selected disabled>Seleccione</option>');                
               if(id===undefined)
               {      
                    $.each(data, function(i, item)
                    {
                         //console.log(data)
                         $('#cmbCategoria').append('<option value='+item.id+'>'+item.descripcion+'</option>');
     
                    });
               }
               else
               {
                  
                    $.each(data, function(i, item)
                    {
                         if(item.id===id)
                         {
                             $('#cmbCategoria').append('<option value='+item.id+' selected>'+item.descripcion+'</option>');
                        
                         }
                         else
                         {
                             
                              $('#cmbCategoria').append('<option value='+item.id+'>'+item.descripcion+'</option>');
                         }
                    });
               }
          }      
         },
         error:function(xhr, status, errorThrown)
         {
              alert(xhr.status);
              alert(errorThrown);
         }
     });
}


$("#cmbCategoria").on('change', function()
{
      
     $("#table_medicamentos").dataTable().fnDestroy();
     var id_categoria =$('#cmbCategoria').val();
     
     var cronicos =$('#cronico').val();
     $('#categoria').val(id_categoria);
     let nombre_categoria=$('#cmbCategoria option:selected').text();
    
     listarmedicamento(id_categoria,cronicos,nombre_categoria);   
      if (id_categoria==1) {
          //***DESELECCIONAR EL CHECKBOX**
          $("input[type=checkbox]").prop("checked",false);
          $('.medic_cronico').css('display', 'block');  
          $('.botones').css('display', 'block'); 
      }else if(id_categoria!=1) {
   
          $('.medic_cronico').css('display', 'none');  
          $('.botones').css('display', 'none'); 
      }
             
});

$(document).on('click','#btnfiltrar ', function(e)
  {
      e.preventDefault(); 

     var id_categoria =$('#cmbCategoria').val();
     $('#categoria').val(id_categoria);
     let nombre_categoria=$('#cmbCategoria option:selected').text();
     let cronicos           =$('#cronico').val();
     var id_categoria =$('#cmbCategoria').val();
     if(cronicos!=null)
     {
      
     
     if(cronicos=='1')
          {
               cronicos='true';
          }
          else 
          {
               cronicos='false';
          } 
     }
  
          $("#table_medicamentos").dataTable().fnDestroy();
     listarmedicamento(id_categoria,cronicos,nombre_categoria); 
      
   
      
  });

  $(document).on('click','#btnlimpiar', function(e)
  {
     var id_categoria =$('#cmbCategoria').val();
    cronicos=null;
    $('#cmbCategoria').val('0');
    $('#cronico').val('0');
    let nombre_categoria=$('#cmbCategoria option:selected').text();
    $("#table_medicamentos").dataTable().fnDestroy();
    listarmedicamento(id_categoria,cronicos,nombre_categoria); 
        
  });

// $('#cronico').click(function() {

//      if($('#cronico').is(':checked'))
//      {
//      cronicos='true';
//      }
//      else
//      {
//      cronicos='false';
    
//      } 
//      $("#table_medicamentos").dataTable().fnDestroy();
//      listarmedicamento(cronicos); 
//    });
