/*
 *Este es el document ready
 */
$(function() {
    // $('.medic_cronico').css('display', 'block');
    $("#cronico").prop('disabled', true);
    let desde = $('#desde').val();
    let hasta = $('#hasta').val();
    let id_categoria = 0;
    let id_medico = $('#cmbmedicos').val();
    let id_estatus = $('#id_estatus').val();

    let nombre_estatus = 'Seleccione'

    let cedula_trabajador = $('#cedula_cortesia').val();
    if (desde == '' && hasta == '') {

        desde = 'null'
        hasta = 'null'
    }
    let nombre_categoria = 'Seleccione'
    let cronicos = 'Seleccione';


    $('#categoria').val('0')
    llenar_combo_categoria(Event);
    listar_entradas_medicamentos(id_categoria, cronicos, desde, hasta, nombre_categoria, id_estatus, nombre_estatus);

    // let cronicos='2023-03-15'
    // let id_categoria='sdhf243'

    // listar_entradas_medicamentos(id_categoria,cronicos); 
    //$('#formulario')[0].reset();     
});



function listar_entradas_medicamentos(id_categoria = 0, cronicos = null, desde = null, hasta = null, nombre_categoria = null, id_estatus, nombre_estatus)

{

    // alert(cronicos);

    var encabezado = '';
    // data=new Date(desde);
    // let dataFormatada_desde = ((data.getDate() + 1 )) + "/" + ((data.getMonth() + 1)) + "/" + data.getFullYear(); 
    // data=new Date(hasta);
    // let dataFormatada_hasta = ((data.getDate() + 1 )) + "/" + ((data.getMonth() + 1)) + "/" + data.getFullYear(); 

    // Convertir la fecha
    var fechaOriginal = desde;
    var dataFormatada_desde = moment(fechaOriginal).format("DD-MM-YYYY");

    var fechaOriginal2 = hasta;
    var dataFormatada_hasta = moment(fechaOriginal2).format("DD-MM-YYYY");

    id_categoria = (isNaN(parseInt(id_categoria))) ? 0 : parseInt(id_categoria);
    if (id_categoria != 0) {
        encabezado = encabezado + 'CATEGORIA:' + ' ' + ' ' + nombre_categoria + ' ' + ' ';
    }

    if (dataFormatada_desde != 'NaN/NaN/NaN' && dataFormatada_hasta != 'NaN/NaN/NaN') {
        encabezado = encabezado + 'DESDE:' + ' ' + dataFormatada_desde + ' ' + ' ' + 'HASTA' + ' ' + ' ' + dataFormatada_hasta + ' ' + ' ';
    }

    if (cronicos == 'true') {
        descr_cronicos = 'Si'
    } else if (cronicos == 'false') {
        descr_cronicos = 'NO'
    }

    if (cronicos != 'Seleccione') {
        encabezado = encabezado + 'CRONICO:' + ' ' + descr_cronicos + ' ' + ' ';
    }




    let fecha_de_hoy = $('#fecha_del_dia').val();

    let ruta_imagen = rootpath
    var now = new Date();
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var today = day + "/" + month + "/" + now.getFullYear();
    var table = $('#table_relacion_entradas').DataTable({

        dom: "Bfrtip",
        buttons: {
            dom: {
                button: {
                    className: 'btn-xs-xs'
                },

            },

            buttons: [{
                    //definimos estilos del boton de pdf
                    extend: "pdf",
                    text: 'PDF',
                    className: 'btn-xs btn-dark',
                    orientation: 'landscape',
                    pageSize: 'LETTER',
                    // title:'RELACION ENTRADAS MEDICAMENTOS',
                    header: true,
                    footer: true,
                    download: 'open',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5],

                    },
                    alignment: 'center',

                    customize: function(doc) {
                        //Remove the title created by datatTables
                        doc.content.splice(0, 1);
                        doc.styles.title = {
                            color: '#4c8aa0',
                            fontSize: '18',
                            alignment: 'center'
                        }
                        doc.styles['td:nth-child(2)'] = {
                                width: '130px',
                                'max-width': '130px'
                            },
                            doc.styles.tableHeader = {
                                fillColor: '#4c8aa0',
                                color: 'white',
                                alignment: 'center'
                            },
                            // Create a header
                            doc.pageMargins = [90, 95, 0, 70];
                        doc['header'] = (function(page, pages) {
                            doc.styles.title = {
                                color: '#4c8aa0',
                                fontSize: '18',
                                alignment: 'center',
                            }
                            return {
                                columns: [{
                                        margin: [170, 3, 40, 40],
                                        image: ruta_imagen,
                                        width: 500,

                                    },
                                    {
                                        margin: [-300, 50, -25, 0],
                                        color: '#4c8aa0',
                                        fontSize: '18',
                                        alignment: 'center',
                                        text: 'RELACION ENTRADAS DEL MEDICAMENTO',
                                        fontSize: 18,
                                    },
                                    {
                                        margin: [-500, 80, -25, 0],
                                        text: encabezado,
                                    },
                                ],
                            }
                        });
                        // Create a footer
                        doc['footer'] = (function(page, pages) {
                            return {
                                columns: [{
                                    alignment: 'center',
                                    text: ['pagina ', { text: page.toString() }, ' of ', { text: pages.toString() }]
                                }],
                            }
                        });

                    },
                },

                {
                    //definimos estilos del boton de excel
                    extend: "excel",
                    text: 'Excel',
                    className: 'btn-xs btn-dark',
                    title: 'RELACION ENTRADAS MEDICAMENTOS',

                    download: 'open',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5],
                    },
                    excelStyles: {
                        "template": [
                            "blue_medium",
                            "header_blue",
                            "title_medium"
                        ]
                    },

                }
            ]
        },


        "order": [
            [0, "desc"]
        ],
        "paging": true,
        "lengthChange": true,

        dom: 'Blfrtip',
        "searching": true,
        "lengthMenu": [
            [10, 25, 50, -1],
            ['10', '25', '50', 'Todos']
        ],

        "ordering": true,
        "info": true,
        "autoWidth": true,
        //"dom": 'Bfrt<"col-md-6 inline"i> <"col-md-6 inline"p>',
        "ajax": {

            "url":base_url+ "/VerEntradasMedicamentosPdf/" + id_categoria + '/' + cronicos + '/' + desde + '/' + hasta + '/' + nombre_categoria + '/' + id_estatus + '/' + nombre_estatus,
            "type": "GET",
            dataSrc: ''
        },
        "columns": [

            //{data:'cedula_beneficiario'},
            { data: 'fecha_entrada' },
            { data: 'descripcion' },
            { data: 'fecha_vencimiento_convertida' },
            { data: 'categoria_inventario' },
            { data: 'med_cronico' },
            { data: 'cantidad' },
            { data: 'nombre' },
            { data: 'estatus' },

        ],

        "language": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": [{
                "targets": [0],
                "visible": false,
                "searchable": false
            }, ]

        },
    });
}

function llenar_combo_categoria(e, id) {

    e.preventDefault
    url = '/listar_Categoria_activas';
    $.ajax({
        url: url,
        method: 'GET',
        //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
        dataType: 'JSON',
        beforeSend: function(data) {},
        success: function(data) {
            /*
            console.log(data);
            alert(data[0].id)
            alert(data[0].descripcion)
            */
            if (data.length >= 1) {

                $('#cmbCategoria').empty();
                $('#cmbCategoria').append('<option value=0  selected disabled>Seleccione</option>');
                if (id === undefined) {
                    $.each(data, function(i, item) {
                        //console.log(data)
                        $('#cmbCategoria').append('<option value=' + item.id + '>' + item.descripcion + '</option>');

                    });
                } else {

                    $.each(data, function(i, item) {
                        if (item.id === id) {
                            $('#cmbCategoria').append('<option value=' + item.id + ' selected>' + item.descripcion + '</option>');

                        } else {

                            $('#cmbCategoria').append('<option value=' + item.id + '>' + item.descripcion + '</option>');
                        }
                    });
                }
            }
        },
        error: function(xhr, status, errorThrown) {
            alert(xhr.status);
            alert(errorThrown);
        }
    });
}



$("#cmbCategoria").on('change', function() {

    $("#table_relacion_entradas").dataTable().fnDestroy();
    let id_categoria = $('#cmbCategoria').val();
    let desde = $('#desde').val();
    let hasta = $('#hasta').val();
    let id_medico = $('#cmbmedicos').val();
    let id_estatus = $('#id_estatus').val();

    let nombre_estatus = 'Seleccione'

    let cedula_trabajador = $('#cedula_cortesia').val();
    if (desde == '' && hasta == '') {

        desde = 'null'
        hasta = 'null'
    }
    let nombre_categoria = 'Seleccione'
    let cronicos = 'Seleccione';


    listar_entradas_medicamentos(id_categoria, cronicos, desde, hasta, nombre_categoria, id_estatus, nombre_estatus);

    if (id_categoria === '1') {
        //***DESELECCIONAR EL CHECKBOX**
        $("input[type=checkbox]").prop("checked", false);
        $("#cronico").prop('disabled', false);
    } else if (id_categoria != 1) {

        $("#cronico").prop('disabled', true);

    }


});




$(document).on('click', '#btnfiltrar ', function(e) {
    e.preventDefault();

    var id_categoria = $('#cmbCategoria').val();
    var id_estatus = $('#id_estatus').val();
    if (id_categoria == null) {
        id_categoria = 0;
    }

    if (id_estatus == null) {
        id_estatus = 0;
    }

    let nombre_estatus = $('#id_estatus option:selected').text();


    let nombre_categoria = $('#cmbCategoria option:selected').text();

    let desde = $('#desde').val();
    let hasta = $('#hasta').val();
    if (desde == '' && hasta == '') {

        desde = 'null'
        hasta = 'null'
    }

    let cronicos = $('#cronico').val();

    if (cronicos != 'null' && cronicos != '1' && cronicos != '2') {

        cronicos = 'Seleccione'
    } else if (cronicos == '1') {
        cronicos = 'true';
    } else if (cronicos = '2') {
        cronicos = 'false';

    }
    //alert(typeof(cronicos));
    $("#table_relacion_entradas").dataTable().fnDestroy();
    listar_entradas_medicamentos(id_categoria, cronicos, desde, hasta, nombre_categoria, id_estatus, nombre_estatus);

    if (desde == 'null' && hasta != 'null') {
        alert('DEDE INDICAR EL CAMPO DESDE');

    } else if (hasta == 'null' && desde != 'null') {
        alert('DEDE INDICAR EL CAMPO HASTA');
    } else if (hasta < desde) {
        alert('EL CAMPO DESDE ES MAYOR AL CAMPO HASTA')
    }

});
$(document).on('click', '#btnlimpiar', function(e) {
    e.preventDefault();
    $("#table_relacion_entradas").dataTable().fnDestroy();
    $("#cronico").prop('disabled', true);
    $('#cronico').val('0');
    $('#cmbCategoria').val('0');
    $('#id_estatus').val('0');
    let id_categoria = $('#cmbCategoria').val();
    let desde = $('#desde').val();
    let hasta = $('#hasta').val();
    let id_medico = $('#cmbmedicos').val();
    let id_estatus = $('#id_estatus').val();

    let nombre_estatus = 'Seleccione'

    let cedula_trabajador = $('#cedula_cortesia').val();
    if (desde == '' && hasta == '') {

        desde = 'null'
        hasta = 'null'
    }
    let nombre_categoria = 'Seleccione'
    let cronicos = 'Seleccione';


    listar_entradas_medicamentos(id_categoria, cronicos, desde, hasta, nombre_categoria, id_estatus, nombre_estatus);

    $("#cronico").prop('disabled', true);

});