/*
 *Este es el document ready
 */
 $(function()
 {
  
     //$( "#fecha" ).datepicker({dateFormat:'dd/mm/yy',changeMonth:true, changeYear:true});	
     //$( "#fecha1" ).datepicker({dateFormat: 'dd/mm/yy',changeMonth:true, changeYear:true});	
     listarentradas();
     //alert($('#id_medicamento').val());
 });
 /*
  * Función para definir datatable:
  */
 function listarentradas()
 {
     let ruta_imagen =rootpath
     let descripcion = $('#descripcion').val();
     let total_entradas = $('#total_entradas').val();
     let total_salidas = $('#total_salidas').val();
     
     var now = new Date();
     var day = ("0" + now.getDate()).slice(-2);
     var month = ("0" + (now.getMonth() + 1)).slice(-2);
     var today= day+"/"+month+"/"+now.getFullYear();
     var encabezado=''; 
     
     if(descripcion!='') 
     { 
          encabezado=encabezado+'DESCRIPCION:'+' '+' '+descripcion+' '+' ';  
     }
     if(total_entradas!='') 
     { 
          encabezado=encabezado+' TOTAL ENTRADAS:'+' '+' [ '+total_entradas+' ]'+' ';  
     }
  

     var table = $('#table_entradas').DataTable( {
          
          dom: "Bfrtip",
          buttons:{
              dom: {
                  button: {
                      className: 'btn-xs-xs'
                  },
                  
              },
              
              buttons: [
              {
                  //definimos estilos del boton de pdf
                  extend: "pdf",
                  text:'PDF',
                  className:'btn-xs btn-dark',
                 // title:'RELACION ENTRADAS MEDICAMENTOS',
                  header:true,
                  footer: true,
                  download: 'open',
                  exportOptions: {
                              columns: [0,1,2,3],
                              
                  },   
                  alignment: 'center',
                  
                  customize:function(doc)
                  {                       
                  //Remove the title created by datatTables
                       doc.content.splice(0,1);
                       doc.styles.title= 
                       { 
                            color: '#4c8aa0',
                            fontSize: '18',
                            alignment: 'center'                     
                       }
                       doc.styles['td:nth-child(2)'] = 
                       {
                            width: '130px',
                            'max-width': '130px'
                       },
                       doc.styles.tableHeader = 
                       {
                            fillColor:'#4c8aa0',
                            color:'white',
                            alignment:'center'
                       }, 
             // Create a header
             doc.pageMargins = [140,95,15,30];
             doc['header'] = (function (page, pages)
             {                      
                  doc.styles.title = 
                  {    
                       color: '#4c8aa0',
                       fontSize: '18',
                       alignment: 'center',  
                  } 
                  return {  
                            columns:
                            [
                                 {                              
                                   margin: [ 40, 3, 40, 40 ],                                 
                                      image:ruta_imagen,
                                      width: 500,  
                                                               
                                 },                        
                                 {
                                      margin :[ -470, 50, -25, 0 ],
                                      color: '#4c8aa0',
                                      fontSize: '18',
                                      alignment: 'center' , 
                                      text: 'DETALLES ENTRADAS DEL MEDICAMENTO',
                                      fontSize: 18,                             
                                 },
                                 {
                                   margin :[ -410, 80, -25, 0 ],     
                                   text:encabezado,
                                   },
                                 
                               
                            ], 
                       }                               
              }); 
             // Create a footer
             doc['footer'] = (function (page, pages)
             {    
                  return {
                            columns:
                            [
                                 {
                                      alignment: 'center',
                                      text: ['pagina ', { text: page.toString() },	' of ',	{ text: pages.toString() }]
                                 }
                            ],               
                            }                            
                  });
    
                  },
             },
 
              {    
                   //definimos estilos del boton de excel
                   extend: "excel",
                   text:'Excel',
                   className:'btn-xs btn-dark',
                   title:'DETALLES ENTRADAS MEDICAMENTOS', 
                  
                   download: 'open',
                   exportOptions: {
                     columns: [0,1,2,3,4,5],
                   },
                   excelStyles: {                                                
                     "template": [
                         "blue_medium",
                         "header_blue",
                         "title_medium"
                     ]                                  
                 },
 
              }
              ]            
          }, 




            "order":[[0,"asc"]],					
            "paging":true,
            "info":true,
            "filter":true,
            "responsive": true,
            "autoWidth": true,					
            //"stateSave":true,
            "ajax":
            {
             "url":base_url+"/listar_entradas/"+$('#id_medicamento').val(),
             "type":"GET",
             dataSrc:''
            },
            "columns":
            [
                {data:'id'},
                {data:'fecha_entrada'},
                {data:'fecha_vencimiento'},                
                {data:'entradas'},     
                {data:'salidas'},
                {data:'stock'},
                {orderable: true,
                    render:function(data, type, row)
                    {
                     return '<a href="javascript:;" class="btn btn-xs reversar" style=" font-size:1px" data-toggle="tooltip" title="Reversar" id='+row.id+' fecha_entrada="'+row.fecha_entrada+'" fecha_vencimiento='+row.fecha_vencimiento+' entradas='+row.entradas+'> <i class="material-icons " >autorenew</i></a>'+'<a href="javascript:;" class="btn btn-xs Editar" style=" font-size:1px" data-toggle="tooltip" title="Editar" id='+row.id+' fecha_entrada="'+row.fecha_entrada+'" fecha_vencimiento='+row.fecha_vencimiento+' entradas='+row.entradas+'> <i class="material-icons " >create</i></a>'
                       }
                   }
               
            ],
            "language":
            {
             "sProcessing":    "Procesando...",
             "sLengthMenu":    "Mostrar _MENU_ registros",
             "sZeroRecords":   "No se encontraron resultados",
             "sEmptyTable":    "Ningún dato disponible en esta tabla",
             "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
             "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
             "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
             "sInfoPostFix":   "",
             "sSearch":        "Buscar:",
             "sUrl":           "",
             "sInfoThousands":  ",",
             "sLoadingRecords": "Cargando...",
             "oPaginate": 
             {
              "sFirst":    "Primero",
              "sLast":    "Último",
              "sNext":    "Siguiente",
              "sPrevious": "Anterior"
             },
             "oAria":
             {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
             },
             "columnDefs": 
             [
                {
                 "targets": [ 0 ],
                 "visible": false,
                 "searchable": false
             }
             ],			
            }
       });
 }
 $('#btnAgregar').on('click',function(e)
 { 
      $('#modal').find('#btnGuardar').show();
      $('#modal').find('#btnActualizar').hide();
      $('#modal').find('#borrado').hide();
      $('#modal').find('#activo').hide();
      $("#modal").modal("show");
      $('#fecha1').val('');
      $('.modal_reversar').show();
      $('.observacion').hide();
      $('#cantidad').val('');
      $('#modal').find('#btnActualizar_fechav').hide();
      var input = document.getElementById("cantidad");
      input.disabled = false;
      var input_2 = document.getElementById("fecha");
      input_2.disabled = false;

 });
 



 $(document).on('click','#btnGuardar', function(e)
 {
     e.preventDefault();
     //var accion='REALIZO UNA ENTRADA'
     var id_medicamento=$('#id_medicamento').val();
     var fecha_entrada=$('#fecha').val();
     var fecha_vencimiento=$('#fecha1').val();
     var cantidad=$('#cantidad').val();

  


     if (fecha_vencimiento=='') {
          alert('DEBE INGRESAR LA FECHA DE VENCIMIENTO');  
     }

     else if ( cantidad==''  ) {
        alert('DEBE INGRESAR UNA CANTIDAD');  
     }else
     {

    
     var url='/agregar_entrada';
     var ruta_regreso='/vistaEntradas/'+id_medicamento;
     var data=
    {
         id_medicamento    :id_medicamento,
         fecha_entrada     :fecha_entrada,
         fecha_vencimiento :fecha_vencimiento,
         cantidad          :cantidad,
       //  accion          :accion,
    }
        $.ajax
     ({
         url:url,
         method:'POST',
         data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
         dataType:'JSON',
         beforeSend:function(data)
         {
         },
         success:function(data)
         {
             
         // alert(data);
              if(data=='1')
              {
                   alert('Registro Incorporado ');
               
                   
                   window.location = ruta_regreso;
              }
              else
              {
                   alert('Error en la Incorporación del Registro');
              }
         },
         error:function(xhr, status, errorThrown)
         {
              alert(xhr.status);
              alert(errorThrown);
         }
     });
}
});



 $(document).on('click','#btnRegresar ', function(e)
 {
     e.preventDefault();
     window.location = '/vistamedicamentos/';
     
 });

 $('#lista_medicamento').on('click','.reversar', function(e)
 {
     var id_entrada   =$(this).attr('id');
     var fecha_vencimiento   =$(this).attr('fecha_vencimiento');
     var fecha_entrada   =$(this).attr('fecha_entrada');
     var cantidad   =$(this).attr('entradas');
     $('#modal').find('#btnGuardar').hide();
     $('#modal').find('#btnActualizar_fechav').hide();
     $('#modal').find('#btnActualizar').show();
     $('#modal').find('#borrado').show();
     $('#modal').find('#activo').show();
     $("#modal").modal("show");
     $('#id_entrada').val(id_entrada);
     $('#fecha').val(fecha_entrada);
     $('#fecha1').val(fecha_vencimiento);
     $('#cantidad').val(cantidad);

     $('.modal_reversar').hide();
     $('.observacion').show();
     
   

     var borrado='f';

     if(borrado=='f')
     {
          $('#borrado').removeAttr('checked')
     }
    else if(borrado=='t')
     {
          $('#borrado').attr('checked','checked');
     }


     var borrado='f';

     if(borrado=='f')
     {
          $('#borrado').removeAttr('checked')
     }
    else if(borrado=='t')
     {
          $('#borrado').attr('checked','checked');
     }
 

 });



 $(document).on('click','#borrado ', function(e)
 {
    
     $("#btnActualizar").prop('disabled', false);
     
 });
// ************************METODO PARA GUARDAR EL REVERSO DEL MEDICAMENTO************************
$(document).on('click','#btnActualizar ', function(e)
{
     e.preventDefault();
     var fecha_entrada=$('#fecha').val();
     var fecha_vencimiento=$('#fecha1').val();
     var cantidad=$('#cantidad').val();
     var id_entrada =$('#id_entrada ').val();
     var id_medicamento =$('#id_medicamento').val();
     var estatus ='false';

     if($('#borrado').is(':checked'))
     {
          estatus='true';
     }
     else
     {
          estatus='false';
     }
   
    var data=
    {
         id_entrada         :id_entrada,
         estatus           :estatus,   
         id_medicamento    :id_medicamento,
         fecha_entrada     :fecha_entrada,
         fecha_vencimiento :fecha_vencimiento,
         cantidad          :cantidad,
  
    }
   var url='/actualizar_entrada';
   $.ajax
   ({
         url:url,
         method:'POST',
         data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
         dataType:'TEXT',
         beforeSend:function(data)
         {
              //alert('Procesando Información ...');
         },
         success:function(data)
         {
          //alert(data);
          
            if(data==1)
            {
              alert('Reverso Exitoso');
            }
           else if(data==2)
            {
              alert('Erro en el Proceso de reverso');
            }
            else
            {
              alert('Error, hay salidas asociadas a esta entrada del medicamento, por Favor realize el reverso de la salida primero');
            }
            window.location = '/vistaEntradas/'+id_medicamento;             
          
         },
         error:function(xhr, status, errorThrown)
         {
            alert(xhr.status);
            alert(errorThrown);
         }
    });
});

$('#lista_medicamento').on('click','.Editar', function(e)
{

     var id_entrada   =$(this).attr('id');
     var fecha_vencimiento   =$(this).attr('fecha_vencimiento');
     var fecha_entrada   =$(this).attr('fecha_entrada');
     var cantidad   =$(this).attr('entradas');
     var input = document.getElementById("cantidad");
     input.disabled = true;
     var input_2 = document.getElementById("fecha");
     input_2.disabled = true;

     $('.borrado').prop('checked', false);
     $('#modal').find('#btnGuardar').hide();
     $('#modal').find('#btnActualizar').hide();
     $('#modal').find('#btnActualizar_fechav').show();
     $("#modal").modal("show");
     $('#modal').find('#fecha1').val(fecha_vencimiento);
     $('#modal').find('#fecha').val(fecha_entrada);
     $('#modal').find('#cantidad').val(cantidad);
     $('#modal').find('#id_entrada').val(id_entrada);
        
});

// ************************METODO PARA ACTULIZAR  la fecha de vencimiento ************************
$(document).on('click','#btnActualizar_fechav ', function(e)
{
     e.preventDefault();
   

     var id_entrada=$('#id_entrada').val();
     var id_medicamento =$('#id_medicamento').val();
     var fecha_entrada=$('#fecha').val();
     var fecha_vencimiento=$('#fecha1').val();
     var cantidad=$('#cantidad').val();
     var estatus='false'
     
    
    var data=
    {
         id_entrada         :id_entrada,  
         id_medicamento    :id_medicamento,
         fecha_entrada:fecha_entrada,
         fecha_vencimiento :fecha_vencimiento,
         cantidad:cantidad,
         estatus:estatus,	
		
    }  
    
   var url='/actualizar_fecha_v';


   $.ajax
   ({
         url:url,
         method:'POST',
         data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
         dataType:'JSON',
         beforeSend:function(data)
         {
              //alert('Procesando Información ...');
         },
         success:function(data)
         {
          //console.log(data);
            //alert(data);
            if(data===1)
            {
              alert('Registro Actualizado');
            }
            else
            {
              alert('Error en la Incorporación del registro');
            }
            window.location = '/vistaEntradas/'+id_medicamento;             
         },
         error:function(xhr, status, errorThrown)
         {
            alert(xhr.status);
            alert(errorThrown);
         }
    });
    
});


 