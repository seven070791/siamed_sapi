/*
 *Este es el document ready
 */

 $(function()
 {
     
     listarentradas();
     $('#modalEntradasParaDarSalida').modal('show'); 
     $('#modalEntradasParaDarSalida').find('#btnGuardar').show();
     $('#modalEntradasParaDarSalida').find('#btnActualizar').hide();
     $('#modalEntradasParaDarSalida').find('#borrado').hide();
     $('#modalEntradasParaDarSalida').find('#activo').hide();
    
     //alert($('#id_medicamento').val());
 });

 function  llenar_combo_especialidad(e,id_especialidad)
 {
    
     e.preventDefault;
     
       url='/listar_especialidades_activas';
        $.ajax
       ({
            url:url,
            method:'GET',
           dataType:'JSON',
           beforeSend:function(data)
           {
           },
           success:function(data)
           {   
 if(data.length>=1)
 {
      $('#especialidad').empty();
      $('#especialidad').append('<option value=0>Seleccione</option>');     
      if(id_especialidad===undefined)
     {
          
          $.each(data, function(i, item)
          {
               //console.log(data)
               $('#especialidad').append('<option value='+item.id_especialidad+'>'+item.descripcion+'</option>');

          });
     }
     else
     {
          $.each(data, function(i, item)
          {
              

               if(item.id_especialidad===id_especialidad)
              
               {
                    $('#especialidad').append('<option value='+item.id_especialidad+' selected>'+item.descripcion+'</option>');
               }
               else
               {
                    $('#especialidad').append('<option value='+item.id_especialidad+'>'+item.descripcion+'</option>');
               }
          });
     }
 }      
 },
 error:function(xhr, status, errorThrown)
 {
     alert(xhr.status);
     alert(errorThrown);
 }
 });
 }
 function  llenar_combo_MedicoTratante(e,id)
 {

     e.preventDefault;
     
       url='/listar_medicos_activos';
        $.ajax
       ({
            url:url,
            method:'GET',
           dataType:'JSON',
           beforeSend:function(data)
           {
           },
           success:function(data)
           {

 if(data.length>=1)
 {
      $('#cmbmedicotratante').empty();
      $('#cmbmedicotratante').append('<option value=0>Seleccione</option>');     
      if(id===undefined)
     {
          
          $.each(data, function(i, item)
          {
               //console.log(data)
               $('#cmbmedicotratante').append('<option value='+item.id+'>'+item.nombre+'  '+item.apellido+'</option>');

          });
     }
     else
     {
          
          $.each(data, function(i, item)
          {
              

               if(item.id===id)
              
               {
                    $('#cmbmedicotratante').append('<option value='+item.id+' selected>'+item.nombre+'  '+item.apellido+'</option>');
               }
               else
               {
                    $('#cmbmedicotratante').append('<option value='+item.id+'>'+item.nombre+'  '+item.apellido+'</option>');
               }
          });
     }
 }      
 },
 error:function(xhr, status, errorThrown)
 {
     alert(xhr.status);
     alert(errorThrown);
 }
 });
 }




 
 function  llenar_combo_MedicoReferido(e,id_especialidad)
 {

     e.preventDefault;
     
       url='/listar_medicos_activos';
        $.ajax
       ({
            url:url,
            method:'GET',
           //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
           dataType:'JSON',
           beforeSend:function(data)
           {
           },
           success:function(data)
           {
           
 if(data.length>=1)
 {
      $('#cmbmedicosreferidos').empty();
      $('#cmbmedicosreferidos').append('<option value=0  selected disabled>Medicos</option>');   
      verificar=7;  
      if(id_especialidad===undefined)
     {
          
          $.each(data, function(i, item)
          {
               //console.log(data)
               $('#cmbmedicosreferidos').append('<option value='+item.id+'>'+item.nombre+'  '+item.apellido+'</option>');

          });
     }
     else
     {
        data=data.filter(dato=>dato.id_especialidad==id_especialidad);
        //console.log(buscar);
           $.each(data, function(i, item)
           {
            $('#cmbmedicosreferidos').append('<option value='+item.id+'>'+item.nombre+'  '+item.apellido+'</option>');

              
          });
     }
 }      
 },
 error:function(xhr, status, errorThrown)
 {
     alert(xhr.status);
     alert(errorThrown);
 }
 });
 }

 $("#especialidad").on('change', function(e)
 {
     document.getElementById("cmbmedicosreferidos").disabled=false;
    id_especialidad=$('#especialidad option:selected').val(); 

    llenar_combo_MedicoReferido(e,id_especialidad)
 }); 


 /*
  * Función para definir datatable:
  */
 function listarentradas()
 {

     let ruta_imagen =rootpath
     let descripcion = $('#descripcion').val();
     let total_salidas = $('#total_salidas').val();
    
     
     var now = new Date();
     var day = ("0" + now.getDate()).slice(-2);
     var month = ("0" + (now.getMonth() + 1)).slice(-2);
     var today= day+"/"+month+"/"+now.getFullYear();
     var encabezado=''; 
     
     if(descripcion!='') 
     { 
          encabezado=encabezado+'DESCRIPCION:'+' '+' '+descripcion+' '+' ';  
     }
     if(total_salidas!='') 
     { 
          encabezado=encabezado+' TOTAL SALIDAS:'+' '+' [ '+total_salidas+' ]'+' ';  
     }
  
     var table = $('#table_entradas').DataTable( {
          
          dom: "Bfrtip",
          buttons:{
              dom: {
                  button: {
                      className: 'btn-xs-xs'
                  },
                  
              },
              
              buttons: [
              {
                  //definimos estilos del boton de pdf
                  extend: "pdf",
                  text:'PDF',
                  className:'btn-xs btn-dark',
                 // title:'RELACION ENTRADAS MEDICAMENTOS',
                  header:true,
                  footer: true,
                  download: 'open',
                  exportOptions: {
                              columns: [0,1,2,4,5],
                              
                  },   
                  alignment: 'center',
                  
                  customize:function(doc)
                  {                       
                  //Remove the title created by datatTables
                       doc.content.splice(0,1);
                       doc.styles.title= 
                       { 
                            color: '#4c8aa0',
                            fontSize: '18',
                            alignment: 'center'                     
                       }
                       doc.styles['td:nth-child(2)'] = 
                       {
                            width: '130px',
                            'max-width': '130px'
                       },
                       doc.styles.tableHeader = 
                       {
                            fillColor:'#4c8aa0',
                            color:'white',
                            alignment:'center'
                       }, 
             // Create a header
             doc.pageMargins = [140,95,15,30];
             doc['header'] = (function (page, pages)
             {                      
                  doc.styles.title = 
                  {    
                       color: '#4c8aa0',
                       fontSize: '18',
                       alignment: 'center',  
                  } 
                  return {  
                            columns:
                            [
                                 {                              
                                   margin: [ 40, 3, 40, 40 ],                                 
                                      image:ruta_imagen,
                                      width: 500,  
                                                               
                                 },                        
                                 {
                                      margin :[ -470, 50, -25, 0 ],
                                      color: '#4c8aa0',
                                      fontSize: '18',
                                      alignment: 'center' , 
                                      text: 'DETALLES SALIDAS  DEL MEDICAMENTO',
                                      fontSize: 18,                             
                                 },
                                 {
                                   margin :[ -410, 80, -25, 0 ],     
                                   text:encabezado,
                                   },
                                 
                               
                            ], 
                       }                               
              }); 
             // Create a footer
             doc['footer'] = (function (page, pages)
             {    
                  return {
                            columns:
                            [
                                 {
                                      alignment: 'center',
                                      text: ['pagina ', { text: page.toString() },	' of ',	{ text: pages.toString() }]
                                 }
                            ],               
                            }                            
                  });
    
                  },
             },
 
              {    
                   //definimos estilos del boton de excel
                   extend: "excel",
                   text:'Excel',
                   className:'btn-xs btn-dark',
                   title:'DETALLES ENTRADAS MEDICAMENTOS', 
                  
                   download: 'open',
                   exportOptions: {
                     columns: [0,1,2,4,5],
                   },
                   excelStyles: {                                                
                     "template": [
                         "blue_medium",
                         "header_blue",
                         "title_medium"
                     ]                                  
                 },
 
              }
              ]            
          }, 


            "order":[[0,"desc"]],					
            "paging":true,
            "info":true,
            "filter":true,
            "responsive": true,
            "autoWidth":false,					
            //"stateSave":true,
            "ajax":
            {
             "url":base_url+"/listar_entradas/"+$('#id_medicamento').val(),
             "type":"GET",
             dataSrc:''
            },
            "columns":
            [


               {data:'id'},
               {data:'fecha_entrada'},
               {data:'fecha_vencimiento'},                
               {data:'entradas'},     
               {data:'salidas'},
               {data:'stock'},
              
              
     
               {orderable: true,
                    render:function(data, type, row)
                    {
                     if(row.vigencia=='Vencido')
                      {
                         return '<button id="btnvencido"class=" btn-danger btnvencido "disabled="disabled">'+row.vigencia+'</button>'
                      }
                      else
                      {
                         return '<button id="btnvigente"class=" btn-success btnvigente "disabled="disabled">'+row.vigencia+'</button>'
                       
                      } 
                   
                    }
               },
               {orderable: true,
                    render:function(data, type, row)
                    {
                     

                         return '<a href="javascript:;" class="btn btn-xs btn-primary  salida " style=" font-size:2px" data-toggle="tooltip" title="Retirar" id_entrada='+row.id+' estatus='+row.vigencia+' stock='+row.stock+' ><i class="material-icons ">remove_circle</i></a>'+' '+'<a href="javascript:;" class="btn btn-xs btn-secondary VerSalidasContraEntrada" style=" font-size:2px" data-toggle="tooltip" title="Detalles" id='+row.id+' rol='+row.descripcion+' estatus="'+row.estatus+'"><i class="material-icons ">search</i></a>'
                    
                   
                    }
               }

                
            ],
            "language":
            {
             "sProcessing":    "Procesando...",
             "sLengthMenu":    "Mostrar _MENU_ registros",
             "sZeroRecords":   "No se encontraron resultados",
             "sEmptyTable":    "Ningún dato disponible en esta tabla",
             "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
             "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
             "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
             "sInfoPostFix":   "",
             "sSearch":        "Buscar:",
             "sUrl":           "",
             "sInfoThousands":  ",",
             "sLoadingRecords": "Cargando...",
             "oPaginate": 
             {
              "sFirst":    "Primero",
              "sLast":    "Último",
              "sNext":    "Siguiente",
              "sPrevious": "Anterior"
             },
             "oAria":
             {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
             },
             "columnDefs": 
             [
                {
                 "targets": [ 0 ],
                 "visible": false,
                 "searchable": false
             }
             ],			
            }
       });
 }
 $('#btnAgregar').on('click',function(e)
 { 
      $('#modal').find('#btnGuardar').show();
      $('#modal').find('#btnActualizar').hide();
      $('#modal').find('#borrado').hide();
      $('#modal').find('#activo').hide();
      $("#modal").modal("show")
     

 });

 $('#lista_entradas_medicamento').on('click','.salida', function(e)

 {
 
     var id_entrada              =$(this).attr('id_entrada');
     var stock                   =$(this).attr('stock');

     var estatus                  =$(this).attr('estatus');
     if (estatus=='Vencido') {
       alert('ALERTA!!!, EL PRODUCTO EXPIRO')   
     }
    
     // $("#modalEntradasParaDarSalida").hide(5000);
      $("#modal_retirar").modal("show");
      document.getElementById("cedula_beneficiario").disabled=true;
    

      $("#btncerrarbeneficiario").hide();

      $("#cajas").hide();
      $("#beneficiarios").css("display", "none");
     
      $("#labelcedulat").hide();
      $("#label_titular").hide(); 
      $("#labelcantidad").hide(); 
      $("#cantidad").hide(); 
      $("#btnGuardarSalida").hide();
    

     
     
      $("#nombre_titular").hide(); 
      $("#verificar").hide();
      $('#modal_retirar').find('#btnGuardar').show();
      $('#modal_retirar').find('#btnActualizar').show();
      $('#modal_retirar').find('#borrado').show();
      $('#modal_retirar').find('#activo').show();
      $('#stock').val(stock);
      $('#id_entrada').val(id_entrada);
     
 });

 
 $(document).on('click','#Radio1', function(e)
 {
     document.getElementById("cedula_beneficiario").disabled=false;
     $("#beneficiarios").css("display", "none");
     $("#cedula_beneficiario").val('');
     $("#nombre_beneficiario").val('');
     $("#apellido_beneficiario").val('');
     $("#verificar").show();  
     $("#btntitulares").show();
     $("#btnfamiliares").hide();  
     $("#btncortesia").hide(); 
     $("#cajas").hide(); 
     $("#labelcedulat").hide(); 
     $("#nombre_beneficiario ").hide(); 
     $("#label_nombre").hide();
     $("#apellido_beneficiario ").hide(); 
     $("#label_apellido").hide();    
     $("#label_titular").hide();
     $("#nombre_titular").hide();
     $("#labelcantidad").hide(); 
     $("#cantidad").hide(); 
     $("#btnGuardarSalida").hide();
    
 });

 $(document).on('click','#Radio2', function(e)
 {
     document.getElementById("cedula_beneficiario").disabled=false;
     $("#beneficiarios").css("display", "none");
     $("#labelcedulat").hide(); 
      $("#cedula_beneficiario").val('');
      $("#nombre_beneficiario").val('');
      $("#apellido_beneficiario").val('');
      $("#verificar").show();  
      $("#btntitulares").hide();  
      $("#btnfamiliares").show(); 
      $("#btncortesia").hide();   
      $("#cajas").hide();  
      $("#nombre_beneficiario ").hide(); 
      $("#label_nombre").hide();
      $("#apellido_beneficiario ").hide(); 
      $("#label_apellido").hide();  
      $("#label_titular").hide();
      $("#nombre_titular").hide(); 
      $("#labelcantidad").hide(); 
      $("#cantidad").hide(); 
      $("#btnGuardarSalida").hide();

    
 });

 $(document).on('click','#Radio3', function(e)
 {
     document.getElementById("cedula_beneficiario").disabled=false;
     $("#beneficiarios").css("display", "none");
     $("#labelcedulat").hide(); 
     $("#cedula_beneficiario").val('');
     $("#nombre_beneficiario").val('');
     $("#apellido_beneficiario").val('');
     $("#verificar").show();  
     $("#btncortesia").show();
     $("#btnfamiliares").hide();  
     $("#btntitulares").hide();   
     $("#cajas").hide(); 
     $("#nombre_beneficiario ").hide(); 
     $("#label_nombre").hide();
     $("#apellido_beneficiario ").hide(); 
     $("#label_apellido").hide(); 
     $("#label_titular").hide();
     $("#nombre_titular").hide();
     $("#labelcantidad").hide(); 
     $("#cantidad").hide(); 
     $("#btnGuardarSalida").hide();
 });


 $(document).on('click','#btntitulares', function(e)
 {
     var cedula_beneficiario = $('#cedula_beneficiario').val();
     url='/buscartitular';
     data=
     {
          cedula_beneficiario:cedula_beneficiario
     };

     if(cedula_beneficiario=='')
     {
         alert('POR FAVOR INGRESE UNA CEDULA');    
     }
     else
     {


 
       $.ajax(
            {
                 url:url,
                 method:'GET',
                data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
                dataType:'JSON',
                beforeSend:function(data)
                {
                     //alert('Procesando');
                },
                success:function(data)
                {
               
                if(data.borrado=='INACTIVO')
                {
                    alert('El TITULAR SE ENCUENTRA INACTIVO');
                }
                else
                {
                    $("#btncerrarbeneficiario").show();                   
                    $('#datosbeneficiario').prop('disabled', false);
                    llenar_combo_especialidad(Event)
                    llenar_combo_MedicoTratante(Event)
                    document.getElementById("cmbmedicosreferidos").disabled=true;
                    $("#cajas").show();  
                    $("#nombre_beneficiario ").show(); 
                    $("#label_nombre").show();
                    $("#apellido_beneficiario ").show(); 
                    $("#label_apellido").show();      
                    $("#modal_retirar").modal("show");
                    $('#modal_retirar').find('#nombre_beneficiario').val(data.nombre); 
                    $('#modal_retirar').find('#apellido_beneficiario').val(data.apellido);
                    $('#modal_retirar').find('#tipo_beneficiario').val('T');
                    $("#beneficiarios").css("display", "block");
                    $("#label_titular").show(); 
                    $("#labelcantidad").show(); 
                    $("#cantidad").show(); 
                    $("#btnGuardarSalida").show();
                }
                },
                error:function(xhr, status, errorThrown)
                {
                   alert('LA CEDULA NO PERTENECE A UN TITULAR');
                }
           });
          }
 });

 $(document).on('click','#btnfamiliares', function(e)
 {
     var cedula_beneficiario = $('#cedula_beneficiario').val();
     url='/buscarfamiliar';
     data=
     {
          cedula_beneficiario:cedula_beneficiario
     };
     if(cedula_beneficiario=='')
     {
         alert('POR FAVOR INGRESE UNA CEDULA');    
     }
     else
     {
       $.ajax(
            {
                 url:url,
                 method:'GET',
                data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
                dataType:'JSON',
                beforeSend:function(data)
                {
                     //alert('Procesando');
                },
                success:function(data)
                {            
                if(data.borrado=='t')
                    {
                        alert('El FAMILIAR SE ENCUENTRA INACTIVO');
                    }
               else
               {
                    $("#btncerrarbeneficiario").show();
                    llenar_combo_especialidad(Event)
                    llenar_combo_MedicoTratante(Event)
                    document.getElementById("cmbmedicosreferidos").disabled=true;              
                    $('#datosbeneficiario').prop('disabled', false);
                    $("#cajas").show();  
                    $("#nombre_beneficiario ").show(); 
                    $("#label_nombre").show();
                    $("#apellido_beneficiario ").show(); 
                    $("#label_apellido").show();      
                    $("#modal_retirar").modal("show");
                    $('#modal_retirar').find('#nombre_beneficiario').val(data.nombre); 
                    $('#modal_retirar').find('#apellido_beneficiario').val(data.apellido);
                    $('#modal_retirar').find('#nombre_titular').val(data.nombre_titular + ' ' + data.apellido_titular);
                    $("#label_titular").show(); 
                    $("#nombre_titular").show(); 
                    $('#modal_retirar').find('#tipo_beneficiario').val('F');
                    $("#labelcedulat").show(); 
                    $("#beneficiarios").css("display", "block");
                    $("#label_titular").show(); 
                    $("#labelcantidad").show(); 
                    $("#cantidad").show(); 
                    $("#btnGuardarSalida").show();
               }
                },
                error:function(xhr, status, errorThrown)
                {
                   alert('LA CEDULA NO PERTENECE A UN FAMILIAR');
                }
           });   
          }    
 });
 $(document).on('click','#btncortesia', function(e)
 {
   
     var cedula_beneficiario = $('#cedula_beneficiario').val();
     url='/getAllBuscarCortesia';
     data=
     {
          cedula_beneficiario:cedula_beneficiario
     };
     if(cedula_beneficiario=='')
     {
         alert('POR FAVOR INGRESE UNA CEDULA');    
     }
     else
     {
       $.ajax(
            {
                 url:url,
                 method:'GET',
                data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
                dataType:'JSON',
                beforeSend:function(data)
                {
                     //alert('Procesando');
                },
                success:function(data)
                {
                    if(data.borrado=='t')
                    {
                        alert('El BENEFICIARIO POR CORTESIA  SE ENCUENTRA INACTIVO');
                    }
               else
               {
                    $("#btncerrarbeneficiario").show();
                    $('#datosbeneficiario').prop('disabled', false);
                    llenar_combo_especialidad(Event)
                    llenar_combo_MedicoTratante(Event)
                    document.getElementById("cmbmedicosreferidos").disabled=true;
                    $("#cajas").show();  
                    $("#nombre_beneficiario ").show(); 
                    $("#label_nombre").show();
                    $("#apellido_beneficiario ").show(); 
                    $("#label_apellido").show();      
                    $("#modal_retirar").modal("show");
                    $('#modal_retirar').find('#nombre_beneficiario').val(data.nombre); 
                    $('#modal_retirar').find('#apellido_beneficiario').val(data.apellido);
                    $('#modal_retirar').find('#nombre_titular').val(data.nombre_titular + ' ' + data.apellido_titular);
                    $("#label_titular").show(); 
                    $("#nombre_titular").show(); 
                    $('#modal_retirar').find('#tipo_beneficiario').val('C');
                    $("#labelcedulat").show(); 

                    $("#beneficiarios").css("display", "block");
                    $("#label_titular").show(); 
                    $("#labelcantidad").show(); 
                    $("#cantidad").show(); 
                    $("#btnGuardarSalida").show();
               }
                },
                error:function(xhr, status, errorThrown)
                {
                   alert('LA CEDULA NO PERTENECE A BENEFICIARIO POR CORTESIA');
                }
           });   
          }
 });






$(document).on('click','#btnGuardarSalida', function(e)
{

     var  control=$('#id_control_x').val();
     e.preventDefault();
     var  stock=$('#stock').val();
     var  stock=stock.trim();
     var  cantidad=$('#cantidad').val(); 
     var  cantidad=cantidad.trim();
     stock = (isNaN(parseInt(stock)))? 0 : parseInt(stock);
     cantidad = (isNaN(parseInt(cantidad)))? 0 : parseInt(cantidad);
     if($('#especialidad').val()==0) 
     {
          alert('Debe Seleccionar una  especialidad');   
     }
     else if($('#cmbmedicosreferidos').val()>=1) 
     {  
     
          if (cantidad<=0 || cantidad==='NaN') 
          {
           alert('Por Favor Ingrese Una Cantidad Real');
          }
          else if (cantidad<=stock)
          {   
               var id_medico=$('#cmbmedicosreferidos').val();
               var cedula_beneficiario =$('#cedula_beneficiario').val();  
               var tipo_beneficiario=$('#tipo_beneficiario').val();  
               var  id_medicamento=$('#id_medicamento').val();
               var  id_entrada=$('#id_entrada').val();
               var  fecha=$('#fecha').val();

               let fechaconvertida=moment(fecha,'DD-MM-YYYY'); 
               fechaconvertida=fechaconvertida.format('YYYY-MM-DD');
               var url='/agregar_salida';
               var ruta_regreso='/vista_salidas/'+id_medicamento;
               var data=
               {
                    id_medicamento    :id_medicamento,
                    id_entrada        :id_entrada,
                    cantidad          :cantidad,
                    tipo_beneficiario:tipo_beneficiario,
                    cedula_beneficiario:cedula_beneficiario,
                    control:control,
                    id_medico:id_medico,
                    fechaconvertida:fechaconvertida,
               }

               
             // console.log(data);
               $.ajax
               ({
                    url:url,
                    method:'POST',
                    data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
                    dataType:'text',
                    beforeSend:function(data)
                    {
                    },
                    success:function(data)
                    {                             
                         if(data=='1')
                         {
                              alert('Retiro Correcto ');
                              window.location = ruta_regreso;
                         }else
                               {
                                   alert('Error en la Incorporación del Registro');
                               }
                    },
                    error:function(xhr, status, errorThrown)
                    {
                         alert(xhr.status);
                         alert(errorThrown);
                    }
               });  
          }else
               {
                alert('La Cantidad Ingresada supera el Stock ');
               }
     }else 
     {
          alert('Debe Seleccionar un Medico'); 
     }


     
    
});



 $('#lista_entradas_medicamento').on('click','.VerSalidasContraEntrada', function(e)
{ 
     var id_medicamento  =$('#id_medicamento').val();  
     var id_entrada      =$(this).attr('id');  
    // window.location = '/vista_salidas_contra_entrada/'+id+descripcion; 
     window.location = '/VistaSalidasContraEntrada/'+id_entrada+'/'+id_medicamento; 
});


 $(document).on('click','#btnRegresar ', function(e)
 {
     e.preventDefault();
     window.location = '/vistamedicamentos/';
 });
 
  $(document).on('click','#btnclose ', function(e)
{
     e.preventDefault();
     var id_medicamento = $('#id_medicamento').val();
     var ruta_regreso='/vista_salidas/'+id_medicamento;
     window.location=ruta_regreso;
  });



  $(document).on('click','#btncerrarbeneficiario ', function(e)
  {  
  
     $("#beneficiarios").css("display", "none")
     $("#labelcantidad").hide(); 
     $("#cantidad").hide(); 
     $("#btnGuardarSalida").hide();
     $("#btnclose").show(); 
     $("#btncerrarbeneficiario").hide(); 
     $("#cedula_beneficiario").val('');
     $("#nombre_beneficiario").val('');
     $("#apellido_beneficiario").val('');
     $("#nombre_titular").val('');
     $("#labelcedulat").hide(); 
    
  });

  
  

 