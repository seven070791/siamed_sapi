/*
 *Este es el document ready
 */
 $(function()
 {
    var n_historial=$("#numeroHistorial").val();
    var cedula=$("#cedula").val();
   
   
 listarHistorialMedico(n_historial,cedula);
 });

/*
  * Función para definir datatable:
  */
function listarHistorialMedico(n_historial,cedula)
{

     
     $('#table_historial').DataTable
     (
      {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":base_url+"/getAllHistorialFamiliar/"+n_historial+'/'+cedula,
            "type":"GET",
            dataSrc:''
           },
           "columns":
           [
              
               {data:'n_historial'}, 
              {data:'nombre'},     
              {data:'apellido'},
              {data:'cedula'},
              {data:'fecha_nac'},
              {data:'fecha_regist'},
              {data:'borrado'},

               {orderable: true,
                   render:function(data, type, row)
                   {
                 return '<a href="javascript:;" class="btn btn-xs btn-info Consultar" style=" font-size:2px" data-toggle="tooltip" title="Consulta Medica" n_historial='+row.n_historial+' cedula='+row.cedula+' descripcion="'+row.descripcion+'" tipodesangre_id='+row.tipo_de_sangre+' estadocivil_id='+row.estado_civil+' fecha_creacion='+row.fecha_creacion+' estatus='+row.estatus+'> <i class="material-icons " >add_to_photos</i></a>'+' '+'<a href="javascript:;" class="btn btn-xs btn-success Citas" style=" font-size:2px" data-toggle="tooltip" title="Agendar Citas"  id='+row.id+' cedula='+row.cedula+' n_historial='+row.n_historial+' rol='+row.descripcion+' estatus="'+row.estatus+'"><i class="material-icons ">import_contacts</i></a> '+'<a href="javascript:;" class="btn btn-xs btn-primary Habitos" style=" font-size:2px" data-toggle="tooltip" title="Habitos Psicosociales" id='+row.id+' borrado='+row.borrado+' n_historial='+row.n_historial+' cedula='+row.cedula+' rol='+row.descripcion+' estatus="'+row.estatus+'"><i class="material-icons ">content_paste</i></a>'+' ' +'<a href="javascript:;" class="btn btn-xs btn-dark Bloquear" style=" font-size:2px" data-toggle="tooltip" title="Eliminar" id='+row.id+' borrado='+row.borrado+' cedula='+row.cedula+' rol='+row.descripcion+' estatus="'+row.estatus+'"><i class="material-icons ">delete_forever</i></a>'
                    }
               }
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           }
      });

}
 $('#btnAgregar_familiar').on('click',function(e)
 {   
    $("#modal_historial_medico").modal("show"); 


 });

 // ******************METODO PARA GENERAL EL NUMERO DE HISTORIAL ********************
$(document).on('click','#btnAgregarHistorial', function(e)
{
    e.preventDefault();
    var cedulatrabajador=$('#cedulatrabajador').val();
    var cedula=$('#cedulaF').val();
    var numero_historial=$('#numeroHistorial').val();
    var nombre=$('#nombre').val();
    var apellido=$('#apellido').val();
    var fecha_nac=$('#fecha_nacimiento').val();
    var fecha_registro=$('#fecha_del_dia').val();
   // ***SE USO EL  moment PARA CONVERTIR LA FECHA******
   let fechaconvertida=moment(fecha_registro,'DD-MM-YYYY'); 
   fechaconvertida=fechaconvertida.format('YYYY-MM-DD');

    var url='/agregar_historial_medico/'+cedula;


    var ruta_regreso='/datos_titular_familiares/'+cedulatrabajador+'/'+cedula;
    var data=
     {
        numero_historial        :numero_historial,
        cedula                  :cedula,               
        fecha_registro          :fechaconvertida,
          
     }
//console.log(data);
         $.ajax
      ({
          url:url,
          method:'POST',
          data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
          },
          success:function(data)
          {
               
               if(data=='1')
               {
                    alert('Registro Incorporado ');
                
                    
                    window.location = ruta_regreso;
               }
               else if(data=='0')
               {
                    alert('Error en la Incorporación del Registro');
               }

               else if(data=='2')
               {
                    alert('La cedula posee Historial');
               }


          },
          error:function(xhr, status, errorThrown)
          {
               alert(xhr.status);
               alert(errorThrown);
          }
      });   
});

$('#historial_medico').on('click','.Consultar', function(e)
{   

    e.preventDefault();
    var cedula=$('#cedula').val();
    var tipo_beneficiario=$('#tipo_beneficiario').val();
    var n_historial =$(this).attr('n_historial'); 
    window.location = '/vista_ConsultasFamiliares/'+cedula+'/'+tipo_beneficiario+'/'+n_historial;
});
 $(document).on('click','#btnRegresar_hacia_titulares ', function(e)
 {
     e.preventDefault();
     
     window.location = '/titulares/';
     
 });

 $('#historial_medico').on('click','.Citas', function(e)
 {   
     e.preventDefault();
     var cedula=$('#cedula').val();
     var tipo_beneficiario=$('#tipo_beneficiario').val();
     var n_historial =$(this).attr('n_historial');
     window.location = '/vista_Citas_Familiares/'+cedula+'/'+tipo_beneficiario+'/'+n_historial;
 });

 $('#historial_medico').on('click','.Habitos', function(e)
{   

    e.preventDefault();
    var cedula=$('#cedula').val();
    var cedulat=$('#cedulat').val();
    var n_historial =$(this).attr('n_historial'); 
    var historial_id=$(this).attr('id'); 
    var tipo_beneficiario=$('#tipo_beneficiario').val();
    window.location ='/vista_habitos/'+n_historial+'/'+cedula+'/'+historial_id+'/'+tipo_beneficiario+'/'+cedulat;

});


//  }


                 

$('#historial_medico').on('click','.Bloquear', function(e)    

{
     Swal.fire({
          title: 'Borrar el Registro?',
          icon:'warning',
          showCancelButton: true,
          cancelButtonColor:'#d33',
          confirmButtonText:'Confirmar',
          input:'text',
                   
          }).then((result)=> {
                 if(result.isConfirmed){ 
     
                    Swal.fire(  
                                                 
                         'BORRADO!',
                         'El Registro ha sido Borrado.',
                         'success',
                         {
                              
                         }
                       )
                       var cedula=$('#cedula_trabajador').val();
                       var historial_id =$(this).attr('id'); 
                       var borrado ='true'
                       var data=
                       {
                            historial_id:historial_id,
                            borrado     :borrado,
                       }               
                       var url='/borrar_historial';
                       $.ajax
                       ({
                            url:url,
                            method:'POST',
                            data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
                            dataType:'JSON',
                            beforeSend:function(data)
                            {
                                 //alert('Procesando Información ...');
                            },
                            success:function(data)
                            {
                               //alert(data);
                               if(data===1)
                               {
                                 
                                 
        
                               }
                               else
                               {
                                 alert('Error en la Incorporación del registro');
                               }
                               setTimeout(function () {
                                   window.location = '/datos_titular/'+cedula;  ; 
                                }, 1500);
                               //window.location = '/datos_titular/'+cedula;             
                            },
                            error:function(xhr, status, errorThrown)
                            {
                               alert(xhr.status);
                               alert(errorThrown);
                            }
                      });  
                      
                   }
                   
               })  
     
        
 });


//  