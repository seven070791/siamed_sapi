/*
 *Este es el document ready
 */
 $(function()
 {
     var historial_id=$("#historial_id").val();
    listarHistorialMedico(historial_id);
 });

/*
  * Función para definir datatable:
  */
function listarHistorialMedico(historial_id)
{
     $('#table_historial').DataTable
     (
      {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":base_url+"/listar_habitos_personales/"+historial_id,
            "type":"GET",
            dataSrc:''
           },
           "columns":
           [
              {data:'id'}, 
              {data:'descripcion'},     
            

               {orderable: true,
                   render:function(data, type, row)
                   {
                 return '<a href="javascript:;" class="btn btn-xs btn-secondary Bloquear" style=" font-size:2px" data-toggle="tooltip" title="Eliminar"  id='+row.id+'  borrado='+row.borrado+' cedula='+row.cedula+' rol='+row.descripcion+' estatus="'+row.estatus+'"><i class="material-icons ">delete_forever</i></a>'
                    }
               }
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           }
      });

}
 $('#btnAgregar').on('click',function(e)
 {   
    $("#modal_citas").modal("show"); 

    llenar_combo_habitos(Event);

 });
/*
* Función para llenar el combo tipo de Inventario
*/    
function llenar_combo_habitos(e,id)
{
      e.preventDefault;
      url='/listar_habitos';
      $.ajax
      ({
          url:url,
          method:'GET',
          //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
          },
          success:function(data)
          {
       
if(data.length>=1)
{
     $('#habitos_psicosociales').empty();
     $('#habitos_psicosociales').append('<option value=0 selected disabled >Seleccione</option>');     
     if(id===undefined)
     {
          $.each(data, function(i, item)
          {
               //console.log(data)
               $('#habitos_psicosociales').append('<option value='+item.id+'>'+item.descripcion+'</option>');

          });
     }
     else
     {
          $.each(data, function(i, item)
          {
               if(item.id===id)
               {
                    $('#habitos_psicosociales').append('<option value='+item.id+' selected>'+item.descripcion+'</option>');
               }
               else
               {
                    $('#habitos_psicosociales').append('<option value='+item.id+'>'+item.descripcion+'</option>');
               }
          });
     }
}      
},
error:function(xhr, status, errorThrown)
{
    alert(xhr.status);
    alert(errorThrown);
}
});
}

// ******************METODO PARA AGREGAR EL HABITO ********************
$(document).on('click','#btnGuardar', function(e)
{
     e.preventDefault();  
     var historial_id=$('#historial_id').val();
     var n_historial=$('#n_historial').val();
     var cedulat=$('#cedulat').val();
     var cedula=$('#cedula').val();
     var tipo_beneficiario=$('#tipo_beneficiario').val();
     var habitos_psicosociales=$('#habitos_psicosociales').val();
     var url='/agregar_habito_historial';
     var data=
     {
        historial_id          :historial_id,
        habitos_psicosociales :habitos_psicosociales,                        
     }

         $.ajax
      ({
          url:url,
          method:'POST',
          data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
          },
          success:function(data)
          {
               
               if(data=='1')
               {
                    alert('Registro Incorporado ');
                    
                    window.location ='/vista_habitos/'+n_historial+'/'+cedula+'/'+historial_id+'/'+tipo_beneficiario+'/'+cedulat;
                    
               }
               else if(data=='0')
               {
                    alert('Error en la Incorporación del Registro');
               }

               else if(data=='2')
               {
                    alert('La cedula posee Historial');
               }


          },
          error:function(xhr, status, errorThrown)
          {
               alert(xhr.status);
               alert(errorThrown);
          }
      });   
});

 $(document).on('click','#btnRegresar', function(e)
 {
     e.preventDefault();
     var historial_id=$('#historial_id').val();
     var cedula=$('#cedula').val();
     var cedulat=$('#cedulat').val();
     var tipo_beneficiario=$('#tipo_beneficiario').val();
     
     if (tipo_beneficiario=='T') {
       window.location = '/datos_titular/'+cedula;   
     }
     else if (tipo_beneficiario=='F') 
     {
          window.location = '/datos_titular_familiares/'+cedulat+'/'+cedula;   
     }
     else 
     {
          window.location = '/datos_titular_cortesia/'+cedulat+'/'+cedula;   
     }


 });

 $('#historial_habitos').on('click','.Bloquear', function(e)
 {   
 
     e.preventDefault();
     var tipo_beneficiario=$('#tipo_beneficiario').val();
     var historial_id=$('#historial_id').val();
     var n_historial=$('#n_historial').val();
     var cedulat=$('#cedulat').val();
     var cedula=$('#cedula').val();
     var habito_id =$(this).attr('id'); 
     var borrado =$(this).attr('borrado');
     var borrado='true';
     var data=
     {
          habito_id :habito_id,
          borrado   :borrado
     }
     var url='/borrar_habitos';
     $.ajax
     ({
          url:url,
          method:'post',
          data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
               //alert('Procesando Información ...');
          },
          success:function(data)
          {
             //alert(data);
             if(data===1)
             {
                alert('Registro Actualizado');
             }
             else
             {
               alert('Error en la Incorporación del registro');
             }
             window.location ='/vista_habitos/'+n_historial+'/'+cedula+'/'+historial_id+'/'+tipo_beneficiario+'/'+cedulat;      
          },
          error:function(xhr, status, errorThrown)
          {
             alert(xhr.status);
             alert(errorThrown);
          }
     });
 });

 