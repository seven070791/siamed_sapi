

$(function()
{
   let direccion_ip= $('#direccion_ip').val();
   let dispositivo= $('#dispositivo').val();
   listarAuditoria(direccion_ip,dispositivo);

  
});



/*
  * Función para definir datatable:
  */
function listarAuditoria(direccion_ip,dispositivo)
{
     $('#auditoria').DataTable
     (
      {
          // "order":[[3,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":base_url+"/listar_auditoria/"+direccion_ip+'/'+dispositivo,
            "type":"GET",
            dataSrc:''
           },
           "columns":
           [
            {data:'direccion_ip'},
            {data:'nombre'},               
            {data:'fecha_sesion'},
            {data:'accion'}, 
            {data:'hora'},
            {data:'cantidad'},
            {data:'dispositivo'},
               
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           }
      });
}