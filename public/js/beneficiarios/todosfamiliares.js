/*
 *Este es el document ready
 */
 $(function()
 {
    
     listarfamiliares();
    
     
     //alert($('#id_medicamento').val());
 });

 

 /*
  * Función para definir datatable:
  */
 function listarfamiliares()
 {
      $('#familiares').DataTable
      ({
           "order":[[0,"asc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
              "url":base_url+"/buscar_Todos_Familiares",
              "type":"GET",
              dataSrc:''
           },
           "columns":
           [    
               { data:'cedula_trabajador'},
                {data:'cedula'},
                {data:'nombre'},                
                {data:'apellido'},  
                {data:'parentesco_id'},
                {data:'sexo'}, 
                {data:'telefono'}, 
                 
                    
                {orderable: true,
                  render:function(data, type, row)
                  {
                //    return '<a href="javascript:;" class="btn btn-xs btn-success editar_lista_medicamento" style=" font-size:2px" data-toggle="tooltip" title="Editar Medicamento" id='+row.id+' descripcion="'+row.descripcion+'"> <i class="material-icons " >create</i></a>'
                return '<a href="javascript:;" class="btn btn-xs btn-secondary Editar" style=" font-size:2px" data-toggle="tooltip" title="Editar" cedula_trabajador='+row.cedula_trabajador+' descripcion="'+row.descripcion+'" fecha_creacion='+row.fecha_creacion+' estatus='+row.estatus+'> <i class="material-icons " >create</i></a>'+'<a href="javascript:;" class="btn btn-xs btn-success Familiares" style=" font-size:4px" data-toggle="tooltip" title="Familiares" cedula_trabajador='+row.cedula_trabajador+' nombre='+row.nombre+' estatus="'+row.estatus+'"><i class="material-icons ">wc</i></a>'+'<a href="javascript:;" class="btn btn-xs btn-info Cortesia" style=" font-size:2px" data-toggle="tooltip" title="Cortesia" cedula_trabajador='+row.cedula_trabajador+' rol='+row.descripcion+' estatus="'+row.estatus+'"><i class="material-icons ">group_add</i></a>'

                     }
                 }
           ],
           "language":
           {
              "sProcessing":    "Procesando...",
              "sLengthMenu":    "Mostrar _MENU_ registros",
              "sZeroRecords":   "No se encontraron resultados",
              "sEmptyTable":    "Ningún dato disponible en esta tabla",
              "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
              "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
              "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
              "sInfoPostFix":   "",
              "sSearch":        "Buscar:",
              "sUrl":           "",
              "sInfoThousands":  ",",
              "sLoadingRecords": "Cargando...",
              "oPaginate": 
              {
               "sFirst":    "Primero",
               "sLast":    "Último",
               "sNext":    "Siguiente",
               "sPrevious": "Anterior"
              },
              "oAria":
              {
               "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
               "sSortDescending": ": Activar para ordenar la columna de manera descendente"
              },
              "columnDefs": 
              [
                {
                  "targets": [ 0 ],
                  "visible": false,
                  "searchable": false
                }
              ],			
           }
      });
 }