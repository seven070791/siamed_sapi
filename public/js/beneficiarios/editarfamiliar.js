/*
 *Este es el document ready
 */
 $(function()
 {   
$(".motivo_estatus").hide();
   $("#modal_editar_familiar").modal("show"); 
   var parentesco_id= $('#parentesco_id').val();
   var tipodesangre_id=$('#tipodesangre_id').val();
   var estadocivil_id=$('#estadocivil_id').val();
   var grado_intruccion_id=$('#grado_intruccion_id').val();
   var ocupacion_id=$('#ocupacion_id').val();
   var estatus=$('#borrado').val();
  

   if(estatus=='f')
   {
        $('#check_borrado').attr('checked','checked');
        $('#check_borrado').val('false');
        $('#borrado_anterior').val('false');
   }
   if(estatus=='t')
   {
        $('#check_borrado').removeAttr('checked')
        $('#check_borrado').val('true')
        $('#borrado_anterior').val('true');
   } 


    
     llenar_combo_parentesco(Event, parentesco_id);
     llenar_combo_tipo_de_sangre(Event,tipodesangre_id)
     llenar_combo_estado_civil(Event,estadocivil_id)
     llenar_combo_Grado_Instruccion(Event,grado_intruccion_id)
     llenar_combo_Ocupacion(Event,ocupacion_id)



   //******METODO QUE TOMA EL ESTATUS ORIGINAL DEL CHECKBO*****
   if($('#check_borrado').is(':checked'))
   {
   estatus='false';
   $('#estatus_actual').val(estatus);
   
   $('#estatus_anterior').val(estatus);
   }
   else
   {
   estatus='true';
   $('#estatus_actual').val(estatus);
   $('#estatus_anterior').val(estatus); 
   } 

 });


/*
 *Metodo para llenar el combo tipo de sangre
 */
 function llenar_combo_Ocupacion(e,ocupacion_id)
 {
       e.preventDefault;
     
       url='/listar_Ocupacion';
        $.ajax
       ({
            url:url,
            method:'GET',
           //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
           dataType:'JSON',
           beforeSend:function(data)
           {
           },
           success:function(data)
           {
            
            //console.log(data);
          // alert(data[0].id)
           //alert(data[0].descripcion)
           
 if(data.length>1)
 {
      $('#ocupacion').empty();
      $('#ocupacion').append('<option value=0>Seleccione</option>');     
      if(ocupacion_id===undefined)
      {
           $.each(data, function(i, item)
           {
                //console.log(data)
                $('#ocupacion').append('<option value='+item.id+'>'+item.descripcion+'</option>');
 
           });
      }
      else
      {
           $.each(data, function(i, item)
           {
                if(item.id===ocupacion_id)
                {
                     $('#ocupacion').append('<option value='+item.id+' selected>'+item.descripcion+'</option>');
                     $('#ocupacion_anterior').append('<option value='+item.id+' selected>'+item.descripcion+'</option>');
                    }
                else
                {
                     $('#ocupacion').append('<option value='+item.id+'>'+item.descripcion+'</option>');
                }
           });
      }
 }      
 },
 error:function(xhr, status, errorThrown)
 {
     alert(xhr.status);
     alert(errorThrown);
 }
 });
 }

/*
 *Metodo para llenar el combo tipo de sangre
 */
 function llenar_combo_Grado_Instruccion(e,grado_intruccion_id)
 {
       e.preventDefault;
     
       url='/listar_Grado_Instruccion';
        $.ajax
       ({
            url:url,
            method:'GET',
           //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
           dataType:'JSON',
           beforeSend:function(data)
           {
           },
           success:function(data)
           {
            
            //console.log(data);
          // alert(data[0].id)
           //alert(data[0].descripcion)
           
 if(data.length>1)
 {
      $('#gradointruccion').empty();
      $('#gradointruccion').append('<option value=0>Seleccione</option>');     
      if(grado_intruccion_id===undefined)
      {
           $.each(data, function(i, item)
           {
                //console.log(data)
                $('#gradointruccion').append('<option value='+item.id+'>'+item.descripcion+'</option>');
 
           });
      }
      else
      {
           $.each(data, function(i, item)
           {
                if(item.id===grado_intruccion_id)
                {
                     $('#gradointruccion').append('<option value='+item.id+' selected>'+item.descripcion+'</option>');
                     $('#gradointruccion_anterior').append('<option value='+item.id+' selected>'+item.descripcion+'</option>');
                    }
                else
                {
                     $('#gradointruccion').append('<option value='+item.id+'>'+item.descripcion+'</option>');
                }
           });
      }
 }      
 },
 error:function(xhr, status, errorThrown)
 {
     alert(xhr.status);
     alert(errorThrown);
 }
 });
 }


 /*
* Función para llenar el combo del control de medicamento
*/    
function llenar_combo_parentesco(e,parentesco_id)
{
      e.preventDefault;
    
      url='/listar_Parentesco';
       $.ajax
      ({
           url:url,
           method:'GET',
          //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
          },
          success:function(data)
          {
           
           //console.log(data);
         // alert(data[0].id)
          //alert(data[0].descripcion)
          
if(data.length>1)
{
     $('#cmbParentesco').empty();
     $('#cmbParentesco').append('<option value=0>Seleccione</option>');     
     if(parentesco_id===undefined)
     {
          $.each(data, function(i, item)
          {
               //console.log(data)
               $('#cmbParentesco').append('<option value='+item.id+'>'+item.descripcion+'</option>');

          });
     }
     else
     {
          $.each(data, function(i, item)
          {
               if(item.id===parentesco_id)
               {
                    $('#cmbParentesco').append('<option value='+item.id+' selected>'+item.descripcion+'</option>');
                    $('#cmbParentesco_anterior').append('<option value='+item.id+' selected>'+item.descripcion+'</option>');
               }
               else
               {
                    $('#cmbParentesco').append('<option value='+item.id+'>'+item.descripcion+'</option>');
               }
          });
     }
}      
},
error:function(xhr, status, errorThrown)
{
    alert(xhr.status);
    alert(errorThrown);
}
});
}

/*
 *Metodo para llenar el combo tipo de sangre
 */
 function llenar_combo_tipo_de_sangre(e,tipodesangre_id)
 {
    
       e.preventDefault;
     
       url='/listar_tipo_de_sangre';
        $.ajax
       ({
            url:url,
            method:'GET',
           //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
           dataType:'JSON',
           beforeSend:function(data)
           {
           },
           success:function(data)
           {
            
            //console.log(data);
          // alert(data[0].id)
           //alert(data[0].descripcion)
           
 if(data.length>1)
 {
      $('#tipodesangre').empty();
      $('#tipodesangre').append('<option value=0>Seleccione</option>');     
      if(tipodesangre_id===undefined)
      {
           $.each(data, function(i, item)
           {
                //console.log(data)
                $('#tipodesangre').append('<option value='+item.id+'>'+item.descripcion+'</option>');
 
           });
      }
      else
      {
         
           $.each(data, function(i, item)

           {
                if(item.id===tipodesangre_id)
                {

                     $('#tipodesangre').append('<option value='+item.id+' selected>'+item.descripcion+'</option>');
                     $('#tipodesangre_anterior').append('<option value='+item.id+' selected>'+item.descripcion+'</option>');
                    }
                else
                {
                     $('#tipodesangre').append('<option value='+item.id+'>'+item.descripcion+'</option>');
                }
           });
      }
 }      
 },
 error:function(xhr, status, errorThrown)
 {
     alert(xhr.status);
     alert(errorThrown);
 }
 });
 }

/*
 *Metodo para llenar el combo estado civil
 */
 function llenar_combo_estado_civil(e,estadocivil_id)
 {

    
       e.preventDefault;
     
       url='/listar_estado_civil';
        $.ajax
       ({
            url:url,
            method:'GET',
           //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
           dataType:'JSON',
           beforeSend:function(data)
           {
           },
           success:function(data)
           {      
 if(data.length>1)
 {
      $('#estadocivil').empty();
      $('#estadocivil').append('<option value=0>Seleccione</option>');     
      if(estadocivil_id===undefined)
      {
           $.each(data, function(i, item)
           {
                //console.log(data)
                $('#estadocivil').append('<option value='+item.id+'>'+item.descripcion+'</option>');
 
           });
      }
      else
      {
           $.each(data, function(i, item)
           {
                if(item.id===estadocivil_id)
                {
                     $('#estadocivil').append('<option value='+item.id+' selected>'+item.descripcion+'</option>');
                     $('#estadocivil_anterior').append('<option value='+item.id+' selected>'+item.descripcion+'</option>');
                    }
                else
                {
                     $('#estadocivil').append('<option value='+item.id+'>'+item.descripcion+'</option>');
                }
           });
      }
 }      
 },
 error:function(xhr, status, errorThrown)
 {
     alert(xhr.status);
     alert(errorThrown);
 }
 });
 }
 

 //******METODO QUE TOMA EL ESTATUS ACTUAL DEL CHECKBO*****
 $('#check_borrado').click(function () {

     $(".motivo_estatus").show();
     if($('#check_borrado').is(':checked'))
     {
     
     estatus_check='false';
     
     }
     else
     {
     estatus_check='true';
     
     } 
      $('#estatus_actual').val(estatus_check);
 });
 

$(document).on('click','#btnActualizar ', function(e)
 {
     e.preventDefault();
     var cedula_trabajador          =$('#cedula_trabajador').val();
     var cedula                     =$('#cedula').val();
     var cedula_anterior            =$('#cedula_anterior').val();
     var nombre                     =$('#nombre_familiar').val();
     var apellido                   =$('#apellido').val();
     var telefono                   =$('#telefono').val();
     var sexo                       =$('#sexo').val();

     let nombre_anterior=$('#nombre_familiar_anterior').val();
     let apellido_anterior=$('#apellido_anterior').val();
     //alert(sexo);
     var parentesco_id             =$('#cmbParentesco').val();
     var tipo_de_sangre_id             =$('#tipodesangre').val();
     var estado_civil_id             =$('#estadocivil').val();
     var grado_intruccion_id            =$('#gradointruccion').val(); 
     var ocupacion_id           =$('#ocupacion').val(); 
     var fecha_nac_familiares          =$('#fecha').val();
     let fechaconvertida=moment(fecha_nac_familiares,'DD-MM-YYYY'); 
     fechaconvertida=fechaconvertida.format('YYYY-MM-DD');
     let observacion =$('#observacion').val();
     let estatus_modificado =$('#estatus_actual').val();
     let estatus_anterior =$('#estatus_anterior').val();
     let id_usuario =$('#id_user').val();
     if(estatus_anterior != estatus_modificado)
     {
       bandera_Modificacion='true'
     }else
     {
       bandera_Modificacion='false'    
     }

     if (bandera_Modificacion=='true' && observacion==' ') {
        alert('DEBE INDICAR EL MOTIVO DEL CAMBIO DE ESTATUS') ; 
     }
     else{
               var objeto_anterior = 
               {
                    "Cedula": $('#cedula_anterior').val(),
                    "Nombre": $('#nombre_familiar_anterior').val(),
                    "Apellido": $('#apellido_anterior').val(),
                    "Telefono": $('#telefono_anterior').val(),
                    "Fecha": $('#fecha_anterior').val(),
                    "Parentesco": $('#cmbParentesco_anterior option:selected').text(),
                    "Ocupacion": $('#ocupacion_anterior option:selected').text(),
                    "TipoSangre": $('#tipodesangre_anterior option:selected').text(),
                    "EstadoCivil": $('#estadocivil_anterior option:selected').text(),
                    "GradoIntruccion": $('#gradointruccion_anterior option:selected').text(),
                    "Sexo": $('#sexo_anterior option:selected').text(),
                    "Inactivo": $('#borrado_anterior').val(),                   
               }
                var objeto_actual= 
                {
                    "Cedula": $('#cedula').val(),
                    "Nombre": $('#nombre_familiar').val(),
                    "Apellido": $('#apellido').val(),
                    "Telefono": $('#telefono').val(),
                    "Fecha": $('#fecha').val(),
                    "Parentesco": $('#cmbParentesco option:selected').text(),
                    "Ocupacion": $('#ocupacion option:selected').text(),
                    "TipoSangre": $('#tipodesangre option:selected').text(),
                    "EstadoCivil": $('#estadocivil option:selected').text(),
                    "GradoIntruccion": $('#gradointruccion option:selected').text(),
                    "Sexo": $('#sexo option:selected').text(),
                    "Inactivo": $('#estatus_actual').val(),
                }
                var camposModificados = [];
                for (var propiedad in objeto_anterior) {
                    if (objeto_anterior.hasOwnProperty(propiedad)) {
                        if (objeto_anterior[propiedad] !== objeto_actual[propiedad]) {
                            camposModificados.push({
                                propiedad: propiedad,
                                valorAnterior: objeto_anterior[propiedad],
                                valorNuevo: objeto_actual[propiedad]
                            });
                        }
                    }
                }
                if (camposModificados.length > 0) {
                    var datos_modificados = camposModificados.map(function(campo) {
                        let string_anterior = campo.valorAnterior;
                        let patron = /option-/;
                        if (patron.test(string_anterior)) {
                            campo.valorAnterior = string_anterior.replace(patron, "");
                        }
                        let string_Actual = campo.valorNuevo;
                        let patron_Actual = /option-/;
                        if (patron.test(string_Actual)) {
                            campo.valorNuevo = string_Actual.replace(patron, "");
                        }
                        campo.valorAnterior = campo.valorAnterior.trim();
        
                        return campo.valorAnterior === '' ?
                            `Ingreso el Valor de ${campo.propiedad} = ${campo.valorNuevo}` :
                            `El campo: ${campo.propiedad} = ${campo.valorAnterior} fue modificado a: ${campo.valorNuevo}`;
                    }).join(", ");      
                }
                if (datos_modificados == undefined) {
                    alert('NO SE HA REALIZADO NINGUNA MODIFICACION');
        
                } else 
                {
                    var data=
               {
                    cedula_trabajador          :cedula_trabajador  ,
                    cedula                     :cedula,
                    nombre                     :nombre,
                    apellido                   :apellido ,
                    telefono                   :telefono  ,
                    sexo                       :sexo  ,
                    fecha_nac_familiares       :fechaconvertida  ,
                    parentesco_id              :parentesco_id,  
                    tipo_de_sangre_id :tipo_de_sangre_id , 
                    estado_civil_id :estado_civil_id,
                    grado_intruccion_id:grado_intruccion_id,
                    ocupacion_id  :ocupacion_id  ,
                    estatus_modificado:estatus_modificado,
                    bandera_Modificacion,bandera_Modificacion,
                    observacion,observacion,
                    id_usuario,id_usuario,
                    observacion,observacion,
                    datos_modificados:datos_modificados,   
                    nombre_anterior:nombre_anterior,
                    apellido_anterior:apellido_anterior,
                        
               }
                    var url='/actualizar_familiar'+'/'+cedula_anterior;
                    var ruta_regreso='/listar_Familiares/'+cedula_trabajador;
                    $.ajax
                    ({
                         url:url,
                         method:'POST',
                         data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
                         dataType:'JSON',
                         beforeSend:function(data)
                         {
                              //alert('Procesando Información ...');
                         },
                         success:function(data)
                         {
                         //alert(data);
                         if(data===1)
                         {
                              alert('Registro Actualizado');
                              window.location = ruta_regreso;   
                         }
                         else
                         {
                              alert('Error en la Incorporación del registro');
                         }
                                   
                         },
                         error:function(xhr, status, errorThrown)
                         {
                         alert(xhr.status);
                         alert(errorThrown);
                         }
                    });

                }
           }







 });

 /*
* METODO PARA CAMBIAR EL SEXO EN FUNCION DEL PARENTESCO
*/   
// $(document).on('change','#cmbParentesco ', function(e)
//  {
//      let parentesco_id= $('#cmbParentesco').val();
     
//      if(parentesco_id=='1')
//      {
  
//           $('#sexo').val('2');
          
//      }
//      else if(parentesco_id=='2')  
//      {
       
//           $('#sexo').val('1');
//      }
//    else if(parentesco_id=='3')   
//       {
       
//           $('#sexo').val('2');
//       }
//      else if(parentesco_id=='4')  
//      {
//           $('#sexo').val('1'); 
//      }
   
     
//  });

 $(document).on('click','#btncerrar ', function(e)
 {
      e.preventDefault();
      var cedula_trabajador =$('#cedula_trabajador').val();
      var ruta_regreso='/listar_Familiares/'+cedula_trabajador;
      window.location = ruta_regreso;
    
 });


