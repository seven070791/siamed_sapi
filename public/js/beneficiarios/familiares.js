/*
 *Este es el document ready
 */
$(function() {

    listarfamiliares();
    llenar_combo_parentesco(Event)
    llenar_combo_tipo_de_sangre(Event)
    llenar_combo_estado_civil(Event)
    llenar_combo_Grado_Instruccion(Event)
    llenar_combo_Ocupacion(Event)
        //alert($('#id_medicamento').val());
});

function llenar_combo_parentesco(e, parentesco_id) {
    e.preventDefault;

    url = '/listar_Parentesco';
    $.ajax({
        url: url,
        method: 'GET',
        //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
        dataType: 'JSON',
        beforeSend: function(data) {},
        success: function(data) {

            //console.log(data);
            // alert(data[0].id)
            //alert(data[0].descripcion)

            if (data.length >= 1) {
                $('#parentesco').empty();
                $('#parentesco').append('<option value=0>Seleccione</option>');
                if (parentesco_id === undefined) {
                    $.each(data, function(i, item) {
                        //console.log(data)
                        $('#parentesco').append('<option value=' + item.id + '>' + item.descripcion + '</option>');

                    });
                } else {
                    $.each(data, function(i, item) {
                        if (item.id === parentesco_id) {
                            $('#parentesco').append('<option value=' + item.id + ' selected>' + item.descripcion + '</option>');
                        } else {
                            $('#parentesco').append('<option value=' + item.id + '>' + item.descripcion + '</option>');
                        }
                    });
                }
            }
        },
        error: function(xhr, status, errorThrown) {
            alert(xhr.status);
            alert(errorThrown);
        }
    });
}

/*
 *Metodo para llenar el combo tipo de sangre
 */
function llenar_combo_Grado_Instruccion(e, grado_intruccion_id) {
    e.preventDefault;

    url = '/listar_Grado_Instruccion';
    $.ajax({
        url: url,
        method: 'GET',
        //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
        dataType: 'JSON',
        beforeSend: function(data) {},
        success: function(data) {

            //console.log(data);
            // alert(data[0].id)
            //alert(data[0].descripcion)

            if (data.length >= 1) {
                $('#gradointruccion').empty();
                $('#gradointruccion').append('<option value=0>Seleccione</option>');
                if (grado_intruccion_id === undefined) {
                    $.each(data, function(i, item) {
                        //console.log(data)
                        $('#gradointruccion').append('<option value=' + item.id + '>' + item.descripcion + '</option>');

                    });
                } else {
                    $.each(data, function(i, item) {
                        if (item.id === grado_intruccion_id) {
                            $('#gradointruccion').append('<option value=' + item.id + ' selected>' + item.descripcion + '</option>');
                        } else {
                            $('#gradointruccion').append('<option value=' + item.id + '>' + item.descripcion + '</option>');
                        }
                    });
                }
            }
        },
        error: function(xhr, status, errorThrown) {
          //  alert(xhr.status);
          //  alert(errorThrown);
        }
    });
}

/*
 *Metodo para llenar el combo ocupacion
 */
function llenar_combo_Ocupacion(e, ocupacion_id) {
    e.preventDefault;

    url = '/listar_Ocupacion';
    $.ajax({
        url: url,
        method: 'GET',
        //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
        dataType: 'JSON',
        beforeSend: function(data) {},
        success: function(data) {

            //console.log(data);
            // alert(data[0].id)
            //alert(data[0].descripcion)

            if (data.length >= 1) {
                $('#ocupacion').empty();
                $('#ocupacion').append('<option value=0>Seleccione</option>');
                if (ocupacion_id === undefined) {
                    $.each(data, function(i, item) {
                        //console.log(data)
                        $('#ocupacion').append('<option value=' + item.id + '>' + item.descripcion + '</option>');

                    });
                } else {
                    $.each(data, function(i, item) {
                        if (item.id === ocupacion_id) {
                            $('#ocupacion').append('<option value=' + item.id + ' selected>' + item.descripcion + '</option>');
                        } else {
                            $('#ocupacion').append('<option value=' + item.id + '>' + item.descripcion + '</option>');
                        }
                    });
                }
            }
        },
        error: function(xhr, status, errorThrown) {
            alert(xhr.status);
            alert(errorThrown);
        }
    });
}


/*
 *Metodo para llenar el combo tipo de sangre
 */
function llenar_combo_tipo_de_sangre(e, tipodesangre_id) {
    e.preventDefault;

    url = '/listar_tipo_de_sangre';
    $.ajax({
        url: url,
        method: 'GET',
        //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
        dataType: 'JSON',
        beforeSend: function(data) {},
        success: function(data) {

            //console.log(data);
            // alert(data[0].id)
            //alert(data[0].descripcion)

            if (data.length >= 1) {
                $('#tipodesangre').empty();
                $('#tipodesangre').append('<option value=0>Seleccione</option>');
                if (tipodesangre_id === undefined) {
                    $.each(data, function(i, item) {
                        //console.log(data)
                        $('#tipodesangre').append('<option value=' + item.id + '>' + item.descripcion + '</option>');

                    });
                } else {
                    $.each(data, function(i, item) {
                        if (item.id === tipodesangre) {
                            $('#parentesco').append('<option value=' + item.id + ' selected>' + item.descripcion + '</option>');
                        } else {
                            $('#parentesco').append('<option value=' + item.id + '>' + item.descripcion + '</option>');
                        }
                    });
                }
            }
        },
        error: function(xhr, status, errorThrown) {
            alert(xhr.status);
            alert(errorThrown);
        }
    });
}

/*
 *Metodo para llenar el combo estado civil
 */
function llenar_combo_estado_civil(e, estado_civil) {
    e.preventDefault;

    url = '/listar_estado_civil';
    $.ajax({
        url: url,
        method: 'GET',
        //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
        dataType: 'JSON',
        beforeSend: function(data) {},
        success: function(data) {
            if (data.length >= 1) {
                $('#estadocivil').empty();
                $('#estadocivil').append('<option value=0>Seleccione</option>');
                if (estado_civil === undefined) {
                    $.each(data, function(i, item) {
                        //console.log(data)
                        $('#estadocivil').append('<option value=' + item.id + '>' + item.descripcion + '</option>');

                    });
                } else {
                    $.each(data, function(i, item) {
                        if (item.id === estado_civil) {
                            $('#estadocivil').append('<option value=' + item.id + ' selected>' + item.descripcion + '</option>');
                        } else {
                            $('#estadocivil').append('<option value=' + item.id + '>' + item.descripcion + '</option>');
                        }
                    });
                }
            }
        },
        error: function(xhr, status, errorThrown) {
            alert(xhr.status);
            alert(errorThrown);
        }
    });
}



/*
 * Función para definir datatable:
 */
function listarfamiliares() {
    $('#table_familiares').DataTable({
        "order": [
            [0, "asc"]
        ],
        "paging": true,
        "info": true,
        "filter": true,
        "responsive": true,
        "autoWidth": true,
        //"stateSave":true,
        "ajax": {
            "url":base_url+ "/buscar_Familiares/" + $('#cedula_trabajador').val(),
            "type": "GET",
            dataSrc: ''
        },
        "columns": [
            { data: 'cedula' },
            { data: 'nombre' },
            { data: 'apellido' },
            { data: 'parentesco' },
            { data: 'sexo' },
            { data: 'telefono' },
            { data: 'fecha_nac_familiares' },

            {
                orderable: true,
                render: function(data, type, row) {
                    return '<a href="javascript:;" class="btn btn-xs btn-secondary Editar" style=" font-size:2px" data-toggle="tooltip" title="Editar"  ocupacion_id=' + row.ocupacion_id + ' grado_intruccion_id=' + row.grado_intruccion_id + '  cedula=' + row.cedula + ' descripcion="' + row.descripcion + '" tipodesangre_id=' + row.tipo_de_sangre + ' estadocivil_id=' + row.estado_civil + ' fecha_creacion=' + row.fecha_creacion + ' estatus=' + row.estatus + ' sexo=' + row.sexo + '> <i class="material-icons " >create</i></a>' + ' ' + '<a href="javascript:;" class="btn btn-xs btn-info Detalle" style=" font-size:2px" data-toggle="tooltip" title="Historico de  Medicamentos Entregados" cedula=' + row.cedula + ' rol=' + row.descripcion + ' estatus="' + row.estatus + '"><i class="material-icons ">add_to_photos</i></a>' + ' ' + '<a href="javascript:;" class="btn btn-xs btn-primary Historial" style=" font-size:2px" data-toggle="tooltip" title="Historial  Medico" cedula=' + row.cedula + ' rol=' + row.descripcion + ' estatus="' + row.estatus + '"><i class="material-icons ">create_new_folder</i></a>'
                }
            }
        ],
        "language": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": [{
                "targets": [0],
                "visible": false,
                "searchable": false
            }],
        }
    });


}
$(document).on('click', '#btnAgregar_familiar ', function(e) {
    //alert('click familiar');

    $("#modal_agregar_familiar").modal("show");
    $('#fecha').val('');
    $('#cedula').val('');
    $('#nombre_familiar').val('');
    $('#apellido').val('');
    $("#parentesco").val(0);




});

// ******************METODO PARA AGREGAR FAMILIAR********************
$(document).on('click', '#btnAgregarFamiliar', function(e) {
    e.preventDefault('');


    var cedula_trabajador = $('#cedula_trabajador').val();
    var cedula = $('#cedula').val();
    var nombre_familiar = $('#nombre_familiar').val();
    var apellido = $('#apellido').val();
    var telefono = $('#telefono').val();
    var sexo = $('#sexo').val();
    var fecha_nac_familiares = $('#fecha').val();
    var parentesco_id = $('#parentesco').val();
    var tipodesangre = $('#tipodesangre').val();
    var estadocivil = $('#estadocivil').val();
    var gradoinstruccion_id = $('#gradointruccion').val();
    var ocupacion_id = $('#ocupacion').val();
    var telefono = $('#telefono').val();
    if (telefono.trim() == ''); {
        telefono = 'Sin Registro'
    }
    if (cedula == '' || nombre_familiar == '' || apellido == '') {
        alert('Los campos Cedula ,Nombre , Apellido! son obligatorios');

    } else if (parentesco_id <= 0) {
        alert('Debe indicar el parentesco del Familiar');
    } else if (fecha_nac_familiares == ' ') {
        alert('Debe indicar la Fecha De Nacimiento');
    } else {

        var url = '/agregar_Familiares';
        var ruta_regreso = '/listar_Familiares/' + cedula_trabajador;
        var data = {
                cedula_trabajador: cedula_trabajador,
                cedula: cedula,
                nombre_familiar: nombre_familiar,
                apellido: apellido,
                telefono: telefono,
                sexo: sexo,
                fecha_nac_familiares: fecha_nac_familiares,
                parentesco_id: parentesco_id,
                tipodesangre: tipodesangre,
                estadocivil: estadocivil,
                gradoinstruccion_id: gradoinstruccion_id,
                ocupacion_id: ocupacion_id,
            }
            //console.log(data);
        $.ajax({
            url: url,
            method: 'POST',
            data: { data: btoa(unescape(encodeURIComponent(JSON.stringify(data)))) },
            dataType: 'JSON',
            beforeSend: function(data) {},
            success: function(data) {

                if (data == '0') {
                    alert('Error en la Incorporación del Registro');
                
                } else if (data == '1') {
                    alert('Registro Incorporado');
                    window.location = ruta_regreso;
                }
                else if (data == '2') {
                    alert('Registro Incorporado');
                    window.location = ruta_regreso;
                }

            },
            error: function(xhr, status, errorThrown) {
                alert(xhr.status);
                alert(errorThrown);
            }
        });
    }

});

// **********************Editar Familiar*****************

$('#lista_familiares').on('click', '.Editar', function(e) {
    var cedula = $(this).attr('cedula');
    var tipodesangre_id = $(this).attr('tipodesangre_id');
    var estadocivl_id = $(this).attr('estadocivil_id');
    var grado_intruccion_id = $(this).attr('grado_intruccion_id');
    var ocupacion_id = $(this).attr('ocupacion_id');
    var sexo = $(this).attr('sexo');



    window.location = '/editar_Familiar/' + cedula;

});

$('#lista_familiares').on('click', '.Detalle', function(e) {
    var cedula_trabajador = $('#cedula_trabajador').val();
    var cedula = $(this).attr('cedula');
    window.location = '/info_medicamentos_familiares/' + cedula + '/' + cedula_trabajador;

});




// $(document).on('change', '#parentesco ', function(e) {
//     if ($('#parentesco').val() == 1) {

//         $('#sexo').val('1');

//     } else if ($('#parentesco').val() == 2) {

//         $('#sexo').val('2');
//     } else if ($('#parentesco').val() == 3) {

//         $('#sexo').val('1');
//     } else {
//         $('#sexo').val('2');
//     }

// });


$(document).on('click', '#btnRegresar_hacia_titulares ', function(e) {
    e.preventDefault();
    window.location = '/titulares/';

});

$(document).on('click', '#btnBuscar ', function(e) {
    e.preventDefault();
    let cedula_trabajador = $('#cedula_trabajador').val();
    let cedula = $('#cedula').val();
    if (cedula == '') {
        alert('Debe Introducir la Cedula');
    }
    let sigespKey = '$2y$12$hhlBQA2SKFyl1XABdBaNg.LSOuWoI9Sz6Amu3UFS/evwksYNCYN7G';
    let url = '/api/familiares/datos.php?cedula_trabajador=' + cedula_trabajador + '&cedula=' + cedula + '&key=' + sigespKey;
    $.ajax({
        url: url,
        method: 'GET',
        dataType: 'JSON',
        contentType: "application/json",
        beforeSend: function() {
            //alert('Antes');
        },
        success: function(data) {
            //console.log(data);


            if (data.cedula == null) {
                alert('No existen registros coincidentes para el Nùmero de Cèdula suministrado', '#intCedula');
                $('#intCedula').focus();
            } else if (data.cedula == 000) {
                alert('Error: ' + data.cedula);
                alert('No Existen Registros ...');
            } else if (data.cedula == 555) {
                alert('Error: ' + data.cedula);
                alert('Problemas con la BD del Servicio o con la conexión a ella ...');
            } else if (data.cedula == 777) {
                alert('Error: ' + data.cedula);
                alert('Problemas de Conexión ...');
            } else if (data.cedula == 888) {
                alert('Error: ' + data.cedula);
                alert('Las claves no son iguales ...');
            } else if (data.cedula == 999) {
                alert('Error: ' + data.cedula);
                alert('No se Recibió clave alguna para la API ...');
            } else {

                $('#nombre_familiar').val(data.nombres);
                $('#apellido').val(data.apellidos);
                $("#fecha").val(data.fecha_nacimiento);
                $("#parentesco").val(data.parentesco);

                if ($('#parentesco').val() == 1) {
                    $('#sexo').val('2');
                } else if ($('#parentesco').val() == 2) {
                    $('#sexo').val('1');
                } else if ($('#parentesco').val() == 3) {
                    $('#sexo').val('2');
                } else {
                    $('#sexo').val('1');
                }

            }

        }
    });

});



$('#lista_familiares').on('click', '.Historial', function(e) {
    var cedula_trabajador = $('#cedula_trabajador').val();
    var cedula_beneficiario = $(this).attr('cedula');
    window.location = '/datos_titular_familiares/' + cedula_trabajador + '/' + cedula_beneficiario;
});