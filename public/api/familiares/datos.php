<?php
header('Access-Control-Allow-Origin:  http://carnet.sapi.gob.ve');
header('Access-Control-Allow-Origin:  http://carnet.com');
///header('Access-Control-Allow-Origin : http://localhost:8080');
header('Access-Control-Allow-Methods: GET');
///header('Access-Control-Allow-Methods : GET');
header('Access-Control-Allow-Headers: Content-Type, Authorization');

//header("Access-Control-Allow-Credentials : false");
////header("Access-Control-Allow-Methods : GET, POST, OPTIONS");
////header("Access-Control-Allow-Headers : Origin, Content-Type, Accept");

$cedula = "";
$apikey = "";
$filtroAnd = "";

if (isset($_GET['key'])) {
	$apiKey = $_GET['key'];

	//módulo de conexion a la BD
	require('Conexion_class.php');

	//Me conecto a la BD con las credenciales que están por allá
	$con = new Conexion_class();
	$conexion = $con->conectar();

	//Verifico si me pude conectar
	if (!isset($conexion)) {
		//('Problemas en la conexión, favor verificar  ...');
		//No se conectó;
		$respuesta =
			[
				"cedula"   => '777',
			];
	} else {
		//die('Se Conectó al SIGESP');
		//Se conectó;
		$sqlConsulta = "SELECT key FROM cnt_tabkey;";
		$Rs = $con->registros($sqlConsulta);
		if (!$Rs) {
			//No se conectó;
			$respuesta =
				[
					"cedula"   => '555',
				];
		} else {
			//die('consiguió la tabla con la llave de acceso al SIGESP');
			//Hay registros
			while ($campo = $con->arrCamposAsociativos($Rs)) {
				$respuesta = ["key"   => $campo['key']];
			}
			//Si lo que le estoy pasando es igual al contenido de la BD
			if ($apiKey === $respuesta["key"]) {
				//die('Api Key iguales');
				//Pregunto por la CI:
				if (!isset($_GET['cedula_trabajador'])) {
					$filtroWhere = "";
				} else {
					$cedula_trabajador = $_GET['cedula_trabajador'];
					$filtroWhere .= " where cedper='$cedula_trabajador'";
				}
				if (!isset($_GET['cedula'])) {
					$filtroAnd = "";
				} else {
					$cedula = $_GET['cedula'];
					$filtroAnd .= " AND cedula='$cedula'";
				}
				//Armo la Consulta:
				$sqlConsulta = "SELECT ";
				$sqlConsulta .= "f.cedula";
				$sqlConsulta .= ",f.nomfam";
				$sqlConsulta .= ",f.apefam";
				$sqlConsulta .= ",f.fecnacfam";
				$sqlConsulta .= ",f.sexfam";
				$sqlConsulta .= ",f.nexfam";
				$sqlConsulta .= ",CASE ";
				$sqlConsulta .= "WHEN f.nexfam='P' AND f.sexfam='F' THEN 1 ";
				$sqlConsulta .= "WHEN f.nexfam='P' AND f.sexfam='M' THEN 2 ";
				$sqlConsulta .= "WHEN f.nexfam='H' AND f.sexfam='F' THEN 3 ";
				$sqlConsulta .= "WHEN f.nexfam='H' AND f.sexfam='M' THEN 4 ";
				$sqlConsulta .= "ELSE 5 ";
				$sqlConsulta .= "END AS parentesco ";
				$sqlConsulta .= "FROM ";
				$sqlConsulta .= "sno_familiar f ";
				$sqlConsulta .= "WHERE ";
				//$sqlConsulta.="f.codper IN (SELECT codper FROM sno_personal WHERE cedper='16124521') ";
				$sqlConsulta .= "f.codper IN (SELECT codper FROM sno_personal " . $filtroWhere . ") ";
				//$sqlConsulta.="AND cedula='9220373'";
				$sqlConsulta .= $filtroAnd;
				

				//$Rs=$con->registros($conexion,$sqlConsulta);
				//Obtengo los registros de la consulta
				$Rs = $con->registros($sqlConsulta);

				//Verifico si hay registros
				if (!$Rs) {
					//No hay registros
					//echo('No existen registros coincidentes');
					$respuesta =
						[
							"cedula"   => '000',
						];
				} else {
					//Hay registros
					//Esta es la Respuesta de la API
					//Aqui guardo todos los registros de la consulta
					//$respuesta=[];

					//while($registro=pg_fetch_assoc($Rs))
					//Obtengo un arreglo asociativo con los registros de la consulta 
					while ($campo = $con->arrCamposAsociativos($Rs)) {
						$respuesta =
							[
								"cedula"   => $campo['cedula'],
								"nombres"  => $campo['nomfam'] . ".",
								"apellidos" => $campo['apefam'] . ".",
								"fecha_nacimiento"    => $campo['fecnacfam'],
								"parentesco" => $campo['parentesco'],
							];
					}
				}
				//echo json_encode($respuesta);
			} else {
				//echo('Api Key desiguales ...<br>');
				$respuesta =
					[
						"cedula"   => '888',
					];
			}
		}
	}
	//echo $respuesta["key"];
} else {
	//echo('No se recibió clave alguna');
	$respuesta =
		[
			"cedula"   => '999',
		];
}
$respuesta = array_map('utf8_encode', $respuesta);
$json = json_encode($respuesta);
echo $json;
